# Information System Support Technical Service RestTeam

_Proyecto para la metria de Sistemas de información II de la UAGRM-FICCT._
_Sistema de Informacion para la Gestión de Servicio de Soporte Técnico y Administrador de INventario Basado en la Metodología Ágil SCRUM y Tecnologías Web y Móvil_

## Comenzando 🚀

_A continuación, estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node JS version LTS
PostgreSQL
Navegador Web
```

### Instalación 🔧

_Cargar la BD en un motor SQL PostgreSQL_

_* el codigo SQL se encuentra en backen/src/config/database_

_Paso a paso que te dice lo que debes ejecutar para tener un entorno de desarrollo ejecutandose_


```
npm install
npm run dev
```

_Y detener la compilación_

```
Ctrl+c
```
_Si no funciona._

```
Ctrl+Alt+Supr
```

## Ejecutando las pruebas ⚙️

_Ya esta explicado alli arriba_

### Analice las pruebas end-to-end 🔩

_Buscar en google http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/_


### Y las pruebas de estilo de codificación ⌨️

_Servicio de Soporte Técnico y Solicitud_
_Este software web esta dividido en dos partes, Web para la Administración de Operaciones y Solicitude por Garantia y solicitud de soprte técnico desde la Aplicacion movil_


## Despliegue 📦

_pm2_

## Construido con 🛠️

_herramientas que utiliza para crear tu proyecto_

* [NodeJS](https://nodejs.org/en/) - Compilador de JS en Desktop
* [Express](http://RestTeamJS.com/) - Framweork Backend
* [PostgreSQL](https://www.postgresql.org/) - Motor de Base de Datos

## Contribuyendo 🖇️

Por favor lee el [Comercio-Ropa Documentación](https://onedrive.live.com/edit.aspx?resid=31375A1FB22EE2B3!336&ithint=file%2cdocx&authkey=!APi9DnmIGtfpb3w) para detalles de nuestro código de conducta, y el proceso para enviarnos pull requests.

## Versionado 📌

Usamos [Git](http://git.org/) para el versionado de código fuente.

## Autores ✒️

_Agradecido con el de Arriba_

* **Cruz Rodrigue Zuleny** - *Equipo Scrum* - [Github](http://enlace_de_repositorio_de_zuleny) [Bitbucket](http://enlace_de_repositorio_de_zuleny)
* **Quispe Mamani Ruddy Bryan** - *Equipo Scrum* - [Github](https://github.com/RuddyQuispe) [Bitbucket](https://bitbucket.org/dashboard/repositories)
* **Heredia Claros Stephani** - *Equipo Scrum* - [Github](http://enlace_de_repositorio_de_stephani) [Bitbucket](http://enlace_de_repositorio_de_stephani)
* **Cerezo Calderón Margarita** - *Equipo Scrum* - [Github](http://enlace_de_repositorio_de_magui) [Bitbucket](http://enlace_de_repositorio_de_magui)
* **Nina Aguilar Cristian Yamil** - *Equipo Scrum* - [Github](http://enlace_de_repositorio_de_nina) [Bitbucket](http://enlace_de_repositorio_de_nina)

## Licencia 📄

Este proyecto está bajo la Licencia (Sin Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Agradecido con el de Arriba 📢
* Este proyecto tiene fines de aportar con la comunidad de software libre
* Este proyecto fue diseñado con muhco esfuerzo y dedicacion

---
⌨️ con ❤️ por el equipo scrum 😊
