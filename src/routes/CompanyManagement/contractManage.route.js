const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');

const {getContractManage, getContractsList,postContractManage, getContractPDF, putChangeStatusContract} = require('../../controllers/CompanyManagement/contractManage/contractManage.controller');

router.get('/company_management/contract_manage/:nit', isAuthenticated, getContractManage);

router.get('/company_management/contract_list/', isAuthenticated, getContractsList);

router.post('/company_management/contract_manage', isAuthenticated, postContractManage);

router.put('/company_management/contract_manage/status/:nit/:cod_contract', isAuthenticated, putChangeStatusContract);

router.post('/company_management/contract_manage/watch_contract/:cod', isAuthenticated, getContractPDF);


module.exports = router;