const router = require('express').Router();
const {getRepresentative, postRepresentative, getUpdateRepresentative, putUpdateRepresentative} = require('../../controllers/CompanyManagement/representativeManage/representativeManage.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.get('/company_management/representative_manage', isAuthenticated, getRepresentative);

router.post('/company_management/representative_manage', isAuthenticated, postRepresentative);

router.get('/company_management/representative_update/:ci', isAuthenticated, getUpdateRepresentative);

router.put('/company_management/representative_update/:ci', isAuthenticated, putUpdateRepresentative);

module.exports = router;