const router = require('express').Router();
const { getCompany, postCompany, putEnableDisableCompany, getUpdateCompany, putUpdateCompany, getReportInvoiceCompany, postGenerateReport } = require('../../controllers/CompanyManagement/companyManage/companyManage.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.get("/company_management/company_manage", isAuthenticated, getCompany);

router.post("/company_management/company_manage", isAuthenticated, postCompany);

router.put("/company_management/enable_disable_company/:id", isAuthenticated, putEnableDisableCompany);

router.get('/company_management/company_manage/update/:nit', isAuthenticated, getUpdateCompany);

router.put('/company_management/company_update/:nit', isAuthenticated, putUpdateCompany);

router.get('/company_management/report_company_invoice', isAuthenticated, getReportInvoiceCompany);

router.post('/company_management/report_company_invoice/:nit', isAuthenticated, postGenerateReport);

module.exports = router;