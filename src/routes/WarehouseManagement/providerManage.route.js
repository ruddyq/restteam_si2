const router = require('express').Router();
const {getProvider, postProvider, enableDisableProvider, getUpdateProvider, putUpdateProvider } = require('../../controllers/WarehouseManagement/providerManage/providerManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/warehouse_management/provider_manage', isAuthenticated, getProvider);

router.post('/warehouse_management/provider_manage', isAuthenticated, postProvider);

router.put('/warehouse_management/provider_manage/enable_disable/:nit_ci', isAuthenticated, enableDisableProvider);

router.get('/warehouse_management/provider_update/:nit_ci', isAuthenticated, getUpdateProvider);

router.put('/warehouse_management/provider_update/:nit_ci', isAuthenticated, putUpdateProvider);

module.exports = router;