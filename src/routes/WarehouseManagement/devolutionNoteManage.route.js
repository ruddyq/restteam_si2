const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');
const {getDevolutionNote, postDevolutionNote, getUpdateDevolutionNote, putUpdateDevolutionNote, getDevolutionNoteDetail, postDevolutionNoteDetail, deleteDetailDevolutionNote, generateNotePDFFile} = require('../../controllers/WarehouseManagement/devolutionNoteManage/devolutionNoteManage.controller');

router.get('/warehouse_management/devolution_note_manage', isAuthenticated, getDevolutionNote);

router.post('/warehouse_management/devolution_note_manage', isAuthenticated, postDevolutionNote);

router.get('/warehouse_management/devolution_note_update/:note_no', isAuthenticated, getUpdateDevolutionNote);

router.put('/warehouse_management/devolution_note_update/:note_no', isAuthenticated, putUpdateDevolutionNote);

router.get('/warehouse_management/devolution_note_manage/devolution_note_detail_manage/:note_no', isAuthenticated, getDevolutionNoteDetail);

router.post('/warehouse_management/devolution_note_manage/devolution_note_detail_manage/:note_no', isAuthenticated, postDevolutionNoteDetail);

router.delete('/warehouse_management/devolution_note_manage/devolution_note_detail_delete/:note_no/:id_detail', isAuthenticated, deleteDetailDevolutionNote);

router.post('/warehouse_management/devolution_note_manage/devolution_note_detail_generate_pdf/:note_no', isAuthenticated, generateNotePDFFile);

module.exports = router;

