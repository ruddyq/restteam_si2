const router = require('express').Router();
const {getReplacementCategory, postReplacementCategory, getUpdateReplacementCategory, putUpdateReplacementCategory} = require('../../controllers/WarehouseManagement/replacementCategoryManage/replacementCategoryManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/warehouse_management/replacement_category_manage', isAuthenticated, getReplacementCategory);

router.post('/warehouse_management/replacement_category_manage', isAuthenticated, postReplacementCategory);

router.get('/warehouse_management/replacement_category_update/:id_category', isAuthenticated, getUpdateReplacementCategory);

router.put('/warehouse_management/replacement_category_update/:id_category', isAuthenticated, putUpdateReplacementCategory);

module.exports = router;