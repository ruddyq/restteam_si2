const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');
const { getReplacementManage, enableDisableReplacement, postReplacement, getUpdateReplacement, putUpdateReplacement, getReportReplacementTendency, getReportReplacementSpecific } = require('../../controllers/WarehouseManagement/replacementManage/replacementManage.controller');

router.get('/warehouse_management/replacement_manage', isAuthenticated, getReplacementManage);

router.post('/warehouse_management/replacement_manage', isAuthenticated, postReplacement);

router.put('/warehouse_management/replacement_manage/disable_enable/:codReplacement', isAuthenticated, enableDisableReplacement);

router.get('/warehouse_management/replacement_manage/update/:cod_replacement', isAuthenticated, getUpdateReplacement);

router.put('/warehouse_management/replacement_manage/update_replacement/:cod_replacement', isAuthenticated, putUpdateReplacement);

router.get('/warehouse_management/replacement_report_tendency', isAuthenticated, getReportReplacementTendency);

router.get('/warehouse_management/replacement_report/:replacement', isAuthenticated, getReportReplacementSpecific);

module.exports = router;