const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');
const { getExitNote, postExitNoteRegister, getExitNoteDetail, registerExitNoteDetail, deleteDetailOfTheExitNote, getUpdateExitNote, putUpdateExitNote, generateExitNotePDFFile } = require('../../controllers/WarehouseManagement/exitNoteManage/exitNotemanage.controller');

router.get('/warehouse_management/exit_note_manage', isAuthenticated, getExitNote);

router.post('/warehouse_management/exit_note_register', isAuthenticated, postExitNoteRegister);

router.get('/warehouse_management/exit_note_update/:note_no', isAuthenticated, getUpdateExitNote);

router.put('/warehouse_management/exit_note_update/:note_no', isAuthenticated, putUpdateExitNote);

router.get('/warehouse_management/exit_note_manage/exit_note_detail/:note_no', isAuthenticated, getExitNoteDetail);

router.post('/warehouse_management/exit_note_manage/exit_note_detail_register/:note_no', isAuthenticated, registerExitNoteDetail);

router.delete('/warehouse_management/exit_note_manage/exit_note_detail_delete/:note_no/:id_detail', isAuthenticated, deleteDetailOfTheExitNote);

router.post('/warehouse_management/exit_note_manage/exit_note_detail_generate_pdf/:note_no', isAuthenticated, generateExitNotePDFFile);

module.exports = router;