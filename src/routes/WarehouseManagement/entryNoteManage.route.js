const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');
const {getEntryNote, postEntryNote, getUpdateEntryNote, putUpdateEntryNote, getIncomeDetail, postIncomeDetail, deleteIncomeDetail } = require('../../controllers/WarehouseManagement/entryNoteManage/entryNoteManage.controller');

router.get('/warehouse_management/entry_note_manage', isAuthenticated, getEntryNote);

router.post('/warehouse_management/entry_note_manage', isAuthenticated, postEntryNote);

router.get('/warehouse_management/entry_note/update/:id', isAuthenticated, getUpdateEntryNote);

router.put('/warehouse_management/entry_note/update/:id', isAuthenticated, putUpdateEntryNote);

router.get('/warehouse_management/entry_note_manage/income_detail/:id', isAuthenticated, getIncomeDetail);

router.post('/warehouse_management/entry_note_manage/income_detail/:id', isAuthenticated, postIncomeDetail);

router.delete('/warehouse_management/entry_note_manage/income_detail/:id', isAuthenticated, deleteIncomeDetail);

module.exports = router;