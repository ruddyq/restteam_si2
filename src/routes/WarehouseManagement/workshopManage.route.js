const router = require('express').Router();
const { getWorkshop, postWorkshop, getWarehouseInventory, generatePDF, getUpdateWorkshop, putUpdateWorkshop, getStockMinimalWorkshop} = require('../../controllers/WarehouseManagement/WorkshopManage/workshopManage.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.get('/warehouse_management/workshop_manage',isAuthenticated, getWorkshop);

router.post('/warehouse_management/workshop_manage',isAuthenticated,postWorkshop);

router.get('/warehouse_management/workshop_manage/warehouse_inventory/:id',isAuthenticated, getWarehouseInventory);

router.post('/warehouse_management/workshop_manage/warehouse_inventory/print/:id',isAuthenticated, generatePDF);

router.get('/warehouse_management/workshop_manage/update_workshop/:cod_workshop',isAuthenticated, getUpdateWorkshop);

router.put('/warehouse_management/workshop_manage/update_workshop/:cod_workshop',isAuthenticated, putUpdateWorkshop);

router.get('/warehouse_management/workshop_manage/stock_minimal/:cod', isAuthenticated, getStockMinimalWorkshop);

module.exports = router;