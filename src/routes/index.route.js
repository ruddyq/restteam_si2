const router = require('express').Router();
const passport = require('passport');
const { isAuthenticated } = require('../helpers/auth');

//      Import Files dependents
const { getLogin, logout, getMoreInfo, getForgotMyPassword, postForgotPassword, getConfirmKey, postConfirmKey, getRestorePassword ,postVerifyPasswd, getIndex } = require('../controllers/login.controller');

router.get('/', isAuthenticated, getIndex);

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

router.get('/login', getLogin);

router.get('/more_info', isAuthenticated, getMoreInfo);

router.get('/logout/:username', logout);

router.get('/forgot_my_passwd', getForgotMyPassword);

router.post('/forgot_my_passwd', postForgotPassword);

router.get('/confirm_key', getConfirmKey);

router.post('/confirm_key', postConfirmKey);

router.get('/restore_new_password', getRestorePassword);

router.post('/restore_new_password', postVerifyPasswd);

module.exports = router;
