const router = require('express').Router();


const { getBacklogOwner, generatePDFFileBitacora } = require('../../controllers/OwnerManagement/backlogOwnerManage/backlogOwnerManage.controller');

router.get('/owner_management/backlog_manage/:email', getBacklogOwner);

router.post('/user_management/bitacora_manage/print', generatePDFFileBitacora);

module.exports = router;