const router = require('express').Router();
const path = require('path');
const {getProduct, postProduct, enableDisableProduct, getUpdateProduct, putUpdateProduct, getListProducts, postProductOwner, getProductList} = require('../../controllers/OwnerManagement/productManage/productManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/owner_management/product_manage', isAuthenticated, getProduct);

router.post('/owner_management/product_manage', isAuthenticated, postProduct);

router.put('/owner_management/product_manage/enable_disable/:cod_product', isAuthenticated, enableDisableProduct);

router.get('/owner_management/product_update/:cod_product', isAuthenticated, getUpdateProduct);

router.put('/owner_management/product_update/:cod_product', isAuthenticated, putUpdateProduct);

router.get('/owner_management/product_manage/owner', getListProducts);

router.post('/owner_management/product_manage/owner/register_product', postProductOwner);

router.get('/owner_management/product_manage/list/:ci', getProductList);

router.get("/photo_product/:id",function(req, res){
    res.sendFile(path.join(__dirname, '../../../../security/profiles/'+req.params.id));
});

module.exports = router;