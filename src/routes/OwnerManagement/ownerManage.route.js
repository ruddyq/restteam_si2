const router = require('express').Router();

const { isAuthenticated } = require('../../helpers/auth');

const { getListProductOwner,
        putEnableDisableProductOwner,
        postLoginOwner, 
        postRegisterOwner,
        getOwnerNotifications
 } = require('../../controllers/OwnerManagement/ownerManage/ownerManage.controller');

router.get('/owner_management/owner_manage',isAuthenticated, getListProductOwner);

router.put('/owner_management/enable_disable_product_owner/:ci',isAuthenticated, putEnableDisableProductOwner);

router.post('/owner_management/owner_manage/owner_login', postLoginOwner);

/**Mobile */

router.post('/owner_management/owner_manage/', postRegisterOwner);

router.get('/owner_management/owner_manage/notifications/:ci', getOwnerNotifications);


module.exports = router;
