const router = require('express').Router();
const path = require('path');
const { getGuaranteePolicyList, postValidateGuaranteePolicy, getRecoveryQRCode, getPolicyList, getDataGuaranteePolicyRegister, postRegisterPolicy} = require('../../controllers/OwnerManagement/guaranteePolicyManage/guaranteePolicyManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/owner_management/guarantee_policy_manage', isAuthenticated, getGuaranteePolicyList);

router.post('/owner_management/guarantee_policy_manage/:cod_policy/:state', isAuthenticated, postValidateGuaranteePolicy);

router.get('/rest_team/recovery/:policy_guarantee', getRecoveryQRCode);

router.get('/owner_management/guarantee_policy_manage/owner/:cod_product_owner', getDataGuaranteePolicyRegister);

//Get Photo Guarantee policy File to client http://localhost:3000/photo/*
router.get("/photo/:id",function(req, res){
    res.sendFile(path.join(__dirname, '../../../../security/profiles/'+req.params.id));
});

/**Mobile */
router.get('/owner_management/guarantee_policy_list/:ci', getPolicyList);

router.post('/owner_management/guarantee_policy_manage/register', postRegisterPolicy);

module.exports = router;