const router = require('express').Router();

const { isAuthenticated } = require('../../helpers/auth');

const { getTypeSupportService,
        postTypeSupportService,
        putEnableDisableTypeSupportService,
        getUpdateTypeSupportService,
        putTypeSupportService
    } = require('../../controllers/OwnerManagement/typeSupportServiceManage/typeSupportService.controller');

router.get('/owner_management/type_support_service',isAuthenticated,getTypeSupportService );

router.post('/owner_management/type_support_service', isAuthenticated,postTypeSupportService);

router.put('/owner_management/type_support_service/enable_disable/:id',isAuthenticated,putEnableDisableTypeSupportService);

router.get('/owner_management/type_support_service/update/:id',isAuthenticated,getUpdateTypeSupportService);

router.put('/owner_management/type_support_service/update/:id',isAuthenticated,putTypeSupportService);


module.exports = router;