const router = require('express').Router();
const { getRequestSupportService, postChangeStatusRequest, putChangeStatusRequest, getTypeSupportList, postRequestSupport, getRequestSupportList, getRequestWithoutGuarantee, postRequestSupportServiceWithoutPolicy } = require('../../controllers/OwnerManagement/requestSupportServiceManage/requestSupportServiceManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/owner_management/request_support_service_manage', isAuthenticated, getRequestSupportService);

router.post('/owner_management/request_support_service_manage/:state/:request_no', isAuthenticated, postChangeStatusRequest);

router.put('/owner_management/request_support_service_manage/:state/:request_no', isAuthenticated, putChangeStatusRequest);

/** Mobile */

router.get('/owner_management/reques_support_manage/type_support_list', getTypeSupportList);

router.post('/owner_management/reques_support_manage', postRequestSupport);

router.get('/owner_management/reques_support_manage/list/:ci', getRequestSupportList);

router.get('/owner_management/request_support_manage/without_guarantee/:cod_product_owner', getRequestWithoutGuarantee);

router.post('/owner_management/request_support_manage/register_without_guarantee', postRequestSupportServiceWithoutPolicy);

module.exports = router;