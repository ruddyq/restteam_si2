const router = require('express').Router();

const { isAuthenticated } = require('../../helpers/auth');

const { getListProductCategory,
    postProductCategory,
    getUpdateProductCategory,
    putUpdateProductCategory

} = require('../../controllers/OwnerManagement/productCategory/productCategory.controller');

router.get('/owner_management/product_category', isAuthenticated, getListProductCategory);

router.post('/owner_management/product_category', isAuthenticated, postProductCategory);

router.put('/owner_management/update_product_category/:id_category', isAuthenticated, putUpdateProductCategory);

router.get('/owner_management/update_product_category/:id_category', isAuthenticated, getUpdateProductCategory);

module.exports = router;