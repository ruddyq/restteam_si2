const router = require('express').Router();
const path = require('path');
const { getUser, postUser, getUpdateUser, putUpdateUser, putUpdatePassword, putDisableEnableUser, getBitacora, generatePDFFileActivity, getProfileUser, generateReportPDFUser, getReportInvoiceUser } = require('../../controllers/UserManagement/userManage/userManage.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.get('/user_management/user_manage', isAuthenticated, getUser);

router.post('/user_management/user_manage', isAuthenticated, postUser);

router.get('/user_management/update/:id', isAuthenticated, getUpdateUser);

router.put('/user_management/user_update/:id', isAuthenticated, putUpdateUser);

router.post('/user_management/user_update_password/:id', putUpdatePassword);

router.put('/user_management/user_disable_enable/:id', isAuthenticated, putDisableEnableUser);

router.post('/user_management/bitacora_user_manage', isAuthenticated, getBitacora);

router.post('/user_management/bitacora_user_manage/print/:username', isAuthenticated, generatePDFFileActivity);

router.get('/user_management/user_profile_manage/:id_user', isAuthenticated, getProfileUser);

router.get('/user_management/report_invoice_user', isAuthenticated, getReportInvoiceUser);

router.post('/user_management/report_invoice_user', isAuthenticated, generateReportPDFUser);

//Get Image File to client http://localhost:3000/img/*
router.get("/img/:id",function(req, res){
    //debes enviar a security las imagenes y mediantes estebtener en las views 
    res.sendFile(path.join(__dirname, '../../../../security/profiles/'+req.params.id));
});

module.exports = router;
