const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');

const { getPrivilege, postPrivilege } = require('../../controllers/UserManagement/privilegeManage/privilegeManage.controller');

router.get('/user_management/privilege_manage', isAuthenticated, getPrivilege);

router.post('/user_management/privilege_manage', isAuthenticated, postPrivilege);

module.exports = router;