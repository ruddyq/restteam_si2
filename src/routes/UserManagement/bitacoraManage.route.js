const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');

const { getBitacora, generatePDFFileBitacora } = require('../../controllers/UserManagement/bitacoraManage/bitacoraManage.controller');

router.get('/user_management/bitacora_manage', isAuthenticated, getBitacora);

router.post('/user_management/bitacora_manage/print', isAuthenticated, generatePDFFileBitacora);

module.exports = router;