const router = require('express').Router();
const { getRole, postRole, putRolePermit, getUpdateRole, putRoleUpdateData, putRoleAssignPrivileges, putRoleRemovePrivilege } = require('../../controllers/UserManagement/roleManage/roleManage.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.get('/user_management/role_manage', isAuthenticated, getRole);

router.post('/user_management/role_manage', isAuthenticated, postRole);

router.put('/user_management/role_permit/:id', isAuthenticated, putRolePermit);

router.get('/user_management/role_update/:idRole', isAuthenticated, getUpdateRole);

router.put('/user_management/role_update/role_update_data/:idRole', isAuthenticated, putRoleUpdateData);

router.put('/user_management/role_update/role_assign_privileges/:idRole', isAuthenticated, putRoleAssignPrivileges);

router.put('/user_management/role_update/role_remove_privilege/:idRole', isAuthenticated, putRoleRemovePrivilege);

module.exports = router;