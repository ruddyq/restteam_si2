const router = require('express').Router();
const { getServiceManage, postServiceManage, putServiceManage, getUpdateService, includeExcludeServiceGuarantee } = require('../../controllers/SupportServiceManagement/serviceManage/serviceManage.controller');
const { isAuthenticated } = require('../../helpers/auth');
router.get('/support_service_manage/service_manage', isAuthenticated, getServiceManage);

router.post('/support_service_manage/service_manage', isAuthenticated, postServiceManage);

router.put('/support_service_manage/service_manage_include_exclude/:id_service', isAuthenticated, includeExcludeServiceGuarantee);

router.put('/support_service_manage/update_service_manage/:id_service', isAuthenticated, putServiceManage);

router.get('/support_service_manage/service_manage/update/:id_service', isAuthenticated, getUpdateService);

module.exports = router;