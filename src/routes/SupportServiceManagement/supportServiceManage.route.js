const router = require('express').Router();
const { isAuthenticated } = require('../../helpers/auth');

const {getSupportService, 
        postSupportService, 
        putStateSupportService,
        getUpdateSupportService, 
        putUpdateSupportServiceData, 
        putServiceAddSupportTechnical, 
        deleteServiceToSupportTechnical, 
        putReplacementAddSupportTechnical, 
        deleteReplacementToSupportTechnical, 
        getAddServicesReplacementToTheSupportService, 
        postAddServiceToSupportService, 
        postAddReplacementToSupportService, 
        deleteServiceToSupportService, 
        deleteReplacementToSupportService} = require('../../controllers/SupportServiceManagement/supportServiceManage/supportServiceManage.controller');

router.get('/support_service_management/support_service_manage',isAuthenticated, getSupportService);

router.post('/support_service_management/support_service_manage',isAuthenticated, postSupportService);

router.put('/support_service_manage/support_service_manage/state/:support_no', isAuthenticated, putStateSupportService);

router.get('/support_service_manage/support_service_manage/update/:support_no', isAuthenticated, getUpdateSupportService);

router.put('/support_service_manage/support_service_manage/update_data/:support_no', isAuthenticated, putUpdateSupportServiceData);

router.put('/support_service_manage/support_service_manage/update_service_add/:support_no', isAuthenticated, putServiceAddSupportTechnical);

router.delete('/support_service_manage/support_service_manage/update_service_delete/:support_no/:id_service', isAuthenticated, deleteServiceToSupportTechnical);

router.put('/support_service_manage/support_service_manage/update_replacement_add/:support_no', isAuthenticated, putReplacementAddSupportTechnical);

router.delete('/support_service_manage/support_service_manage/update_replacement_delete/:support_no/:cod_replacement', isAuthenticated, deleteReplacementToSupportTechnical);

router.get('/support_service_management/support_service_manage/:support_no', isAuthenticated, getAddServicesReplacementToTheSupportService);

router.post('/support_service_management/support_service_manage/add_service/:support_no', isAuthenticated, postAddServiceToSupportService);

router.post('/support_service_management/support_service_manage/add_replacement/:support_no', isAuthenticated, postAddReplacementToSupportService);

router.delete('/support_service_manage/support_service_manage/service_delete/:support_no/:id_service', isAuthenticated, deleteServiceToSupportService);

router.delete('/support_service_manage/support_service_manage/replacement_delete/:support_no/:cod_replacement', isAuthenticated, deleteReplacementToSupportService);

module.exports = router; 