const router = require('express').Router();
const {getSpecialty, postSpecialty, getUpdateSpecialty, putUpdateSpecialty} = require('../../controllers/SupportServiceManagement/specialtyManage/specialtyManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/support_service_management/specialty_manage', isAuthenticated, getSpecialty);

router.post('/support_service_management/specialty_manage', isAuthenticated, postSpecialty);

router.get('/support_service_management/specialty_update/:id_specialty', isAuthenticated, getUpdateSpecialty);

router.put('/support_service_management/specialty_update/:id_specialty', isAuthenticated, putUpdateSpecialty);

module.exports = router;