const router = require('express').Router();
const { getListServiceInvoice, getRegisterInvoice,
    putServiceInvoice, putEnableDisable,
    generatePDFFileInvoice,
    wachtInvoice,
    getInvoiceOwner
} = require('../../controllers/SupportServiceManagement/serviceInvoice/serviceInvocie.controller');
const { isAuthenticated } = require('../../helpers/auth');

router.put('/support_service_management/service_invoice/disable_invoice/:bill_no', isAuthenticated, putEnableDisable);

router.get('/support_service_management/service_invoice', isAuthenticated, getListServiceInvoice);

router.put('/support_service_management/register_invoice/:support_no', isAuthenticated, putServiceInvoice);

router.get('/support_service_management/register_invoice/:support_no', isAuthenticated, getRegisterInvoice);

router.post('/support_service_management/service_invoice/service_invoice_generate_pdf/:nit', isAuthenticated, generatePDFFileInvoice);

router.get('/support_service_management/service_invoice/wacht_invoice/:bill', isAuthenticated, wachtInvoice);

router.get('/support_service_management/service_invoice_owner/:ci', getInvoiceOwner);

module.exports = router;
