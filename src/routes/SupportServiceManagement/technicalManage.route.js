const router = require('express').Router();
const {getTechnical, postTechnical, enableDisableTechnical, getUpdateTechnical, putUpdateTechnical, postTechnicalSpecialty, getSpareBox, getTechnicalSpecialty} = require('../../controllers/SupportServiceManagement/technicalManage/technicalManage.controller');
const {isAuthenticated } = require('../../helpers/auth');

router.get('/support_service_management/technical_manage', isAuthenticated, getTechnical);

router.post('/support_service_management/technical_manage', isAuthenticated, postTechnical);

router.post('/support_service_management/technical_manage/add_technical_specialty/:id', isAuthenticated, postTechnicalSpecialty);

router.put('/support_service_management/technical_manage/enable_disable/:id', isAuthenticated, enableDisableTechnical);

router.get('/support_service_management/technical_manage/technical_update/:id', isAuthenticated, getUpdateTechnical);

router.put('/support_service_management/technical_manage/technical_update/:id', isAuthenticated, putUpdateTechnical);
router.get('/support_service_management/technical_manage/spare_box/:id',isAuthenticated, getSpareBox);

module.exports = router;