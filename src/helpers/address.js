const path = require('path');
const addressTheBitacoraFile = path.join(__dirname,'../../../security/bitacora.json');
const addressThePasswordFile = path.join(__dirname,'../../../security/password.json');
const addressThePasswordOwnerFile = path.join(__dirname, '../../../security/passwordOwner.json');
const addressTheBitacoraOwnerFile = path.join(__dirname, '../../../security/bitacoraOwner.json');
const addressTheReport = path.join(__dirname, '../../../security/report/');
const addressInvoice = path.join(__dirname, '../../../security/invoice/');
const addressDevolutionNote= path.join(__dirname, '../../../security/Notes/Devolution_Note/');
const addressExitNote= path.join(__dirname, '../../../security/Notes/Exit_Note/');
const addressContractCompany = path.join(__dirname, '../../../security/profiles/');

module.exports = {
    addressTheBitacoraFile,
    addressThePasswordFile,
    addressThePasswordOwnerFile,
    addressTheBitacoraOwnerFile,
    addressTheReport,
    addressInvoice,
    addressDevolutionNote,
    addressExitNote,
    addressContractCompany
}