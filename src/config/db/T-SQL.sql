-- First Process: Management Client Company
create or replace function register_company(nit_i bigint, name_company_i text,
											corporate_i text, name_representative_i integer,
											email_i text, phone_i integer) returns boolean as
$BODY$
declare nit_query integer=(select count(*) from client_company where nit=nit_i);
begin
	if (nit_query=0) then
		insert into client_company(nit, name_company, corporate, ci_representative, email, phone, "enable") values
					(nit_i, name_company_i, corporate_i, name_representative_i, email_i, phone_i, true);
		raise notice 'the Client Company was registred successfully';
		return true;
	else
		raise notice 'the Client Company already exists: nit: % ',nit_i;
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function generate_contract_company(nit_company_i bigint, cod_contract_i integer, start_date_i date, end_date_i date, descripction_i text, file_name text) returns boolean as
$BODY$
declare company_exists integer = (select count(*) from client_company where nit=nit_company_i);
		contract_exists integer = (select count(*) from contract_company where cod_contract=cod_contract_i);
begin
	if (company_exists=1 and contract_exists=0) then
		insert into contract_company(cod_contract, description_contract, start_date, end_date, nit_company, status_contract, file_contract) values
					(cod_contract_i, descripction_i, start_date_i, end_date_i, nit_company_i, true, file_name);
		raise notice 'the Contract of Client Company was created successfully';
		return true;
	else
		raise notice 'The Company does not exists or the contract code already exists: nit: % code: %',nit_company_i,cod_contract_i;
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_client_company(nit_disable bigint) returns boolean as
$BODY$
declare nit_exists integer = (select count(*) from client_company where nit=nit_disable);
begin
	if (nit_exists=0) then
		raise notice 'Error: company does not exists';
		return false;
	else
		update client_company set "enable"=not "enable" where nit= nit_disable;
		raise notice 'The Company was updated successfully';
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function  register_representative(ci_i integer, issued_place_i varchar(2), name_i text, address_i text, email_i text, phone_i integer) returns boolean as 
$BODY$
declare exist_representative integer= (select count(*) from representative_company where ci=ci_i);
begin
	if (exist_representative=0) then
		insert into representative_company(ci, issued_place, name_representative, address, email, phone) values
					(ci_i,issued_place_i, name_i, address_i, email_i, phone_i);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_representative(ci_i integer, name_i text, address_i text, email_i text, phone_i integer)returns boolean as
$BODY$
declare email_exist text= (select email from representative_company where ci=ci_i);
		exist_representative integer = (select count(*) from representative_company where email = email_i);
begin
	if ((exist_representative=1) or (exist_representative=0)) then
		if(email_i=email_exist) then
			update representative_company set name_representative = name_i, address= address_i, email= email_i, phone = phone_i where ci = ci_i;
			return true;
		else 
			if (exist_representative=0) then 
				update representative_company set name_representative = name_i, address= address_i, email= email_i, phone = phone_i where ci = ci_i;
					return true;
			end if;
		end if;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function get_last_date_contract_company(nit bigint)returns text as 
$BODY$
begin
	return (select to_char(max(end_date)::DATE,'yyyy-mm-dd') from contract_company where nit_company=nit);
end $BODY$ language plpgsql;

create or replace function enable_disable_contract(cod_contract_i integer, nit_i bigint)returns boolean as 
$BODY$
declare enabled integer = (select count(*) from contract_company where cod_contract=cod_contract_i and nit_company= nit_i and status_contract=true);
begin 
	if (enabled=1) then
		update contract_company set status_contract=false where cod_contract=cod_contract_i and nit_company=nit_i;
		return true;
	else
		return false;
	end if;
end $BODY$ language plpgsql;

-- Second Process: Management of Owner and Request Technical Support Service

create or replace function register_owner(ci_i integer, issued_place_i varchar(2), name_i text, phone_i integer, email_i text) returns boolean as
$BODY$
declare exist_owner integer= (select count(*) from "owner" where ci=ci_i and issued_place=issued_place_i);
begin
	if (exist_owner>0) then
		raise notice 'the ci of the owner al ready exist: ci: % issued place: %',ci_i,issued_place_i;
		return false;
	else
		insert into "owner"(ci, issued_place, "name", phone, email, "enable") values
					(ci_i, issued_place_i, name_i, phone_i, email_i, true);
		raise notice 'the owner was registred successfully';
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_owner(ci_params integer) returns boolean as
$BODY$
declare ci_exists integer = (select count(*) from "owner" where ci=ci_params);
begin
	if (ci_exists=0) then
		raise notice 'Error: The owner does not exists';
		return false;
	else
		update "owner" set "enable"=not "enable" where ci= ci_params;
		raise notice 'The Company was disabled or enabled successfully';
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function get_product_guarantee(code_i integer)returns integer as 
$BODY$
declare exists_policy smallint = (select count(*) from guarantee_policy where code_owner=code_i);
begin 
	if (exists_policy=0) then
		return 0;
	else
		return 1;
	end if;
end; 
$BODY$ language plpgsql;

create or replace function register_product(description_product_i text, model_i text, brand_i text, id_category_i integer, product_image_i text)returns integer as
$BODY$
declare product_Register integer;
declare exist_product integer = (select count(*) from product where description_product=description_product_i);
begin
	if (exist_product=0) then
		insert into product(description_product, model, brand, "enable", id_category, product_image) values
									(description_product_i, model_i, brand_i, true, id_category_i, product_image_i) returning cod_product into product_Register;
		return product_Register;
	else
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_product(cod_product_i integer, description_product_i text, model_i text, brand_i text, id_category_i integer, product_image_i text)returns boolean as
$BODY$
declare description_exist text= (select description_product from product where cod_product=cod_product_i);
		exist_product integer = (select count(*) from product where description_product=description_product_i and cod_product<>cod_product_i);
begin
	raise notice 'solve: %',exist_product;
	if (exist_product=0) then
			raise notice 'aqui ppise';
			update product set description_product = description_product_i, model =model_i, brand= brand_i, id_category= id_category_i, product_image= product_image_i
							where cod_product=cod_product_i;
			return true;
	else
		raise notice 'Exist Product in database restteam';
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_product(cod_params integer) returns boolean as
$BODY$
declare cod_exists integer = (select count(*) from product p where cod_product=cod_params);
begin
	if (cod_exists=0) then
		raise notice 'Error: The product does not exists';
		return false;
	else
		update product set "enable"=not "enable" where cod_product= cod_params;
		raise notice 'The Company was disabled successfully';
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_product_owner(ci_owner integer, cod_product_owner integer) returns integer as
$BODY$
declare exists_owner integer = (select count(*) from "owner" where ci=ci_owner);
		exists_product integer = (select count(*) from product where cod_product=cod_product_owner);
		get_category smallint;
		code_r integer;
begin
	if (exists_owner=1 and exists_product=1) then
		get_category:=(select id_category from product where cod_product=cod_product_owner);
		insert into product_owner(ci, cod_product, id_category) values
					(ci_owner, cod_product_owner, get_category) returning code into code_r;
		raise notice 'the product owner was registred successfully';
		return code_r;
	else
		raise notice 'Error: owner or product does not exists ci:% cod product:%',ci_owner,cod_product_owner;
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_guarantee_policy(cod_policy_i text, date_purchase_i date,
													 deadline_i date, proof_i text, invoice_no text,
													 nit_client_company_i bigint, cod_product_owner integer)returns boolean
as $BODY$ 
declare exists_policy integer =(select count(*) from guarantee_policy where cod_policy=cod_policy_i);
		validate_date boolean = (date_purchase_i < deadline_i);
		exists_product_owner integer = (select count(*) from product_owner where code=cod_product_owner);
		ci_owner_i integer = (select ci from product_owner where code=cod_product_owner);
		cod_product_i integer = (select cod_product from product_owner where code=cod_product_owner);
		id_category_i integer = (select id_category from product where cod_product=cod_product_i);
begin
	if (exists_policy=0 and exists_product_owner=1 and validate_date) then
		insert into guarantee_policy(cod_policy, date_purchase, deadline, proof, state_guarantee, nro_invoice, code_owner , ci_owner, cod_product, id_category, nit_client_company) values
									(cod_policy_i, date_purchase_i, deadline_i, proof_i, 2, invoice_no, cod_product_owner, ci_owner_i, cod_product_i, id_category_i, nit_client_company_i);
		raise notice 'The guarantee policy was added successfully';
		return true;
	else
		raise notice 'Error: the guarantee policy had problems on data as policy code or date failure, or product does not exists';
		return false;
	end if;
end $BODY$ language plpgsql;

create or replace function validate_policy_guarantee(cod_policy_v text, state int)returns boolean
as $BODY$
declare state_policy int = (select state_guarantee from guarantee_policy where cod_policy=cod_policy_v);
begin
	if ( (state = 1 or state = 0) and state_policy <> state) then
		update guarantee_policy set state_guarantee=state where cod_policy=cod_policy_v;
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

create or replace function register_request_support(description_i text, cod_policy_i text, id_type_i smallint, code_owner_i integer)returns integer as
$BODY$
declare validate_request integer= (select count(*) from product_owner where code = code_owner_i);
        ci_owner_i integer= (select ci from product_owner po where code = code_owner_i);
        cod_product_i integer= (select cod_product from product_owner po2 where code = code_owner_i);
        id_category_i smallint= (select id_category from product_owner po3 where code = code_owner_i);
		owner_enabled integer = (select count(*) from "owner" o where o.ci = ci_owner_i and o."enable" = true);
		product_enabled integer = (select count(*) from product where cod_product=cod_product_i and "enable" = true);
        request_no_i integer;
begin
	if (validate_request =1 and owner_enabled = 1 and product_enabled = 1) then
		insert into request_support_service(description, cod_policy, date_request, state_request, id_type, code_owner ,ci_owner, cod_product , id_category ) values
					(description_i, cod_policy_i, cast(now() as date) , cast(2 as smallint), id_type_i, code_owner_i , ci_owner_i, cod_product_i, id_category_i) returning request_no into request_no_i;
		raise notice 'request support added successfully';
		return request_no_i;
	else
		raise notice 'Error: the owner and its product does not exists or does not validate, also the owner.';
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_request_support_without_policy(description_i text, id_type_i smallint, code_owner_i integer)returns integer as
$BODY$
declare validate_request integer= (select count(*) from product_owner where code=code_owner_i);
        ci_owner_i integer= (select ci from product_owner po where code = code_owner_i);
        cod_product_i integer= (select cod_product from product_owner po2 where code = code_owner_i);
        id_category_i smallint= (select id_category from product_owner po3 where code = code_owner_i);
		owner_enabled integer = (select count(*) from "owner" o where o.ci = ci_owner_i and o."enable" = true);
		product_enabled integer = (select count(*) from product where cod_product=cod_product_i and "enable" = true);
        request_no_i integer;
begin
	if (validate_request =1 and owner_enabled = 1 and product_enabled = 1) then
		insert into request_support_service(description, cod_policy, date_request, state_request, id_type, code_owner ,ci_owner, cod_product , id_category ) values
					(description_i, null, cast(now() as date) , cast(2 as smallint), id_type_i, code_owner_i , ci_owner_i, cod_product_i, id_category_i) returning request_no into request_no_i;
		raise notice 'request support without guarantee added successfully';
		return request_no_i;
	else
		raise notice 'Error: the owner and its product does not exists or does not validate, also the owner.';
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_request(request_no_e integer, state int)returns boolean as
$BODY$
declare status int = (select state_request from request_support_service where request_no=request_no_e);
begin
	if ((state=1 or state=0) and status<>state) then
		update request_support_service set state_request=state where request_no=request_no_e;
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

create or replace function register_type_support(description text)returns boolean as
$BODY$
declare support integer = (select count(*) from type_support_service where description_type=description);
begin
	if (support=0) then
		insert into type_support_service(description_type, "enable") values
					(description, true);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_type_support_service(id_type_t integer) returns boolean as
$BODY$
declare exist_typeSupport integer = (select count(*) from type_support_service where id_type=id_type_t);
begin
	if (exist_typeSupport=1) then
		update "type_support_service" set "enable"=not "enable" where id_type= id_type_t;
		return true;return false;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function quantity_type_support(idtype integer)returns integer as
$BODY$
declare quantity integer = (select coalesce (count(*),0) from request_support_service rss where id_type = idType);
begin
	return quantity;
end;
$BODY$ language plpgsql;

create or replace function insert_invoice(nit_i bigint,corporate_i text,total_cost_i decimal(12,2),
quantity_day_support_i smallint, support_no_i integer, user_i smallint) returns integer as
$BODY$
declare invoice_exists integer=(select count(*) from service_invoice si,technical_support_service tss where si.support_no=tss.support_no and  si.support_no=support_no_i and si."enable");
declare billNo integer;
begin
	if (invoice_exists = 0) then
		insert into service_invoice (nit,corporate ,total_cost ,service_date ,quantity_day_support ,support_no ,id_user,enable)
		values (nit_i, corporate_i, total_cost_i,now() , quantity_day_support_i, support_no_i,user_i, true) returning bill_no into billNo;
		return billNo;
	else
		return -1;
	end if;
end; $BODY$ language plpgsql;

create or replace function exists_invoice(support_no_i integer) returns integer as
$BODY$
declare noBill integer= (select bill_no from service_invoice where service_invoice.support_no = support_no_i and "enable"); 
begin
	return nobill;
end; $BODY$ language plpgsql;

create or replace function bill_is_canceled(bill_no_i integer) returns boolean as
$BODY$
declare state_bill boolean = (select "enable" from service_invoice where bill_no = bill_no_i);
begin
	if (state_bill) then
		return false;
	else
		return true;
	end if;
	return state_bill;
end; $BODY$ language plpgsql;

create or replace function enable_disable_invoice(bill_no_i integer) returns boolean as
$BODY$
declare invoice_enable boolean = (select service_invoice.enable from service_invoice where bill_no =bill_no_i);
		support_no_get integer = (select si.support_no from service_invoice si where si.bill_no=bill_no_i);
		count_invoice integer = (select count(*) from service_invoice si2 where si2.support_no=support_no_get and not si2."enable");
begin
	if (invoice_enable and count_invoice<1) then
		update service_invoice set enable=false where bill_no=bill_no_i;
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

create or replace function getTechnicalSpecialty(idtechnical integer)returns text as
$BODY$
declare
    specialty text:= '';
    reg RECORD;
    cur_specialty CURSOR FOR select s.description_specialty
                            from specialty s , technical_specialty ts
                            where s.id_specialty = ts.id_specialty and ts.id_technical = idtechnical;
BEGIN
   OPEN cur_specialty;
   FETCH cur_specialty INTO reg;
   WHILE( FOUND ) loop
       specialty := CONCAT(specialty ,', ', reg.description_specialty);
       FETCH cur_specialty INTO reg;
   END LOOP ;
  close cur_specialty;
   return substr(specialty,2);
END
$BODY$ language plpgsql;

create or replace function enable_disable_product_owner(ci_owner integer)returns boolean as
$BODY$
begin
	update owner set "enable"= not "enable" where ci=ci_owner;
	return true;
end; $BODY$ language plpgsql;

create or replace function update_product_category(id_category_i integer,description_category_i text) returns boolean as
$BODY$
declare exist_category integer = (select count(*) from product_category where description_category=description_category_i);
begin
	if (exist_category=0) then
		update product_category set description_category=description_category_i where id_category= id_category_i;
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

-- Third Process: Workshop Management and Inventories
create or replace function register_replacement(description_replacement_i text,id_category_i integer)returns boolean as
$BODY$
declare parts_exists integer = (select count(*) from replacement where description_replacement=description_replacement_i);
begin
	if (parts_exists=0) then
		insert into replacement(description_replacement,"enable",id_category) values
					(description_replacement_i, true ,id_category_i);
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

create or replace function update_replacement(cod_replacement_i integer, description_replacement_i text, id_category_i integer)returns boolean as
$BODY$
declare description_exist text= (select description_replacement from replacement where cod_replacement =cod_replacement_i);
		exist_product integer = (select count(*) from replacement where description_replacement =description_replacement_i);
		category_same integer =(select id_category from replacement where cod_replacement =cod_replacement_i);
begin
	if ((exist_product=1) or (exist_product=0)) then
		if(description_replacement_i=description_exist) then
			update replacement set description_replacement = description_replacement_i, id_category= id_category_i 
							where cod_replacement =cod_replacement_i;
			return true;
		else if (exist_product=0 or id_category_i =category_same) then 
				update replacement set description_replacement = description_replacement_i, id_category= id_category_i 
							where cod_replacement =cod_replacement_i;
				return true;
			else if (id_category_i =category_same)then 
					update replacement set description_replacement = description_replacement_i, id_category= id_category_i 
							where cod_replacement =cod_replacement_i;
					return true;
				end if;
			end if;
		end if;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;


create or replace function enable_disable_replacement(cod_replacement_e integer)returns boolean as
$BODY$
begin
	update replacement set "enable"= not "enable" where cod_replacement=cod_replacement_e;
	return true;
end; $BODY$ language plpgsql;

create or replace function register_parts_to_warehouse_inventory(cod_workshop_i smallint, cod_replacement_i integer, id_category_i integer, quantity_i integer)returns boolean as
$BODY$
begin
	if (quantity_i>=0) then
		insert into warehouse_inventory(cod_workshop, cod_replacement, id_category , quantity) values
					(cod_workshop_i, cod_replacement_i, id_category_i, quantity_i);
		return true;
	else
		raise notice 'Error: I am not add quantity with negative numbers.';
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_replacement_category(description_category_i text) returns boolean as 
$BODY$
declare description_exists integer= (select count(*) from replacement_category where description_category=description_category_i);
begin
	if (description_exists=0) then
		insert into replacement_category(description_category) values (description_category_i);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_replacement_category(id_category_i integer, description_category_i text)returns boolean as
$BODY$
declare exist_replacement integer = (select count(*) from replacement_category where description_category= description_category_i);
begin
	if (exist_replacement=0) then
		update replacement_category set description_category = description_category_i where id_category = id_category_i;
		return true;
	else 
		return false;
	end if;
end;
$BODY$ language plpgsql;


create or replace function register_provider(nit_ci_i bigint, name_i text, address_i text, email_i text, phone_i integer) returns boolean as
$BODY$
declare exist_provider integer= (select count(*) from provider where nit_ci=nit_ci_i);
begin
	if (exist_provider=0) then
		insert into provider(nit_ci, provider_name, address, email, phone, "enable") values
					(nit_ci_i, name_i, address_i, email_i, phone_i, true);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_provider(nit_ci_params bigint) returns boolean as
$BODY$
declare cod_exists integer = (select count(*) from provider p where nit_ci=nit_ci_params);
begin
	if (cod_exists=0) then
		raise notice 'Error: The provider does not exists';
		return false;
	else
		update provider set "enable"=not "enable" where nit_ci= nit_ci_params;
		raise notice 'The Provider was disabled successfully';
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_provider(nit_ci_i bigint, name_i text, address_i text, email_i text, phone_i integer)returns boolean as
$BODY$
declare email_exist text= (select email from provider where nit_ci=nit_ci_i);
		exist_provider integer = (select count(*) from provider where email = email_i);
begin
	if ((exist_provider=1) or (exist_provider=0)) then
		if(email_i=email_exist) then
			update provider set provider_name = name_i, address= address_i, email= email_i, phone = phone_i where nit_ci = nit_ci_i;
			return true;
		else
			if (exist_provider=0) then
				update provider set provider_name = name_i, address= address_i, email= email_i, phone = phone_i where nit_ci = nit_ci_i;
					return true;
			end if;
		end if;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_entry_note(entry_date_i date, description_i text, cod_workshop_i smallint, nit_ci_i bigint)returns integer as
$BODY$
declare income integer;
begin
	 insert into entry_note(entry_date, description, cod_workshop, nit_ci_provider) values
							(entry_date_i, description_i, cod_workshop_i, nit_ci_i)returning income_no into income;
	 return income;
end; $BODY$ language plpgsql;

create or replace function register_exit_note(note_date_i date, description_i text, id_technical_i integer, cod_workshop_i smallint)returns integer as
$BODY$
declare enable_technical boolean = (select "enable" from technical where id_technical=id_technical_i);
		exit_note_no integer;
begin
	if (enable_technical) then
		insert into note(note_date, description, id_technical, note_type, cod_workshop) values
					(note_date_i, description_i, id_technical_i, true, cod_workshop_i)returning note_no into exit_note_no;
		return exit_note_no;
	else
		return -1;	--error -1: technical does not enabled.
	end if;
end $BODY$ language plpgsql;

create or replace function register_devolution_note(note_date_i date, description_i text, id_technical_i integer, cod_workshop_i smallint)returns integer as
$BODY$
declare enable_technical boolean = (select "enable" from technical where id_technical=id_technical_i);
		exit_note_no integer;
begin
	if (enable_technical) then
		insert into note(note_date, description, id_technical, note_type, cod_workshop) values
					(note_date_i, description_i, id_technical_i, false, cod_workshop_i)returning note_no into exit_note_no;
		return exit_note_no;
	else
		return -1;	--error -1: technical does not enabled.
	end if;
end $BODY$ language plpgsql;

create or replace function register_income_detail(income_no_i integer, replacement_i text, quantity_i smallint)returns integer as
$BODY$
declare id_income_no integer;
begin
	if (quantity_i>0) then
		insert into income_detail(income_no, replacement, quantity) values
					(income_no_i, replacement_i, quantity_i)returning id_income_detail into id_income_no;
		return id_income_no;
	else
		return -1; --Error: quatity<=0
	end if;
end $BODY$ language plpgsql;

create or replace function register_income_replacement()returns trigger as
$BODY$
declare get_workshop smallint = (select cod_workshop from entry_note where income_no=new.income_no);
		get_cod_replacement integer = (select cod_replacement from replacement where description_replacement=new.replacement);
	    get_id_category integer = (select id_category from replacement where description_replacement=new.replacement);
		replacement_workshop_exists integer = (select count(*) from warehouse_inventory where cod_workshop=get_workshop and cod_replacement=get_cod_replacement);
		b boolean;
begin
	raise notice 'cod workshop: % , cod_replacement: %',get_workshop,get_cod_replacement;
	if (replacement_workshop_exists=0) then
	    raise notice 'replacement_workshop_does_not_exists';
		b = (select register_parts_to_warehouse_inventory(get_workshop, get_cod_replacement, get_id_category,new.quantity));
	else
		raise notice 'replacement_workshop_exists';
		update warehouse_inventory set quantity=quantity+new.quantity where cod_workshop=get_workshop and cod_replacement=get_cod_replacement;
	end if;
	return new;
end $BODY$ language plpgsql;

create trigger income_detail_replacement after insert
on income_detail
for each row
	execute procedure register_income_replacement();

create or replace function register_exit_note_detail(note_no_i integer, cod_replacement_i integer, quantity_i smallint)returns integer as
$BODY$
declare type_note boolean = (select note_type from note where note_no=note_no_i);
		id_note_detail_r integer;
		replacement_description text = (select description_replacement from replacement where cod_replacement=cod_replacement_i);
		cod_workshop_r integer = (select cod_workshop from note where note_no=note_no_i);
		stock_replacement integer = (select quantity from warehouse_inventory where cod_workshop=cod_workshop_r and cod_replacement=cod_replacement_i);
begin
	if (quantity_i > 0 and type_note and stock_replacement>=quantity_i) then
		insert into note_detail(note_no, replacement, quantity) values
					(note_no_i, replacement_description, quantity_i) returning id_note_detail into id_note_detail_r;
		return id_note_detail_r;
	else
		return -1;	--Error: quantity does not valid, or is not exit note or does not count replacement suficient
	end if;
end; $BODY$ language plpgsql;

create or replace function register_devolution_note_detail(note_no_i integer, cod_replacement_i integer, quantity_i smallint)returns integer as
$BODY$
declare type_note boolean = (select note_type from note where note_no=note_no_i);
		id_note_detail_r integer;
		get_id_technical integer = (select id_technical from note where note_no=note_no_i);
		replacement_description text = (select description_replacement from replacement where cod_replacement=cod_replacement_i);
		spare_box_quantity integer = (select quantity from spare_box where id_technical=2 and cod_replacement=3);
begin
	if (quantity_i > 0 and not type_note and spare_box_quantity>=quantity_i) then
		insert into note_detail(note_no, replacement, quantity) values
					(note_no_i, replacement_description, quantity_i) returning id_note_detail into id_note_detail_r;
		return id_note_detail_r;
	else
		return -1;	--Error: quantity does not valid, or is not devolution note or his spare_box don't has quantity major.
	end if;
end $BODY$ language plpgsql;

create or replace function register_note_devolution_or_exit()returns trigger as
$BODY$
declare get_workshop smallint = (select cod_workshop from note where note_no=new.note_no);
		get_cod_replacement integer = (select cod_replacement from replacement where description_replacement=new.replacement);
		get_type_note boolean = (select note_type from note where note_no=new.note_no);
		replacement_workshop_exists integer = (select count(*) from warehouse_inventory where cod_workshop=get_workshop and cod_replacement=get_cod_replacement);
		get_id_technical integer =(select id_technical from note where note_no=new.note_no);
		spare_box_exists integer;
		id_category_r integer = (select id_category from replacement where cod_replacement=get_cod_replacement);
begin
	if (replacement_workshop_exists=1) then
		raise notice 'replacement, %, %', new.replacement,get_cod_replacement; 
		if (get_type_note) then
			raise notice 'exit, debe aumentar a la caja de repuestos';
			spare_box_exists = (select count(*) from spare_box where id_technical=get_id_technical and cod_replacement=get_cod_replacement);
			if (spare_box_exists=0) then
				raise notice 'no existe su caja de repuestos';
				insert into spare_box(id_technical, quantity, cod_replacement, id_category) values
							(get_id_technical, new.quantity, get_cod_replacement, id_category_r);
				raise notice 'creado su caja de repuestos';
			else
				raise notice 'si existe su caja de repuestos';
				update spare_box set quantity=quantity+new.quantity where id_technical=get_id_technical and cod_replacement=get_cod_replacement;
				raise notice 'aunmentado de repuestos';
			end if;
			update warehouse_inventory set quantity=quantity-new.quantity where cod_workshop=get_workshop and cod_replacement=get_cod_replacement;
		else
			raise notice 'devolution, debe quitar a la caja de repuestos';
			update spare_box set quantity=quantity-cast(new.quantity as integer) where id_technical=get_id_technical and cod_replacement=get_cod_replacement;
			update warehouse_inventory set quantity=quantity+new.quantity where cod_workshop=get_workshop and cod_replacement=get_cod_replacement;
		end if;
	else
		raise notice 'the replacement at workshop does not exists';
	end if;
	return new;
end $BODY$ language plpgsql;

create trigger note_exit_or_devolution_detail_replacement after insert
on note_detail
for each row
	execute procedure register_note_devolution_or_exit();

create or replace function update_stock_decrement_Dev(id_technical_u integer, stock_u integer, cod_replacement_u integer, dev_note_no integer)returns boolean as 
$BODY$
declare exists_debt integer;
		cod_workshop_d integer = (select cod_workshop from note where note_no= dev_note_no);
		idtechnical_i smallint = (select id_technical from note where note_no=dev_note_no and note_type =false);
begin 
	update warehouse_inventory set quantity=quantity-stock_u where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u;
	exists_debt := (select quantity from warehouse_inventory where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u);
	if (exists_debt=0) then
		delete from warehouse_inventory where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u;
	end if;
	update spare_box set quantity=quantity+stock_u where id_technical=id_technical_u and cod_replacement=cod_replacement_u;
	return true;
end $BODY$ language plpgsql;

create or replace function update_stock_increment_Dev(id_technical_new integer, stock_u integer, cod_replacement_u integer, dev_note_no integer)returns boolean as
$BODY$
declare cod_workshop_d smallint = (select cod_workshop from note where note_no=dev_note_no);
		technical integer = (select id_technical from note where note_no=dev_note_no);
		id_category_d integer =(select id_category from spare_box where id_technical=technical and cod_replacement= cod_replacement_u );
		exists_debt integer = (select count(quantity) from warehouse_inventory where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u);
begin	
	if (exists_debt=0) then
		insert into warehouse_inventory values (cod_workshop_d, cod_replacement_u, id_category_d, stock_u);
	else
		update warehouse_inventory set quantity=quantity+stock_u where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u;
	end if;
	update spare_box set quantity=quantity-stock_u where cod_replacement=cod_replacement_u and id_technical=id_technical_new;
	return true;
end $BODY$ language plpgsql;

create or replace function getCountReplacementsInNote(note_no_c integer)returns integer as
$BODY$
begin
	return (select count(*) from note_detail where note_no=note_no_c);
end $BODY$ language plpgsql;

create or replace function getAvailableModifyNote(note_no_v integer, count_days integer)returns boolean as
$BODY$
declare date_note date = (select note_date from note where note_no=note_no_v);
begin
	if ((cast(now() as date)-date_note)>count_days) then
		return false;
	else
		return true;
	end if;
end $BODY$ language plpgsql;

create or replace function update_stock_in_spare_box_decrement(id_technical_u integer, stock_u integer, cod_replacement_u integer, exit_note_no integer)returns boolean as
$BODY$
declare exists_debt integer;
		cod_workshop_i smallint = (select cod_workshop from note where note_no=exit_note_no and note_type =true);
begin
	update spare_box set quantity=quantity-stock_u where id_technical=id_technical_u and cod_replacement=cod_replacement_u;
	exists_debt := (select quantity from spare_box where id_technical=id_technical_u and cod_replacement=cod_replacement_u);
	if (exists_debt=0) then
		delete from spare_box where id_technical=id_technical_u and cod_replacement=cod_replacement_u;
	end if;
	update warehouse_inventory set quantity=quantity+stock_u where cod_workshop=cod_workshop_i and cod_replacement=cod_replacement_u;
	return true;
end $BODY$ language plpgsql;

create or replace function update_stock_in_spare_box_increment(id_technical_new integer, stock_u integer, cod_replacement_u integer, exit_note_no integer)returns boolean as
$BODY$
declare exists_debt integer = (select count(*) from spare_box where id_technical=id_technical_new and cod_replacement=cod_replacement_u);
		cod_workshop_d smallint = (select cod_workshop from note where note_no=exit_note_no);
begin
	if (exists_debt=0) then
		insert into spare_box values (id_technical_new, stock_u, cod_replacement_u);
	else
		update spare_box set quantity=quantity+stock_u where cod_replacement=cod_replacement_u and id_technical=id_technical_new;
	end if;
	update warehouse_inventory set quantity=quantity-stock_u where cod_workshop=cod_workshop_d and cod_replacement=cod_replacement_u;
	return true;
end $BODY$ language plpgsql;


create or replace function delete_detail_of_note(note_no_w integer, id_detail integer)returns integer as
$BODY$
declare cod_replacement_registered integer = (select r.cod_replacement
								   			  from replacement r, note_detail nd
								   			  where r.description_replacement=nd.replacement and nd.id_note_detail=id_detail and nd.note_no=note_no_w);
		id_technical_regitered integer = (select id_technical from note where note_no=note_no_w);
		quantity_registered integer = (select quantity from note_detail where id_note_detail=id_detail and note_no=note_no_w);
		quantity_in_spare_box integer = (select quantity from spare_box where cod_replacement=cod_replacement_registered and id_technical=id_technical_regitered);
		cod_workshop_debt smallint = (select cod_workshop from note where note_no=note_no_w);
begin
	delete from note_detail where note_no=note_no_w and id_note_detail=id_detail;
	if (quantity_registered=quantity_in_spare_box) then
		delete from spare_box where cod_replacement=cod_replacement_registered and id_technical=id_technical_regitered;
		update warehouse_inventory set quantity=quantity+quantity_registered where cod_workshop=cod_workshop_debt and cod_replacement=cod_replacement_registered;
		return 1;
	else
		update spare_box set quantity=quantity-quantity_registered where id_technical=id_technical_regitered and cod_replacement=cod_replacement_registered;
		update warehouse_inventory set quantity=quantity+quantity_registered where cod_workshop=cod_workshop_debt and cod_replacement=cod_replacement_registered;
		return 2;
	end if;
end $BODY$ language plpgsql;

create or replace function delete_detail_of_note_devolution(note_no_w integer, id_detail integer)returns integer as
$BODY$
declare cod_replacement_registered integer = (select r.cod_replacement
								   			  from replacement r, note_detail nd
								   			  where r.description_replacement=nd.replacement and nd.id_note_detail=id_detail and nd.note_no=note_no_w);
		id_technical_regitered integer = (select id_technical from note where note_no=note_no_w);
		quantity_registered integer = (select quantity from note_detail where id_note_detail=id_detail and note_no=note_no_w);
		cod_workshop_debt smallint = (select cod_workshop from note where note_no=note_no_w);
		quantity_in_warehouse integer = (select quantity from warehouse_inventory where cod_replacement=cod_replacement_registered and cod_workshop=cod_workshop_debt);
begin
	delete from note_detail where note_no=note_no_w and id_note_detail=id_detail;
	if (quantity_registered=quantity_in_warehouse) then
		delete from warehouse_inventory where cod_replacement=cod_replacement_registered and cod_workshop=cod_workshop_debt;
		update spare_box set quantity=quantity+quantity_registered where cod_workshop=cod_workshop_debt and cod_replacement=cod_replacement_registered;
		return 1;
	else
		update warehouse_inventory set quantity=quantity-quantity_registered where cod_workshop=cod_workshop_debt and cod_replacement=cod_replacement_registered;
		update spare_box set quantity=quantity+quantity_registered where id_technical=id_technical_regitered and cod_replacement=cod_replacement_registered;
		return 2;
	end if;
end $BODY$ language plpgsql;

create or replace function register_service(description_i text, enable_i boolean)returns integer as
$BODY$
declare service_exists integer = (select count(*) from service where description_service=description_i);
		id_service_g integer;
begin
	if (service_exists=0) then
		insert into service (description_service,service_guarantee) values (description_i, enable_i) returning id_service into id_service_g;
		return id_service_g;
	else
		return -1;
	end if;
end; $BODY$ language plpgsql;

create or replace function update_service(id_service_i integer,description_service_i text,service_guarantee_i boolean) returns boolean as
$BODY$
declare description_exists integer = (select count(*) from service where id_service<>id_service_i and description_service=description_service_i);
begin
	if (description_exists=0 ) then
		update service set description_service=description_service_i, service_guarantee=service_guarantee_i where id_service=id_service_i;
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

create or replace function register_technical_personal(name_i text, email_i text, phone_i integer)returns integer as
$BODY$
declare id_technical_return integer;
begin
	insert into technical("name", email, phone, "enable") values
				(name_i, email_i, phone_i, true)returning id_technical into id_technical_return;
	return id_technical_return;
end $BODY$ language plpgsql;

create or replace function enable_disable_technical(id_technical_e integer)returns boolean
as $BODY$
begin
	update technical set "enable"=not "enable" where id_technical=id_technical_e;
	return true;
end $BODY$ language plpgsql;

create or replace function assign_specialty(id_tecnhinal_a integer, id_specialty_a smallint)returns boolean as
$BODY$
declare exists_specilty_registred integer = (select count(*) from technical_specialty where id_technical=id_tecnhinal_a and id_specialty=id_specialty_a);
begin
	if (exists_specilty_registred = 0) then
		insert into technical_specialty values (id_tecnhinal_a, id_specialty_a);
		return true;
	else
		return false;
	end if;
end $BODY$ language plpgsql;

create or replace function register_specialty(description_specialty_t text) returns boolean as
$BODY$
declare specialty integer = (select count(*) from specialty where description_specialty=description_specialty_t);
begin
	if (specialty=0) then
		insert into specialty(description_specialty) values
					(description_specialty_t);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_specialty(id_specialty_i integer, description_specialty_i text)returns boolean as
$BODY$
declare exist_specialty integer = (select count(*) from specialty where description_specialty = description_specialty_i);
begin
	if (exist_specialty=0) then
		update specialty set description_specialty = description_specialty_i where id_specialty = id_specialty_i;
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_support_service(service_date_i date, observations_i text, request_no_i integer, id_technical_i integer)returns integer as
$BODY$
declare date_request_service date = (select date_request from request_support_service where request_no=request_no_i);
		get_state_request boolean = (select state_request from request_support_service where request_no=request_no_i);
		suppor_no_ret integer;
		technical_validate boolean = (select "enable" from technical where id_technical=id_technical_i);
begin
	if (date_request_service <= service_date_i and get_state_request and technical_validate) then
		insert into technical_support_service(total_cost, service_date, observations, state_support_service ,request_no, id_technical) values (cast(0 as decimal(12,2)), service_date_i, observations_i, false ,request_no_i, id_technical_i)returning support_no into suppor_no_ret;
		return suppor_no_ret;
	else
		return -1; --fecha del servicio con solicitud no concuerda, or solicitud de servicio no aceptada, o tecnico no habilitado.
	end if;
end $BODY$ language plpgsql;

create or replace function register_service_in_support(support_no_i integer, id_service_i integer, cost_i decimal(12,2))returns boolean as
$BODY$
begin
	if (cost_i > 0) then
		insert into list_service(support_no, id_service, "cost") values
					(support_no_i, id_service_i, cost_i);
		update technical_support_service set total_cost=total_cost+cost_i where support_no=support_no_i;
		raise notice 'total cost of no: % is updated.',support_no_i;
		return true;
	else
		return false;	-- costo del servicio no es lo debido
	end if;
end $BODY$ language plpgsql;

create or replace function delete_service_to_support_service(support_no_d integer, id_service_d integer)returns boolean as
$BODY$
declare cost_to_decrement decimal(12,2) = (select "cost" from list_service where support_no=support_no_d and id_service=id_service_d);
begin
	delete from list_service where support_no=support_no_d and id_service=id_service_d;
	update technical_support_service set total_cost=total_cost-cost_to_decrement where support_no=support_no_d;
	return true;
end $BODY$ language plpgsql;

create or replace function register_spare_parts(support_no_i integer, cod_replacement_i integer,
												quantity_i integer, cost_i decimal(12,2))returns boolean as
$BODY$
declare id_technical_r integer = (select id_technical from technical_support_service where support_no=support_no_i);
		quantity_has_replacement_technical integer = (select coalesce(quantity,-1)
											 		  from spare_box
											 		  where id_technical=id_technical_r and cod_replacement=cod_replacement_i);
		category integer = (select id_category from replacement where cod_replacement = cod_replacement_i);
begin
	if (quantity_has_replacement_technical > 0 and quantity_has_replacement_technical >= quantity_i) then
		insert into list_spare_parts(support_no, cod_replacement,id_category ,quantity, "cost") values
					(support_no_i, cod_replacement_i, category, quantity_i, cost_i);
		update spare_box set quantity=quantity-quantity_i where id_technical=id_technical_r and cod_replacement=cod_replacement_i;
		update technical_support_service set total_cost=total_cost+quantity_i*cost_i where support_no=support_no_i;
		return true;
	else
		return false; --cantidad de la caja de repuestos del tecnico no existe o no tiene el stock necesario
	end if;
end $BODY$ language plpgsql;

create or replace function delete_replacement_support_service(support_no_d integer, cod_replacement_d integer)returns boolean as
$BODY$
declare id_techinal_inv integer = (select id_technical from technical_support_service where support_no=support_no_d);
		quantity_inv integer = (select quantity from list_spare_parts where cod_replacement=cod_replacement_d and support_no=support_no_d);
		cost_inv decimal(12,2) = (select "cost" from list_spare_parts where cod_replacement=cod_replacement_d and support_no=support_no_d);
begin
	delete from list_spare_parts where cod_replacement=cod_replacement_d and support_no=support_no_d;
	update technical_support_service set total_cost=total_cost-cost_inv*quantity_inv where support_no=support_no_d;
	update spare_box set quantity=quantity+quantity_inv where id_technical=id_techinal_inv and cod_replacement=cod_replacement_d;
	return true;
end $BODY$ language plpgsql;

create or replace function register_service_invoice(nit_i bigint, corporate_i text,
										   service_date_i date, quantity_day_support_i smallint,
										   support_no_i integer, id_user_i smallint)returns boolean as
$BODY$
declare enabled_user boolean = (select "enable" from "user" where id_user=id_user_i);
		total_cost_support decimal(12,2) = (select total_cost from technical_support_service where support_no=support_no_i);
begin
	if (quantity_day_support_i>0 and enabled_user) then
		insert into service_invoice(nit, corporate, total_cost, service_date, quantity_day_support, support_no, id_user) values
					(nit_i, corporate_i, total_cost_support, service_date_i, quantity_day_support_i, support_no_i, id_user_i);
		return true;
	else
		return false;
	end if;
end $BODY$ language plpgsql;

create or replace function register_workshop(name_w varchar(20), address_w text, latitude_w varchar(15), longitude_w varchar(15)) returns boolean as
$BODY$
declare workshop_exists integer= (select count(*) from workshop where "name"=name_w);
		workshop_address integer= (select count(*) from workshop where address =address_w);
begin
	if(workshop_exists=0 and workshop_address=0) then
		insert into workshop("name", address, latitude, longitude) values (name_w, address_w, latitude_w, longitude_w);
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_workshop(cod_workshop_w integer,name_w varchar(20), address_w text, latitude_w varchar(15), longitude_w varchar(15)) returns boolean as
$BODY$
declare exists_name integer = (select count(*) from workshop where cod_workshop<>cod_workshop_w and "name"=name_w);
		exists_address integer = (select count(*) from workshop where address=address_w and cod_workshop<>cod_workshop_w);
		repeat_coord integer = (select count(*) from workshop w2 where (latitude=latitude_w and longitude=longitude_w) and cod_workshop<>cod_workshop_w);	
begin
	if (exists_name=0 and exists_address=0 and repeat_coord=0) then
		update workshop set "name"=name_w, address=address_w, latitude=latitude_w, longitude=longitude_w where cod_workshop=cod_workshop_w;
		return true;
	else
		return false;
	end if;
end; $BODY$ language plpgsql;

--Fourth Process: Management System User

create or replace function register_privilege(description text, id_super_privilege_i integer) returns integer as
$BODY$
declare exists_descr integer = (select count(*) from privilege where description_privilege=description);
		id_privilege_added integer;
begin
	if(exists_descr=0) then
		if (id_super_privilege_i=-1) then
			insert into privilege(description_privilege, id_super_privilege) values
					(description, null)returning id_privilege into id_privilege_added;
			return id_privilege_added;
		else
			insert into privilege(description_privilege, id_super_privilege) values
					(description, cast(id_super_privilege_i as smallint))returning id_privilege into id_privilege_added;
			return id_privilege_added;
		end if;
	else
		return -1;
	end if;
end; $BODY$ language plpgsql;

create or replace function register_role(nameR text, descriptionR text) returns integer as
$BODY$
declare exists_role integer= (select count(*) from "role" where role_name=nameR);
		exists_descr integer= (select count(*) from "role" where description=descriptionR);
		id_role_r integer;
begin
	if(exists_role=0 and exists_descr=0) then
		insert into role(role_name,description, "enable") values
					(nameR,descriptionR, true)returning id_role into id_role_r;
		return id_role_r;
	else
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_role(id_role_i integer) returns boolean as
$BODY$
declare exist_role integer = (select count(*) from role where id_role=id_role_i );
begin
	if (exist_role=1) then
		update "role" set "enable"=not "enable" where id_role= id_role_i;
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_rol_privilege( id_role_i integer, id_privilege_i integer) returns boolean as
$BODY$
declare	exists_role integer = (select count(*) from role where id_role=id_role_i);
		exists_privilege integer = (select count(*) from privilege where id_privilege=id_privilege_i);
		exists_same integer = (select count(*) from role_privilege where id_role=id_role_i and id_privilege=id_privilege_i);
begin
	if ((exists_role=0 and exists_privilege=0) or (exists_same=1))then
		return false;
	else
		insert into role_privilege(id_role, id_privilege) values
					(id_role_i, id_privilege_i);
		return true;
	end if;
end;
$BODY$ language plpgsql;

create or replace function register_user(username_i text, email_i text, profile_i text, id_role_i integer) returns integer as
$BODY$
declare exist_email_user integer = (select count(*) from "user" where email=email_i);
declare idUser integer;
begin
	if (exist_email_user=0) then
		insert into "user"(username, email,user_profile, "enable", id_role) values
					(username_i, email_i, profile_i, true, id_role_i) returning id_user into idUser;
		return idUser;
	else
		return -1;
	end if;
end;
$BODY$ language plpgsql;

create or replace function enable_disable_user(id_user_i integer) returns boolean as
$BODY$
declare exist_user integer = (select count(*) from "user" where id_user=id_user_i );
begin
	if (exist_user=1) then
		update "user" set "enable"=not "enable" where id_user= id_user_i;
		return true;
	else
		return false;
	end if;
end;
$BODY$ language plpgsql;

create or replace function update_user(id_user_i smallint, username_i varchar(50), email_i text, user_profile_i text, id_role_i smallint)returns boolean as
$BODY$
declare email_exist text= (select email from "user" where id_user=id_user_i);
		exist_user integer = (select count(*) from "user" where email = email_i);
begin
if ((exist_user=1) or (exist_user=0)) then
		if(email_i=email_exist) then
			update "user" set username = username_i, email= email_i, user_profile= user_profile_i where id_user = id_user_i;
			return true;
		else 
        if (exist_user=0) then 
			update "user" set username = username_i, email= email_i, user_profile= user_profile_i where id_user = id_user_i;
			return true;
		end if;
end if;
else
	return false;
end if;
end;
$BODY$ language plpgsql;

create or replace function delete_role_privilege(id_privilege_r integer, id_role_r integer)returns boolean as
$BODY$
declare countPrivileges integer= (select count(*) from role_privilege where id_role=id_role_r);
begin
	if (countPrivileges>1) then
		delete from role_privilege where id_role=id_role_r and id_privilege=id_privilege_r;
		return true;
	else
		return false;
	end if;
end $BODY$ language plpgsql;

create or replace function verifyPermitUser(id_user_v integer, id_privilege_v integer)returns boolean as
$BODY$
declare id_role_v integer = (select id_role from "user" where id_user=id_user_v);
		enable_user boolean = (select "enable" from "user" where id_user=id_user_v);
		enable_role boolean = (select "enable" from "role" where id_role=id_role_v);
		exists_permit boolean = (select verifyRelationExists(id_role_v, id_privilege_v));
begin
	if (enable_user and enable_role and exists_permit) then
		return true;
	else
		return false;
	end if;
end $BODY$ language plpgsql;

create or replace function verifyRelationExists(id_role_v integer, id_privilege_v integer)returns boolean as
$BODY$
declare exists_relation integer = (select count(*) from role_privilege where id_privilege=id_privilege_v and id_role=id_role_v);
		id_privilege smallint = (select id_super_privilege from privilege where id_privilege=id_privilege_v);
begin
	if (exists_relation=1) then
		return true;
	else
		if (id_privilege is null) then
			return false;
		else
			return (select verifyRelationExists(id_role_v, id_privilege));
		end if;
	end if;
end $BODY$ language plpgsql;

create or replace function get_description_super_privilege(id_super_privilege_v integer)returns text as
$BODY$
begin
	return (select description_privilege from privilege where id_privilege=id_super_privilege_v);
end $BODY$ language plpgsql;

create or replace function get_count_user_in_role(id_role_v smallint)returns int as 
$BODY$
begin 
	return (select count(id_user) from "user" where id_role=id_role_v);
end $BODY$ language plpgsql;

--Devuelve el nombre del rol de un determinado usuario
create or replace function getRoleName(idUser integer) returns varchar(30)as
$BODY$
declare idRole integer =(select id_role from "user" where id_user =idUSer);
declare roleName varchar(30)=(select role_name from "role" where id_role =idRole );
begin
	return rolename;
end
$BODY$ language plpgsql;

--Devuelve una tabla con la lista de usuarios habilitados, ademas con el nombre de rol de cada uno de ellos
create or replace function getListUsers() returns table(id_user smallint,username varchar(50),email text, user_profile text,"enable" boolean,id_role smallint,role_name varchar(30))as
$BODY$
begin
	return query select "user".id_user ,"user".username ,"user".email ,"user".user_profile ,"user"."enable" ,"user".id_role,"role".role_name
                 from "user","role"
                 where "user".id_role = "role".id_role and "user"."enable" ='true'
                 order by "user".id_user;
end
$BODY$ language plpgsql;

--Devuelve una tabla con la lista de usuarios inahbilitados, ademas con el nombre de rol de cada uno de ellos
create or replace function getListDisabledUsers() returns table(id_user smallint,username varchar(50),email text, user_profile text,"enable" boolean,id_role smallint,role_name varchar(30))as
$BODY$
begin
	return query select "user".id_user ,"user".username ,"user".email ,"user".user_profile ,"user"."enable" ,"user".id_role,"role".role_name
                 from "user","role"
                 where "user".id_role = "role".id_role and "user"."enable" ='false'
                 order by "user".id_user;
end
$BODY$ language plpgsql;

--Devuelve el nombre de rol dado un id de rol
create or replace function getRoleName_Id(idRole integer) returns varchar(30)as
$BODY$
declare roleName varchar(30)=(select role_name from "role" where id_role =idRole );
begin
	return rolename;
end
$BODY$ language plpgsql;

--Devuelve una tabla con la lista de las factiras emitidas por una determinada empresa
create or replace function list_invoce_company_general() returns table(name_client text,cod_policy text, request_no integer, name_tec text,total_cost numeric)as
$BODY$
begin
	return query select "owner".name_client,cod_policy, "technical_support_service".request_no,"technical".name_tec, total_cost
	from "owner", "guarantee_policy","technical", "request_support_service","technical_support_service", "client_company"
	where "owner".ci = "request_support_service".ci_owner
	and "request_support_service".cod_product = "guarantee_policy".cod_product
	and "technical_support_service".id_technical = "technical".id_technical
	and "request_support_service".request_no = "technical_support_service".request_no;
end
$BODY$ language plpgsql;

create or replace function delete_income_replacement()returns trigger as
$BODY$
declare get_workshop smallint = (select cod_workshop from entry_note where income_no=old.income_no);
		get_cod_replacement integer = (select cod_replacement from replacement where description_replacement=old.replacement);
begin
	raise notice 'replacement_workshop_exists';
		update warehouse_inventory set quantity = quantity-old.quantity where cod_workshop=get_workshop and cod_replacement=get_cod_replacement;
	return old;
end $BODY$ language plpgsql;

create trigger income_detail_replacement_d after delete
on income_detail
for each row
	execute procedure delete_income_replacement();

create or replace function get_company_name(cod_policy_i text)returns text as
 $BODY$
begin
	return (select cc.name_company from client_company cc, guarantee_policy gp where cc.nit = gp.nit_client_company and gp.cod_policy = cod_policy_i);
end;
$BODY$ language plpgsql;

create or replace function get_count_services_technical(id_technical_v integer)returns integer as 
$BODY$
begin 
	return (select count(distinct request_no ) from technical_support_service tss where tss.id_technical=id_technical_v);
end $BODY$ language plpgsql;

create or replace function register_product_category(description_r text)returns boolean as 
$BODY$
declare exists_category integer = (select count(*) from product_category where description_category=description_r);
		id_cat smallint;
begin 
	if (exists_category=0) then
		insert into product_category(description_category) values (description_r) returning id_category into id_cat;
		if (id_cat>0) then
			return true;
		else
			return false;
		end if;
	else
		return false;
	end if;
end
$BODY$ language plpgsql;