/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**********************************************                                            *****************************************/
/**********************************************  First Process: Management Client Company  *****************************************/
/**********************************************                                            *****************************************/
/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**
  * function register_company
  * params:
  * nit_i bigint: nit de la empresa
  * name_company text: nombre de la empresa
  * corporate_i text: razon social de la empresa
  * name_representative_i text: nombre de la persona representante de la empresa
  * email_i text: email de la empresa
  * phone_i integer: telefono de la empresa 
  * objetive:
  * 	registra la empresa comercializadora, respecto a sus datos; despues urge que registrar su contrato.
  * 	se puede inhabilitar la empresa.
  *	PRUEBA:
  *		select register_company(54458695325482, 'Dell Computers SA','Dell Computers Germany SA','Joshep Guardiola', 'dell.germany.computer@dell.com',75395373);
  * returns:
  * 	boolean : true=registro coorectamente; false=no se registro
 **/
 create or replace function register_company(
     nit_i bigint, name_company_i text, corporate_i text, name_representative_i text, email_i text, phone_i integer) 
     returns boolean
/**
  * FUNCTION generate_contract_company
  * PARAMS:
  * nit_company_i bigint: nit de la empresa comercializadora
  * cod_contract_i integer: codigo de contrato. El notario registra un contrato oficial con un respectivo id, cual debe ser registrado de esa forma
  * start_date_i date: la fecha de inicio del contrato
  * end_date_i date: la fecha en el que se termina el contrato
  * descripction_i text: alguna observación o especificacion que tiene el contrato para que se pueda especificar
  * OBJETIVE:
  * 	registra el contrato de la empresa comercializadora, para que se pueda controlar las garantias de soporte a sus clientes y evitar dar
  *		dar soporte a sus productos sin un contrato que lo respalde. 
  *	PRUEBA:
  *		select generate_contract_company(54458695325482, 12354, '08/04/2020', '08/04/2021', 'Daremos soporte tecnico a sus dispositivos informaticos ya sean articulos o productos enteros');
  * RETURNS:
  * 	boolean : true=registro correctamente; false=no se registro porque la empresa no existe o hay algun contrato con el mismo "cod_contract"
 **/
 create or replace function generate_contract_company(nit_company_i bigint, cod_contract_i integer, start_date_i date, end_date_i date, descripction_i text) returns boolean
 /**
  * FUNCTION enable_disable_client_company
  * PARAMS:
  * nit_disable bigint: nit de la empresa comercializadora
  * OBJETIVE:
  * 	habilita o inhabilita a la empresa comercializadora con el que tenemos contrato
  *		si esta habilitada, entonces la inhabilita. pero si esta inhabilitada la funcion la habilita.
  * PRUEBA: 
  * 	select enable_disable_client_company(54458695325482);
  * RETURNS:
  * 	boolean : true=habilitacion o inhabilitacion correcta; false=no esta registrada esa empresa
 **/
 create or replace function enable_disable_client_company(nit_disable bigint) returns boolean
 /**
  * FUNCTION get_last_date_contract_company
  * PARAMS:
  * nit bigint: nit de la empresa cliente
  * OBJETIVE:
  * 	obtiene la fecha de conclusion del ultimo contrato realizado con la empresa
  * PRUEBA:
  * 	select enable_disable_user(21);
  * RETURNS:
  * 	boolean : true :se habilito o deshabilito al usuario correctamente<---> false: no existe usuario
 **/
create or replace function get_last_date_contract_company(nit bigint)returns date

/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/***********************                                                                             *******************************/
/***********************  Second Process: Management of Owner and Request Technical Support Service  *******************************/
/***********************                                                                             *******************************/
/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**
  * FUNCTION register_owner
  * PARAMS:
  * ci_i integer: CI del cliente propietario
  * issued_place_i varchar(2): lugar de expedidp del CI (CH ,LP, CB, OR, PT, TJ, SC, BE, PD)
  * name_i text: nombre del cliente propietario (persona que compro el producto de la empresa comercializadora)
  * phone_i integer: telefono del cliente propietario
  * email_i text: email del cliente propietario
  * OBJETIVE:
  * 	registra al cliente pripetario (persona que compro el producto de la empresa comercializadora) y va a solicitar su servicio
  *		su servicio de soporte tecnico.
  * PRUEBA:
  * 	select register_owner(9719838, 'SC', 'Zuleny Cruz', 76627871, 'zuleny.cr@gmail.com');
  * RETURNS:
  * 	boolean : true=registro correcto; false=el cliente propietario ya existe por su CI
 **/
 create or replace function register_owner(ci_i integer, issued_place_i varchar(2), name_i text, phone_i integer, email_i text) returns boolean
 /**
  * FUNCTION enable_disable_owner
  * PARAMS:
  * ci_params integer: CI del cliente propietario
  * OBJETIVE:
  * 	habilita o inhabilita al propietario para razones de seguridad o personales de parte de la empresa comercializadora
  * PRUEBA:
  * 	select enable_disable_owner(12345687);
  * RETURNS:
  * 	boolean : true=habilitacion o habilitacio correcta; false=el cliente propietario no existe
 **/
 create or replace function enable_disable_owner(ci_params integer) returns boolean
 /**
  * FUNCTION register_product_category
  * PARAMS:
  * description_category_i text: Descripcion breve de la categoria
  * OBJETIVE:
  * 	registrar una nueva categoria
  * PRUEBA:
  * 	select register_product_category('Industrial');
  * RETURNS:
  * 	boolean : true=categoria registrada; false= ya existe la categoria
 **/
 create or replace function register_product_category(description_category_i text) returns boolean
 /**
  * FUNCTION register_product
  * PARAMS:
  * description_product_i text: el nombre del producto
  * model_i text: modelo del producto
  * brand_i text: marca del producto
  * id_category_i integer: id de la categoria a la que pertenece el producto,
  * product_image: nombre de la imgen del producto
  * OBJETIVE:
  * 	registra el producto por si no existe en el sistema
  * PRUEBA:
  * 	
  * RETURNS:
  * 	boolean : true=producto registrado; false=errores de desarrollo
 **/
create or replace function register_product(description_product_i text, model_i text, brand_i text, id_category_i integer, product_image text)returns boolean
 /**
  * FUNCTION enable_disable_product
  * PARAMS:
  * cod_params integer: codigo del producto
  * OBJETIVE:
  * 	habilita o inahbilita el producto.
  * PRUEBA:
  * 	
  * RETURNS:
  * 	boolean : true=habilitacion o inhabilitacion correcto; false=el producto no existe
 **/
 create or replace function enable_disable_product(cod_params integer) returns boolean
 /**
  * FUNCTION register_product_owner
  * PARAMS:
  * ci_owner integer: CI del cliente propietario
  * cod_product_owner integer: codigo del producto (producto es algun equipo informatico, electrodomestico, automovil o aeronave)
  * quantity smallint: cantidad del producto
  * OBJETIVE:
  * 	registra los productos pertenecientes del cliente propietario, cual lo usuará para registrar al producto que tiene garantia
  *		inclusive a los que no; pero solo registran los que tiene garantia o solicitan algun servicio de soporte tecnico
  * PRUEBA:
  * 	select register_product_owner(12345687, 1, cast(1 as smallint));
  * RETURNS:
  * 	boolean : true=el producto del propietario fue registrado correctamente; false=el producto no existe o no existe el cliente propietario
 **/
 create or replace function register_product_owner(ci_owner integer, cod_product_owner integer, quantity smallint) returns boolean
 /**
  * FUNCTION register_guarantee_policy
  * PARAMS:
  * cod_policy_i text: codigo de la poliza de garantia (codigo que se encuentra en el comprobante)
  * date_purchase_i date: fecha de compra del producto
  * deadline_i date: fecha de la conclusion de la poliza de garantia
  * proof_i text: comprobante de la poliza de garantia (nombre de la imagen jpg, bmp, jpeg, png, etc).
  * ci_owner_i integer: CI del cliente propietario, quien compro el producto
  * cod_product_i integer: codigo del producto a registrar
  * nit_client_company_i bigint: nit de la empresa comercializadora (empresa quien vendio el producto)
  * OBJETIVE:
  * 	registra la poliza de garantia para que reciba los servicios de soporte tecnico de manera gratuita el propietario
  *		y los costos se irá a la empresa comercializadora.
  * PRUEBA:
  * 	
  * RETURNS:
  * 	boolean : true=la poliza de garantia fue registrado correctamente; false=la fecha de compra con la conclusion de garantia no coinciden o ya existe la poliza
  *		o el producto no existe
 **/
 create or replace function register_guarantee_policy(cod_policy_i text, date_purchase_i date,
													 deadline_i date, proof_i text,
													 ci_owner_i integer, cod_product_i integer,
													 nit_client_company_i bigint)returns boolean
/**
  * FUNCTION validate_policy_guarantee
  * PARAMS:
  * cod_policy_v text: codigo de la poliza de garantia
  * OBJETIVE:
  * 	Nuestra empresa debe verificar la poliza de garantia que nos manda el cliente proietario para que 
  *		nosotros la  hagamos valida o invalida la poliza de garantia respecto a sus datos o su comprobante. 
  * PRUEBA:
  * 	select validate_policy_guarantee('15E101');
  * RETURNS:
  * 	boolean : true=la poliza de garantia fue habilitado correctamente; false=errores en el desarrollo o syntax error 
 **/
 create or replace function validate_policy_guarantee(cod_policy_v text)returns boolean
 /**
  * FUNCTION register_request_support
  * PARAMS:
  * description_i text: alguna descripcion especifica de la razon de la solicitud (o explicando el problema si tiene alguno)
  * cod_policy_i text: codigo de la poliza
  * id_type_i smallint: id del tipo del servicio tecnico
  * code_owner_i integer: codigo unico por cada producto con garantia
  * OBJETIVE:
  * 	el cliente propietario debe solicitar servicio de soporte tecnico para luego nuestra empresa deba evaluar los datos y aceptar la
  *		solicitud de soporte tecnico.
  * PRUEBA:
  * 	select register_request_support(cast('La maquina no enciende' as text), cast('CO1458' as text), cast(1 as smallint), 1);
  * RETURNS:
  *  devuelve el numero de solicitud, en caso de que haya ocurrido un error de insercion devueve -1 
 **/
 create or replace function register_request_support(description_i text, cod_policy_i text, id_type_i smallint, code_owner_i integer)returns integer
 /**
  * FUNCTION enable_disable_request
  * PARAMS:
  * request_no_e integer: nro de solicitud de soporte
  * OBJETIVE:
  * 	permite aceptar la solicitud de soporte tecnico o negarlo, si esta aceptado esta habilitado para recibir el servicio; caso contrario no puede
  * PRUEBA:
  * 	select enable_disable_request(1)
  * RETURNS:
  * 	boolean : true=la solicitud de soporte tecnico se acepto correctamente; false=errores de desarrollo o syntax error
 **/
create or replace function enable_disable_request(request_no_e integer)returns boolean
 /**
  * FUNCTION register_type_support
  * PARAMS:
  * description text: Descripcion del tipo de soporte
  * OBJETIVE:
  * 	 registrar un nuevo tipo de servicio de soporte tecnico
  * PRUEBA:
  * 	select register_type_support('Oficina');
  * RETURNS:
  * 	boolean : true=se registro correctamente; false=el tipo de soporte ya existe
 **/
 create or replace function register_type_support(description text)returns boolean
 /**
  * FUNCTION enable_disable_type_support_service
  * PARAMS:
  * id_type_t integer: id tipo de soporte tecnico
  * OBJETIVE:
  * 	permite habilitar o inhabilitar un tipo de servicio de soporte tecnico
  * PRUEBA:
  * 	select enable_disable_type_support_service(4);
  * RETURNS:
  * 	boolean : true= se deshabilito o inhabilito correctamente; false= no existe tipo de servicio de soporte
 **/
 create or replace function enable_disable_type_support_service(id_type_t integer) returns boolean

 /**
  * FUNCTION quantity_type_support
  * PARAMS:
  * idtype integer: id tipo de soporte tecnico
  * OBJETIVE:
  * 	permite contar la cantidad de solicitudes de soporte tecnico para un determinado tipo de soporte tecnico
  * PRUEBA:
  * 	select quantity_type_support(1);
  * RETURNS:
  * 	la cantidad de solicitudes para un determinado tipo de soporte tecnico, en el caso de que no hay ninguno devuelve 0
 **/

 create or replace function quantity_type_support(idtype integer)returns integer 
 /***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/********************************                                                      *********************************************/
/********************************  Third Process: Workshop Management and Inventories  *********************************************/
/********************************                                                      *********************************************/
/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**
  * FUNCTION register_replacement
  * PARAMS:
  * description_replacement_i text: nombre del repuesto
  * OBJETIVE:
  * 	registra al respuesto en el sistema, asi estara habiliatdo para la administracion en los almacenes que contienen este repuesto
  * PRUEBA:
  * 	select register_replacement('Procesador Intel Core i5');
  * RETURNS:
  * 	boolean : true=se registro el repuesto correctamente; false=el repuesto existe, no fue registrado
 **/
 create or replace function register_replacement(description_replacement_i text)returns boolean
 /**
  * FUNCTION enable_disable_replacement
  * PARAMS:
  * cod_replacement_e integer: codigo del repuesto
  * OBJETIVE:
  * 	habilita o inhabilita el repuesto, asi se controlará el uso de este producto
  * PRUEBA:
  * 	select enable_disable_replacement(1)
  * RETURNS:
  * 	boolean : true=se habilito o inhabilito el repuesto; false=no existe el repuesto
 **/
 create or replace function enable_disable_replacement(cod_replacement_e integer)returns boolean
 /**
  * FUNCTION register_workshop
  * PARAMS:
  * name_i varchar(20): nombre del taller
  * address_i text: direccion (ubicacion) del taller en la ciudad
  * OBJETIVE:
  * 	al registrar un nuevo taller, tomamos en cuenta que cada taller tiene un unico almacen que le corresponde
  * PRUEBA:
  * 	select register_workshop('Los Pinos', 'Av. Los Pinos, alado de xasa de chumacero');
  * RETURNS:
  * 	boolean : true=se creo un nuevo taller; false=el taller ya existe en esa direccion o hay algun taller que tiene el mismo nombre
 **/
 create or replace function register_workshop(name_i varchar(20), address_i text)returns boolean
 /**
  * FUNCTION register_parts_to_warehouse_inventory
  * PARAMS:
  * cod_workshop_i smallint: codigo del taller
  * cod_replacement_i integer: codigo del repuesto
  * quantity_i integer: cantidad del repuesto a ingresar al taller
  * OBJETIVE:
  * 	crea el producto en un especifico taller para que pueda ingresar o salir del taller ese repuesto.
  * PRUEBA:
  * 	select register_parts_to_warehouse_inventory(cast(1 as smallint), 1, 0);
  * RETURNS:
  * 	boolean : true=se registro correctamente; false=la cantidad es menor a 0, no es valido numeros negativos
 **/
 create or replace function register_parts_to_warehouse_inventory(cod_workshop_i smallint, cod_replacement_i integer, quantity_i integer)returns boolean
 /**
  * FUNCTION register_entry_note
  * PARAMS:
  * entry_date_i date: fecha de ingreso
  * description_i text: descripcion de la razon de ingreso de repuestos
  * cod_workshop_i smallint: codigo del taller a ingresar
  * ni/ci bigint: nit o ci del proveedor
  * OBJETIVE:
  * 	Registra una nota de ingreso para registrar los repuestos que ingresan al almacen del taller, es importante decir que solo se registra los ingresos de parte
  * 	de proveedores pero no de los mismos tecnicos
  * PRUEBA:
  * 	select register_entry_note(cast(now() as date),cast('Some description' as text) ,cast(1 as smallint), 8747155);
  * RETURNS:
  * 	integer : retorna el nro de nota ingreso (income_no)
 **/
create or replace function register_entry_note(entry_date_i date, description_i text, cod_workshop_i smallint, nit_ci bigint)returns integer
 /**
  * FUNCTION register_exit_note
  * PARAMS:
  * note_date_i date: fecha de salida de repuestos
  * description_i text: descripcion de la razon por la que salen los repuestos del almacen
  * id_technical_i integer: id del tecnico que es responsable de la nota de salida
  * cod_workshop_i smallint: cod del taller por donde salen los repuestos
  * OBJETIVE:
  * 	Registra una nota de salida o Egreso especficamente para algun tecnico que lo necesite, lo cual se registrara
  *		en su caja de herramientas para su respectivo control y para el tecnico un repuesto para arreglar un producto.
  *		es importante aclarar que la nota de salida se caracteriza por tener el atributo "NOTE_TYPE=TRUE" OJO
  * PRUEBA:
  * 	select register_exit_note(cast(now() as date), 'Nota de salida para la salida del tecnico chapatin', 1, cast(1 as smallint));
  * RETURNS:
  * 	integer : resultado>=0: retorna el nro de nota de salida <---> resultado=-1: si el tecnico no esta habilitado
 **/
 create or replace function register_exit_note(note_date_i date, description_i text, id_technical_i integer, cod_workshop_i smallint)returns integer
 /**
  * FUNCTION register_devolution_note
  * PARAMS:
  * note_date_i date: fecha del registro de la nota de devolucion
  * description_i text: descripcion de la razon por la devolucion de los repuestos
  * id_technical_i integer: id del tecnico responsable de esta nota
  * cod_workshop_i smallint: codigo del taller al cual entrega los repuestos  
  * OBJETIVE:
  * 	Registra una nota de Devolucion especficamente para algun tecnico que quiera devolver repuestos, lo cual se registrara
  *		en su caja de herramientas para su respectivo control.
  *		es importante aclarar que la nota de devolucion se caracteriza por tener el atributo "NOTE_TYPE=FALSE" OJO!!!!
  * PRUEBA:
  * 	select register_devolution_note(cast(now() as date), 'Nota de Devolucion de parte del tecnico chapatin', 6, cast(1 as smallint));
  * RETURNS:
  * 	integer : resultado>=0: retorna el nro de nota de devolucion <---> resltado=-1: si el tecnico no esta habilitado
 **/
 create or replace function register_devolution_note(note_date_i date, description_i text, id_technical_i integer, cod_workshop_i smallint)returns integer
 /**
  * FUNCTION register_income_detail
  * PARAMS:
  * income_no_i integer: nro de nota de ingreso
  * replacement_i text: nombre del repuesto a ingresar
  * quantity_i smallint: cantidad del repuesto a ingresar
  * OBJETIVE:
  * 	Registra un detalle de la nota de ingreso, cual registra un repuesto en la nota de ingreso
  * PRUEBA:
  * 	select register_income_detail(1, 'Procesador Intel Core i5', cast(1 as smallint));
  * RETURNS:
  * 	integer : resultado>=0: retorna el id del detalle de ingreso <---> resltado=-1: si la cantidad a ingresar es negativa
 **/
create or replace function register_income_detail(income_no_i integer, replacement_i text, quantity_i smallint)returns integer
 /**
  * TRIGGER register_income_replacement
  *	TABLE: income_detail 
  * PARAMS:
  * OBJETIVE:
  * 	Actualiza el stock del almacen de un taller respecto a los repuestos que ingresan, si no esta registrado el repuesto en, este
  *		trigger lo registra.
  * PRUEBA: select register_income_detail(2, 'Repuestos para computadora, procesador', cast(3 as smallint));
  * RETURNS:
 **/  
 create or replace function register_income_replacement()returns trigger
 create trigger income_detail_replacement after insert on income_detail
/**
  * FUNCTION register_exit_note_detail
  * PARAMS:
  * note_no_i integer: nro de nota de salida
  * replacement_i text: nombre del repuesto a salir del taller
  * quantity_i smallint: cantidad derepuestos a sacar del taller
  * OBJETIVE:
  * 	Registra un detalle de la nota de Egreso, cual registra un repuesto en la nota de Egreso, asi actualiza la caja de 
  *		repuestos del tecnico responsable de esta nota de Egreso
  * PRUEBA:
  * 	select register_exit_note_detail(5, 'Repuestos para computadora, procesador', cast(2 as smallint));
  * RETURNS:
  * 	integer : resultado>=0: retorna el id del detalle de Egreso <---> resltado=-1: si la cantidad a ingresar es negativa o no existe la nota de Egreso o no hay cantidad disponible en el almacen.
 **/
 create or replace function register_exit_note_detail(note_no_i integer, replacement_i text, quantity_i smallint)returns integer
 /** chage line: spare_box_quantity
  * FUNCTION register_devolution_note_detail
  * PARAMS:
  * note_no_i integer: nro de nota de devolucion
  * replacement_i text: nombre del repuesto a devolver
  * quantity_i smallint: cantidad a devolver
  * OBJETIVE:
  * 	Registra un detalle de la nota de Devolucion, cual registra un repuesto en la nota de Devolucion, asi actualiza la caja de 
  *		repuestos del tecnico responsable de esta nota de Devolucion
  * PRUEBA:
  * 	select register_devolution_note_detail(6, 'Repuestos para computadora, procesador', cast(1 as smallint));
  * RETURNS:
  * 	integer : resultado>=0: retorna el id del detalle de Devolucion <---> resltado=-1: si la cantidad a ingresar es negativa o no existe la nota de Devolucion o 
  *		el stock de la caja de repuestos no tiene cantidad suficiente para reponer lo que registra.
 **/ 
create or replace function register_devolution_note_detail(note_no_i integer, replacement_i text, quantity_i smallint)returns integer
 /**
  * TRIGGER register_note_devolution_or_exit
  *	TABLE: note_detail
  * PARAMS:
  * OBJETIVE:
  * 	Actualiza el stock del almacen de un taller respecto a los repuestos que salen; tomando en cuenta el tipo de nota, si es Egreso o Devolucion, si
  *		no esta registrado el repuesto en la caja de repuestos, este trigger lo crea en su caja de repuestos.
  *		Actualizar: se refiere a disminuir o aumentar el stock del almacen respecto a su tipo de nota
  * PRUEBA: select register_exit_note_detail(5, 'Repuestos para computadora, procesador', cast(1 as smallint));
  * RETURNS:
 **/
 create or replace function register_note_devolution_or_exit()returns trigger
 create trigger note_exit_or_devolution_detail_replacement after insert on note_detail
/**
  * FUNCTION getCountReplacementsInNote
  * PARAMS:
  * note_no_c integer: Nro. de nota de salida o devolucion
  * OBJETIVE:
  * 	devuelve la cantidad de registros detalles de repuestos realizados en una nota, ya sea de salida o devolucion
  * PRUEBA:
  * 
  * RETURNS:
  * 	integer : cantidad de repuestos registrados en una nota
 **/
 create or replace function getCountReplacementsInNote(note_no_c integer)returns integer
 
 /**
  * FUNCTION getAvailableModifyNote
  * PARAMS:
  * note_no_v integer : nro de nota de salida o devolucion
  * count_days integer : cantidad de dias habiles
  * OBJETIVE:
  * 	devuelve la disponibilidad de update o registrar detalles de una nota. maximo puede update o registrar detalles (count_days) dias
  * PRUEBA:
  * 
  * RETURNS:
  * 	boolean: true: esta disponible. false: No esta disponible
 **/
  create or replace function getAvailableModifyNote(note_no_v integer, count_days integer)returns boolean 
 /**
  * FUNCTION delete_detail_of_note
  * PARAMS:
  * note_no_w integer: nro de nota de salida
  * id_detail integer: id detalle de la nota de salida
  * OBJETIVE:
  * 	devuelve la disponibilidad de update o registrar detalles de una nota. maximo puede update o registrar detalles (count_days) dias
  * PRUEBA:
  * 
  * RETURNS:
  * 	boolean: true: esta disponible. false: No esta disponible
 **/
  create or replace function delete_detail_of_note(note_no_w integer, id_detail integer)returns integer
 /***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/********************************                                                        *******************************************/
/********************************  Fourth Process: Management Technical Support Service  *******************************************/
/********************************                                                        *******************************************/
/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**
  * FUNCTION register_service
  * PARAMS:
  * description text: descripcion del servicio (ej. Servicio de Mantenimiento).
  * OBJETIVE:
  * 	Registra un servicio, cual esta será usado en el servicio de soporte tecnico y factura de servicios.
  * PRUEBA:
  * 	select register_service('servicio de mantenimiento');
  * RETURNS:
  * 	integer : resultado>0: retorna el id del servicio <---> error : Error de desarrollo o error syntax
 **/
 create or replace function register_service(description text)returns integer
 /**
  * FUNCTION register_technical_personal
  * PARAMS:
  * name_i text: nombre completo del tecnico
  * email_i text: email del tecnico
  * phone_i integer: telefono
  * OBJETIVE:
  * 	Registra un tecnico para su inicio en el sistema y el registro de su trabajo en servicio de soporte tecnico
  * PRUEBA:
  * 	select register_technical_personal('Mario Gotzé', 'gotze.fc.munchen@hotmail.com',65987125);
  * RETURNS:
  * 	integer : resultado>0: retorna el id del tecnico <---> error : Error de desarrollo o error syntax
 **/
 create or replace function register_technical_personal(name_i text, email_i text, phone_i integer)returns integer
 
/**
  * FUNCTION enable_disable_technical
  * PARAMS:
  * id_technical_e integer: id del tecnico
  * OBJETIVE:
  * 	hailita o desabilita a un tecnico para su suspencion de sus actividades o alguna razon bloquear sus actividades.
  * PRUEBA:
  * 	select enable_disable_technical(6);
  * RETURNS:
  * 	boolean : true: habilitacion e inhailitacion correcta <---> false : no
 **/
 create or replace function enable_disable_technical(id_technical_e integer)returns boolean
 /**
  * FUNCTION assign_specialty
  * PARAMS:
  * id_tecnhinal_a integer: id del tecnico
  * id_specialty_a smallint: id de la especialidad
  * OBJETIVE:
  * 	asigna las especialidades que tiene el tecnico para registrar su experiencia
  * PRUEBA:
  * 	select assign_specialty(2, cast(1 as smallint));
  * RETURNS:
  * 	boolean : true: registro correcto <---> false : no existe especialidad
 **/
 create or replace function assign_specialty(id_tecnhinal_a integer, id_specialty_a smallint)returns boolean
 /**
  * FUNCTION register_specialty
  * PARAMS:
  * description_specialty_t text: descripcion de la especialidad a registrar
  * OBJETIVE:
  * 	registra una nueva especialidad del personal tecnico
  * PRUEBA:
  * 	select register_specialty('Ingenieria en Software');
  * RETURNS:
  * 	boolean : true: registro correcto <---> false : ya existe la especialidad
 **/
 create or replace function register_specialty(description_specialty_t text) returns boolean
 /**
  * FUNCTION register_support_service
  * PARAMS:
  * service_date_i date: fecha del registro del servicio de soporte tecnico
  * observations_i text: alguna observacion que tenga el tecnico repecto al servicio realizado
  * request_no_i integer: nro de solicitud de servicio (IMPORTANTE)
  * id_technical_i integer: id de tecnico que realizo o es responsable del trabajo
  * OBJETIVE:
  * 	registra los detalles que tiene el servicio de soporte tecnico y despues se debe registrar la lista de servicios y la lista de repuesto que se
  *		necesito para el soporte tecnico.
  * PRUEBA:
  * 	select register_support_service(cast(now() as date), 'Ninguno', 1, 6);
  * RETURNS:
  * 	integer : resultado>=0: nro de servicio de soporte tecnico <---> resultado=-1 : la fecha no coincide con la solicitud o la solicitud no fue aceptada o el tecnico no esta habilitado
 **/
 create or replace function register_support_service(service_date_i date, observations_i text, request_no_i integer, id_technical_i integer)returns integer
 /**
  * FUNCTION register_service_in_support
  * PARAMS:
  * support_no_i integer: nro de servicio de soporte tecnico
  * id_service_i integer: id del servicio a registrar
  * cost_i decimal(12,2): costo economico del servicio a registrar
  * OBJETIVE:
  * 	registra un servicio en el soporte tecnico que se realizo y se actualiza el costo total del soporte tecnico.
  * PRUEBA:
  * 	select register_service_in_support(1,2,150.50);
  * RETURNS:
  * 	boolean : true :servicio registrado correctamente <---> false: el costo es negativo e invalido.
 **/
 create or replace function register_service_in_support(support_no_i integer, id_service_i integer, cost_i decimal(12,2))returns boolean
 /**
  * FUNCTION register_spare_parts
  * PARAMS:
  * support_no_i integer: nro de servicio de soporte tecnico
  * cod_replacement_i integer: codigo del repuesto a registrar
  * quantity_i integer: cantidad
  * cost_i decimal(12,2): costo economico $
  * OBJETIVE:
  * 	registra un repuesto usado en el soporte tecnico que se realizo y se actualiza el costo total del soporte tecnico respecto al precio del repuesto
  * PRUEBA:
  * 	select register_spare_parts(1,1,2,100);
  * RETURNS:
  * 	boolean : true :se registro correctamente el repuesto en el soporte tecnico <---> false: el stock de repuestos no alcanza para el soporte tecnico o no existe
 **/ 
 create or replace function register_spare_parts(support_no_i integer, cod_replacement_i integer, quantity_i integer, cost_i decimal(12,2))returns boolean
 /**
  * FUNCTION register_service_invoice
  * PARAMS:
  * nit_i bigint: nit de la persona responsable a cubrir los costos
  * corporate_i text: razon social
  * service_date_i date: fecha de la emision de factura
  * quantity_day_support_i smallint: cantidad de dias de soporte tecnico
  * support_no_i integer: nro de servicio de soporte tecnico
  * id_user_i smallint: id del usuario responsable
  * OBJETIVE:
  * 	genera una factura con su repectivo servicio de soporte tecnico
  * PRUEBA:
  * 	select register_service_invoice(cast(456321354834864 as bigint), 'Dell Corporation SA',cast(now() as date),cast(1 as smallint), 1, cast(4 as smallint))
  * RETURNS:
  * 	boolean : true :se registro correctamente la factura <---> false: cantidad de dias de soporte no correcto o el usuario no esta habilitado
 **/
 create or replace function register_service_invoice(nit_i bigint, corporate_i text, service_date_i date, quantity_day_support_i smallint, support_no_i integer, id_user_i smallint)returns boolean

 /**
  * FUNCTION getTechnicalSpecialty
  * PARAMS:
  * idtechnical integer: id del tecnico
  * OBJETIVE:
  * 	devolver en una cadena todas las especialidades de un determinado tecnico
  * PRUEBA:
  * 	select getTechnicalSpecialty(11);
  * RETURNS:
  * 	text : una cadena con todas las especialdes separadas por una coma
 **/

 create or replace function getTechnicalSpecialty(idtechnical integer)returns text

 /***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**********************************                                           ******************************************************/
/**********************************  Fourth Process: Management System User   ******************************************************/
/**********************************                                           ******************************************************/
/***********************************************************************************************************************************/
/***********************************************************************************************************************************/
/**
  * FUNCTION register_privilege
  * PARAMS:
  * description text: Una breve descripcion del privilegio
  * id_super_role_i: id del super rol o si no tiene (-1)
  * OBJETIVE:
  * 	registrar un  nuevo privilegio; pero si tiene como valor id_super_role_i=-1 se tome como
  *     null 
  * PRUEBA:
  * 	select register_privilege('Gestion de Facturas', 12);
  * RETURNS:
  * 	boolean : true :se registro correctamente el nuevo privilegio <---> false: privilegio ya existe
 **/
 create or replace function register_privilege(description text, id_super_role_i integer) returns boolean
 /**
  * FUNCTION register_role
  * PARAMS:
  * nameR text: nombre del rol
  * descriptionR text: una breve descripcion del rol
  * OBJETIVE:
  * 	registrar un  nuevo rol
  * PRUEBA:
  * 	select register_role('Gerente', 'Administrar Todo el sistema');
  * RETURNS:
  * 	integer : retorna el id_role del rol <---> -1: ya existe rol
 **/
 create or replace function register_role(nameR text, descriptionR text) returns integer
 /**
  * FUNCTION enable_disable_role
  * PARAMS:
  * id_role_i integer: id del rol
  * OBJETIVE:
  * 	habilitar o deshabilitar un rol
  * PRUEBA:
  * 	select enable_disable_role(3);
  * RETURNS:
  * 	boolean : true :se habilito o inhabilito correctamente el nuevo rol <---> false: no existe rol
 **/
create or replace function enable_disable_role(id_role_i integer) returns boolean
 /**
  * FUNCTION register_rol_privilege
  * PARAMS:
  * id_role_i integer: id del rol
  * id_privilege_i integer: id del privilegio
  * OBJETIVE:
  * 	asignar un privilegio a un rol.
  * PRUEBA:
  * 	select register_rol_privilege(2,3);
  * RETURNS:
  * 	boolean : true :se registro correctamente el nuevo privilegio al rol indicado<---> false: no existe rol y/o privilegio, o ya esta asiganado el privilegio al rol.
 **/
 create or replace function register_rol_privilege( id_role_i integer, id_privilege_i integer) returns boolean
 /**
  * FUNCTION register_user
  * PARAMS:
  * username_i text: nombre de usuario
  * email_i text: email del usuario
  * profile_i text: foto de perfil del usuario
  * id_role_i integer: rol que ocupa en el sistema (tecnico, administrador,gerente,etc)
  * OBJETIVE:
  * 	registrar un nuevo usuario
  * PRUEBA:
  * 	select register_user('Juanita Rios', 'juanita74@gmail.com','myprofile.jpg',3);
  * RETURNS:
  * 	integer : si el email no esta registrado devuelve el id del usuario<---> caso contario devuelve -1 si el email ya esta registrado
 **/
 create or replace function register_user(username_i text, email_i text, profile_i text, id_role_i integer) returns integer 
 /**
  * FUNCTION enable_disable_user
  * PARAMS:
  * id_user_i integer: id del usuario
  * OBJETIVE:
  * 	habilitar o inhabilitar a un usuario
  * PRUEBA:
  * 	select enable_disable_user(21);
  * RETURNS:
  * 	boolean : true :se habilito o deshabilito al usuario correctamente<---> false: no existe usuario
 **/
 create or replace function enable_disable_user(id_user_i integer) returns boolean
/**
  * FUNCTION update_user
  * PARAMS:
  * id_user_i smallint: id de usuario
  * username_i text: nombre de usuario
  * email_i text: email del usuario
  * profile_i text: foto de perfil del usuario
  * id_role_i integer: rol que ocupa en el sistema (tecnico, administrador,gerente,etc)
  * OBJETIVE:
  * 	Modificar a un usuario
  * PRUEBA:
  * 	select update_user(cast(2 as smallint),cast('stephani heredia' as varchar(50)),cast('stephani.hc.97@gmail' as text),cast('' as text),cast(1 as smallint));
  * RETURNS:
  * 	true : si el email no esta registrado y ha actualizado correctamente<---> caso contario devuelve false si el email ya esta registrado o hubo fallas al actualizar
 **/
 create or replace function update_user(id_user_i smallint, username_i varchar(50), email_i text, user_profile_i text, id_role_i smallint)returns boolean
 /**
  * FUNCTION delete_role_privilege
  * PARAMS:
  * id_privilege_r integer: id del privielgio
  * id_role_r integer: id rol
  * OBJETIVE:
  * 	elimina un privilegio asignado a un rol; pero con la excepcion
  *   de que por lo menos tenga 1 privilegio asignado
  * PRUEBA:
  * 	
  * RETURNS:
  * 	boolean : true :se elimino el privilegio asignado correctamente<---> false: tan solo tiene 1 privilegio, no se puede eliminar
 **/
 create or replace function delete_role_privilege(id_privilege_r integer, id_role_r integer)returns boolean
 /**
  * FUNCTION verifyPermitUser
  * PARAMS:
  * id_user_v integer: id usuario
  * id_privilege_v integer: id del privilegio
  * OBJETIVE:
  * 	Verifica si el usuario tiene el privilegio correspondiente
  * PRUEBA:
  * 	select verifyPermitUser(1,6);
  * RETURNS:
  * 	boolean : true : si tiene los privilegios <---> false: no tiene el privilegio
 **/
create or replace function verifyPermitUser(id_user_v integer, id_privilege_v integer)returns boolean
 /**
  * FUNCTION verifyRelationExists
  * PARAMS:
  * id_role_v integer: id rol del usuario
  * id_privilege_v integer: id del privilegio a evaluar
  * OBJETIVE:
  * 	Verifica si el rol tiene el privilegio correspondiente
  * PRUEBA:
  * 	select verifyRelationExists(2,2)
  * RETURNS:
  * 	boolean : true : si tiene los privilegios <---> false: no tiene el privilegio
 **/
create or replace function verifyRelationExists(id_role_v integer, id_privilege_v integer)returns boolean
/**
  * FUNCTION get_count_user_in_role
  * PARAMS:
  * id_role_v smallint: id rol a buscar
  * OBJETIVE:
  * 	busca la cantidad de usuario que tienen el rol a buscar
  * PRUEBA:
  * 	select get_count_user_in_role(1)
  * RETURNS:
  * 	integer : devuelve la cantidad de usuarios que tienen el rol asignado
 **/
create or replace function get_count_user_in_role(id_role_v smallint)returns int as 
/**
	obtiene los detalles de un a empresa con la cnatidad de contratos registrado y su fecha limite del ultimo 
	contrato y la cantidad de polizas de garantia registrada al sistema.
*/
select cc.nit, cc.corporate, cc.name_company, 
	   cc.email, cc."enable", cc.name_representative, 
	   cc.phone, max(cn.end_date) as last_date, 
	   count(*) as contract_count, count(gp.nit_client_company) as guarantee_policy_counts
from client_company as cc, contract_company as cn, guarantee_policy as gp
where cc.nit=cn.nit_company and cc.nit=gp.nit_client_company
group by cc.nit

-- obtiene los detalles de un a empresa con la cnatidad de contratos registrado y su fecha limite del ultimo contrato y la cantidad de polizas de garantia registrada al sistema.
select cc.nit, cc.corporate, cc.name_company,
	   cc.email, cc."enable", cc.name_representative,
	   cc.phone, max(cn.end_date) as last_date,
	   count(*) as contract_count, count(gp.nit_client_company) as guarantee_policy_counts
from client_company cc, contract_company cn, guarantee_policy gp
where cc.nit=cn.nit_company and cc.nit=gp.nit_client_company
group by cc.nit;

-- la cantidad de servicios brindados por empresa
select client_company.nit,client_company.name_company, client_company.corporate,
count (service_invoice.corporate), sum(total_cost)
from client_company, service_invoice
where client_company.nit = service_invoice.nit
group by client_company.nit;