/* ******** Create user or Role * **** */
/*
CREATE ROLE expresssi2
	WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	REPLICATION
	CONNECTION LIMIT -1
	PASSWORD '123456';
*/
/* ********* Create Database ************* */
/*
CREATE DATABASE expresssi2
	with owner=expresssi2
	encoding='UTF8'
	tablespace=pg_default
	CONNECTION LIMIT=-1;
*/

create table type_support_service(
	id_type smallserial primary key,
	description_type varchar(50) not null,
	enable boolean not null --enable=true -> type_support_service is enable, if enable=false type_support_service is "deleted"
);

create table product_category(
	id_category smallserial primary key,
	description_category text not null
);

create table representative_company(
	ci integer primary key,
	issued_place varchar(2) not null,
	name_representative text not null,
	address text not null,
	email text not null,
	phone integer not null
);

create table client_company(
	nit bigint not null primary key,
	name_company text not null,
	corporate text not null,
	email text not null,
	phone integer not null,
	enable boolean not null, --enable=true -> client_company is enable, if enable=false client_company is "deleted"
	ci_representative integer not null references representative_company
		on update cascade
		on delete cascade
);

create table contract_company(
	cod_contract integer not null,	--el contrato tiene un nro. cual esta registrado en el fisico. asignado por una notaría y de ello se podra buscar en su respectiva oficina fisica
	description_contract text not null,
	start_date date not null,
	end_date date not null,
	nit_company bigint not null,
	status_contract boolean not null,
	file_contract text not null,
	primary key(cod_contract, nit_company),
	foreign key (nit_company) references client_company(nit)
	on update cascade
	on delete cascade
);

create table "owner"(
	ci integer not null primary key,
	issued_place varchar(2) not null,
	name text not null,
	phone integer not null,
	email text not null,
	enable boolean not null --enable=true -> owner is enable, if enable=false owner is "deleted"
);

create table product(
	cod_product serial,
	description_product text not null,
	model varchar(30) not null,
	brand varchar(30) not null,
	enable boolean not null, --enable=true -> product is enable, if enable=false product is "deleted"
	id_category integer not null references product_category
	on update cascade
	on delete cascade,
	product_image text not null,
	primary key(cod_product, id_category)
);

create table product_owner(
	code serial not null,
	ci integer not null references "owner"
	on update cascade
	on delete cascade,
	cod_product integer not null,
	id_category smallint not null,
	primary key(code, ci, cod_product, id_category),
	foreign key (cod_product, id_category) references product(cod_product , id_category)
	on update cascade
	on delete cascade
);

create table guarantee_policy(
	cod_policy text not null primary key,
	date_purchase date not null,
	deadline date not null,
	proof text not null,
	state_guarantee smallint not null,
	nro_invoice varchar not null,  --nro de factura del producto
	code_owner integer not null,
	ci_owner integer not null,
	cod_product integer not null,
	id_category smallint not null,
	nit_client_company bigint not null,
	foreign key (code_owner, ci_owner, cod_product, id_category) references product_owner(code, ci, cod_product, id_category)
	on update cascade
	on delete cascade,
	foreign key (nit_client_company) references client_company(nit)
	on update cascade
	on delete cascade
);

create table request_support_service(
	request_no serial not null primary key,
	description text not null,
	cod_policy text null, --seleccionar la poliza
	date_request date not null,
	state_request smallint not null,	--0:denegado; 1:aceptado; 2:enEspera
	id_type smallint not null references type_support_service
	on update cascade
	on delete cascade,
	code_owner integer not null,
	ci_owner integer not null,
	cod_product integer not null,
	id_category smallint not null,
	foreign key (code_owner, ci_owner, cod_product, id_category) references product_owner(code, ci, cod_product, id_category)
	on update cascade
	on delete cascade
);
create table privilege (
	id_privilege smallserial primary key,
	description_privilege text not null,
	id_super_privilege smallint null
);

alter table privilege add
	foreign key (id_super_privilege) references privilege(id_privilege)
	on update no action
	on delete no action;

create table "role" (
	id_role smallserial primary key,
	role_name varchar(30) not null,
	description text not null,
	enable boolean not null --enable=true -> role is enable, if enable=false role is "deleted"
);

create table role_privilege (
	id_role smallint not null references role,
	id_privilege smallint not null references privilege,
	primary key (id_role, id_privilege)
);

create table "user" (
	id_user smallserial primary key,
	username varchar(50) not null,
	email text not null,
	user_profile text not null,
	enable boolean not null, --enable=true -> user is enable, if enable=false user is "deleted"
	id_role smallint not null references role
);

create table specialty (
	id_specialty smallserial primary key,
	description_specialty text not null
);

create table technical (
	id_technical serial primary key,
	name text not null,
	email text not null,
	phone integer not null,
	enable boolean not null --enable=true -> technical is enable, if enable=false technical is "deleted"
);

create table technical_specialty (
	id_technical integer not null references technical
	on update cascade
	on delete cascade,
	id_specialty smallint not null references specialty
	on update cascade
	on delete cascade
);

create table workshop (
	cod_workshop smallserial primary key,
	name varchar(20) not null,
	latitude varchar(15) not null,
	longitude varchar(15) not null,
	address text not null
);

create table provider(
	nit_ci bigint primary key,
	provider_name text not null,
	address text not null,
	email text not null,
	phone integer not null,
	enable boolean not null
);

create table entry_note (
	income_no serial primary key,
	entry_date date not null,
	description text not null,
	cod_workshop smallint references workshop
	on update cascade
	on delete cascade,
	nit_ci_provider bigint not null references provider
	on update cascade
	on delete cascade
);

create table income_detail (
	income_no integer not null references entry_note
	on update cascade
	on delete cascade,
	id_income_detail serial not null,
	replacement text not null,
	quantity smallint not null,
	primary key (income_no, id_income_detail)
);

create table note (
	note_no serial primary key,
	note_date date not null,
	description text not null,
	id_technical integer not null,
	note_type boolean not null,
	cod_workshop smallint not null references workshop
	on update cascade
	on delete cascade
);

create table note_detail (
	note_no integer not null references note
	on update cascade
	on delete cascade,
	id_note_detail serial,
	replacement text not null,
	quantity smallint not null,
	primary key (note_no, id_note_detail)
);

create table replacement_category(
	id_category smallserial primary key,
	description_category text not null
);

create table replacement (
	cod_replacement serial,
	description_replacement text not null,
	enable boolean not null, --enable=true -> replacement is enable, if enable=false replacement is "deleted"
	id_category integer not null references replacement_category
	on update cascade
	on delete cascade,
	primary key(cod_replacement, id_category)
);

create table warehouse_inventory (
	cod_workshop smallint not null references workshop
	on update cascade
	on delete cascade,
	cod_replacement integer not null,
	id_category integer not null,
	foreign key (cod_replacement, id_category) references replacement(cod_replacement , id_category)
	on update cascade
	on delete cascade,
	quantity integer not null,
	primary key (cod_workshop, cod_replacement, id_category)
);

--caja de repuestos
create table spare_box (
	id_technical integer not null references technical
	on update cascade
	on delete cascade,
	quantity integer not null,
	cod_replacement integer not null,
	id_category integer not null,
	foreign key (cod_replacement, id_category) references replacement(cod_replacement , id_category)
	on update cascade
	on delete cascade,
	primary key (id_technical, cod_replacement, id_category)
);

create table technical_support_service (
	support_no serial primary key,
	total_cost decimal(12,2) not null,
	service_date date not null,
	observations text not null,
	state_support_service boolean not null,
	request_no integer not null references request_support_service
	on update cascade
	on delete cascade,
	id_technical integer not null references technical
	on update cascade
	on delete cascade
);

create table list_spare_parts (
	support_no integer not null references technical_support_service
	on update cascade
	on delete cascade,
	cod_replacement integer not null,
	id_category integer not null,
	foreign key (cod_replacement, id_category) references replacement(cod_replacement , id_category)
	on update cascade
	on delete cascade,
	quantity integer not null,
	cost decimal(12,2) not null,
	primary key (support_no, cod_replacement, id_category)
);

create table service (
	id_service serial primary key,
	description_service text not null,
	service_guarantee boolean not null -- si la poliza cubre el servicio
);

create table list_service (
	support_no integer not null references technical_support_service
	on update cascade
	on delete cascade,
	id_service integer not null references service
	on update cascade
	on delete cascade,
	cost decimal(12,2) not null,
	primary key (support_no, id_service)
);

create table service_invoice (
	bill_no serial primary key,
	nit bigint not null,
	corporate text not null,
	total_cost decimal(12,2) not null,
	service_date date not null,
	quantity_day_support smallint not null,
	support_no integer not null references technical_support_service
	on update cascade
	on delete cascade,
	id_user smallint not null references "user"
	on update cascade
	on delete cascade,
	enable boolean not null
);

insert into technical (name, email, phone, enable) values('Juan Perez', 'juan123_perez@gmail.com', 78495123, true);
insert into technical (name, email, phone, enable) values('Rodrigo Lopez', 'rodri123lopez@gmail.com', 77514982, true);
insert into technical (name, email, phone, enable) values('Jose Rodriguez', 'joserodriguez45@hotmail.com', 60874129, true);
insert into technical (name, email, phone, enable) values('Mario Flores', 'mario_flores75@gmail.com', 63749518, true);
insert into technical (name, email, phone, enable) values('Felipe Guzman', 'felipe2_guzman@gmail.com', 72395481, true);

insert into specialty (description_specialty) values('Tecnico en Computacion');
insert into specialty (description_specialty) values('Tecnico Electronico');
insert into specialty (description_specialty) values('Tecnico Mecanico');

insert into technical_specialty (id_technical,id_specialty) values(1,1);
insert into technical_specialty (id_technical,id_specialty) values(1,2);
insert into technical_specialty (id_technical,id_specialty) values(2,3);
insert into technical_specialty (id_technical,id_specialty) values(4,3);
insert into technical_specialty (id_technical,id_specialty) values(5,2);
insert into technical_specialty (id_technical,id_specialty) values(3,3);

insert into replacement_category (description_category) values('Electricos');
insert into replacement_category (description_category) values('Artefacto');
insert into replacement_category (description_category) values('Computacional');

insert into replacement (description_replacement, enable, id_category) values('Repuestos para neveras, motor',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para neveras, condensador',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para aire acondicionado, filtro secador',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para aire acondicionado, compresor',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para microondas, filtro',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para batidora, resorte',true,2);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para computadora, fuente de poder',true,3);
insert into replacement (description_replacement, enable, id_category) values('Repuestos para computadora, procesador',true,3);

insert into spare_box (id_technical, quantity, cod_replacement, id_category) values(1, 1, 7, 3);
insert into spare_box (id_technical, quantity ,cod_replacement, id_category) values(1, 3, 8, 3);
insert into spare_box (id_technical, quantity, cod_replacement, id_category) values(2, 2, 1, 2);
insert into spare_box (id_technical, quantity ,cod_replacement, id_category) values(2, 5, 3, 2);
insert into spare_box (id_technical, quantity, cod_replacement, id_category) values(3, 1, 2, 2);
insert into spare_box (id_technical, quantity ,cod_replacement, id_category) values(3, 2, 4, 2);
insert into spare_box (id_technical, quantity, cod_replacement, id_category) values(4, 6, 6, 2);
insert into spare_box (id_technical, quantity ,cod_replacement, id_category) values(4, 4, 5, 2);
insert into spare_box (id_technical, quantity, cod_replacement, id_category) values(5, 3, 7, 3);

insert into workshop ("name", address, latitude, longitude) values('Repair Shop','Av. Alemana entre 3er y 4to anillo N#338','-17.756104', '-63.166007');
insert into workshop ("name", address, latitude, longitude) values('Arregla Todo','Av. Landivar N#758','-17.784523', '-63.192186');

insert into note(note_date ,description ,id_technical ,note_type ,cod_workshop ) values('2020/01/03','Entrega de repuestos para realizar mantenimiento',1,true,1);
insert into note(note_date ,description ,id_technical ,note_type ,cod_workshop ) values('2020/02/02','Entrega de repuestos para realizar soporte',2,true,1);
insert into note(note_date ,description ,id_technical ,note_type ,cod_workshop ) values('2020/03/01','Devolucion de repuestos llevados para mantenimiento',3,false,1);
insert into note(note_date ,description ,id_technical ,note_type ,cod_workshop ) values('2020/03/15','Devolucion de repuestos que fueron llevados para realizar soporte',2,false,1);

insert into note_detail(note_no ,replacement ,quantity ) values(1,'Camara para portatil hp',1);
insert into note_detail(note_no ,replacement ,quantity ) values(1,'Camara para portatil dell',2);
insert into note_detail(note_no ,replacement ,quantity ) values(2,'Camara Huawei',1);
insert into note_detail(note_no ,replacement ,quantity ) values(2,'Pantalla Xiaomi',1);
insert into note_detail(note_no ,replacement ,quantity ) values(3,'Camara para portatil hp',1);
insert into note_detail(note_no ,replacement ,quantity ) values(3,'Camara para portatil dell',1);
insert into note_detail(note_no ,replacement ,quantity ) values(4,'Camara Huawei',1);
insert into note_detail(note_no ,replacement ,quantity ) values(4,'Pantalla Xiaomi',1);

insert into provider(nit_ci, provider_name, address, email, phone, enable) values(2574102015, 'Replacement SA', 'Av. Beni c/los Lirios', 'replacemente_123@gmail.com', 3347817, true);
insert into provider(nit_ci, provider_name, address, email, phone, enable) values(8747155, 'Juan Mendoza', 'Av. San Martin c-2', 'juanM_74@gmail.com', 77478714, true);
insert into provider(nit_ci, provider_name, address, email, phone, enable) values(8417987017, 'Repair Key SA', 'Av. Alemana c/las rosas', 'repair_key@gmail.com', 3311479, true);
insert into provider(nit_ci, provider_name, address, email, phone, enable) values(3547114014, 'Mechanical Wheel', 'Av. 2 de Agosto C/4 N:345', 'mechanical_wheel@gmail.com', 3355671, true);
insert into provider(nit_ci, provider_name, address, email, phone, enable) values(8174796, 'Rosa Salvatierra', 'Av. Banzer 4to anillo C/los lirios', 'rosaSalvatierra@gmail.com', 67477814, true);

insert into entry_note(entry_date ,description ,cod_workshop, nit_ci_provider) values('2019/10/23','Entrada de articulos de repuestos para moviles huawei',1, 2574102015);
insert into entry_note(entry_date ,description ,cod_workshop, nit_ci_provider) values('2019/10/23','Entrada de articulos de repuestos para portatiles dell',1, 8417987017);

insert into income_detail(income_no ,replacement ,quantity ) values(1,'Camara Huawei',10);
insert into income_detail(income_no ,replacement ,quantity ) values(1,'Pantalla Huawei',20);
insert into income_detail(income_no ,replacement ,quantity ) values(2,'Camara Dell',5);
insert into income_detail(income_no ,replacement ,quantity ) values(2,'Disco Duro 1 Gb SATA',15);

insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,1,2,2);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,2,2,1);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,3,2,5);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,4,2,2);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,5,2,4);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,6,2,6);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,7,3,3);
insert into warehouse_inventory (cod_workshop, cod_replacement, id_category, quantity) values(1,8,3,3);

insert into type_support_service (description_type, enable) values('Domicilio',true);
insert into type_support_service (description_type, enable) values('Taller',true);
insert into type_support_service (description_type, enable) values('Remoto',true);

insert into product_category (description_category) values('Computacion');
insert into product_category (description_category) values('Electrodomestico');
insert into product_category (description_category) values('Automovil');
insert into product_category (description_category) values('Aparato electrico');

insert into product (description_product, model, brand, enable, id_category,product_image ) values('Heladera, temperatura automatica, espacio adaptable con dos puertas, capacidad neta 462 Lts.', 'Top Mount', 'Whirlpool', true, 2, 'test.png');
insert into product (description_product, model, brand, enable, id_category, product_image) values('Aire Acondicionado, Capacidad: 12000 BTU, voltaje: 110, tipo de Refrigerante:Inverter gas 410A', 'Silver', 'Mabe', true, 2, 'test.png');
insert into product (description_product, model, brand, enable, id_category, product_image) values('Portatil, pantalla de 15 pulgadas con resoluci\F3n Full HD, procesador Intel Core i7, almacenamiento de 1TB,tres puertos USB, 16GB de RAM', 'Inspiration157000', 'Dell', true, 1 , 'test.png');
insert into product (description_product, model, brand, enable, id_category, product_image) values('Batidora, Bol extra grande de acero pulido con 5,5 litros de capacidad', 'MUM9Y43S00', 'Bosh', true, 2, 'test.png');

insert into product (description_product, model, brand, enable, id_category, product_image)
values
('Laptop de 14", Core i5, disco duro 1 TB/ RAM 8GB', '81MU000HLM', 'Leno', true, 1, 'test.png'),
('Laptop de 15.6", Core i3, disco duro 1 TB/ RAM 8GB', '81HN00TALM', 'Leno', true, 1, 'test.png'),
('Laptop de 14", AMD A6-9225,8GB Memoria RAM, 1TB disco Duro', '3PX45LA#ABM', 'HP', true, 1, 'test.png'),
('Laptop de 14", Intel Core i5 8265u', '55D7P', 'Dell', true, 1, 'test.png'),
('Laptop de 15.6" AMD R3', '3PX55LA#ABM', 'HP', true, 1, 'test.png'),
('Laptop de 14" AMD A4-9125', '6NY94LT#ABM', 'HP', true, 1, 'test.png'),
('Laptop de 14" 240 g7 celeron', '6FU25LT#ABM', 'HP', true, 1, 'test.png'),
('Laptop de 14" HD, Intel Celeron N4000 1.10GHz, 4GB, 500GB', 'A407MA-BV044T', 'ASUS', true, 1, 'test.png'),
('Laptop de Gamer 15.6" Full HD, Intel Core i5-9300H 2.40GHz, 8GB, 1TB + 128GB SSD', 'GF63', 'MSI', true, 1, 'test.png'),
('Laptop de Gamer 17.3" Full HD, Intel Core i5-9300H 2.40GHz, 8GB, 1TB + 128GB SSD', 'GF75', 'MSI', true, 1, 'test.png'),
('Laptop Gamer 15.6" Full HD, Intel Core i7-9750H 2.60GHz, 8GB, 1TB + 128GB SSD', 'GF63', 'MSI', true, 1, 'test.png'),
('Laptop Gamer 15.6" Full HD, Intel Core i7-8750H 2.20GHz, 16GB (2 x 8GB), 512GB SSD', 'GS65', 'MSI', true, 1, 'test.png'),
('Laptop Gamer de 17.3" Full HD, Intel Core i7-8750H 2.20GHz, 16GB, 512GB SSD', 'MSI GS750', 'MSI', true, 1, 'test.png'),
('Laptop 14" HD, AMD Ryzen 5 Pro 3500U 2.10GHz, 8GB, 512GB SSD', 'R89M2', 'LENOVO', true, 1, 'test.png'),
('Laptop 14" Full HD, Intel Core i7-8665U 1.90GHz, 16GB, 512GB SSD', '90NB0HR1-M00540 ', 'LENOVO', true, 1, 'test.png'),
('Laptop ThinkBook 14-IML 14" Full HD, Intel Core i5-10210U 1.60GHz, 8GB, 256GB SSD', '20NKS0TG00', 'LENOVO', true, 1, 'test.png'),
('Laptop 15.6" Full HD, AMD Ryzen 3 3200U 2.60GHz, 4GB, 1TB', '81LW006WLM', 'LENOVO', true, 1, 'test.png'),
('Impresora multifunción L3150, conectividad: wifi', 'C11CG86303', 'Epson', true, 1, 'test.png'),
('Impresora multifunción L5190, conectividad: wifi', 'C11CG85303', 'Epson', true, 1, 'test.png'),
('Impresora de tinta continua', 'G4100', 'Canon', true, 1, 'test.png'),
('Impresora de tinta continua', 'G3100', 'Canon', true, 1, 'test.png'),
('Impresora de tinta continua', 'G2100', 'Canon', true, 1, 'test.png'),
('Impresora de tinta continua', 'G1100', 'Canon', true, 1, 'test.png'),
('Impresora de tinta continua', 'G3100', 'Canon', true, 1, 'test.png');

insert into product (description_product, model, brand, enable, id_category, product_image)
values
('Licuadora Sport Blender 400w, ideal para  batidos y smoothies con capacidad: 600cc, 300cc', 'BSB076', 'Blanik', true, 2, 'test.png'),
('Licuadora Black & Decker 350w de potencia, con 10 velocidades y un control de pulso, capacidad de 1,5 Litros', 'BLBD210PW-CL', 'Blanik', true, 2, 'test.png'),
('Licuadora inox con 500w de potencia, para alimentos semi duros. Jarra de vidrio, cuchillos de acero removible, capacidad de 1,5 Litros', 'TH-501V', 'Thomas', true, 2, 'test.png'),
('Licuadora 400w de potencia, con dos velicidades + pulsar, capacidad de 1,5 Litros', 'TH-320V', 'Thomas', true, 2, 'test.png'),
('Licuadora Powerblender 1200w de potencia para alimentos duros, capacidad 1,5 litros', 'BL2200', 'Somela', true, 2, 'test.png'),
('Licuadora Berryblender 400w de potencia, con 5 velocidades y un control de pulso', 'BL1600', 'Somela', true, 2, 'test.png'),
('Batidora heavy duty roja, 500w con velocidad de 40-200 rpm con 10 velocidades', '', 'Kitchenaid', true, 2, 'test.png'),
('Batidora planetaria MUM4,500 w de potencia,Bowl acero 3,9Lts, 4 velocidades', 'MUM4407', 'Bosch', true, 2, 'test.png'),
('Batidora artisan misty blue, 300w,Bowl de acero inox 4.8Lts, 10 velocidades, 2 tipo de varillas,1 gancho,1 vertedor', '5KSM180RPEMB', 'Kitchenaid', true, 2, 'test.png'),
('Cafetera 2Lts expreso-capuchino,1000w', 'CE7240', 'Ufesa', true, 2, 'test.png'),
('Cafetera inox, capacidad 12 tazas, filtro, apagado automático ,funcion anti goteo', 'TH-138', 'Thomas', true, 2, 'test.png'),
('Refrigerador frio seco de 252 Lts,2 puertas Inox', 'HD-333FWEN-WD', 'Midea', true, 2, 'test.png'),
('Refrigerador frio seco de 390 Lts,2 puertas plata', 'RGE-X41DF', 'Daewooo', true, 2, 'test.png'),
('Refrigerador frio seco de 400 Lts, 2 puertas inox', 'WRM54AKDWC', 'Whirlpool', true, 2, 'test.png'),
('Refrigerador semi seco 465Lts, dos puertas color blanco', 'RDSE465K20W', 'Beko', true, 2, 'test.png'),
('Refrigerador frio seco 615Lts side by side inox', 'GN162430X', 'Beko', true, 2, 'test.png'),
('Refrigerador semi seco 460Lts, 2 puertas inox', 'CRD49AKDWX', 'Consul', true, 2, 'test.png'),
('Freezer horizontal de 198 Lts blanco', 'HS-258C(N)', 'Midea', true, 2, 'test.png'),
('Freezer horizontal 1 puerta de 220 Lts blanco', 'CHA22KBDWX', 'Consul', true, 2, 'test.png'),
('Freezer de 568 litros- 2 puertas', 'KDR-738C', 'Kernig', true, 2, 'test.png'),
('Freezer horizontal 1 puerta de 100 Lts blanco', 'LFH-100', 'Libero', true, 2, 'test.png'),
('Freezer horizontal 1 puerta de 311 Lts blanco', 'CHA31KBDWX', 'Consul', true, 2, 'test.png'),
('Freezer vertical 280 Lts blanco', 'CVG28HBDWX', 'Consul', true, 2, 'test.png'),
('Exhibidor vertical 540Lts 1 puerta,Termostato digital', 'VC-540L', 'Ventus', true, 2, 'test.png'),
('Exhibidor vertical 370Lts 1 puerta,Termostato digital', 'VC-370L', 'Ventus', true, 2, 'test.png'),
('Lavadora carga superior de 18Kg negro', 'WWG18BN9LA', 'Whirlpool', true, 2, 'test.png'),
('Lavadora carga superior de 16Kg silver', 'WWG16BS9LA', 'Whirlpool', true, 2, 'test.png'),
('Lavadora carga frontal de 8Kg blanca', 'NEXT 8.12 ECO', 'Drean', true, 2, 'test.png'),
('Lavadora carga frontal de 9Kg blanca', 'WLF902B25F', 'Whirlpool', true, 2, 'test.png'),
('Lavadora carga frontal de 9kg blanca', 'WAT28491ES', 'Bosch', true, 2, 'test.png'),
('Lavadora carga superior de 8Kg blanca', 'KDTL080DW', 'Kernig', true, 2, 'test.png'),
('Lavadora carga superior de 16Kg blanco', 'WWG16BB9LA', 'Whirlpool', true, 2, 'test.png'),
('Aire acondicionado split 18000 btu', 'WASC18B5MBETW', 'White Westinghouse', true, 2, 'test.png'),
('Aire acondicionado split 12000 btu', 'WASC12B5MBETW', 'White Westinghouse', true, 2, 'test.png'),
('Aire acondicionado smart 24000 btu', 'HYDSAF-H24A4/FW-W', 'Hyundai', true, 2, 'test.png'),
('Aire acondicionado smart 18000 btu', 'HYDSAF-H18A4/FW-W', 'Hyundai', true, 2, 'test.png'),
('Aire acondicionado smart 9000 btu', 'HYDSAF-H09A4/FW-W', 'Hyundai', true, 2, 'test.png'),
('Aire acondicionado portatil 12000 btu', 'AA410-PORTA-12K-S', 'Ika', true, 2, 'test.png');

insert into product (description_product, model, brand, enable, id_category, product_image)
values
('4 motorizaciones: diésel (90cv 115cv), gasolina (120T), híbrida (136cv)', 'Auris', 'Toyota', true, 3, 'test.png'),
('7 plazas, motores gasolina TSI como diésel TDI', 'SEAT', 'Toyota', true, 3, 'test.png'),
('Sports 2016, modelo familiar', 'Avensis Touring', 'Toyota', true, 3, 'test.png'),
('Deportivo con motor 2.0 200cv bóxer y tracción trasera.', 'GT86', 'Toyota', true, 3, 'test.png'),
('Cabina simple, extra y doble. Motorizaciones de 144cv y 171cv', 'Hilux', 'Toyota', true, 3, 'test.png'),
('Tecnología en seguridad y prestaciones 4x4', 'LAND CRUISER ', 'Toyota', true, 3, 'test.png'),
('Tecnología híbrida dispone de un motor de 122cv', 'Prius', 'Toyota', true, 3, 'test.png'),
('7 plazas y un espacio interior muy bien aprovechado. Motores: diesel 115cv y gasolina 136cv y 147cv', 'VERSO', 'Toyota', true, 3, 'test.png'),
('DIG-T 85 kW (115 CV) ACENTA', 'Qashqai', 'Nissan', true, 3, 'test.png'),
('Pick Up 4X4 Doble Cabina Comfort', 'NP300', 'Nissan', true, 3, 'test.png'),
('Motor Ti-VCT de 3.3L', 'F-150 XL', 'Ford', true, 3, 'test.png'),
('Motor EcoBoost 2.5L', 'F-150 LARIAT', 'Ford', true, 3, 'test.png'),
('Motor EcoBoost 3.5L, 24 valvulas', 'F-150 Raptor', 'Ford', true, 3, 'test.png'),
('MOTOR 1.6 L, trasmision XTRONIC® CVT', 'KICKS', 'Nissan', true, 3, 'test.png'),
('Transmision CVT XTRONIC,6 velocidades ', 'JUKE', 'Nissan', true, 3, 'test.png'),
('6 cilindros en línea DOHC, 24 válvulas, 248 HP', 'PATROL 24V-4.8', 'Nissan', true, 3, 'test.png'),
('Motor V6 de 3.5 litros y 260 HP,Traccion ALL-MODE 4x4-I', 'PATHFINDER', 'Nissan', true, 3, 'test.png');

insert into product (description_product, model, brand, enable, id_category, product_image)
values
('Televisor 65" smart tv serie KB585 UHD', 'LT-65KB595', 'JVC', true, 4, 'test.png'),
('Televisor 58" smart tv serie KB585 UHD', 'LT-58KB585', 'JVC', true, 4, 'test.png'),
('Televisor 43" smart tv serie KB97 full HD', 'LT-43KB397', 'JVC', true, 4, 'test.png'),
('Televisor 65" smart tv serie X855 UHD 4k', 'XBR-65X855G-LA8', 'Sony', true, 4, 'test.png'),
('Televisor 75" smart tv serie X805 UHD 4k', 'XBR-75X805G-LA8', 'Sony', true, 4, 'test.png'),
('Televisor 85" smart tv serie X855 UHD 4k', 'XBR-85X855G-LA8', 'Sony', true, 4, 'test.png'),
('Televisor 65" serie X805 UHD android tv', 'XBR-65X805G', 'Sony', true, 4, 'test.png'),
('Televisor 55" serie X805 UHD android tv', 'XBR-55X805G', 'Sony', true, 4, 'test.png'),
('Televisor 49" serie X805 UHD android tv', 'XBR-49X805G', 'Sony', true, 4, 'test.png'),
('Televisor 85" Q950T QLED Smart TV 8K ', 'QN85Q950TSPXPA', 'Samsung', true, 4, 'test.png'),
('Televisor 65" Q800T QLED 8K Smart TV', 'QN82Q950TSPXPA', 'Samsung', true, 4, 'test.png'),
('Televisor The Frame Smart 4K TV ', 'LS03T', 'Samsung', true, 4, 'test.png'),
('Televisor 65" Crystal UHD ', 'UN65TU8500PXPA', 'Samsung', true, 4, 'test.png');

insert into "owner" (ci, issued_place, name, phone, email, enable) values(1798749, 'SC', 'Lucia Mendez', 74198745, 'lucia74.Mend@gmail.com', true);
insert into "owner" (ci, issued_place, name, phone, email, enable) values(8274955, 'BN', 'Daniel Arancibia', 67894135, 'Danielito_Aran@hotmail.com', true);
insert into "owner" (ci, issued_place, name, phone, email, enable) values(7365389, 'SC', 'Pamela Ramos', 75684179, 'pamela74.ramos@gmail.com', true);
insert into "owner" (ci, issued_place, name, phone, email, enable) values
(2132212, 'SC', 'Juan Leon',78564589 ,'juanleon.@gmail.com', true),
(3254265, 'SC', 'Yaquelin Perez',76521498 , 'yaque.p5@gmail.com', true),
(6521451, 'CH', 'Rilda Cespedes',65432154 , 'rildita_ces@gmail.com', true),
(2132547, 'CH', 'Corali Mendoza',79854124 , 'mendoca_corali@gmail.com', true),
(4521325, 'SC', 'Gustavo Rodas',72156864 , 'rodastavo@gmail.com', true),
(2154878, 'SC', 'Denil Perez', 65215487, 'denil_p10@gmail.com', true),
(9865241, 'CH', 'Yulissa Sanchez',72459832 , 'fbiYuli@gmail.com', true),
(6598542, 'SC', 'Marcos Leon',69854211 , 'marcos07leon@gmail.com', true),
(3125487, 'LP', 'Maria Jose Nuñez', 78514000 , 'mariajosese@gmail.com', true),
(5012140, 'LP', 'Matias Aponta', 70124141 , 'mati_apionta@gmail.com', true),
(4518794, 'LP', 'Lucas Cespedes', 78459820 , 'cespe_lucas@gmail.com', true),
(3215216, 'LP', 'Jhanet Ferrufino', 65219805 , 'ferrufino_jf@gmail.com', true),
(3458204, 'BN', 'Abimael Barriga', 65218702, 'abi_barriga@gmail.com', true),
(3215646, 'BN', 'Mario Cerezo', 79218525, 'cerezo.e.m@gmail.com', true),
(3272398, 'BN', 'Sonia Vasquez', 7635284 , 'sonia.vc@gmail.com', true),
(1035740, 'BN', 'Moises Cabrera', 65982082 , 'muss.cabrera@gmail.com', true),
(9065424, 'BN', 'Carla Yesenia Ruiz',65872098 , 'carlitacc@gmail.com', true),
(6120151, 'CO', 'Yunaida Aguilar',69810781 , 'yunaida34mn@gmail.com', true),
(2147451, 'CO', 'Judit Mendez', 68932104, 'judit435f@gmail.com', true),
(3545447, 'CO', 'Blanca Rodas', 65982984, 'rodas.blanca01@gmail.com', true),
(5024584, 'CO', 'Marcelo Campos', 68975420, 'marcelo.coch@gmail.com', true),
(6502358, 'CO', 'Mateo Ferrufino Leon', 78206050, 'mateo_0987@gmail.com', true),
(3902875, 'OR', 'Analí Garcia',78854130 , 'anita17g@gmail.com', true),
(9420564, 'OR', 'David Huanca', 69985105, 'david.huanca10@gmail.com', true),
(5801505, 'OR', 'Angelica Rojas', 75488411, 'angelica.rrr@gmail.com', true),
(5740464, 'OR', 'Francisco Mamani', 75405015, 'mamani1987@gmail.com', true),
(6850645, 'OR', 'Gael Quispe', 78452054, 'gael_qm@gmail.com', true),
(6806846, 'OR', 'ALfredo Rojas', 65205550, 'alfre98mc@gmail.com', true);

insert into product_owner (ci, cod_product, id_category) values(1798749, 1, 2);
insert into product_owner (ci, cod_product, id_category) values(8274955, 4, 2);
insert into product_owner (ci, cod_product, id_category) values(7365389, 3, 1);
insert into product_owner (ci, cod_product, id_category)
values
(2132547, 5, 1),
(9865241, 6, 1),
(6598542, 7, 1),
(2132547, 29, 2),
(2154878, 30, 2),
(6598542, 31, 2),
(9865241, 32, 2),
(3254265, 68, 3),
(4521325, 67, 3),
(9865241, 69, 3),
(3254265, 70, 3),
(4521325, 71, 3),
(3125487, 8, 1),
(5012140, 9, 1),
(4518794, 10, 1),
(3215216, 11, 1),
(3458204, 12, 1),
(3215646, 13, 1),
(3272398, 15, 1),
(1035740, 16, 1),
(9065424, 17, 1),
(6120151, 33, 2),
(2147451, 34, 2),
(3545447, 35, 2),
(5024584, 36, 2),
(6502358, 37, 2),
(3902875, 38, 2),
(5801505, 39, 2),
(5740464, 40, 2),
(6850645, 41, 2);

insert into representative_company (ci, issued_place, name_representative, address, email, phone) values (8147047, 'SC', 'Jaime Robles', 'Av. 2 de Agosto c/6','jaime.robles@gmail.com', 74787147);
insert into representative_company (ci, issued_place, name_representative, address, email, phone) values (3874174, 'SC', 'Arturo Villegas', 'Av. mutualista c/los claveles','ar_villegas@gmail.com', 60147814);
insert into representative_company (ci, issued_place, name_representative, address, email, phone) values (7417899, 'SC', 'Laura Rosales', 'Av. Paragua 4to anillo','laura_ro@gmail.com', 62147747);

insert into client_company (nit, name_company, corporate, email, phone, enable, ci_representative) values(15748914014, 'Bosh', 'office appliances', 'BoshSantaCruz@gmail.com', '3564789', true, 8147047);
insert into client_company (nit, name_company, corporate, email, phone, enable, ci_representative) values(87198526015, 'Whirlpool', 'electrical appliances', 'WhirlpoolSC@gmail.com', '3564789', true, 3874174);
insert into client_company (nit, name_company, corporate, email, phone, enable, ci_representative) values(56874123017, 'Dell', 'Destok', 'Destok.Dell@gmail.com', '3564789', true, 7417899);

insert into contract_company (cod_contract, description_contract, start_date, end_date, nit_company, status_contract, file_contract ) values(100, 'El siguiente contrato abarca a todos los electrodomesticos de la marca Bosh, cubre los posibles defectos de fabrica que estos contengan en su fecha limite una ves realizada la compra', '2018/10/15', '2021/10/15', 15748914014, true, '');
insert into contract_company (cod_contract, description_contract, start_date, end_date, nit_company, status_contract, file_contract ) values(101, 'El siguiente contrato abarca a todos los electrodomesticos de la marca Whirlpool, cubre los posibles defectos de fabrica que estos contengan en su fecha limite una ves realizada la compra', '2020/02/20', '2023/02/20', 87198526015, true,'');
insert into contract_company (cod_contract, description_contract, start_date, end_date, nit_company, status_contract, file_contract ) values(102, 'El siguiente contrato abarca a todas las computadoras Dell, cubre los posibles defectos de fabrica que estos contengan en su fecha limite una ves realizada la compra', '2019/08/10', '2022/08/10', 56874123017, true, '');

insert into guarantee_policy (cod_policy, date_purchase, deadline, proof, state_guarantee, nro_invoice, code_owner, ci_owner, cod_product, id_category, nit_client_company) values('CE1458','2016/08/20', '2019/08/20', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 2, '00010144', 1, 1798749, 1, 2, 87198526015);
insert into guarantee_policy (cod_policy, date_purchase, deadline, proof, state_guarantee, nro_invoice, code_owner, ci_owner, cod_product, id_category, nit_client_company) values('CE1460', '2018/01/15', '2020/01/15', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 2, '00010471', 2, 8274955, 4, 2, 15748914014);
insert into guarantee_policy (cod_policy, date_purchase, deadline, proof, state_guarantee, nro_invoice, code_owner, ci_owner, cod_product, id_category, nit_client_company) values('CO2455', '2020/05/01', '2022/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '00012574', 3, 7365389, 3, 1, 56874123017);
insert into guarantee_policy (cod_policy, date_purchase, deadline, proof, state_guarantee, nro_invoice, code_owner, ci_owner, cod_product, id_category, nit_client_company)
values
('CO2405', '2020/05/01', '2022/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001478', 4, 2132547, 5, 1, 56874123017),
('CO2454', '2020/05/01', '2022/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001480', 5, 9865241, 6, 1, 56874123017),
('CO2453', '2020/05/01', '2022/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001495', 6, 6598542, 7, 1, 56874123017),
('CO2452', '2020/05/01', '2022/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001508', 7, 2132547, 29, 2, 15748914014),
('CO2451', '2020/05/01', '2023/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001608', 8, 2154878, 30, 2, 15748914014),
('CO2460', '2020/05/01', '2023/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001747', 9, 6598542,31 , 2, 15748914014),
('CO2400', '2020/05/01', '2023/05/01', '0f3cd7d3-315e-4b04-b046-48dc094f749d.jpg', 1, '0001752', 10, 9865241,32 , 2, 15748914014);

insert into request_support_service (description, cod_policy, date_request, state_request, id_type, code_owner, ci_owner, cod_product, id_category) values ('Solicita revisar el producto en domicilio, debido a que este no realia con totalidad su funcion', 'CE1460', '2020/01/23', 1, 1, 2, 8274955, 4, 2);
insert into request_support_service (description, cod_policy, date_request, state_request, id_type, code_owner, ci_owner, cod_product, id_category) values ('Solicita revisar el producto en remoto, debido a que este no realia con totalidad su funcion y el cliente no puede ir al taller para la verificacion', 'CO2455', '2019/12/15', 2, 3, 3, 7365389, 3, 1);
insert into request_support_service (description, cod_policy, date_request, state_request, id_type, code_owner, ci_owner, cod_product, id_category) values ('Solicita revisar el producto en taller, debido a que este no realia con totalidad su funcion y tiene algunos ruidos', 'CE1458','2020/01/23', 1, 2, 1, 1798749, 1, 2);
insert into request_support_service (description, cod_policy, date_request, state_request, id_type, code_owner, ci_owner, cod_product, id_category)
values
('Fuente de energia en mal estado', 'CO2454', '2020/01/02', 1, 1, 5, 9865241, 6, 1),
('Problemas de la tarjeta de video', 'CO2453', '2020/02/12', 1, 1, 6, 6598542, 7, 1),
('Mal funcionamiento del motor', 'CO2452', '2020/02/21', 1, 2, 7, 2132547, 29, 2),
('Mal funcionamiento del motor', 'CO2460', '2020/03/01', 1, 2, 9, 6598542, 31, 2);

insert into technical_support_service (total_cost, service_date, observations, state_support_service, request_no, id_technical) values(90, '2020/02/18', 'Ninguna', false, 1, 2);
insert into technical_support_service (total_cost, service_date, observations, state_support_service, request_no, id_technical) values(120, '2020/02/18', 'Dejo cancelado 50Bs debe 70Bs, pagara monto al momento de recoger su producto', false, 3, 4);
insert into technical_support_service (total_cost, service_date, observations, state_support_service, request_no, id_technical) values
(140, '2020/02/20', 'Ninguna', true,  5, 4),
(200, '2020/02/25', 'Ninguna', true, 4, 3),
(40, '2020/02/28', 'Ninguna', true, 7, 4),
(80, '2020/03/05', 'Ninguna', true, 6, 3);

insert into "role"(role_name ,description ,"enable" ) values('Gerente','Tiene todos los privilegios',true);
insert into "role"(role_name ,description ,"enable" ) values('Administrador','Tiene privilegios limitados',true);

insert into "user"(username ,email ,user_profile ,"enable" ,id_role ) values('master','master@gmail.com','',true, 1);

insert into service_invoice (nit,corporate ,total_cost ,service_date ,quantity_day_support ,support_no ,id_user, enable) 
values
('6598542018', 'Marcos Leon Espada',140,'2020/02/21',2,3,1,true),
('9865241', 'Yulissa Sanchez',200,'2020/03/26',1, 4,1,true),
('2132547', 'Corali Mendoza',80,'2020/03/01',2, 5,1,true),
('8274955018', 'Daniel Arancibia Pereez',40,'2020/03/07',1, 6,1,true);

insert into service (description_service, service_guarantee) values('Realiza mantenimiento a todo tipo de electrodomestico del hogar, desde una batidora hasta una heladera. Verifica estados de los componentes de los equipos', false);
insert into service (description_service, service_guarantee) values('Realiza reparacion del software de computadoras y mantenimientos', false);
insert into service (description_service, service_guarantee) values('Realiza reparacion de electrodomesticos y verifica posibles fallas a futuro', false);
insert into service (description_service, service_guarantee) values('Realiza cambios de componentes de los electrodomesticos', false);

insert into list_service (support_no, id_service, cost) values(1, 1, 90);
insert into list_service (support_no, id_service, cost) values(2, 3, 120);

insert into list_spare_parts (support_no, cod_replacement, id_category, quantity, cost) values(1, 3, 2, 1, 75);
insert into list_spare_parts (support_no, cod_replacement, id_category, quantity, cost) values(2, 1, 2, 1, 100);

insert into privilege(description_privilege, id_super_privilege) values ('Privilege Company', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Company', 1);
insert into privilege(description_privilege, id_super_privilege) values ('Write Company', 2);
insert into privilege(description_privilege, id_super_privilege) values ('Read Company', 2);
insert into privilege(description_privilege, id_super_privilege) values ('Update Company', 2);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable Company', 1);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Contract', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Contract', 7);
insert into privilege(description_privilege, id_super_privilege) values ('Write Contract', 8);
insert into privilege(description_privilege, id_super_privilege) values ('Read Contract', 8);
insert into privilege(description_privilege, id_super_privilege) values ('Update Contract', 7);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege User', null);
insert into privilege(description_privilege, id_super_privilege) values ('Write User', 12);
insert into privilege(description_privilege, id_super_privilege) values ('Read User', 12);
insert into privilege(description_privilege, id_super_privilege) values ('Update User', 12);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable User', 12);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Role', null);
insert into privilege(description_privilege, id_super_privilege) values ('Write Role', 17);
insert into privilege(description_privilege, id_super_privilege) values ('Read Role', 17);
insert into privilege(description_privilege, id_super_privilege) values ('Update Role', 17);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disbale Role', 17);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Note',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write Note',22);
insert into privilege(description_privilege, id_super_privilege) values ('Read Note',22);
insert into privilege(description_privilege, id_super_privilege) values ('Update Note',22);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Detail Note',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write Detail Note',26);
insert into privilege(description_privilege, id_super_privilege) values ('Read Detail Note',26);
insert into privilege(description_privilege, id_super_privilege) values ('Delete Detail Note',26);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Workshop',null);
insert into privilege(description_privilege, id_super_privilege) values ('General Workshop',30);
insert into privilege(description_privilege, id_super_privilege) values ('Write Workshop',30);
insert into privilege(description_privilege, id_super_privilege) values ('Read Workshop',31);
insert into privilege(description_privilege, id_super_privilege) values ('Update Workshop',30);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Replacement', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Replacement',35);
insert into privilege(description_privilege, id_super_privilege) values ('Write Replacement',36);
insert into privilege(description_privilege, id_super_privilege) values ('Read Replacement',36);
insert into privilege(description_privilege, id_super_privilege) values ('Update Replacement',36);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disbale Replacement',35);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Warehouse Inventory',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write Warehouse Inventory',41);
insert into privilege(description_privilege, id_super_privilege) values ('Read Warehouse Inventory',41);
insert into privilege(description_privilege, id_super_privilege) values ('Delete Warehouse Inventory',41);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Technical',null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Technical',45);
insert into privilege(description_privilege, id_super_privilege) values ('Write Technical',45);
insert into privilege(description_privilege, id_super_privilege) values ('Update Technical',45);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable Technical',45);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Specialty',null);
insert into privilege(description_privilege, id_super_privilege) values ('General Specialty',50);
insert into privilege(description_privilege, id_super_privilege) values ('Write Specialty',50);
insert into privilege(description_privilege, id_super_privilege) values ('Read Specialty',51);
insert into privilege(description_privilege, id_super_privilege) values ('Update Specialty',50);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Service',null);
insert into privilege(description_privilege, id_super_privilege) values ('General Service',55);
insert into privilege(description_privilege, id_super_privilege) values ('Read Service',56);
insert into privilege(description_privilege, id_super_privilege) values ('Write Service',55);
insert into privilege(description_privilege, id_super_privilege) values ('Update Service',55);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege invoice',null);
insert into privilege(description_privilege, id_super_privilege) values ('General invoice',60);
insert into privilege(description_privilege, id_super_privilege) values ('Write invoice',61);
insert into privilege(description_privilege, id_super_privilege) values ('Read invoice',61);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable invoice',60);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Technical Support',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write Technical Support',65);
insert into privilege(description_privilege, id_super_privilege) values ('Read Technical Support',65);
insert into privilege(description_privilege, id_super_privilege) values ('Update Technical Support',65);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege List Service',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write List Service',69);
insert into privilege(description_privilege, id_super_privilege) values ('Read List Service',69);
insert into privilege(description_privilege, id_super_privilege) values ('Delete List Service',69);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege List Spare Parts',null);
insert into privilege(description_privilege, id_super_privilege) values ('Write List Spare Parts',73);
insert into privilege(description_privilege, id_super_privilege) values ('Read List Spare Parts',73);
insert into privilege(description_privilege, id_super_privilege) values ('Delete List Spare Parts',73);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Spare Box',null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Spare Box',77);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Product', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Product', 79);
insert into privilege(description_privilege, id_super_privilege) values ('Read Product', 80);
insert into privilege(description_privilege, id_super_privilege) values ('write Product', 80);
insert into privilege(description_privilege, id_super_privilege) values ('Update Product', 79);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable Product', 79);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Product Category', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Product Category', 85);
insert into privilege(description_privilege, id_super_privilege) values ('Read Product Category', 86);
insert into privilege(description_privilege, id_super_privilege) values ('write Product Category', 86);
insert into privilege(description_privilege, id_super_privilege) values ('Update Product Category', 85);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Type Support Service', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Type Support Service', 90);
insert into privilege(description_privilege, id_super_privilege) values ('Read Type Support Service', 91);
insert into privilege(description_privilege, id_super_privilege) values ('write Type Support Service', 90);
insert into privilege(description_privilege, id_super_privilege) values ('Update Type Support Service', 90);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Owner', null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Owner', 95);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Guarantee Policy', null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Guarantee Policy', 97);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Request Support Service', null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Support Service', 99);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable Type Support Service', 90);
insert into privilege(description_privilege, id_super_privilege) values ('Accept/Decline Guarentee Policy', 97);
insert into privilege(description_privilege, id_super_privilege) values ('Accept/Decline Request Support Service', 99);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Provider', null);
insert into privilege(description_privilege, id_super_privilege) values ('Read Provider', 104);
insert into privilege(description_privilege, id_super_privilege) values ('write Provider', 104);
insert into privilege(description_privilege, id_super_privilege) values ('Update Provider', 104);
insert into privilege(description_privilege, id_super_privilege) values ('Enable/Disable Provider', 104);
insert into privilege(description_privilege, id_super_privilege) values ('Enabled/Disabled Service For Guarantee', 55);
insert into privilege(description_privilege, id_super_privilege) values ('Valid/Disvalid Contract', 7);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Replacement Category', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Replacement Category', 111);
insert into privilege(description_privilege, id_super_privilege) values ('Read Replacement Category', 112);
insert into privilege(description_privilege, id_super_privilege) values ('Write Replacement Category', 112);
insert into privilege(description_privilege, id_super_privilege) values ('Update Replacement Category', 111);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege Representative Company', null);
insert into privilege(description_privilege, id_super_privilege) values ('General Representative Company', 116);
insert into privilege(description_privilege, id_super_privilege) values ('Read Representative Company', 117);
insert into privilege(description_privilege, id_super_privilege) values ('Write Representative Company', 117);
insert into privilege(description_privilege, id_super_privilege) values ('Update Representative Company', 116);
insert into privilege(description_privilege, id_super_privilege) values ('Privilege View Activity', null);

insert into role_privilege (id_role ,id_privilege ) values(1,1);
insert into role_privilege (id_role ,id_privilege ) values(2,2);
insert into role_privilege (id_role ,id_privilege ) values(1,7);
insert into role_privilege (id_role ,id_privilege ) values(2,8);
insert into role_privilege (id_role ,id_privilege ) values(1,12);
insert into role_privilege (id_role ,id_privilege ) values(1,17);
insert into role_privilege (id_role ,id_privilege ) values(1,22);
insert into role_privilege (id_role ,id_privilege ) values(2,22);
insert into role_privilege (id_role ,id_privilege ) values(1,26);
insert into role_privilege (id_role ,id_privilege ) values(2,26);
insert into role_privilege (id_role ,id_privilege ) values(1,30);
insert into role_privilege (id_role ,id_privilege ) values(2,31);
insert into role_privilege (id_role ,id_privilege ) values(1,35);
insert into role_privilege (id_role ,id_privilege ) values(2,36);
insert into role_privilege (id_role ,id_privilege ) values(1,41);
insert into role_privilege (id_role ,id_privilege ) values(2,41);
insert into role_privilege (id_role ,id_privilege ) values(1,45);
insert into role_privilege (id_role ,id_privilege ) values(2,45);
insert into role_privilege (id_role ,id_privilege ) values(1,50);
insert into role_privilege (id_role ,id_privilege ) values(2,51);
insert into role_privilege (id_role ,id_privilege ) values(1,55);
insert into role_privilege (id_role ,id_privilege ) values(2,56);
insert into role_privilege (id_role ,id_privilege ) values(1,60);
insert into role_privilege (id_role ,id_privilege ) values(2,61);
insert into role_privilege (id_role ,id_privilege ) values(1,65);
insert into role_privilege (id_role ,id_privilege ) values(2,65);
insert into role_privilege (id_role ,id_privilege ) values(1,69);
insert into role_privilege (id_role ,id_privilege ) values(2,69);
insert into role_privilege (id_role ,id_privilege ) values(1,73);
insert into role_privilege (id_role ,id_privilege ) values(2,73);
insert into role_privilege (id_role ,id_privilege ) values(1,77);
insert into role_privilege (id_role ,id_privilege ) values(2,77);
insert into role_privilege (id_role ,id_privilege ) values(1,79);
insert into role_privilege (id_role ,id_privilege ) values(2,80);
insert into role_privilege (id_role ,id_privilege ) values(1,85);
insert into role_privilege (id_role ,id_privilege ) values(2,86);
insert into role_privilege (id_role ,id_privilege ) values(1,90);
insert into role_privilege (id_role ,id_privilege ) values(2,91);
insert into role_privilege (id_role ,id_privilege ) values(1,95);
insert into role_privilege (id_role ,id_privilege ) values(2,95);
insert into role_privilege (id_role ,id_privilege ) values(1,97);
insert into role_privilege (id_role ,id_privilege ) values(2,97);
insert into role_privilege (id_role ,id_privilege ) values(1,99);
insert into role_privilege (id_role ,id_privilege ) values(2,99);
insert into role_privilege (id_role ,id_privilege ) values(1,104);
insert into role_privilege (id_role ,id_privilege ) values(2,104);
insert into role_privilege (id_role ,id_privilege ) values(1,111);
insert into role_privilege (id_role ,id_privilege ) values(2,112);
insert into role_privilege (id_role ,id_privilege ) values(1,116);
insert into role_privilege (id_role ,id_privilege ) values(2,117);
insert into role_privilege (id_role ,id_privilege ) values(1,121);  
