const fs = require("fs");
const userModel = require("../../../models/UserManagement/userManage/user.model");
const externalLibrary = require("../../../config/externalLibrary");
const helpers = require('../../../helpers/address');

/**
 * Method GET: get user_manage view
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getUser = async (req, res) => {
  const userList = await userModel.getUser("select * from getListUsers();");
  const users = userList.rows;
  const rolesList = await userModel.getListRoles();
  let enabledUsers = await userModel.getUser("select * from getListDisabledUsers();");
  enabledUsers = enabledUsers.rows;
  res.status(200).render("UserManagement/userManage/user_manage", {
    users,
    rolesList,
    enabledUsers,
  });
};

/**
 * Method POST: register user with it writes in bitacora
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const postUser = async (req, res) => {
  try {
    const { username, password, password_retype, email, role } = req.body;
    const responseEmail = await userModel.emailUserExists(email);
    if (password == password_retype && !responseEmail) {
      let origin = req.get('origin');
      let filename = req.file.filename;
      if (await userModel.insertUser(username, password, email, role, `${origin}/img/${filename}`)) {
        if (await externalLibrary.writeBackLog(req.user.username, `register user ${username}, ${email}, ${filename}`, req.device.type)) {
          console.log("write register user successfully");
        } else {
          console.log("write register user failure");
        }
        req.flash("success_msg", "User added Successfully");
        res.status(200).redirect("/user_management/user_manage");
      } else {
        await fs.unlink(path, (err) => {
          if (err) {
            console.error(err);
            return;
          }
        });
        req.flash('error_msgs', `Error! I have to register new user. please call scrum dev`);
        res.status(200).redirect('/user_management/user_manage');
      }
    } else {
      const path = req.file.path;
      await fs.unlink(path, (err) => {
        //remove image
        if (err) {
          console.error(err);
          return;
        }
      });
      req.flash('error_msgs', `Password Incorrect or email exists`);
      res.status(200).redirect('/user_management/user_manage');
    }

  } catch (error) {
    console.error(error, "Error in register new user in user management");
    req.flash('error_msgs', `Error! I have to register new user. please call scrum dev`);
    res.status(200).redirect('/user_management/user_manage');
  }
};

/**
 * Method GET : get view update user
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getUpdateUser = async (req, res) => {
  if (await userModel.userHavePrivilege(req.user.id_user, 15)) {
    const userRow = await userModel.getUser(`SELECT * FROM "user" WHERE id_user=${req.params.id};`);
    const user = userRow.rows;
    var roleName = await userModel.getUser(`select getrolename(${req.params.id});`);
    roleName = roleName.rows[0].getrolename;
    roleName = await userModel.getUser(`select id_role from "role" r2 where role_name = '${roleName}';`);
    roleName = roleName.rows[0].id_role;
    const rolesList = await userModel.getListRoles();
    res.status(200).render("UserManagement/userManage/user_update", {
      idUser: user[0].id_user,
      username: user[0].username,
      email: user[0].email,
      profile: user[0].user_profile,
      enable: user[0].enable,
      roleName: roleName,
      rolesList: rolesList,
    });
  } else {
    req.flash('error_msgs', 'Error! Not Authorized. You Cant Edit A User');
    res.status(200).redirect('/user_management/user_manage');
  }
};

/**
 * METHOD PUT : update data of the user
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const putUpdateUser = async (req, res) => {
  if (await userModel.userHavePrivilege(req.user.id_user, 15)) {
    const { username, email, role } = req.body;
    var file = req.file;
    var filename;
    if (typeof file === "undefined") {
      filename = await userModel.getUser(`select user_profile from "user" u where id_user = ${req.params.id};`);
      filename = filename.rows[0].user_profile;
    } else {
      filename = file.filename;
    }
    let origin = req.get('origin');
    filename =`${origin}/img/${filename}`;
    if (await userModel.updateUser(req.params.id, username, email,filename, role)) {
      if (await externalLibrary.writeBackLog(req.user.username, `Update User with id: ${req.params.id},new data name: ${username} email: ${email} role: ${role}`, req.device.type)) {
        console.log("write backlog update user successfully");
      } else {
        console.log("write backlog update user failure");
      }
      req.flash("success_msg", "User Updated Successfully");
      res.status(200).redirect("../user_manage");
    } else {
      req.flash('error_msgs', 'Error! Email user already exists or an error occurred saving data');
      res.status(200).redirect(`/user_management/update/${req.params.id}`);
    }
  } else {
    req.flash('error_msgs', 'Error! Not Authorized. You Cant Edit A User');
    res.status(200).redirect('/user_management/user_manage');
  }
};

/**
 * METHOD PUT : enable or disable user
 * @param {*} req : response (peticion)
 * @param {*} res : response (respuesta)
 */
const putDisableEnableUser = async (req, res) => {
  if (await userModel.userHavePrivilege(req.user.id_user, 16)) {
    if (await userModel.enableDisableUser(req.params.id)) {
      if (await externalLibrary.writeBackLog(req.user.username, `Enable/Disable User with id: ${req.params.id}`, req.device.type)) {
        console.log("write backlog update user successfully");
      } else {
        console.log("write backlog update user failure");
      }
      req.flash("success_msg", "User Disable or Enable Successfully");
      res.status(200).redirect("../user_manage");
    } else {
      req.flash('error_msgs', "Error! Enable/Disable User Failed. TRY AGAIN!");
      res.status(200).redirect('/user_management/user_manage');
    }
  } else {
    req.flash('error_msgs', "Error! You Can't Disable/Enable A User");
    res.status(200).redirect('/user_management/user_manage');
  }
};

/**
 * METHOD PUT : update password of the user
 * for need it update method you have old password
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const putUpdatePassword = async (req, res) => {
  if (await userModel.userHavePrivilege(req.user.id_user, 15)) {
    const { email, old_password, repeat_old_password, new_password } = req.body;
    if (old_password === repeat_old_password) {
      if (await userModel.matchPassword(email, old_password)) {
        const hashNewPass = await externalLibrary.encryptPassword(new_password);
        if (await externalLibrary.updatePassword(req.params.id, hashNewPass)) {
          if (await externalLibrary.writeBackLog(req.user.username, `User password ${email} has been changed
          `, req.device.type)) {
            console.log("write password changed successfully");
          } else {
            console.log("write password changed failure");
          }
          req.flash("success_msg", "Password Update Successfully");
          res.status(200).redirect(`/user_management/update/${req.params.id}`);
        }
      } else {
        req.flash("error_msgs", "Password Old Incorret");
        res.status(200).redirect(`/user_management/update/${req.params.id}`);
      }
    } else {
      req.flash("error_msgs", "The password does not match.");
      res.status(200).redirect(`/user_management/update/${req.params.id}`);
    }
  } else {
    req.flash('error_msgs', "Error! You Can't Change Password of A User");
    res.status(200).redirect('/user_management/user_manage');
  }
};

const getBitacora = async (req, res) => {
  var idUser = req.user.id_user;
  const { username } = req.body;
  if (await userModel.userHavePrivilege(idUser, 121)) {
    const bitacoraData = await externalLibrary.getListUserBitacora(username);
    res.status(200).render('UserManagement/userManage/bitacora_user_manage', {
      bitacoraData,username
    });
  } else {
    req.flash('error_msgs', 'Error! Not Authorized.');
    res.status(200).redirect('/user_management/bitacora_manage');
  }
};

const generatePDFFileActivity = async (req, res) => {
  const username = req.user.username;
  const user = req.params.username;
  let fileName = await generateBitacora(username, user);
  let filePath = await `${helpers.addressTheReport}${fileName}`;
  setTimeout(function () {
    fs.readFile(filePath, function (err, data) {
      console.log("The PDF File does exists");
      res.contentType("application/pdf");
      res.status(200).send(data);
    });
  }, 5000);
};

const generateBitacora = async (username, user) => {
  const bitacoraData = await externalLibrary.getListUserBitacora(user);
  let bitacoraDataString = `<tbody>`;
  for (var index = 0; index < bitacoraData.length; index++) {
    bitacoraDataString = await bitacoraDataString + `
      <tr>
        <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].user}</td>
        <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].date}</td>
        <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].activity}</td>
      </tr>`;
  }
  bitacoraDataString = bitacoraDataString + `</tbody>`;
  let fileName = `bitacora.pdf`;
  let contentHTML = await `
      <!doctype html>
      <html>
        <head>
            <meta charset="utf-8">
            <title>Bitacora RestTeam</title>
        </head>
        <body>
          <div id="pageHeader">
            <h5 style="text-align: center;"><b>Lista de Actividades del Usuario: </b>${user}</h5>
          </div>
          <div style="padding: 3%;">
            <table style="border: 1px solid black; border-collapse: collapse; width: 100%;">
              <thead>
                <tr>
                  <th style="border: 1px solid black; border-collapse: collapse;">Usuario</th>
                  <th style="border: 1px solid black; border-collapse: collapse;">Fecha</th>
                  <th style="border: 1px solid black; border-collapse: collapse;">Actividad</th>
                </tr>
              </thead>
              ${bitacoraDataString}
            </table>
            <div id="pageFooter" style="text-align: center;">
              <b>printed by: </b>${username}
            </div>
          </div>
        </body>
      </html>`;
  await externalLibrary.generatePDFReport(contentHTML, fileName);
  return await fileName;
};

const getProfileUser = async (req, res) => {
  let id_user  = req.params.id_user;
  console.log("id user: ", id_user);
  let username = await userModel.getUser(`select username from "user" where id_user = ${id_user};`);
  username = username.rows[0].username;
  console.log(username);
  if (await externalLibrary.writeBackLog(req.user.username, `Profile of ${username} Visited by ${req.user.username}`, req.device.type)) {
    console.log('write in backlog');
  } else {
    console.log('problems in write backlog');
  }
  const userData = await userModel.getUserData(id_user);
  console.log("userData 2:" , userData);
  const listBitacora = await externalLibrary.getListUserBitacora(username);
  const reportUser = await userModel.getReportUser(username);
  res.status(200).render('UserManagement/userManage/user_profile_manage', {
    userData,
    listBitacora,
    reportUser
  });
};

/**
 * Drawn the pdf file in ../security/reports/.... report User.pdf
 * @param {string} username : username
 */
const generateReportUser = async (username) => {
  const invoiceData = await userModel.getDataReportsInvoiceUser();
  let invoiceDataString = `<tbody>`;
  for (var index = 0; index < invoiceData.length; index++) {
    invoiceDataString = await invoiceDataString + `
      <tr>
          <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].id_user}</td>
          <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].username}</td>
          <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].role_name}</td>
          <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].count}</td>
      </tr>`;
  }
  invoiceDataString = invoiceDataString + `</tbody>`;
  let timeDate = new Date();
  let fileName = ` ${invoiceData[0].username}` + ' ' + timeDate.getDate() + '-' + timeDate.getMonth() + `-` + timeDate.getFullYear() + ` report User.pdf`;
  let contentHTML = await `
      <!doctype html>
      <html>
      <head>
          <meta charset="utf-8">
      </head>
      <body>
          <div id="pageHeader">
              <h2 style="text-align: center;"><b> Invoices reports made of users </b></h2> <br>
          </div>
          <div style="padding: 3%;">
              <table class="egt" style="width: 100%;">
                  <thead>
                      <tr>
                          <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Id user</th>
                          <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">User name</th>
                          <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Role name</th>
                          <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Count</th>
                      </tr>
                  </thead>
                  ${invoiceDataString}
              </table>
              <div id="pageFooter" style="text-align: center;">
                  <b>printed by: </b>${username}
              </div>
          </div>
      </body>
      </html>`;
  await externalLibrary.generatePDFReport(contentHTML, fileName);
  return await fileName;
}
/**
* METHOD POST : Generate PDF file for a service invoice
* @param {*} req : request
* @param {*} res  : response
*/
const generateReportPDFUser = async (req, res) => {
  let fileName = await generateReportUser(req.user.username);
  let filePath = await `${helpers.addressTheReport}${fileName}`;
  setTimeout(function () {
    fs.readFile(filePath, function (err, data) {
      console.log("The PDF File does exists");
      res.contentType("application/pdf");
      res.status(200).send(data);
    });
  }, 4000);
}

const getReportInvoiceUser = async (req, res) => {
  const reportUser = await userModel.getReportsInvoiceUser();
  res.status(200).render('UserManagement/userManage/report_invoice_user', {
    reportUser
  });
}

module.exports = {
  getUser,
  postUser,
  getUpdateUser,
  putUpdateUser,
  putDisableEnableUser,
  putUpdatePassword,
  getBitacora,
  generatePDFFileActivity,
  getProfileUser,
  generateReportPDFUser,
  getReportInvoiceUser
};
