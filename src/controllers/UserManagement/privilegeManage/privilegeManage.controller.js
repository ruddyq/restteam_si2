/**
 *       Import Files dependents
*/
const privilegeModel = require('../../../models/UserManagement/privilegeManage/privilegeManage.model');
const externalLibrary = require('../../../config/externalLibrary');

/**
 * method GET : get view privilege manage
 * @param {*} req : request (peticion)
 * @param {*} res : response (response)
 */
const getPrivilege = async (req, res) => {
    if (req.user.role_name === 'Gerente') {
        const privilegeList = await privilegeModel.getPrivileges();
        const privilegeOptions = await privilegeModel.getOptionsPrivileges();
        res.status(200).render('UserManagement/privilegeManage/privilege_manage', {
            privilegeList,
            privilegeOptions
        });
    } else {
        req.flash('error_msgs', `Error! not authorized, ${req.user.username} blocked.`);
        res.status(200).redirect('/');
    }
}

/**
 * method POST : register new privilege and assign privilege father
 * @param {*} req : request (peticion)
 * @param {*} res : response (response)
 */
const postPrivilege = async (req, res) => {
    if (req.user.role_name === 'Gerente') {
        const { description, id_super } = req.body;
        const idPrivilege = await privilegeModel.insertPrivilege(description, id_super);
        if (idPrivilege > 0) {
            if (await externalLibrary.writeBackLog(req.user.username, `registered privilege ID: ${idPrivilege}, description: ${description}, ID super privilege: ${id_super}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write to backlog.');
            }
            req.flash('success_msg', `Privilege added Successfully ID: ${idPrivilege}`);
            res.status(200).redirect('/user_management/privilege_manage');
        } else {
            req.flash('error_msgs', `Error! privilege ${description} already exist.`);
            res.status(200).redirect('/user_management/privilege_manage');
        }
    } else {
        req.flash('error_msgs', `Error! not authorized, ${req.user.username} blocked.`);
        res.status(200).redirect('/user_management/privilege_manage');
    }
}

module.exports = {
    getPrivilege,
    postPrivilege
}