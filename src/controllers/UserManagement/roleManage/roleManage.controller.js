/**
 *       Import Files dependents
 */
const roleModel = require('../../../models/UserManagement/roleManage/role.model');
const externalLibrary = require('../../../config/externalLibrary');
/**
 * Method GET: show view Role Manage
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getRole = async (req, res) => {
    if (await roleModel.verifyPermitUser(req.user.id_user, 19)) {
        const roles = await roleModel.getListRoles();
        const privilegeList = await roleModel.getPrivileges();
        if (roles.length > 0) {
            res.status(200).render('UserManagement/roleManage/role_manage', {
                roles,
                privilegeList
            });
        } else {
            res.status(200).send('error');
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.status(200).redirect('/');
    }
}

/**
 * Method POST: Process new Role to database expresssi2
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postRole = async (req, res) => {
    try {
        if (await roleModel.verifyPermitUser(req.user.id_user, 19)) {
            const { role_name, role_description, permission } = req.body;
            if (permission.length < 1) {
                req.flash('error_msgs', 'Error! You would select permissions.');
                res.status(200).redirect('/user_management/role_manage');
            } else {
                if (await roleModel.existsRole(role_name, role_description)) {
                    req.flash('error_msgs', 'Error! role already exist.');
                    res.status(200).redirect('/user_management/role_manage');
                } else {
                    if (await roleModel.insertRoleWithItsPrivileges(role_name, role_description, permission, req.user.username, req.device.type)) {
                        req.flash('success_msg', 'Role added Successfully');
                        res.status(200).redirect('/user_management/role_manage');
                    } else {
                        req.flash('error_msgs', 'There were problems with register.');
                        res.status(200).redirect('/user_management/role_manage');
                    }
                }
            }
        } else {
            req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
            res.status(200).redirect('/user_management/role_manage');
        }
    } catch (error) {
        console.error("error in register role", error);
        req.flash('error_msgs', `I have a problem to register role`);
        res.status(200).redirect('/user_management/role_manage');
    }
}

/**
 * Method PUT: modify the enable or disable of role
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putRolePermit = async (req, res) => {
    if (await roleModel.verifyPermitUser(req.user.id_user, 21)) {
        const { id } = req.params;
        if (await roleModel.enableDisableRole(id)) {
            await externalLibrary.writeBackLog(req.user.username, `change permit in role ${id}`, req.device.type);
            req.flash('success_msg', 'Role updated Successfully');
            res.status(200).redirect('/user_management/role_manage');
        } else {
            await externalLibrary.writeBackLog(req.user.username, `failed change permit in role ${id}`, req.device.type);
            req.flash('error_msgs', 'Role not updated, I have a problem with role Manage.');
            res.status(200).redirect('/user_management/role_manage');
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.status(200).redirect('/user_management/role_manage');
    }
}

/**
 * Method GET: get view for update Role
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getUpdateRole = async (req, res) => {
    if (await roleModel.verifyPermitUser(req.user.id_user, 20)) {
        const { idRole } = req.params;
        const listPrivileges = await roleModel.getPrivilegesTheRole(idRole);
        const roleDetails = await roleModel.getRoleDetails(idRole);
        const listPrivilegesMissing = await roleModel.getPrivilegesDoesNotHaveRole(idRole);
        let notPrivileges;
        if (listPrivilegesMissing.length > 0) {
            notPrivileges = false;
        } else {
            notPrivileges = true;
        }
        res.render('UserManagement/roleManage/role_update', {
            listPrivileges,
            roleDetails: roleDetails[0],
            listPrivilegesMissing,
            notPrivileges
        });
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.status(200).redirect('/user_management/role_manage');
    }
}

/**
 * Method PUT: update role data as role name and its description
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putRoleUpdateData = async (req, res) => {
    const { role_name, role_description } = req.body;
    const { idRole } = req.params;
    if (await roleModel.verifyRoleUpdate(role_name, idRole)) {
        req.flash('error_msgs', 'Role name does exists.');
        res.status(200).redirect(`/user_management/role_update/${idRole}`);
    } else {
        if (await roleModel.updateRole(idRole, role_name, role_description, req.user.username, req.device.type)) {
            req.flash('success_msg', 'Data Role updated Successfully');
            res.status(200).redirect(`/user_management/role_update/${idRole}`);
        } else {
            req.flash('error_msgs', 'Role not updated.');
            res.status(200).redirect(`/user_management/role_update/${idRole}`);
        }
    }
}

/**
 * Method PUT: add more privileges to role 
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putRoleAssignPrivileges = async (req, res) => {
    const { permission, user } = req.body;
    const { idRole } = req.params;
    if (await roleModel.assignPrivilegesToRole(permission, idRole, user, req.device.type)) {
        req.flash('success_msg', 'Assigned new Privileges successfully');
        res.status(200).redirect(`/user_management/role_update/${idRole}`);
    } else {
        req.flash('error_msgs', 'Failed assignation privileges to role.');
        res.status(200).redirect(`/user_management/role_update/${idRole}`);
    }
}

/**
 * Method PUT: remove privileges selected of the role
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putRoleRemovePrivilege = async (req, res) => {
    const { id_privilege, user } = req.body;
    const { idRole } = req.params;
    if (await roleModel.removePrivilegeTheRole(id_privilege, idRole, user, req.device.type)) {
        req.flash('success_msg', 'Removed Privilege of the Role Successfully');
        res.status(200).redirect(`/user_management/role_update/${idRole}`);
    } else {
        req.flash('error_msgs', 'Failed to Remove Privilege because the role would 1 privilege min.');
        res.status(200).redirect(`/user_management/role_update/${idRole}`);
    }
}

module.exports = {
    getRole,
    postRole,
    putRolePermit,
    getUpdateRole,
    putRoleUpdateData,
    putRoleAssignPrivileges,
    putRoleRemovePrivilege
}