const providerModel = require('../../../models/WarehouseManagement/providerManage/providerManage.model');
const externalLibrary = require('../../../config/externalLibrary');


const getProvider = async (req, res) => {
    var idUser = req.user.id_user;
    if (await providerModel.userHavePrivilege(idUser, 105)){
        const providerList = await providerModel.getListProvider();
        res.render('WarehouseManagement/ProviderManage/provider_manage', {
            providerList
        })
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/provider_manage');
    }
}

const postProvider = async (req, res) => {
    var idUser = req.user.id_user;
    if (await providerModel.userHavePrivilege(idUser, 106)){
        const {nit_ci, provider_name, address, email, phone} = req.body;
        if (await providerModel.insertProvider(nit_ci, provider_name, address, email, phone)) {
            if( await externalLibrary.writeBackLog(req.user.username, `Provider register: '${nit_ci}' and '${email}'`, req.device.type)){
                console.log('write Provider successfully');
            } else {
                console.log('write Provider failure');
            }
            req.flash('success_msg', 'Provider added Successfully');
            res.redirect('/warehouse_management/provider_manage');
        } else {
            req.flash('error_msgs', 'Error! Provider already exist.');
            res.redirect('/warehouse_management/provider_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/provider_manage');
    }
}

const enableDisableProvider = async (req, res) => {
    var idUser = req.user.id_user;
    if(await providerModel.userHavePrivilege(idUser, 108)){
        const {nit_ci} = req.params;
        if (await providerModel.enableDisableProvider(nit_ci)) {
            await externalLibrary.writeBackLog(req.user.username, `change permission in provider from cod: ${nit_ci}`, req.device.type);
            req.flash('success_msg', 'Provider permission successfully updated');
            res.redirect('/warehouse_management/provider_manage');
        } else {
            await externalLibrary.writeBackLog(req.user.username, `failed change in provider from cod:: ${nit_ci}`, req.device.type);
            req.flash('error_msgs', 'Provider permission not successfully updated');
            res.redirect('/warehouse_management/provider_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/provider_manage');
    }
}

const getUpdateProvider = async (req, res) => {
    var idUser = req.user.id_user;
    if (await providerModel.userHavePrivilege(idUser, 107)){
        const nit_ci = req.params.nit_ci;
        const providerData = await providerModel.getProviderUpdateData(nit_ci);
        res.render('WarehouseManagement/ProviderManage/provider_update',{
            providerData,
            nit_ci
        });
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/provider_manage');
    }
}

const putUpdateProvider = async (req, res) => {
    const {provider_name, address, email, phone} = req.body;
    if ( await providerModel.updateProvider(req.params.nit_ci, provider_name, address, email, phone)){
        if (await externalLibrary.writeBackLog(req.user.username, `Update Provider id:${req.params.nit_ci}`, req.device.type)) {
            console.log('write in backlog');
        } else {
            console.log('problems in write backlog');
        }
        req.flash('success_msg', 'Provider updated Successfully');
        res.redirect('../provider_manage');
    }else{
        req.flash('error_msgs', 'Error! Provider not update. The email already exists');
        res.redirect('/warehouse_management/provider_manage');
    }
    
}


module.exports = {
    getProvider,
    postProvider,
    enableDisableProvider,
    getUpdateProvider,
    putUpdateProvider
};
