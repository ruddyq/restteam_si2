const externalLibrary = require('../../../config/externalLibrary');
const replacementModel = require('../../../models/WarehouseManagement/ReplacementManage/replacementManage.model');

/**
 * method GET : get view replacement manage
 * @param {*} req : request (peticion)
 * @param {*} res  : response (respuesta)
 */
const getReplacementManage = async (req, res) => {
    if (await replacementModel.verifyPermit(req.user.id_user, 38)) {
        const replacementList = await replacementModel.getListReplacement();
        const categoryReplacementList = await replacementModel.getCategoryReplacement();
        res.render('WarehouseManagement/ReplacementManage/replacement_manage', {
            replacementList,
            categoryReplacementList
        });
    } else {
        req.flash('error_msgs', 'Not Authorized.');
        res.redirect(`/warehouse_management/replacement_manage`);
    }
}

/**
 * method PUT : update status of the replacement to enable or disable
 * @param {*} req : request (peticion)
 * @param {*} res  : response (respuesta)
 */
const enableDisableReplacement = async (req, res) => {
    const status = await replacementModel.enableDisableReplacement(req.params.codReplacement);
    if (status) {
        if (await externalLibrary.writeBackLog(req.user.username, `status changed of the replacement cod: ${req.params.codReplacement}`, req.device.type)) {
            console.log('write to bitacora successfully');
        } else {
            console.log('problmes to write bitacora');
        }
        req.flash('success_msg', `The status of the replacement was changed`);
        res.redirect(`/warehouse_management/replacement_manage`);
    } else {
        if (await externalLibrary.writeBackLog(req.user.username, `problems to changes status of the replacement cod: ${req.params.codReplacement}`, req.device.type)) {
            console.log('write to bitacora successfully');
        } else {
            console.log('problmes to write bitacora');
        }
        req.flash('error_msgs', 'Error! Problems to change status.');
        res.redirect(`/warehouse_management/replacement_manage`);
    }
}

/**
 * method POST : register replacement to database expresssi2
 * @param {*} req : request (peticion)
 * @param {*} res  : response (respuesta)
 */
const postReplacement = async (req, res) => {
    const { description_replacement, id_category } = req.body;
    if (await replacementModel.verifyPermit(req.user.id_user, 36)) {
        if (await replacementModel.registerReplacement(description_replacement, id_category)) {
            if (await externalLibrary.writeBackLog(req.user.username, `Registered the replacement ${description_replacement}`, req.device.type)) {
                console.log('Registered the spare');
            } else {
                console.log('Replacement not registerd')
            }
        } else {
            req.flash('error_msgs', 'Error! Existing replacement.');
            res.redirect(`/warehouse_management/replacement_manage`);
        }
        req.flash('success_msg', 'Remplacement successfully registered.');
        res.redirect(`/warehouse_management/replacement_manage`);
    } else {
        req.flash('error_msgs', 'Error! Not authorized.');
        res.redirect(`/warehouse_management/replacement_manage`);
    }
}
/**
 * method POST : register replacement to database expresssi2
 * @param {*} req : request (peticion)
 * @param {*} res  : response (respuesta)
 */
const getUpdateReplacement = async (req, res) => {
    var idUser = req.user.id_user;
    if (await replacementModel.verifyPermit(idUser, 39)) {
        const dataReplacement = await replacementModel.getDataReplacement(req.params.cod_replacement);
        const categoryReplacementList = await replacementModel.getCategoryReplacement(req.params.cod_replacement);
        res.render('WarehouseManagement/ReplacementManage/update_replacement', {
            dataReplacement,
            categoryReplacementList
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/replacement_manage');
    }
}

/**
 * method POST : register replacement to database expresssi2
 * @param {*} req : request (peticion)
 * @param {*} res  : response (respuesta)
 */
const putUpdateReplacement = async (req, res) => {
    const { cod_replacement } = req.params;
    const { description_replacement, description_category } = req.body;
    if (await replacementModel.update_replacement(cod_replacement, description_replacement, description_category)) {
        if (await externalLibrary.writeBackLog(req.user.username, `Update Replacement cod: ${req.params.cod_replacement}`, req.device.type)) {
            console.log('write in backlog');
        } else {
            console.log('problems in write backlog');
        }
        req.flash('success_msg', 'Replacement updated Successfully');
        res.redirect('/warehouse_management/replacement_manage');
    } else {
        req.flash('error_msgs', 'Error! Replacement Not update, Already exists');
        res.redirect('/warehouse_management/replacement_manage');
    }

}

const getReportReplacementTendency = async (req, res) => {
    const listReport = await replacementModel.getReportReplacementTendency();
    res.render('WarehouseManagement/ReplacementManage/replacement_report',{
        list_report : listReport
    });
}

const getReportReplacementSpecific = async (req, res) => {
    const reportReplacement = await replacementModel.getReportReplacement(req.params.replacement);
    res.render(`WarehouseManagement/ReplacementManage/replacement_report_tendency`,{
        report_tendency : reportReplacement
    });
}

module.exports = {
    getReplacementManage,
    enableDisableReplacement,
    postReplacement,
    getUpdateReplacement,
    putUpdateReplacement,
    getReportReplacementTendency,
    getReportReplacementSpecific
}
