const fs = require('fs');
const helpers = require('../../../helpers/address');
const workshopModel = require('../../../models/WarehouseManagement/WorkshopManage/workshopManage.model');
const externalLibrary = require('../../../config/externalLibrary');

const getWorkshop = async (req, res) => {
    const getListWorkshop = await workshopModel.getListWorkshop();
    console.log(getListWorkshop);
    res.render('WarehouseManagement/WorkshopManage/workshop_manage', {
        getListWorkshop
    });

}

const postWorkshop = async (req, res) => {
    var idUser = req.user.id_user;
    const { name, address, latitude, longitude } = req.body;
    if (await workshopModel.userHavePrivilege(idUser, 32)) {
        if (await workshopModel.insertWorkshop(name, address, latitude, longitude)) {
            if (await externalLibrary.writeBackLog(req.user.username, `register workshop ${name}, address: ${address}`, req.device.type)) {
                console.log('write register workshop successfully');
            } else {
                console.log('write register workshop failure');
            }
            req.flash('success_msg', 'Workshop add Successfully');
            res.redirect('/warehouse_management/workshop_manage');
        } else {
            req.flash('error_msgs', 'Error! Workshop already exist.');
            res.redirect('/warehouse_management/workshop_manage');
        }
    } else {
        req.flash('error_msgs', 'Error, Not autorized');
        res.redirect('/warehouse_manage/workshop_manage');

    }
}

const getWarehouseInventory = async (req, res) => {
    if (await workshopModel.userHavePrivilege(req.user.id_user, 43)) {
        var codWorkshop = req.params.id;
        var listWarehouseInventory = await workshopModel.getListWarehouseInventory(codWorkshop);
        let workshopName = await workshopModel.getQuery(`select w."name" from workshop w where w.cod_workshop =${codWorkshop};`);
        workshopName = workshopName[0].name;
        res.status(200).render('WarehouseManagement/WorkshopManage/warehouse_inventory_manage', {
            codWorkshop, listWarehouseInventory, workshopName
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/workshop_manage');
    }
}
/**
 * METHOD POST : Generate PDF file for warehouse inventory
 * @param {*} req : request
 * @param {*} res  : response
 */
const generatePDF = async (req, res) => {
    let fileName = await generateInventory(req.user.username, req.params.id);
    let filePath = await `${helpers.addressTheReport}${fileName}`;
    console.log(filePath);
    setTimeout(function () {
        fs.readFile(filePath, function (err, data) {
            console.log("The PDF File does exists");
            res.contentType("application/pdf");
            res.status(200).send(data);
        });
    }, 7000);
}

/**
 * Drawn the pdf file in ../security/bitacora.pdf
 * @param {string} username : username
 */
const generateInventory = async (username, codWorkshop) => {
    const data = await workshopModel.getListWarehouseInventory(codWorkshop);
    let workshopName = await workshopModel.getQuery(`select w."name" from workshop w where w.cod_workshop =${codWorkshop};`);
    workshopName = workshopName[0].name;
    let dataString = `<tbody>`;
    for (let index = 0; index < data.length; index++) {
        dataString = await dataString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${data[index].cod_replacement}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${data[index].description_category}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${data[index].description_replacement}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${data[index].quantity}</td>
        </tr>`;
    }
    dataString = dataString + `</tbody>`;
    let fileName = `warehouseInventory.pdf`;
    let contentHTML = await `
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Bitacora RestTeam</title>
        </head>
        <body>
            <div id="pageHeader">
                <h5 style="text-align: center;"><b>Inventario de Repuestos del Taller: ${workshopName}</b></h5>
            </div>
            <img src="logoOficial.PNG" />
            <div style="padding: 3%;">
                <table style="border: 1px solid black; border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;">Cod</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Categoria</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Repuesto</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Cantidad</th>
                        </tr>
                    </thead>
                    ${dataString}
                </table>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </div>
        </body>
        </html>`;
    await externalLibrary.generatePDFReport(contentHTML, fileName);
    return await fileName;
}


const getUpdateWorkshop = async (req, res) => {
    const cod_workshop = req.params.cod_workshop;
    if (await workshopModel.userHavePrivilege(req.user.id_user, 34)) {
        const workshopRow = await workshopModel.getDataWorkshop(req.params.cod_workshop);
        res.render('WarehouseManagement/WorkshopManage/update_workshop', {
            workshopRow, cod_workshop
        });
    } else {
        req.flash('error_msgs', 'Error! Not autorized');
        res.redirect('/warehouse_management/workshop_manage');
    }
}

const putUpdateWorkshop = async (req, res) => {
    if (await workshopModel.userHavePrivilege(req.user.id_user, 34)) {
        const { name, address, latitude, longitude } = req.body;
        if (await workshopModel.updateDataWorkshop(req.params.cod_workshop, name, address, latitude, longitude)) {
            if (await externalLibrary.writeBackLog(req.body.cod_workshop, `Update workshop ${name}, ${address}`, req.device.type)) {
                console.log('workshop update successfully');
            } else {
                console.log('workshop update failure');
            }
            req.flash('success_msg', 'Workshop updated Successfully');
            res.redirect('/warehouse_management/workshop_manage');
        } else {
            req.flash('error_msgs', 'Error! WorkShop not update. Already exists');
            res.redirect('/warehouse_management/workshop_manage');
        }
    } else {
        req.flash('error_msgs', 'Error! Not autorized');
        res.redirect('/warehouse_management/workshop_manage');
    }
}

const getStockMinimalWorkshop = async (req, res) => {
    const stockMinimal = await workshopModel.getStockMinimalOfWorkshop(req.params.cod);
    console.log(stockMinimal);
    res.render('WarehouseManagement/WorkshopManage/stock_minimal_replacement_workshop',{
        list_report : stockMinimal,
        cod_workshop : req.params.cod
    });
}

module.exports = {
    getWorkshop,
    postWorkshop,
    getWarehouseInventory,
    generatePDF,
    getUpdateWorkshop,
    putUpdateWorkshop,
    getStockMinimalWorkshop
}
