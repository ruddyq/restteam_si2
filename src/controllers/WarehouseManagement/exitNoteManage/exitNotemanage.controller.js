const fs = require('fs');
const externalLibrary = require('../../../config/externalLibrary');
const exitNoteModel = require('../../../models/WarehouseManagement/ExitNoteManage/exitNoteManage.model');
const helpers = require('../../../helpers/address');
/**
 * method GET : get view exit note and its exit notes list
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getExitNote = async (req, res) => {
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 24)) {
        const technicalList = await exitNoteModel.getTechnicalAvailable();
        const workshopList = await exitNoteModel.getWorkshopList();
        const exitNotesList = await exitNoteModel.getListNotesReport();
        res.render('WarehouseManagement/ExitNoteManage/exit_note_manage', {
            technicalList,
            workshopList,
            exitNotesList
        });
    } else {
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/');
    }
};

/**
 * method POST : register exit note and save in database
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postExitNoteRegister = async (req, res) => {
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 23)) {
        const { technical, date_note, workshop, description } = req.body;
        const idExitNote = await exitNoteModel.exitNoteRegister(date_note, description, technical, workshop);
        if (idExitNote > 0) {
            if (externalLibrary.writeBackLog(req.user.username, `registro nota de salida:  ${idExitNote} data: [${date_note},${description}, ${technical}, ${workshop}]`, req.device.type)) {
                console.log('Registrado Exitosamente!');
            } else {
                console.log('Fallo en el Registro de Bitacora');
            }
            req.flash('success_msg', `La nueva nota de salida fue registrado exitosamente. nota: ${idExitNote}`);
            res.redirect(`/warehouse_management/exit_note_manage`);
        } else {
            if (externalLibrary.writeBackLog(req.user.username, `Problemas para registrar Nota de Salida`, req.device.type)) {
                console.log('Registrado en la bitacora: no puede registrar nota');
            } else {
                console.log('Error para registrar en la bitacora');
            }
            req.flash('error_msgs', 'Error! Registro de la nota de salida tiene problemas');
            res.redirect(`/warehouse_management/exit_note_manage`);
        }
    } else {
        req.flash('error_msgs', 'No Autorizado para registrar');
        res.redirect(`/warehouse_management/exit_note_manage`);
    }
}

/**
 * method GET : get view for update exit note
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getUpdateExitNote = async (req, res) => {
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 25)) {
        const exitNoteData = await exitNoteModel.getExitNoteData(req.params.note_no);
        const technicalList = await exitNoteModel.getTechnicalListToAssignExitNote(req.params.note_no);
        res.render('WarehouseManagement/ExitNoteManage/exit_note_update', {
            exitNoteData,
            technicalList
        });
    } else {
        req.flash('error_msgs', 'No autorizado para actualizar notas');
        res.redirect(`/warehouse_management/exit_note_manage`);
    }
}

/**
 * method PUT : update exit note detail
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putUpdateExitNote = async (req, res) => {
    const { note_no } = req.params;
    const { id_technical, description } = req.body;
    if (await exitNoteModel.updateDataExitNote(note_no, description, id_technical)) {
        req.flash('success_msg', `nota de Salida Actualizado`);
        await externalLibrary.writeBackLog(req.user.username, `Actualizar Salida nota: ${note_no} (id technical: ${id_technical} and its description)`, req.device.type);
    } else {
        await externalLibrary.writeBackLog(req.user.username, `problemas para actualizar nota: ${note_no}`, req.device.type);
        req.flash('error_msgs', `Error! Salida no Actualizado ${note_no}.`);
    }
    res.redirect(`/warehouse_management/exit_note_manage`);
}

/**
 * method GET : get view of the exit note detail
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getExitNoteDetail = async (req, res) => {
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 28)) {
        const replacementList = await exitNoteModel.getListReplacementAvailableInWorkshop(req.params.note_no);
        const exitNoteDetail = await exitNoteModel.getListExitNoteDetail(req.params.note_no);
        res.render('WarehouseManagement/ExitNoteManage/exit_note_detail_manage', {
            note_no: req.params.note_no,
            replacementList,
            exitNoteDetail
        });
    } else {
        req.flash('error_msgs', 'Not Autorizado para visualizar los detalles de nota de salida');
        res.redirect(`/warehouse_management/exit_note_manage`);
    }
}

/**
 * method POST : register exit note detail and see to exit note detail list
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const registerExitNoteDetail = async (req, res) => {
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 27)) {
        const { note_no } = req.params;
        const { cod_replacement, quantity } = req.body;
        if (cod_replacement==-1) {
            req.flash('error_msgs', `Error, repuesto no valido`);
            res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
        }else{
            const idExitNoteDetail = await exitNoteModel.registerExitNoteDetail(note_no, cod_replacement, quantity);
            if (idExitNoteDetail === -1) {
                if (await externalLibrary.writeBackLog(req.user.username, `Insuficientes partes ${cod_replacement} stock in warehouse (${note_no})`, req.device.type)) {
                    console.log('escrito en la bitacora correctamente');
                } else {
                    console.log('Problemas paraescribir en bitacora');
                }
                req.flash('error_msgs', `Insuficiente stock en almacen.`);
                res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
            } else {
                if (await externalLibrary.writeBackLog(req.user.username, `crear nuevo detalle de nota (${note_no} detalle No. ${note_no})`, req.device.type)) {
                    console.log('write bitacora successfully');
                } else {
                    console.log('Problems to write bitacora');
                }
                req.flash('success_msg', `El detalle de la nota de graduación se registró correctamente. nuevo id detalle: ${idExitNoteDetail}`);
                res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
            }
        }
    } else {
        req.flash('error_msgs', 'No autorizado para registrar detalle de nota de salida');
        res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
    }
}

/**
 * method DELETE : delete detail exit note and updating stock in warehouse inventory and spare box
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const deleteDetailOfTheExitNote = async (req, res) => {
    const { note_no, id_detail } = req.params;
    if (await exitNoteModel.verifyPermitUser(req.user.id_user, 29)) {
        const responseType = await exitNoteModel.deleteDetailOfTheExitNote(note_no, id_detail);
        if (responseType === 1) {
            req.flash('success_msg', `Detalle eliminado correctamente. Técnico sin deuda de los respectivos repuestos y inventario de almacén del taller actualizado`);
            if (await externalLibrary.writeBackLog(req.user.username, `eliminar detalle de nota: ${id_detail} in note No. ${note_no}`, req.device.type)) {
                console.log('escrito en la bitacora correctamente');
            } else {
                console.log('problema para registrar detalle de salida');
            }
        } else if (responseType === 2) {
            if (await externalLibrary.writeBackLog(req.user.username, `eliminar detalle de nota de salida ${id_detail} en nota No. ${note_no}`)) {
                console.log('realizado con exito');
            } else {
                console.log('problemas para registrar detalle de nota de salida');
            }
            req.flash('success_msg', `Detail deleted correctly. Updated spare box of the technical and warehouse inventory of workshop updated`);
        } else {
            if (await externalLibrary.writeBackLog(req.user.username, `problems for delete exit note detail: ${id_detail} in note No. ${note_no}`)) {
                console.log('realizado con exito');
            } else {
                console.log('problems para registrar en nota de salida');
            }
            req.flash('error_msgs', `Poblemas para registrar detalle de nota de salida`);
        }
        res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
    } else {
        req.flash('error_msgs', 'No autorizado para eliminar detallesde nota de salida');
        res.redirect(`/warehouse_management/exit_note_manage/exit_note_detail/${note_no}`);
    }
}

const generateExitNote = async (noteNo, username) => {
    const exitNoteData = await exitNoteModel.getExitNotePrincipalData(noteNo);
    const exitNoteDetailData = await exitNoteModel.getExitNoteDetailData(noteNo);
    let detailExitNoteString = `<tbody>`;
    for (let index = 0; index < exitNoteDetailData.length; index++) {
        detailExitNoteString = await detailExitNoteString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${exitNoteDetailData[index].id_note_detail}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${exitNoteDetailData[index].replacement}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${exitNoteDetailData[index].quantity}</td>
        </tr>`;
    }
    detailExitNoteString = detailExitNoteString + `</tbody>`;
    let fileName = `ExitNote-No#${noteNo}.pdf`;
    let contentHTML = await `
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Exit Note${noteNo}</title>
        </head>
        <body>
            <div id="pageHeader">
                <h5 style="text-align: center;"><b>Nota de Salida del Almacén: ${exitNoteData.name}</b></h5>
            </div>
            <h2 style="text-align: center;">Nota de Salida</h2>
            <div style="padding: 5%;">
                <p>
                    <b>Nro. Nota de Salida:</b> ${exitNoteData.note_no}<br>
                    <b>Fecha de Emisión: </b> ${exitNoteData.note_date}<br>
                    <b>Descripción: </b> ${exitNoteData.description}<br>
                    <b>Nombre Técnico Recibe: </b> ${exitNoteData.technical_name}<br>
                    <b>Cantidad de Repuestos a Ingresar: </b> ${exitNoteData.count_replacement}<br>
                    <b>Nombre de Almacén: </b> ${exitNoteData.name} <br>
                    <br>
                    <br>
                </p>
                <table style="border: 1px solid black; border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;">#</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Repuesto</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Cantidad</th>
                        </tr>
                    </thead>
                    ${detailExitNoteString}
                </table>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </div>
        </body>
        </html>`;
    await externalLibrary.generateExitNotePDF(contentHTML, fileName);
    return await fileName;
}

const generateExitNotePDFFile = async (req, res) => {
    var fileName = await generateExitNote(req.params.note_no, req.user.username);
    let filePath = await `${helpers.addressExitNote}${fileName}`;
    setTimeout(function(){
        fs.readFile(filePath, function (err, data) {
            console.log("The PDF File does exists");
            res.contentType("application/pdf");
            res.send(data);
        });
    },5000);
}

module.exports = {
    getExitNote,
    postExitNoteRegister,
    getUpdateExitNote,
    getExitNoteDetail,
    registerExitNoteDetail,
    deleteDetailOfTheExitNote,
    putUpdateExitNote,
    generateExitNotePDFFile
};