const entryNoteModel = require('../../../models/WarehouseManagement/EntryNoteManage/entryNoteManage.model');
const externalLibrary = require('../../../config/externalLibrary');
/**
 * Method GET: show view all Entry Notes
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getEntryNote = async (req, res) => {
    var idUser = req.user.id_user;
    if(await entryNoteModel.userHavePrivilege(idUser, 24)){
        const listEntryNotes = await entryNoteModel.getListEntryNotes();
        const workshop = await entryNoteModel.getQuery("select cod_workshop,name from workshop");
        const provider = await entryNoteModel.getQuery("select p.provider_name, p.nit_ci from provider p;");
        res.status(200).render('WarehouseManagement/EntryNoteManage/entry_note_manage', {
           listEntryNotes,workshop,provider
        });
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/');
    } 
}

/**
 * Method POST: Process new Company to database expresssi2
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postEntryNote = async (req, res) => {
    const {date_note, description, cod_workshop, nit_ci} = req.body;  
    var idUser = req.user.id_user;

    if(await entryNoteModel.userHavePrivilege(idUser, 23)){
        var incomeNo = await entryNoteModel.insertEntryNote(date_note,description,cod_workshop, nit_ci);
        if(incomeNo>0){
            if( await externalLibrary.writeBackLog(req.user.username, `register entry note ${incomeNo} with N° ${date_note}, ${description}, cod workshop: ${cod_workshop}, nit/ci provider: ${nit_ci}`, req.device.type) ){
                console.log('write register entry note successfully');
            } else {
                console.log('write register entry note failure');
            }
    
            req.flash('success_msg', 'Entry Note added Successfully');
            res.redirect('/warehouse_management/entry_note_manage');

        }else{
            var workshop_name = await entryNoteModel.getQuery(`select name from workshop where workshop.cod_workshop=${cod_workshop}`);
            var errors =[];
            errors.push({ text: "I'm sorry, your data don't was saved." });
            var workshop = await entryNoteModel.getQuery("select cod_workshop,name from workshop");
            res.status(200).render('WarehouseManagement/EntryNoteManage/entry_note_manage', {
               date_note,
               workshop_name,
               description,
               workshop,
               nit_ci,
               errors
            });
        }
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/entry_note_manage');
    }

}

/**
 * METHOD GET : get view for update a entry note
 * @param {*} req 
 * @param {*} res 
 */ 
const getUpdateEntryNote = async (req, res) => {
    
    if (await entryNoteModel.userHavePrivilege(req.user.id_user, 25)) {
        const entryNoteData = await entryNoteModel.getEntryNoteUpdateData(req.params.id);
        res.status(200).render('WarehouseManagement/EntryNoteManage/entry_note_update',{
            entryNoteData
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/entry_note_manage');
    }
}

const putUpdateEntryNote = async (req, res) => {
    const {description} = req.body;
    var idUser = req.user.id_user;
    if(await entryNoteModel.userHavePrivilege(idUser, 25)){    
       if(await entryNoteModel.timeElapsed(req.params.id)>2){
          req.flash('error_msgs', 'Error! Time is Over You only have 2 days for edit this!');
          res.redirect('/warehouse_management/entry_note_manage');
       }else{
          if(await entryNoteModel.updateEntryNote(req.params.id,description)){
            if (await externalLibrary.writeBackLog(req.user.username, `Update Entry Note with Income N°: ${req.params.id} (${description})`, req.device.type)) {
                console.log('write in backlog');
            } else {
               console.log('problems in write backlog');
            }
            req.flash('success_msg', 'Entry Note updated Successfully');
            res.redirect('/warehouse_management/entry_note_manage');
         }else{
            req.flash('error_msgs', 'Error! Sorry your data does not updated!');
            res.redirect('/warehouse_management/entry_note_manage');
         }

       } 
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/entry_note_manage');
    }
    
    
}

/* -------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------Methods For Income Detail---------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------*/ 

/**
 * Method GET: show view all details of a specific Entry Note
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getIncomeDetail = async (req, res) => {
    if(await entryNoteModel.userHavePrivilege(req.user.id_user, 28)){
        var incomeNo = req.params.id;
        var listIncomeDetail = await entryNoteModel.getListIncomeDetail(incomeNo);
        var listReplacement = await entryNoteModel.getListReplacement(incomeNo);
        var b = true;
        if(listReplacement.length===0){
            b = false;
        }
        res.status(200).render('WarehouseManagement/EntryNoteManage/income_detail_manage', {
            listIncomeDetail, listReplacement,incomeNo, b
        });
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/warehouse_management/entry_note_manage');
    }
}

/**
 * Method POST: Process new Company to database expresssi2
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postIncomeDetail = async (req, res) => {
    const {replacement, quantity } = req.body;
    var idUser = req.user.id_user;
    var incomeNo = req.params.id;
    if(await entryNoteModel.userHavePrivilege(idUser, 27)){
        if(await entryNoteModel.timeElapsed(incomeNo)>2){
            req.flash('error_msgs', 'Error! Time is Over You only have 2 days for add new detail!');
            res.redirect('/warehouse_management/entry_note_manage');
        }else{
            var idIncomeDetail = await entryNoteModel.insertIncomeDetail(incomeNo, replacement, quantity);
            if(idIncomeDetail>0){
                if( await externalLibrary.writeBackLog(req.user.username, `register income detail with Entry Note N° ${incomeNo},${idIncomeDetail}, ${replacement}, ${quantity}`, req.device.type) ){
                    console.log('write register income detail successfully');
                } else {
                    console.log('write register income detail failure');
                }
        
                req.flash('success_msg', 'Income Detail added Successfully');
                res.redirect(`/warehouse_management/entry_note_manage/income_detail/${incomeNo}`);
            } else {
                var errors =[];
                errors.push({ text: "I'm sorry, your data don't was saved." });
                var listReplacement = await entryNoteModel.getListReplacement(incomeNo);
                res.status(200).render('WarehouseManagement/EntryNoteManage/income_detail_manage', {
                    replacement,
                    listReplacement,
                    quantity,
                    errors
                });
            }


        }

    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect(`/warehouse_management/entry_note_manage/income_detail/${incomeNo}`);
    }
}

const deleteIncomeDetail = async (req, res) => {
    const {incomeNo} = req.body;
    var idUser = req.user.id_user;
    if(await entryNoteModel.userHavePrivilege(idUser, 29)){
       if(await entryNoteModel.timeElapsed(incomeNo)>2){
          req.flash('error_msgs', 'Error! Time is Over You only have 2 days for deleted this!');
          res.redirect('/warehouse_management/entry_note_manage');
       }else{
          if(await entryNoteModel.deleteIncomeDetail(incomeNo, req.params.id)){
            if (await externalLibrary.writeBackLog( req.user.username, `Delete Detail Entry Note with Income N°: ${incomeNo} and id detail: (${req.params.id})`, req.device.type)) {
                console.log('write delete income detail in backlog');
            } else {
               console.log('problems delete income detail in write backlog');
            }
            req.flash('success_msg', 'Income Detail deleted Successfully');
            res.redirect(`/warehouse_management/entry_note_manage/income_detail/${incomeNo}`);
         }else{
            req.flash('error_msgs', 'Error! Sorry your data does not updated!');
            res.redirect(`/warehouse_management/entry_note_manage/income_detail/${incomeNo}`);
         }

       } 
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect(`/warehouse_management/entry_note_manage/income_detail/${incomeNo}`);
    }
    
    
}


module.exports = {
    getEntryNote,
    postEntryNote,
    getUpdateEntryNote,
    putUpdateEntryNote,
    getIncomeDetail,
    postIncomeDetail,
    deleteIncomeDetail
}