const replacementCategoryModel = require('../../../models/WarehouseManagement/replacementCategoryManage/replacementCategoryManage.model');
const externalLibrary = require('../../../config/externalLibrary');

const getReplacementCategory = async (req, res) => {
    var idUser = req.user.id_user;
    if (await replacementCategoryModel.userHavePrivilege(idUser, 113)){
        const replacementCategoryList = await replacementCategoryModel.getReplacementCategory();
        res.render('WarehouseManagement/ReplacementCategoryManage/replacement_category_manage', {
            replacementCategoryList
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado');
        res.redirect('/warehouse_management/replacement_category_manage');
    }
}

const postReplacementCategory = async (req, res) => {
    var idUser = req.user.id_user;
    if (await replacementCategoryModel.userHavePrivilege(idUser, 114)){
        const {description_replacement_category} = req.body;
        if (await replacementCategoryModel.insertReplacementCategory(description_replacement_category)) {
            if( await externalLibrary.writeBackLog(req.user.username, `Replacement Category register:  ${description_replacement_category}`, req.device.type)){
                console.log('Escrito en la bitacora exitosamente');
            } else {
                console.log('Fallo al escribir en la bitacora');
            }
            req.flash('success_msg', 'Categoria de Repuesto añadido exitosamente');
            res.redirect('/warehouse_management/replacement_category_manage');
        } else {
            req.flash('error_msgs', 'Error! Categoria de Repuesto ya existe');
            res.redirect('/warehouse_management/replacement_category_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! No Autorizado');
        res.redirect('/warehouse_management/replacement_category_manage');
    }
}

const getUpdateReplacementCategory = async (req, res) => {
    var idUser = req.user.id_user;
    if (await replacementCategoryModel.userHavePrivilege(idUser, 115)){
        const replacementCategData = await replacementCategoryModel.getReplacementCategoryUpdateData(req.params.id_category);
        const id_category = req.params.id_category;
        
        res.render('WarehouseManagement/ReplacementCategoryManage/replacement_category_update',{
            replacementCategData,
            id_category
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado');
        res.redirect('/warehouse_management/replacement_category_manage');
    }
}

const putUpdateReplacementCategory = async (req, res) => {
    const {description_category} = req.body;
    if ( await replacementCategoryModel.updateReplacementCategory(req.params.id_category, description_category)){
        if (await externalLibrary.writeBackLog(req.user.username, `Repuesto de Categoria actualizado Id: ${req.params.id_category} (${description_category})`, req.device.type)) {
            console.log('Registrado en la bitacora exitosamente');
        } else {
            console.log('Problmas para registrar en la bitacora');
        }
        req.flash('success_msg', 'Categoria de Repuesto Actualizado exitosamente');
        res.redirect('../replacement_category_manage');
    }else{
        req.flash('error_msgs', 'Error! Categoria de Repuesto no actualizada. Descripcion ya existente');
        res.redirect('/warehouse_management/replacement_category_manage');
    }
    
}


module.exports = {
    getReplacementCategory,
    postReplacementCategory,
    getUpdateReplacementCategory,
    putUpdateReplacementCategory
}