const fs = require('fs');
const devolutionNoteModel = require('../../../models/WarehouseManagement/devolutionNoteManage/devolutionNoteManage.model');
const externalLibrary = require('../../../config/externalLibrary');
const helpers = require('../../../helpers/address');


const getDevolutionNote = async (req, res) => {
    var idUser = req.user.id_user;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 24)){
        const DevolutionNotesList = await devolutionNoteModel.getListNotesReport();
        const listTechnical = await devolutionNoteModel.getTechnicalList();
        const workshop = await devolutionNoteModel.getWorkshopList();
        res.render('WarehouseManagement/DevolutionNoteManage/devolution_note_manage', {
            DevolutionNotesList,
            listTechnical,
            workshop
        })
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/warehouse_management/devolution_note_manage');
    }
}

const postDevolutionNote = async (req, res) => {
    var idUser = req.user.id_user;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 23)) {
        const {note_date, technical, workshop, description } = req.body;
        const idDevolution = await devolutionNoteModel.registerDevolutionNote(note_date, description, technical, workshop);
        if (idDevolution > 0) {
            if (externalLibrary.writeBackLog(req.user.username, `registro nota de devolucion: ${idDevolution} data: [${note_date}]`, req.device.type)) {
                console.log('Registrado Exitosamente!');
            } else {
                console.log('Fallo en el Registro de Bitacora');
            }
            req.flash('success_msg', `La nueva nota de devolucion fue registrado exitosamente. nota: ${idDevolution}`);
            res.redirect(`/warehouse_management/devolution_note_manage`);
        } else {
            if (externalLibrary.writeBackLog(req.user.username, `Problemas para registrar Nota de Devolucion `, req.device.type)) {
                console.log('Registrado en la bitacora: no puede registrar nota');
            } else {
                console.log('Error para registrar en la bitacora');
            }
            req.flash('error_msgs', 'Error! Registro de la nota de devolucion tiene problemas');
            res.redirect(`/warehouse_management/devolution_note_manage`);
        }
    } else {
        req.flash('error_msgs', 'No Autorizado para registrar');
        res.redirect(`/warehouse_management/devolution_note_manage`);
    }
}

const getUpdateDevolutionNote = async (req, res) => {
    var idUser = req.user.id_user;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 25)) {
        const devolutionNoteData = await devolutionNoteModel.getDevolutionNoteData(req.params.note_no);
        const technicalList = await devolutionNoteModel.getTechnicalListforDevolutionNote(req.params.note_no);
        res.render('WarehouseManagement/DevolutionNoteManage/devolution_note_update', {
            devolutionNoteData,
            technicalList
        });
    } else {
        req.flash('error_msgs', 'No autorizado para actualizar notas');
        res.redirect(`/warehouse_management/devolution_note_manage`);
    }
}

const putUpdateDevolutionNote = async (req, res) => {
    const {note_no} = req.params;
    const {id_technical, description} = req.body;
    if (await devolutionNoteModel.updateDataDevolutionNote(note_no, description, id_technical)) {
        req.flash('success_msg', `nota de Devolucion Actualizado`);
        await externalLibrary.writeBackLog(req.user.username, `Actualizar Devolucion nota: ${note_no}`, req.device.type);
    } else {
        await externalLibrary.writeBackLog(req.user.username, `problemas para actualizar nota: ${note_no}`, req.device.type);
        req.flash('error_msgs', `Error! Devolucion no Actualizado`);
    }
    res.redirect(`/warehouse_management/devolution_note_manage`);
}

const getDevolutionNoteDetail = async (req, res) => {
    var idUser = req.user.id_user;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 28)) {
        const replacementList = await devolutionNoteModel.getListReplacementOfTheTechnical(req.params.note_no);
        const devolutionNoteDetail = await devolutionNoteModel.getListDetailNoteDetail(req.params.note_no);
        res.render('WarehouseManagement/DevolutionNoteManage/devolution_note_detail_manage', {
            note_no: req.params.note_no,
            replacementList,
            devolutionNoteDetail
        });
    } else {
        req.flash('error_msgs', 'No Autorizado para visualizar los detalles de nota de devolucion');
        res.redirect(`/warehouse_management/devolution_note_manage`);
    }
}
const postDevolutionNoteDetail = async (req, res) => {
    var idUser = req.user.id_user;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 27)) {
        const {note_no} = req.params;
        const {cod_replacement, quantity} = req.body;
        const idNoteDetail = await devolutionNoteModel.registerDevolutionNoteDetail(note_no, cod_replacement, quantity);
        if (idNoteDetail === -1) {
            if (await externalLibrary.writeBackLog(req.user.username, `Insuficiente stock en la caja de repuesto del tecnico:(${note_no})`, req.device.type)) {
                console.log('Registrado en la Bitacora');
            } else {
                console.log('Problemas para registrar en la Bitacora');
            }
            req.flash('error_msgs', `Insuficiente stock en la caja de repuesto del tecnico`);
            res.redirect(`/warehouse_management/devolution_note_manage/devolution_note_detail_manage/${note_no}`);
        } else {
            if (await externalLibrary.writeBackLog(req.user.username, `Adicionado en la nueva nota de devolucion: (${note_no})`, req.device.type)) {
                console.log('Registrado en la Bitacora');
            } else {
                console.log('Problemas para registrar en la Bitacora');
            }
            req.flash('success_msg', `Los detalles fueron adicionados exitosamente ! Nuevo Detalle:: ${idNoteDetail}`);
            res.redirect(`/warehouse_management/devolution_note_manage/devolution_note_detail_manage/${note_no}`);
        }
    } else {
        req.flash('error_msgs', 'No Autorizado.');
        res.redirect(`/warehouse_management/devolution_note_manage/devolution_note_manage/${note_no}`);
    }
}

const deleteDetailDevolutionNote = async (req, res) => {
    var idUser = req.user.id_user;
    const {note_no, id_detail} = req.params;
    if (await devolutionNoteModel.userHavePrivilege(idUser, 29)) {
        const deleteDevolution = await devolutionNoteModel.deleteDetailDevolution(note_no, id_detail);
        if (deleteDevolution === 1) {
            req.flash('success_msg', `Detalle eliminado correctamente.`);
            if (await externalLibrary.writeBackLog(req.user.username, `Eliminar detalle de devolucion: ${id_detail} in note No. ${note_no}`, req.device.type)) {
                console.log('Registrado en la Bitacora: Eliminacion del detalle');
            } else {
                console.log('problemas para eliminar detalles de la nota de devolucion');
            }
        } else if (deleteDevolution === 2) {
            if (await externalLibrary.writeBackLog(req.user.username, `deleted devolution note detail: ${id_detail} in note No. ${note_no}`)) {
                console.log('Registrado en la Bitacora');
            } else {
                console.log('problemas para eliminar detalles de la nota de devolucion');
            }
            req.flash('success_msg', `Detalle eliminado correctamente.`);
        } else {
            if (await externalLibrary.writeBackLog(req.user.username, `problema para eliminar detalle de la nota de Devolucion: ${id_detail} in note No. ${note_no}`)) {
                console.log('Registrado en la Bitacora');
            } else {
                console.log('problemas para eliminar detalles de la nota de devolucion');
            }
            req.flash('error_msgs', `problemads para eliminar detalles de la nota de devolucion`);
        }
        res.redirect(`/warehouse_management/devolution_note_manage/devolution_note_detail_manage/${note_no}`);
    } else {
        req.flash('error_msgs', 'No Autorizado para eliminar detalles');
        res.redirect(`/warehouse_management/devolution_note_manage`);
    }
}

const generateNote = async (note_no, username) => {
    const noteData = await devolutionNoteModel.getNoteData(note_no);
    const detailData = await devolutionNoteModel.getDetailData(note_no);
    let detailNoteString = `<tbody>`;
    for (let index = 0; index < detailData.length; index++) {
        detailNoteString = await detailNoteString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${detailData[index].id_note_detail}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${detailData[index].replacement}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${detailData[index].quantity}</td>
        </tr>`;
    }
    detailNoteString = detailNoteString + `</tbody>`;
    let fileName = `DevolutionNote_No#${note_no}.pdf`;
    let contentHTML = await `
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Nota de Devolucion${note_no}</title>
        </head>
        <body>
            <br>
            <br>
            <h2 style="text-align: center;">Nota de Devolucion</h2>
            <div style="padding: 6%;">
                <p>
                    <b>Nro. Nota de Devolucion:</b> ${noteData.note_no}<br>
                    <b>Fecha Emitida: </b> ${noteData.note_date}<br>
                    <b>Tecnico Encargado: </b> ${noteData.technical_name}<br>
                    <b>Nombre de Almacen: </b> ${noteData.name} <br>
                    <b>Descripción: </b> ${noteData.description}<br>
                    <br>
                    <br>
                </p>
                <table style="border: 1px solid black; border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Nro</th>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Repuesto</th>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Cantidad</th>
                        </tr>
                    </thead>
                    ${detailNoteString}
                </table>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </div>
        </body>
        </html>`;
    await externalLibrary.generateNotePDF(contentHTML, fileName);
    return await fileName;
}

const generateNotePDFFile = async (req, res) => {
    var fileName = await generateNote(req.params.note_no, req.user.username);
    let filePath = await `${helpers.addressDevolutionNote}${fileName}`;
    setTimeout(function(){
        fs.readFile(filePath, function (err, data) {
            console.log("The PDF File does exists");
            res.contentType("application/pdf");
            res.send(data);
        });
    },5000);
}


module.exports = {
    getDevolutionNote,
    postDevolutionNote,
    getUpdateDevolutionNote,
    putUpdateDevolutionNote,
    getDevolutionNoteDetail,
    postDevolutionNoteDetail,
    deleteDetailDevolutionNote,
    generateNotePDFFile
};
