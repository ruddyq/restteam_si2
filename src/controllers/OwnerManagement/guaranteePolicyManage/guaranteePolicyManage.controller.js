const qrcode = require('qrcode');
const fs = require('fs');
const path = require('path');
const externalLibrary = require('../../../config/externalLibrary');
const guaranteePolicyModel = require('../../../models/OwnerManagement/guaranteePolicyManage/guaranteePolicyManage.model');

async function generateQR(dataContainer) {
    try {
        const QR = await qrcode.toDataURL(dataContainer, { version: 2 });
        return QR;
    } catch (error) {
        console.error(error);
        return null;
    }
}

async function sendEmailWithQR(codPolicy, host) {
    try {
        const ownerData = await guaranteePolicyModel.getDataOwner(codPolicy);
        htmlContentEmail = await `
        <div style="width:75%; margin:0px auto;">
            <div style="display:table-cell; vertical-align:middle; text-align: center;">
                <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px; background: #373B44;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to left, #4286f4, #373B44);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #4286f4, #373B44); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            ">
                    <img src="http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/img/logo_oficial.png"
                        alt="Logo Technical Support" style="border-radius: 5px 5px 0 0;">
                    <div style="padding: 2px 16px;" style="color: whitesmoke;">
                        <h2 style="color: whitesmoke;">Este es un mensaje de RestTeam</h2>
                        <h3 style="color: whitesmoke;">Estimado Cliente<br>
                            La poliza con codigo: ${codPolicy} del propietario ${ownerData.name} fue aceptado correctamente, a continuación este email tiene el objetivo de enviarle el codigo QR de seguridad y explicarle los pasos a seguir despues y en momento de solicitar el servicio de garantía:
                            <ul>
                                <li>Este codigo QR debe estar pegado a su dispositivo</li>
                                <li>Para solicitar su servicio tecnico con su garantpia, debe poder descargar la APP de RestTeam</li>
                                <li>Con la App podrá escanear el codigo QR pegado en su dispositivo y podrá solicitar el servicio tecnico correspondiente</li>
                                <li>Al enviar la solicitud, tenga paciencia en poder recibir la respuesta del taller correspondiente, si o si recibira una respuesta</li>
                                <li>OJO, esta garantía digital tiene la misma vigencia que la garantia entregada fisica, ¡no se pase de listo!</li>
                            </ul>
                            <br>
                            Si es que hay algun problema sobre esto, porfavor comuniquese con el equipo RestTeam
                            <a style="color: green;" href="http://${host}/rest_team/recovery/${codPolicy}"><i class="fa fa-link"> Enlace de Recuperación del código QR</i></a>
                            <br>
                            No respondas a este email, es un boot.
                        </h3>
                    </div>
                </div>
            </div>
        </div>`;
        await externalLibrary.sendEmail(htmlContentEmail, ownerData.email, 'Send QR Code for Guarantee Policy');
        return true;
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function generateQRtoPDFFile(codPolicy, QR, username) {
    try {
        const fileName = `Policy${codPolicy}.pdf`;
        const htmlContent = await `
            <!doctype html>
            <html>
            <head>
                <meta charset="utf-8">
                <title>QR code for Cod Policy ${codPolicy}</title>
            </head>
            <body>
                <div id="pageHeader">
                    <h5 style="text-align: center;"><b>Codigo QR para la poliza: ${codPolicy}</b></h5>
                </div>
                <div style="display: flex; justify-content: center; align-items: center;">
                <img src="${QR}">
                </div>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </body>
            </html>`;
        const filePath = await externalLibrary.generatePDFReport(htmlContent, fileName);
        return filePath;
    } catch (error) {
        console.error(error);
        return null;
    }
}

const getGuaranteePolicyList = async (req, res) => {
    if (await guaranteePolicyModel.getVerifyPermitService(req.user.id_user, 98)) {
        const guaranteePolicyList = await guaranteePolicyModel.getListGuaranteePolicy();
        res.render('OwnerManagement/guaranteePolicyManage/guarantee_policy_manage', {
            guaranteePolicyList
        });
    } else {
        req.flash('error_msgs', `Not Authorized. user ${req.user.username} don't have permit`);
        res.redirect(`/`);
    }
}

const postValidateGuaranteePolicy = async (req, res) => {
    try {
        if (await guaranteePolicyModel.getVerifyPermitService(req.user.id_user, 102) && req.params.state>-1 && req.params.state<3) {
            if (await guaranteePolicyModel.validateGuaranteePolicy(req.params.cod_policy, req.params.state)) {
                if (req.params.state==0) {
                    const dataOwner = await guaranteePolicyModel.getDataOwner(req.params.cod_policy);
                    contentHTML = `
                    <div style="width:75%; margin:0px auto;">
                        <div style="display:table-cell; vertical-align:middle;">
                            <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px; background: #373B44;  /* fallback for old browsers */
                        background: -webkit-linear-gradient(to left, #4286f4, #373B44);  /* Chrome 10-25, Safari 5.1-6 */
                        background: linear-gradient(to left, #4286f4, #373B44); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                        ">
                                <img text-align: center; src="http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/img/logo_oficial.png"
                                    alt="Logo Technical Support" style="border-radius: 5px 5px 0 0;">
                                <div style="padding: 2px 16px;" style="color: whitesmoke;">
                                    <h2 style="color: whitesmoke;">Este es un mensaje de RestTeam</h2>
                                    <h3 style="color: whitesmoke;">Estimado Cliente<br>
                                        La póliza de Garantía <b style="color: yellow;">${req.params.cod_policy}</b> fue rechazada debido a los siguientes puntos:
                                        <ul>
                                            <li> El comprobante de la garantía no es claro</li>
                                            <li> Los datos de la póliza no concuerdan con el comprobante</li>
                                        </ul>
                                        <br>
                                        Si es que hay algun problema sobre esto, porfavor comuniquese con el equipo RestTeam
                                        <br>
                                        No respondas a este email, es un boot.
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    await externalLibrary.sendEmail(contentHTML, dataOwner.email, `Póliza de Garantía ${req.params.cod_policy}`);
                    req.flash('success_msg', `Guarantee Policy ${req.params.cod_policy} was declined`);
                    res.redirect(`/owner_management/guarantee_policy_manage`);
                }else{
                    const loadDataToQR = await guaranteePolicyModel.getDataForLoadQR(req.params.cod_policy);
                    const QR = await generateQR(loadDataToQR);
                    if (!QR) {
                        req.flash('error_msgs', `Problems to generate QR code.`);
                        res.redirect(`/owner_management/guarantee_policy_manage`);
                    } else {
                        let fileName = await generateQRtoPDFFile(req.params.cod_policy, QR, req.user.username);
                        if (!fileName) {
                            console.log('error with generate PDF File');
                            req.flash('error_msgs', `Problems to generate PDF for QR code.`);
                            res.redirect(`/owner_management/guarantee_policy_manage`);
                        } else {
                            console.log("File: ",fileName);
                            if (await sendEmailWithQR(req.params.cod_policy, req.headers.host)) {
                                if (await externalLibrary.writeBackLog(req.user.username, `validate policy code: ${req.params.cod_policy}`, req.device.type)) {
                                    console.log('write to backlog successfully');
                                } else {
                                    console.log('problems to write to backlog');
                                }
                                setTimeout(function () {
                                    fs.readFile(fileName, function (err, data) {
                                        if (!err) {
                                            console.log("The PDF File was generated");
                                            res.contentType("application/pdf");
                                            res.send(data);
                                        }else{
                                            console.error("I have an error in guarantee policy");
                                        }
                                    });
                                }, 2000);
                            } else {
                                req.flash('error_msgs', `Problems to send email to owner, I'm sorry.`);
                                res.redirect(`/owner_management/guarantee_policy_manage`);
                            }
                        }
                    }
                }
            } else {
                req.flash('error_msgs', `You can't disable a guarantee Policy, it's validated.`);
                res.redirect(`/owner_management/guarantee_policy_manage`);
            }
        } else {
            req.flash('error_msgs', `Not Authorized. user ${req.user.username} don't have permit`);
            res.redirect(`/owner_management/guarantee_policy_manage`);
        }
    } catch (error) {
        req.flash('error_msgs', `I'm sorry. I'm have problmes to read data`);
        res.redirect(`/owner_management/guarantee_policy_manage`);
    }
}

const getRecoveryQRCode = async (req, res) => {
    const { policy_guarantee } = req.params;
    console.log(policy_guarantee);
    res.download(`${path.join(__dirname, '../../../../../security/report/Policy')}${policy_guarantee}.pdf`);
}

const getDataGuaranteePolicyRegister = async (req, res) => {
    const { cod_product_owner } = req.params;
    const companyList = await guaranteePolicyModel.getCompanyListAvailable();
    const dataCompany = await guaranteePolicyModel.getRegisterDataProductOwner(cod_product_owner);
    res.json({
        companyList,
        dataCompany
    });
}

/**Mobile */
const getPolicyList = async (req, res) => {
    const policyList = await guaranteePolicyModel.getListPolicy(req.params.ci);
    res.json({ policyList });
}

const postRegisterPolicy = async (req, res) => {
    console.log("entre a guarantee");
    const { policy_code, date_purchase, end_date, proof_no, nit_company, cod_product_owner } = req.body;
    const { filename } = req.file;
    console.log(policy_code, date_purchase, end_date, proof_no, nit_company, cod_product_owner, filename);
    const response = await guaranteePolicyModel.registerGuaranteePolicy(policy_code, date_purchase, end_date, proof_no, nit_company, cod_product_owner, filename);
    if (response) {
        console.log("true");
        const ownerData = await guaranteePolicyModel.getDataOwner(policy_code);
        if (await externalLibrary.writeBitacoraOwner(ownerData.name, `register guarantee policy code: ${policy_code}`)) {
            console.log("write to backlog Owner successfully");
        } else {
            console.log("problems write to backlog Owner");
        }
    }else{
        console.log("no registro");
    }
    res.json({
        'result': response
    });
}

module.exports = {
    getGuaranteePolicyList,
    postValidateGuaranteePolicy,
    getRecoveryQRCode,
    getPolicyList,
    getDataGuaranteePolicyRegister,
    postRegisterPolicy
}