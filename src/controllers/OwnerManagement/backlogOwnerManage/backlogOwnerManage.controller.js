/**
 *       Import Files dependents
 */
const fs = require('fs');
const helpers = require('../../../helpers/address');
const externalLibrary = require('../../../config/externalLibrary');
const ownerModel = require('../../../models/OwnerManagement/ownerManage/ownerManage.model');


/**
 * METHOD GET : get view bitacora manage
 * @param {*} req : request
 * @param {*} res  : response
 */
const getBacklogOwner = async (req, res) => {
    console.log("Email: "+req.params.email)
    let name = await ownerModel.getQuery(`select name from owner where email = '${req.params.email}';`);
    name = name[0].name;
    const bitacoraData = await externalLibrary.getListOwnerBitacora(name);
    if (await externalLibrary.writeBitacoraOwner(name, `Visit your Activities`)) {
        console.log('write bitacora successfully');
    } else {
        console.log('Problems to writing bitacora');
    }
    res.json({bitacoraData});
}

/**
 * METHOD POST : Generate PDF file for bitacora.json
 * @param {*} req : request
 * @param {*} res  : response
 */
const generatePDFFileBitacora = async (req, res) => {
    let fileName = await generateBitacora(req.user.username);
    let filePath = await `${helpers.addressTheReport}${fileName}`;
    console.log(filePath);
    setTimeout(function () {
        fs.readFile(filePath, function (err, data) {
            console.log("The PDF File does exists");
            res.contentType("application/pdf");
            res.status(200).send(data);
        });
    }, 7000);
}

/**
 * Drawn the pdf file in ../security/bitacora.pdf
 * @param {string} username : username
 */
const generateBitacora = async (username) => {
    const bitacoraData = await externalLibrary.getListBitacora();
    let bitacoraDataString = `<tbody>`;
    for (let index = 0; index < bitacoraData.length; index++) {
        bitacoraDataString = await bitacoraDataString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].user}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].date}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].device}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${bitacoraData[index].activity}</td>
        </tr>`;
    }
    bitacoraDataString = bitacoraDataString + `</tbody>`;
    let fileName = `bitacora.pdf`;
    let contentHTML = await `
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Bitacora RestTeam</title>
        </head>
        <body>
            <div id="pageHeader">
                <h5 style="text-align: center;"><b>Lista de Actividades del Sistema</b></h5>
                
            </div>
            <img src="logoOficial.PNG" />
            <div style="padding: 3%;">
                <table style="border: 1px solid black; border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;">Usuario</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Fecha</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Dispositivo</th>
                            <th style="border: 1px solid black; border-collapse: collapse;">Actividad</th>
                        </tr>
                    </thead>
                    ${bitacoraDataString}
                </table>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </div>
        </body>
        </html>`;
    await externalLibrary.generatePDFReport(contentHTML, fileName);
    return await fileName;
}

module.exports = {
    getBacklogOwner,
    generatePDFFileBitacora
};