const externalLibrary = require('../../../config/externalLibrary');
const ownerModel = require('../../../models/OwnerManagement/ownerManage/ownerManage.model');

/** 
* Method GET: get view to Product Owner
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getListProductOwner = async (req,res) => {
    const getListOwner = await ownerModel.getListProductOwner(); 
    if(await ownerModel.getVerifyPermit(req.user.id_user,95)){
        res.status(200).render('OwnerManagement/ownerManage/owner_manage',{
            getListOwner
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/owner_management/owner_manage');
    }
}

/** 
* Method PUT: method update to Owner account
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putEnableDisableProductOwner = async (req,res) =>{
    const ci= req.params.ci;
    console.log(ci);    
    if(await ownerModel.getVerifyPermit(req.user.id_user,95)){
        if(await ownerModel.enableDisableProductOwner(ci)){
            if(await externalLibrary.writeBackLog(req.user.username,`Disable owner with ci: ${ci}`, req.device.type)){
                console.log('writing in Worklog');
              }else{
                console.log('Typing error in worklog');
              }
            req.flash('success_msg', 'Product owner modified successfully');
            res.redirect('/owner_management/owner_manage');
        }else{
            req.flash('error_msg', 'Error! ');
            res.redirect('/owner_management/owner_manage');  
        }
    }else{
        req.flash('error_msg', 'Error! Not Authorized');
        res.redirect('/owner_management/owner_manage');
    }
}

/** 
* Method POST: process To Login Movil of the owner
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postLoginOwner = async (req, res) => {
    const {email, password} = req.body;
    console.log(email, password);
    const owner = await ownerModel.getDataOwner(email);
    console.log(owner);
    const result = await externalLibrary.verifyLoginOwner(owner.ci, password);
    if (await result) {
        if (await externalLibrary.writeBitacoraOwner(owner.name, "Login successfully to App Movil")) {
            console.log("write to bitacora Owner");
        } else {
            console.log("Error to register to Bitacora Owner");
        }
    } else {
        if (await externalLibrary.writeBitacoraOwner(owner.name, "Login incorrect to App Movil")) {
            console.log("write Error login to bitacora Owner");
        } else {
            console.log("Error write Error login to register to Bitacora Owner");
        }
    }
    console.log(result);
    res.json({
        result
    });
}

/**Mobile*/

/** 
* Method POST: register owner account
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postRegisterOwner = async (req, res) => {
    const {ci, issuedPlaced, name, phone, email, pass} = req.body;
    const owner = await ownerModel.registerOwner(ci, issuedPlaced, name, phone, email, pass);
    res.json({owner});
}

/** 
* Method GET: get a list with all notifications
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getOwnerNotifications = async (req, res) => {
    const notifications = await ownerModel.getOwnerNotifications(req.params.ci);
    res.json({notifications});
}


module.exports = {
    getListProductOwner,
    putEnableDisableProductOwner,
    postLoginOwner,
    postRegisterOwner,
    getOwnerNotifications,
}