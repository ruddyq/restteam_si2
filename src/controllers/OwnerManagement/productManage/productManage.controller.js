const productModel = require('../../../models/OwnerManagement/productManage/productManage.model');
const externalLibrary = require('../../../config/externalLibrary');

const getProduct = async (req, res) => {
    var idUser = req.user.id_user;
    if (await productModel.userHavePrivilege(idUser, 81)){
        const categoryList = await productModel.getListCategory();
        const productList = await productModel.getProduct();
        res.render('OwnerManagement/productManage/product_manage', {
            categoryList,productList,
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/owner_management/owner_manage');
    }
}

const postProduct = async (req, res) => {
    var idUser = req.user.id_user;
    if (await productModel.userHavePrivilege(idUser, 82)){
        let origin = req.get('origin');
        const {description_product, model, brand, id_category} = req.body;
        const cod_product = await productModel.insertProduct(description_product, model, brand, id_category, req.file.filename);
        if (cod_product>0) {
            if( await externalLibrary.writeBackLog(req.user.username, `Register product:  model: (${model}) marca: (${brand}) and description ${description_product})`, req.device.type)){
                console.log('Registrado en la Bitacora Exitosamente');
            } else {
                console.log('Fallo al registrar en la bitacora');
            }
            req.flash('success_msg', 'Producto Registrado Exitosamente');
            res.redirect('/owner_management/product_manage');
        } else {
            req.flash('error_msgs', 'Error! No Registrado');
            res.redirect('/owner_management/product_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/owner_management/product_manage');
    }
}
const enableDisableProduct = async (req, res) => {
    var idUser = req.user.id_user;
    if(await productModel.userHavePrivilege(idUser, 84)){
        const {cod_product} = req.params;
        if (await productModel.enableDisableProduct(cod_product)) {
            await externalLibrary.writeBackLog(req.user.username, `Cambiar Estado del Producto: codigo: ${cod_product}`, req.device.type);
            req.flash('success_msg', 'Estado del Producto cambiado Exitosamente!');
            res.redirect('/owner_management/product_manage');
        } else {
            await externalLibrary.writeBackLog(req.user.username, `Fallo al cambiar el Estado del Producto: codigo: ${cod_product}`, req.device.type);
            req.flash('error_msgs', 'Estado del Producto no actualizado');
            res.redirect('/owner_management/product_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/owner_management/product_manage');
    }
}

const getUpdateProduct = async (req, res) => {
    var idUser = req.user.id_user;
    if (await productModel.userHavePrivilege(idUser, 83)){
        const productData = await productModel.getProductDataUpdate(req.params.cod_product);
        const categoryList= await productModel.getCategoryListForProduct(req.params.cod_product);
        res.render('OwnerManagement/productManage/product_update',{
            productData,
            categoryList
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/owner_management/product_manage');
    }
}

const putUpdateProduct = async (req, res) => {
    const {cod_product} = req.params;
    const {description_product, model, brand, description_category} = req.body;
    var file = req.file;
    var filename;
    if (typeof file === "undefined") {
      filename = await productModel.getQuery(`select product_image from product pro where pro.cod_product = ${cod_product};`);
      filename = filename[0].product_image;
    } else {
      filename = file.filename;
    }
    if (await productModel.updateProduct(cod_product, description_product, model, brand, parseInt(description_category),filename)){
        if (await externalLibrary.writeBackLog(req.user.username, `Update Product cod: ${req.params.cod_product}`, req.device.type)) {
            console.log('Registrado en la Bitacora Exitosamente');
        } else {
            console.log('Problemas para registrar en la Bitacora');
        }
        req.flash('success_msg', 'Producto Actualizado Exitosamente');
        res.redirect('../product_manage');
    }else{
        req.flash('error_msgs', 'Error! Producto no actualizado. La descripcion ya existe');
        res.redirect('/owner_management/product_manage');
    }

}

const getListProducts = async (req, res) => {
    const listProducts = await productModel.getListProductEnable();
    res.json({'listProducts': listProducts});
}

const postProductOwner = async (req, res) => {
    const {cod_product, ci_owner} = req.body;
    const result = await productModel.registerProductOwner(cod_product, ci_owner);
    console.log(cod_product, ci_owner);
    if (result > 0) {
      const owner = productModel.getDataOwner(ci_owner);
      if (
        await externalLibrary.writeBitacoraOwner(
          owner,
          `register your product ${cod_product}`
        )
      ) {
        console.log("write to bitacora Owner successfully");
      } else {
        console.log("problems to write to the bitacora Owner");
      }
    }
    res.json({result});
}

const getProductList = async (req, res) => {
    var productList = await productModel.getProductList(req.params.ci);
    res.json({productList:productList});
}

module.exports = {
    getProduct,
    postProduct,
    enableDisableProduct,
    getUpdateProduct,
    putUpdateProduct,
    getListProducts,
    postProductOwner,
    getProductList
}