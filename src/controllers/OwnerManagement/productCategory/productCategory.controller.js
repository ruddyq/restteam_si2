const productcategoryModel = require('../../../models/OwnerManagement/productCategory/productCategory.model');

const externalLibrary = require('../../../config/externalLibrary');

const getListProductCategory = async (req, res) => {
    if (await productcategoryModel.getVerifyPermit(req.user.id_user, 86)) {
        const listProductCategory = await productcategoryModel.ListProductCategory();
        res.status(200).render('OwnerManagement/productCategory/product_category.hbs', {
            listProductCategory
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/owner_management/product_category');
    }
}
/**
 * Method POST: register new Product category
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postProductCategory = async (req, res) => {
    const { description } = req.body;
    if (await productcategoryModel.getVerifyPermit(req.user.id_user, 85)) {
        if (await productcategoryModel.registerProductCategory(description)) {
            if (await externalLibrary.writeBackLog(req.user.username, `register product category  ${description}`, req.device.type)) {
                console.log("Register to bitacora");
            } else {
                console.log("Not register to bitacora");
            }
            req.flash('success_msg', `Category ${description} successfully registered`);
            res.redirect('/owner_management/product_category');
        } else {
            req.flash('error_msgs', 'Existing Category.');
            res.redirect('/owner_management/product_category');
        }
    } else {
        req.flash('error_msgs', `Error! ${req.user.username} not Authorized.`);
        res.redirect('/owner_management/product_category');
    }
}
const getUpdateProductCategory = async (req, res) => {
    const id_category = req.params.id_category
    if (await productcategoryModel.getVerifyPermit(req.user.id_user, 86)) {
        const description_category = await productcategoryModel.getDataProductCategory(id_category);
        res.status(200).render('OwnerManagement/productCategory/update_product_category.hbs', {
            description_category,
            id_category
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/owner_management/product_category');
    }
}
const putUpdateProductCategory = async (req, res) => {
    const { id_category } = req.params;
    const { description_category } = req.body;
    if (await productcategoryModel.getVerifyPermit(req.user.id_user, 85)) {
        if (await productcategoryModel.updateProductCategory(id_category, description_category)) {
            if (await externalLibrary.writeBackLog(req.user.username, `Update category with id: ${id_category} and description ${description_category}`, req.device.type)) {
                console.log('writing in Worklog');
            } else {
                console.log('Typing error in worklog');
            }
            req.flash('success_msg', 'Category modified successfully');
            res.redirect('/owner_management/product_category');
        } else {
            req.flash('error_msgs', 'Error! Existing category');
            res.redirect('/owner_management/product_category');
        }
    } else {
        req.flash('error_msgs', 'Error! Not Authorized');
        res.redirect('/owner_management/product_category');
    }
}

module.exports = {
    getListProductCategory,
    postProductCategory,
    putUpdateProductCategory,
    getUpdateProductCategory

}