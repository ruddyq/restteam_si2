const externalLibrary = require('../../../config/externalLibrary');
const requestSupportServiceModel = require('../../../models/OwnerManagement/requestSupportServiceManage/requestSupportServiceManage.model');

async function sendEmail(requestData, status) {
    try {
        if (status==1) {
            const contentHTML = `
            <div>
                <img text-align: center; src="http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/img/logo_oficial.png" alt="Logo Technical Support" style="border-radius: 5px 5px 0 0;">
                <h3 style="text-align: center;"><b>Respuesta de la Solicitud de Soporte Tecnico en la Empresa RestTeam</b></h3>
            </div>
            <div style="display: flex; justify-content: center; align-items: center; text-align; center;">
                <p>
                    Buenos dias, querido cliente ${requestData.name} [CI: ${requestData.ci}].<br>
                    Nos tomamaos la molestia de avisarle que aprobamos su solicitud de servicio técnico para el producto: <b>${requestData.description_product}</b>. Mas información. <br>
                </p>
                <table>
                    <tbody>
                        <tr>
                            <th>Solicitud No.</th>
                            <td>${requestData.request_no}</td>
                        </tr>
                        <tr>
                            <th>Fecha de Solicitud</th>
                            <td>${requestData.date_request}</td>
                        </tr>
                        <tr>
                            <th>Tipo de Servicio</th>
                            <td>${requestData.description_type}</td>
                        </tr>
                        <tr>
                            <th>Descripcion del Producto</th>
                            <td>${requestData.description_product}</td>
                        </tr>
                        <tr>
                            <th>Marca</th>
                            <td>${requestData.brand}</td>
                        </tr>
                        <tr>
                            <th>Modelo</th>
                            <td>${requestData.model}</td>
                        </tr>
                        <tr>
                            <th>Categoria del Producto</th>
                            <td>${requestData.description_category}</td>
                        </tr>
                        <tr>
                            <th>Descripcio del Problema</th>
                            <td>${requestData.description}</td>
                        </tr>
                    </tbody>
                </table>
                <p><b>Contactese con la Administración de RestTeam porfavor.</b></p>
            </div>`;
            await externalLibrary.sendEmail(contentHTML, requestData.email, 'Response of Request Support RestTeam');
            return true;
        } else {
            const contentHTML = `
            <div>
                <img text-align: center; src="http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/img/logo_oficial.png" alt="Logo Technical Support" style="border-radius: 5px 5px 0 0;">
                <h3 style="text-align: center;"><b>Respuesta de la Solicitud de Soporte Tecnico en la Empresa RestTeam</b></h3>
            </div>
            <div style="display: flex; justify-content: center; align-items: center; text-align; center;">
                <p>
                    Hola! cliente ${requestData.name} [CI: ${requestData.ci}].<br>
                    Nos tomamos el tiempo de informale que su solicitud de servicio técnico fue <b>RECHAZADA</b>, para el producto: ${requestData.description_product}. La razón es debido a que la soicitud no esta detallada correctamente, por favor comuniquese con el taller RestTeam o vuelva a enviar su solicitud. Gracias!!
                </p>
                <p><b>No responda este email es un boot.</b></p>
            </div>`;
            await externalLibrary.sendEmail(contentHTML, requestData.email, 'Response of Request Support RestTeam');
            return true;
        }
    } catch (error) {
        console.error("error in send email for accept request", error);
        return false;
    }
}

const getRequestSupportService = async (req, res) => {
    if (await requestSupportServiceModel.getVerifyPermitService(req.user.id_user, 100)) {
        const requestList = await requestSupportServiceModel.getRequestSupportServiceList();
        res.render('OwnerManagement/requestSupportServiceManage/request_support_service', {
            requestList
        });
    } else {
        req.flash('error_msgs', `Not Authorized. user ${req.user.username} don't have permit`);
        res.redirect(`/`);
    }
}

const postChangeStatusRequest = async (req, res) => {
    if (await requestSupportServiceModel.getVerifyPermitService(req.user.id_user, 103)) {
        if (await requestSupportServiceModel.changeStateRequest(req.params.request_no, req.params.state)) {
            const requestData = await requestSupportServiceModel.getDataRequest(req.params.request_no);
            let flag = await sendEmail(requestData, req.params.state);
            if (flag) {
                req.flash('success_msgs', `The Request ${req.params.request_no} was Accepted successfully, sent email to owner`);
                res.redirect('/owner_management/request_support_service_manage');
            } else {
                req.flash('error_msgs', `The Request was Accepted successfully; but I have problems to sent email to owner`);
                res.redirect(`/owner_management/request_support_service_manage`);
            }
        } else {
            req.flash('error_msgs', `Error. The Request Support Service with No. ${req.params.request_no} can't change state`);
            res.redirect(`/owner_management/request_support_service_manage`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized. user ${req.user.username} don't have permit`);
        res.redirect(`/owner_management/request_support_service_manage`);
    }
}

const putChangeStatusRequest = async (req, res)=> {
    if (await requestSupportServiceModel.getVerifyPermitService(req.user.id_user, 103)) {
        if (await requestSupportServiceModel.changeStateRequest(req.params.request_no, req.params.state)) {
            const requestData = await requestSupportServiceModel.getDataRequest(req.params.request_no);
            if (await sendEmail(requestData)) {
                req.flash('success_msgs', `The Request ${req.params.request_no} was Accepted successfully, sent email to owner`);
                res.redirect('/');
            } else {
                req.flash('error_msgs', `The Request was Accepted successfully; but I have problems to sent email to owner`);
                res.redirect(`/`);
            }
        } else {
            req.flash('error_msgs', `Error. The Request Support Service with No. ${req.params.request_no} can't change state`);
            res.redirect(`/`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized. user ${req.user.username} don't have permit`);
        res.redirect(`/`);
    }
}

/** Mobile */

const getTypeSupportList = async (req, res) => {
    var typeSupport = await requestSupportServiceModel.getTypeSupportServiceList();
    res.json({type_support:typeSupport});
}

const postRequestSupport = async (req, res) => {
    const {description, cod_policy, description_type, code_owner} = req.body;
    var idRequest = await requestSupportServiceModel.registerRequestSupport(description, cod_policy, description_type, code_owner);
    res.json({idRequest});
}

const getRequestSupportList = async (req, res) => {
    var supportServiceList = await requestSupportServiceModel.getRequestSupportServiceListId(req.params.ci);
    res.json({requestList:supportServiceList});
}

const getRequestWithoutGuarantee = async (req, res) => {
    const listTypeSupport= await requestSupportServiceModel.getTypeSupportServiceList();
    const data = await requestSupportServiceModel.getDataOfCodProductOwner(req.params.cod_product_owner);
     res.json({
        data,
        listTypeSupport
     });
}

const postRequestSupportServiceWithoutPolicy = async (req, res) => {
    const {description_problem, type_support_service, cod_product_owner} = req.body;
    const dataOwner = await requestSupportServiceModel.getDataOfCodProductOwner(cod_product_owner);
    const requestNo = await requestSupportServiceModel.registerRequestSupportWithoutPolicy(description_problem, type_support_service, cod_product_owner);
    if (requestNo>0) {
        if (await externalLibrary.writeBitacoraOwner(dataOwner.name, "sent a request support service without a guarantee policy")) {
            console.log("write to bitacora owner successfully");
        }else{
            console.log("error in write to bitacora owner");
        }
    }
    res.json({
        'result' : requestNo
    });
}

module.exports = {
    getRequestSupportService,
    postChangeStatusRequest,
    getTypeSupportList,
    postRequestSupport,
    putChangeStatusRequest,
    getRequestSupportList,
    getRequestWithoutGuarantee,
    postRequestSupportServiceWithoutPolicy
}