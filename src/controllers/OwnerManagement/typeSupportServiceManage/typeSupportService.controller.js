const typeSupportModel = require('../../../models/OwnerManagement/typeSupportServiceManage/typeSupportService.model');

const externalLibrary = require('../../../config/externalLibrary');

const getTypeSupportService = async (req,res) => {    
    if(await typeSupportModel.getVerifyPermit(req.user.id_user,92)){
        const listTypeSupport = await  typeSupportModel.getTypeSupportServiceList();
        res.status(200).render('OwnerManagement/typeSupportServiceManage/type_support_service',{
            listTypeSupport
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/');
    }
}
/**
 * Method POST: register new Product category
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postTypeSupportService = async (req, res) => {
    const{description} = req.body;
    if(await typeSupportModel.getVerifyPermit(req.user.id_user,93)){
        if (await typeSupportModel.registerTypeSupportService(description)){
            if(await externalLibrary.writeBackLog(req.user.username, `register type support service  ${description}`, req.device.type)){
                console.log("write type support service to bitacora");
            }else{
                console.log("not write type support service to bitacora");
            }  
            req.flash('success_msg', 'Type Support Service successfully registered');
            res.redirect('/owner_management/type_support_service');            
        }else{
            req.flash('error_msgs', 'Existing Type Support Service.');
            res.redirect('/owner_management/type_support_service');
        }
    }else{
        req.flash('error_msgs', `Error! ${req.user.username} not Authorized.`);
        res.redirect('/owner_management/type_support_service');       
    }
}

const putEnableDisableTypeSupportService = async (req, res) =>{
    
    if(await typeSupportModel.getVerifyPermit(req.user.id_user,94)){
        const{status} = req.body;
        if (await typeSupportModel.enableDisableTypeSupportService(req.params.id)){
            if(status=="disable"){
                if(await externalLibrary.writeBackLog(req.user.username, `disable type support service`, req.device.type)){
                    console.log("write disable type support service to bitacora");
                }else{
                    console.log("not write disable type support service to bitacora");
                }
                req.flash('success_msg', 'Type Support Service Disable successfully');
                res.redirect('/owner_management/type_support_service');            
            }else{
                if(await externalLibrary.writeBackLog(req.user.username, `enable type support service`, req.device.type)){
                    console.log("write enable type support service to bitacora");
                }else{
                    console.log("not write enable type support service to bitacora");
                }
                req.flash('success_msg', 'Type Support Service Enable successfully');
                res.redirect('/owner_management/type_support_service');
            }
        }else{
            req.flash('error_msgs', 'Error! Try again please');
            res.redirect('/owner_management/type_support_service');
        }
    }else{
        req.flash('error_msgs', `Error! ${req.user.username} not Authorized.`);
        res.redirect('/owner_management/type_support_service');       
    }
}
const getUpdateTypeSupportService = async (req,res) => {    
    if(await typeSupportModel.getVerifyPermit(req.user.id_user,94)){
        var id = req.params.id;
        const description = await  typeSupportModel.getDataTypeSupportService(id);
        res.status(200).render('OwnerManagement/typeSupportServiceManage/type_support_service_update',{
            description,id
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/owner_management/type_support_service');
    }
}
const putTypeSupportService = async (req,res) =>{
    const id= req.params.id;
    if(await typeSupportModel.getVerifyPermit(req.user.id_user,94)){
        const {description} = req.body;
        var existe = await typeSupportModel.getQuery(`select coalesce (count(*), 0) from type_support_service tss where description_type = '${description}';`);
        existe = existe[0].coalesce;
        if(existe > 0){
            req.flash('error_msgs', 'Error! This description already exists');
            res.redirect(`/owner_management/type_support_service/update/${id}`);
        }else{
            if(await typeSupportModel.updateTypeSupportService(id,description)){
                if(await externalLibrary.writeBackLog(req.user.username,`Update type support service with id: ${id} and description ${description}`, req.device.type)){
                    console.log('write update type support service successfuly');
                  }else{
                    console.log('write update type support service fail');
                  }
                req.flash('success_msg', 'Type Support Service Modified Successfully');
                res.redirect('/owner_management/type_support_service');
            }else{
                req.flash('error_msgs', 'Error! Your data was not saved');
                res.redirect(`/owner_management/type_support_service/update/${id}`);  
            }

        }
        
    }else{
        req.flash('error_msgs', 'Error! Not Authorized');
        res.redirect('/owner_management/type_support_service');
    }
}

module.exports = {
    getTypeSupportService,
    postTypeSupportService,
    putEnableDisableTypeSupportService,
    getUpdateTypeSupportService,
    putTypeSupportService

}