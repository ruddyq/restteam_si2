const contractModel = require('../../../models/CompanyManagement/contractManage/contract.model');
const externalLibrary = require('../../../config/externalLibrary');
var path = require('path');
const address = require('../../../helpers/address');

/**
 * get view list contract
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getContractManage = async (req, res) => {
    if (await contractModel.getVerifyPermitContract(req.user.id_user, 10)) {
        const { nit } = req.params;
        const listContractOfNIT = await contractModel.getListContract(nit);
        const nameCorporate = await contractModel.getCorporateClient(nit);
        res.render('CompanyManagement/contractManage/contract_manage.hbs', {
            nameCorporate,
            listContractOfNIT
        });
    } else {
        req.flash('error_msgs', `Not Authorized, User ${req.user.username} blocked.`);
        res.redirect('/company_management/company_manage');
    }
}

const getContractsList = async (req, res) => {
    const contractsList = await contractModel.getContractsList();
    res.render('CompanyManagement/contractManage/contract_list.hbs', {
        contractsList
    });
}

/**
 * register new contract' process
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const postContractManage = async (req, res) => {
    const { cod_contract, name_corporate, begin_date, end_date, description } = req.body;
    const nit = await contractModel.getNIT(name_corporate);
    if (await contractModel.getVerifyPermitContract(req.user.id_user, 9)) {
        if (begin_date < end_date) {
            const lastDateCompany = await contractModel.getLastDateContract(nit);
            console.log(lastDateCompany, "date", begin_date, lastDateCompany <= begin_date);
            if (lastDateCompany <= begin_date) {
                const response = await contractModel.registerContract(name_corporate, cod_contract, begin_date, end_date, description, req.file.filename);
                if (response) {
                    if (await externalLibrary.writeBackLog(req.user.username, `register contract with code: ${cod_contract} for ${name_corporate} company`, req.device.type)) {
                        console.log('write successfully in BackLog');
                    } else {
                        console.error('problems in write in BackLog');
                    }
                    req.flash('success_msg', 'The contract company was successfully saved');
                    res.redirect(`/company_management/contract_manage/${nit}`);
                } else {
                    req.flash('error_msgs', 'Error! The company does not exists or the contract code already exists.');
                    res.redirect(`/company_management/contract_manage/${nit}`);
                }
            } else {
                req.flash('error_msgs', 'Error! There is still a current contract.');
                res.redirect(`/company_management/contract_manage/${nit}`);
            }
        } else {
            req.flash('error_msgs', 'The start date must be less than the date of conclusion.');
            res.redirect(`/company_management/contract_manage/${nit}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, User ${req.user.username} blocked.`);
        res.redirect(`/company_management/contract_manage/${nit}`);
    }
}

/**
 * get view of the PDF contract
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getContractPDF = async (req, res) => {
    const cod = req.params.cod;
    const responseDataContract = await contractModel.getDataCompleteContract(cod);
    res.download(address.addressContractCompany+responseDataContract);
}

const putChangeStatusContract = async (req, res) => {
    const {nit, cod_contract} = req.params;
    if (await contractModel.changeStatusContract(cod_contract, nit)) {
        if (await externalLibrary.writeBackLog(req.user.username, "disable contract company", req.device.type)) {
            console.log('write successfully in BackLog');
        } else {
            console.error('problems in write in BackLog');
        }
        req.flash('success_msg', 'The contract was disabled successfully');
        res.redirect(`/company_management/contract_manage/${nit}`);
    } else {
        req.flash('error_msgs', `Error! The contract can't enable`);
        res.redirect(`/company_management/contract_manage/${nit}`);
    }
}

module.exports = {
    getContractManage,
    getContractsList,
    postContractManage,
    getContractPDF,
    putChangeStatusContract
}
