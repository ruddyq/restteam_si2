const companyModel = require('../../../models/CompanyManagement/companyManage/company.model');
const externalLibrary = require('../../../config/externalLibrary');
const representativeManageModel = require('../../../models/CompanyManagement/representativeManage/representativeManage.model');
const invoiceModel = require('../../../models/SupportServiceManagement/serviceInvoice/serviceInvoice.model');
/**
 * Method GET: show view all Companies
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getCompany = async (req, res) => {
    if (await companyModel.getVerifyPermitUser(req.user.id_user, 4)) {
        const listCompany = await companyModel.getListCompanies();
        const representativeOptions = await representativeManageModel.getListRepresentative();
        res.status(200).render('CompanyManagement/companyManage/company_manage', {
            listCompany,
            representativeOptions
        });
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/');
    }
}

/**
 * Method POST: Process new Company to database expresssi2
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postCompany = async (req, res) => {
    if (await companyModel.getVerifyPermitUser(req.user.id_user, 3)) {
        const { nit, name_company, corporate, name_representative, email, phone, user } = req.body;
        console.log(user);
        if (await companyModel.insertCompany(nit, name_company, corporate, name_representative, email, phone)) {
            if (await externalLibrary.writeBackLog(user, `register company ${nit}, ${name_company}, ${corporate}, ${name_representative}, ${email}, ${phone}`, req.device.type)) {
                console.log('write register company successfully');
            } else {
                console.log('write register company failure');
            }

            req.flash('success_msg', 'Company added Successfully');
            res.redirect('/company_management/company_manage');
        } else {
            res.render('company_management/company_manage', {
                nit,
                name_company,
                corporate,
                name_representative,
                email,
                phone
            });
        }
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/company_management/company_manage');
    }
}

/**
 * METHOD PUT : enable or disable company
 * @param {*} req : response (peticion)
 * @param {*} res : response (respuesta)
 */
const putEnableDisableCompany = async (req, res) => {
    if (await companyModel.getVerifyPermitUser(req.user.id_user, 6)) {
        const { action } = req.body;
        console.log(action);
        if (await companyModel.enableDisableCompany(req.params.id)) {
            if (action == "disable") {
                if (await externalLibrary.writeBackLog(req.user.username, `Disable company ${req.params.id}`, req.device.type)) {
                    console.log('write disable company successfully');
                } else {
                    console.log('write disable company failure');
                }
                req.flash('success_msg', 'Company Disable Successfully');
            } else {
                if (await externalLibrary.writeBackLog(req.user.username, `Enable company ${req.params.id}`, req.device.type)) {
                    console.log('write enable company successfully');
                } else {
                    console.log('write enable company failure');
                }
                req.flash('success_msg', 'Company Enable Successfully');
            }
            res.redirect('../company_manage');
        } else {
            res.send('error');
        }
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/company_management/company_manage');
    }
}

/**
 * METHOD GET : get view for update company
 * @param {*} req 
 * @param {*} res 
 */
const getUpdateCompany = async (req, res) => {
    const idUser = req.user.id_user;
    if (await companyModel.getVerifyPermitUser(idUser, 5)) {
        const companyData = await companyModel.getCompanyUpdateData(req.params.nit);
        const listOptionRepresentative = await representativeManageModel.getListRepresentative();
        res.render('CompanyManagement/companyManage/company_update', {
            companyData,
            listOptionRepresentative,
            nit: req.params.nit
        });
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/company_management/company_manage');
    }
}

const putUpdateCompany = async (req, res) => {
    const { email, phone, enable, ci_representative } = req.body;
    const result = await companyModel.updateCompany(req.params.nit, ci_representative, email, phone, enable);
    if (result) {
        if (await externalLibrary.writeBackLog(req.user.username, `Update Company with NIT: ${req.params.nit} (${email}, ${phone}, ${enable})`, req.device.type)) {
            console.log('write in backlog');
        } else {
            console.log('problems in write backlog');
        }
        req.flash('success_msg', 'Company updated Successfully');
        res.redirect('../company_manage');
    } else {
        req.flash('error_msgs', `Error! Hubo problemas al cambiar el`);
        res.redirect(`/update/${req.params.nit}`);
    }
}

const getReportInvoiceCompany = async (req, res) => {
    const listReportCompany = await invoiceModel.getListInvoiceReportClientCompany();
    res.render('CompanyManagement/companyManage/invoice_report', {
        list_client_company_report: listReportCompany
    })
}

const postGenerateReport = async (req, res) => {
    const { nit } = req.params;
    const listInvoiceData = await invoiceModel.getReportInvoiceCompanyNIT(nit);
    let invoiceDataString = `<tbody>`;
    for (var index = 0; index < listInvoiceData.length; index++) {
        invoiceDataString = await invoiceDataString + `
         <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${listInvoiceData[index].bill_no}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${listInvoiceData[index].name}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${listInvoiceData[index].cod_policy}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${listInvoiceData[index].request_no}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${listInvoiceData[index].technical_name}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;" bgcolor="2CEF35"><b>${listInvoiceData[index].total_cost}</b></td>
         </tr>`;
    }
    invoiceDataString = invoiceDataString + `</tbody>`;
    var today = new Date().toLocaleString("en-US", { timeZone: "America/La_Paz" });
    let timeDate = new Date(today);
    let fileName = `${nit}_report_services.pdf`;
    let contentHTML = await `
         <!doctype html>
         <html>
         <head>
             <meta charset="utf-8">
         </head>
         <body>
             <div id="pageHeader">
                <img src="https://images.creativemarket.com/0.1.0/ps/1700488/1160/772/m1/fpnw/wm0/camera-sketch-photography-logo-template-01-.png?1474964473&s=81065afc7c2a2340236962bfd5b5ce50" style="width: 50%; height: 50%;" alt="logo">
                <h2 style="text-align: center;"><b>Servicio de Soporte Técnico</b></h2> <br>
             </div>
             <div style="padding: 3%;">
                 <table class="egt" style="width: 100%;">
                     <thead>
                         <tr>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Nro. Factura</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Cliente Propietario</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Codigo de Póliza</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Solicitud de Servicio Técnico</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Nombre Técnico</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="2CEF35"><b>Costo Total</b></th>
                         </tr>
                     </thead>
                     ${invoiceDataString}
                 </table>
                 <div id="pageFooter" style="text-align: center;">
                     <b>printed by: </b>${req.user.username}
                 </div>
             </div>
         </body>
         </html>`;
    await externalLibrary.generatePDFReport(contentHTML, fileName);
    let filePath = await `../security/report/${fileName}`;
    console.log("file path: ", filePath);
    setTimeout(function () {
        res.download(filePath);
    }, 5000);
}

module.exports = {
    getCompany,
    postCompany,
    putEnableDisableCompany,
    getUpdateCompany,
    putUpdateCompany,
    getReportInvoiceCompany,
    postGenerateReport
}