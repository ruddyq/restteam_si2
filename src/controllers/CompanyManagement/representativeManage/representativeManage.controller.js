const representativeModel = require('../../../models/CompanyManagement/representativeManage/representativeManage.model');
const externalLibrary = require('../../../config/externalLibrary');

const getRepresentative = async (req, res) => {
    var idUser = req.user.id_user;
    if (await representativeModel.userHavePrivilege(idUser, 118)){
        const listRepresentative = await representativeModel.getListRepresentative();
        res.render('CompanyManagement/representativeManage/representative_manage', {
            listRepresentative
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/company_management/representative_manage');
    }
}

const postRepresentative = async (req, res) => {
    var idUser = req.user.id_user;
    if (await representativeModel.userHavePrivilege(idUser, 119)){
        const {ci, issued_place, name, address, email, phone} = req.body;
        if (await representativeModel.insertRepresentative(ci, issued_place, name, address, email, phone)) {
            if( await externalLibrary.writeBackLog(req.user.username, `Representative Company register: '${name}' with ci: '${ci}'`, req.device.type)){
                console.log('Registrado en la Bitacora');
            } else {
                console.log('Fallo en el Registro');
            }
            req.flash('success_msg', 'Representante de la compañia Adicionado Correctamente');
            res.redirect('/company_management/representative_manage');
        } else {
            req.flash('error_msgs', 'Error! Representante de la compañia ya existe.');
            res.redirect('/company_management/representative_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/company_management/representative_manage');
    }
}

const getUpdateRepresentative = async (req, res) => {
    var idUser = req.user.id_user;
    if (await representativeModel.userHavePrivilege(idUser, 120)){
        const ci = req.params.ci;
        const representativeData = await representativeModel.getRepresentativeUpdateData(ci);
        res.render('CompanyManagement/representativeManage/representative_update',{
            representativeData,
            ci
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado..');
        res.redirect('/company_management/representative_manage');
    }
}

const putUpdateRepresentative = async (req, res) => {
    const {name, address, email, phone} = req.body;
    if ( await representativeModel.updateRepresentative(req.params.ci, name, address, email, phone)){
        if (await externalLibrary.writeBackLog(req.user.username, `Update Provider id:${req.params.ci}`, req.device.type)) {
            console.log('Registrado en la Bitacora');
        } else {
            console.log('Problemas para registrar en la Bitacora');
        }
        req.flash('success_msg', 'Representante de la compañia Actualizado Exitosamente');
        res.redirect('../representative_manage');
    }else{
        req.flash('error_msgs', 'Error! Representante de la compañia no actualizado. El email o telefono ya existe');
        res.redirect('/company_management/representative_manage');
    }
    
}

module.exports = {
    getRepresentative,
    postRepresentative,
    getUpdateRepresentative,
    putUpdateRepresentative

}