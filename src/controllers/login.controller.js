/**
 *       Import Files dependents
 */
const externalLibrary = require('../config/externalLibrary');
const userModel = require('../models/UserManagement/userManage/user.model');
let keyLogger = new Array();

/**
 * Method GET for principal view of the system web
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getIndex = async (req, res) => {
    const littleListBackLog = await externalLibrary.getLastFiveActivities(5);
    const reportTechnical = await userModel.getReportPrincipal();
    const reportRequest = await userModel.getReportRequest();
    const reportSupportService = await userModel.getReportTechnicalService();
    const reportUser = await userModel.getReportsInvoiceUser();
    res.status(200).render('index', {
        littleListBackLog,
        reportTechnical,
        reportRequest,
        reportSupportService,
        reportUser
    });
}

/**
 * Method GET for show login view
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getLogin = (req, res) => {
    res.status(200).render('login');
};

/**
 * Exit session user on system
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const logout = (req, res) => {
    const { username } = req.params;
    externalLibrary.writeBackLog(username, "Exit the System RestTeamJS", req.device.type);
    req.logout();
    res.status(200).redirect('/login');
};

/**
 * Method GET for info of the system
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getMoreInfo = (req, res) => {
    res.status(200).render('more_info');
}

/**
 * Method GET for send email user view
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getForgotMyPassword = (req, res) => {
    res.status(200).render('forgot_password');
}

/**
 * Method POST for send confirm email to user account
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const postForgotPassword = async (req, res) => {
    try {
        const { email } = req.body;
        if (await userModel.emailUserExists(email)) {
            const numRandom = parseInt(Math.floor((Math.random() * 999999) + 111111), 16); //Generate Key Randomize for confirm code
            const key = numRandom.toString(15);
            keyLogger.push({
                key,
                email
            });
            contentHTML = `
            <div style="width:75%; margin:0px auto;">
                <div style="display:table-cell; vertical-align:middle; text-align: center;">
                    <div style="box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); transition: 0.3s; border-radius: 5px; background: #373B44;  /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #4286f4, #373B44);  /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left, #4286f4, #373B44); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                ">
                        <img src="http://ec2-54-232-175-236.sa-east-1.compute.amazonaws.com/img/logo_oficial.png"
                            alt="Logo Technical Support" style="border-radius: 5px 5px 0 0;">
                        <div style="padding: 2px 16px;" style="color: whitesmoke;">
                            <h2 style="color: whitesmoke;">Usuario RestTeam</h2>
                            <h3 style="color: whitesmoke;">Estimado usuario<br>
                                Este es una llave de seguridad: <b style="color: yellow;">${key}</b>
                                <a style="color: greenyellow;" href="http://${req.headers.host}/confirm_key">Porfavor sigue este
                                    enlace para recuperar tu cuenta</a>
                                gracias. <br>
                                No respondas a este email, es un boot.
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            `;
            const info = await externalLibrary.sendEmail(contentHTML, email, 'User Restore');
            console.log('status email sent:', info);
            req.flash('success', 'Email sent Successfully!!');
            res.status(200).redirect('/login');
        } else {
            req.flash('error', 'Error! you email not exists.');
            res.status(200).redirect('/login');
        }
    } catch (error) {
        console.error(error, "error in process send email for recovery account");
        req.flash('error', 'I have a problem to send email.');
        res.status(200).redirect('/login');
    }
}

/**
 * Method GET for verify key for restore password
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getConfirmKey = (req, res) => {
    res.status(200).render('confirm_key');
}

/**
 * Method POST to verify key sent email to email user
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const postConfirmKey = async (req, res) => {
    const { key } = req.body;
    let email;
    for (let index = 0; index < keyLogger.length; index++) {
        if (await key === keyLogger[index].key) {
            keyLogger[index].key = "@%RST#";
            res.status(200).redirect('/restore_new_password');
        }
    }
    if (email === null) {
        req.flash('error', 'Erroneous key, we already called the police. runs!');
        res.status(200).redirect('/login');
    }
}

/**
 * Method POST for verify new password to user account in password.json
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const postVerifyPasswd = async (req, res) => {
    const { password, password_retype, email } = req.body;
    let flag = false;
    for (let index = 0; index < keyLogger.length; index++) {
        if (await (keyLogger[index].key === "@%RST#" && keyLogger[index].email === email)) {
            flag = true;
        }
    }
    if (await (password === password_retype && flag)) {
        const hash = await externalLibrary.encryptPassword(password);
        const idUser = await userModel.getIdUser(email);
        externalLibrary.updatePassword(idUser, hash);
        req.flash('success', 'new password change successfully');
        res.status(200).redirect('/login');
    } else {
        req.flash('error_msgs', `The password does not match or ${email} doesn't not validated.`);
        res.status(200).redirect('/restore_new_password');
    }
}

/**
 * Method GET for send new password
 * @param {*} req : request(petición)
 * @param {*} res : response (respuesta)
 */
const getRestorePassword = (req, res) => {
    res.status(200).render('restore_new_passwd');
}

module.exports = {
    getIndex,
    getLogin,
    logout,
    getMoreInfo,
    getForgotMyPassword,
    postForgotPassword,
    getConfirmKey,
    postConfirmKey,
    getRestorePassword,
    postVerifyPasswd,
}
