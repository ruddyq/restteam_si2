const serviceInvoiceModel = require('../../../models/SupportServiceManagement/serviceInvoice/serviceInvoice.model');
const externalLibrary = require('../../../config/externalLibrary');
const {addressInvoice} = require(`../../../helpers/address`);
const fs = require('fs');
/**
* Method GET: get view to Invoices
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getListServiceInvoice = async (req, res) => {
    const getListInvoice = await serviceInvoiceModel.getListInvoice();
    const getListReports = await serviceInvoiceModel.getListInvoiceReport();
    let timeDate = new Date();
    let fileName = timeDate.getYear() + ` report.pdf`;
    console.log(fileName);
    if (await serviceInvoiceModel.getVerifyPermitService(req.user.id_user, 61)) {
        res.status(200).render('SupportServiceManagement/serviceInvoice/service_invoice', {
            getListInvoice,
            getListReports
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_manage');
    }
}
/**
* Method GET: get view to Register Invoices
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getRegisterInvoice = async (req, res) => {
    const support_no = req.params.support_no;
    var totalCost = await serviceInvoiceModel.getQuery(`select tss.total_cost from technical_support_service tss where support_no = ${support_no};`);
    totalCost = totalCost[0].total_cost;
    if (await serviceInvoiceModel.getVerifyPermitService(req.user.id_user, 60)) {
        let supportFinished = await serviceInvoiceModel.getQuery(`select state_support_service from technical_support_service where support_no = ${support_no};`);
        supportFinished = supportFinished[0].state_support_service;
        if (supportFinished) {
            let noBill = await serviceInvoiceModel.existsInvoice(support_no);
            if (noBill > 0) {
                if (await serviceInvoiceModel.billIsCanceled(noBill)) {
                    res.status(200).render('SupportServiceManagement/serviceInvoice/register_invoice', {
                        support_no, totalCost
                    });
                } else {
                    req.flash('success_msg', `Ya Existe Factura! Ver N° ${noBill}`);
                    res.redirect('/support_service_management/service_invoice');
                }

            } else {
                res.status(200).render('SupportServiceManagement/serviceInvoice/register_invoice', {
                    support_no, totalCost
                });
            }
        } else {
            req.flash('error_msgs', 'Error! Servicio de Soporte Tecnico aun no Terminado.');
            res.redirect('/support_service_management/support_service_manage');
        }
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/service_invoice');
    }
}
/**
 * Method PUT: register invoice to database expresssi2
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */

const putServiceInvoice = async (req, res) => {
    const support_no = req.params.support_no;
    const idUser = req.user.id_user;
    const { nit, corporate, quantity_day_support, total_cost } = req.body;
    console.log(`Nit: ${nit}, corporate: ${corporate}, quantityday: ${quantity_day_support}, totalCost: ${total_cost}`);
    if (await serviceInvoiceModel.getVerifyPermitService(idUser, 62)) {
        var billNo = await serviceInvoiceModel.insert_invoice(nit, corporate, total_cost, quantity_day_support, support_no, idUser);
        if (billNo > 0) {
            let fileName = await generateInvoice(billNo, support_no, req.user.username);
            let filePath = `${addressInvoice}${fileName}`;
            console.log("Filepath: " + filePath);
            setTimeout(function () {
                fs.readFile(filePath, function (err, data) {
                    console.log("The PDF File does exists");
                    res.contentType("application/pdf");
                    res.send(data);
                });
            }, 5000);

            if (await externalLibrary.writeBackLog(req.user.username, `Register invoice with ${nit} of owner ${corporate}`, req.device.type)) {
                console.log("write register invoice successfully");
            } else {
                console.log("write register invoice fail");
            }
        } else {
            req.flash('error_msgs', 'Tienees Problemas de Permiso Registro de Factura');
            res.redirect('/support_service_management/support_service_manage');
        }
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/service_invoice');
    }
}

const putEnableDisable = async (req, res) => {
    const bill_no = req.params.bill_no;
    if (await serviceInvoiceModel.getVerifyPermitService(req.user.id_user, 60)) {
        if (await serviceInvoiceModel.enable_disableInvoice(bill_no)) {
            if (await externalLibrary.writeBackLog(req.user.username, `Disable invoice ${bill_no}`, req.device.type)) {
                console.log('write disable invoice successfully 7u7');
            } else {
                console.log('write disable invoice failure');
            }
            req.flash('success_msg', 'Invoice disable successfully');
            res.redirect('/support_service_management/service_invoice');
        } else {
            req.flash('error_msgs', 'No puede restaurar una factura anulada ó este servicio ya fue anulado una vez, no puede volver a hacerlo (no valido)');
            res.redirect('/support_service_management/service_invoice');
        }
    } else {
        req.flash('error_msgs', 'Error! Not Authorized');
        res.redirect('/support_service_management/service_invoice');
    }
}
/**
 * Drawn the pdf file in ../security/reports/report.pdf
 * @param {string} username : username
 */
const generateReportInvoice = async (nit, username) => {
    const invoiceData = await serviceInvoiceModel.getListInvoiceReportOfCompany(nit);
    const invoiceSum = await serviceInvoiceModel.getDataInvoiceReport(nit);
    let invoiceDataString = `<tbody>`;
    for (var index = 0; index < invoiceData.length; index++) {
        invoiceDataString = await invoiceDataString + `
         <tr>
             <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].nit}</td>
             <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].corporate}</td>
             <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].service_date}</td>
             <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceData[index].total_cost}</td>
         </tr>`;
    }
    invoiceDataString = invoiceDataString + `</tbody>`;
    let timeDate = new Date();
    let fileName = ` ${invoiceSum[0].name_company}` + ' ' + timeDate.getDate() + '-' + timeDate.getMonth() + `-` + timeDate.getFullYear() + ` report.pdf`;
    let contentHTML = await `
         <!doctype html>
         <html>
         <head>
             <meta charset="utf-8">
         </head>
         <body>
             <div id="pageHeader">
                 <h2 style="text-align: center;"><b>Invoice report </b></h2> <br>
                 <h3 style="text-align: left;"><b>Business ${invoiceSum[0].name_company} NIT: ${invoiceSum[0].nit_client_company}</b></h3>
                 <h3 style="text-align: left;"><b>NIT: ${invoiceSum[0].nit_client_company}</b></h3>
             </div>
             <div style="padding: 3%;">
                 <table class="egt" style="width: 100%;">
                     <thead>
                         <tr>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Nit</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Name company</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Service data</th>
                             <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Total cost</th>
                         </tr>
                     </thead>
                     ${invoiceDataString}
                     <tr>
                      <td colspan="3" style="border: 1px solid black; border-collapse: collapse;">Total sum</td>
                      <td style="border: 1px solid black; border-collapse: collapse;text-align: center">${invoiceSum[0].total_sum}</td>
                     </tr>
                 </table>
                 <div id="pageFooter" style="text-align: center;">
                     <b>printed by: </b>${username}
                 </div>
             </div>
         </body>
         </html>`;
    await externalLibrary.generatePDFReport(contentHTML, fileName);
    return await fileName;
}
/**
 * METHOD POST : Generate PDF file for a service invoice
 * @param {*} req : request
 * @param {*} res  : response
 */
const generatePDFFileInvoice = async (req, res) => {
    const nit = req.params.nit;
    let fileName = await generateReportInvoice(nit, req.user.username);
    let filePath = await `../security/report/${fileName}`;
    console.log(filePath);
    setTimeout(function () {
        fs.readFile(filePath, function (err, data) {
            console.log("The PDF File does exists");
            res.contentType("application/pdf");
            res.send(data);
        });
    }, 5000);
}


/**
 * Drawn the pdf file in ../security/invoice/billNo.pdf
 * @param {integer} billNo: Number Invoice
 * @param {integer} supportNo: Support Nro 
 * @param {string} username : username
 */
const generateInvoice = async (billNo, supportNo, username) => {

    const invoiceHeader = await serviceInvoiceModel.getDataInvoice(billNo);
    const invoiceReplacementList = await serviceInvoiceModel.getReplacementList(supportNo);
    const invoiceServiceList = await serviceInvoiceModel.getServiceList(supportNo);
    let invoiceDataString = `<tbody>`;
    for (var index = 0; index < invoiceServiceList.length; index++) {
        invoiceDataString = invoiceDataString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceServiceList[index].description_service}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">1</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceServiceList[index].cost}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceServiceList[index].cost}</td>
        </tr>`;
    }

    for (var index = 0; index < invoiceReplacementList.length; index++) {
        invoiceDataString = invoiceDataString + `
        <tr>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceReplacementList[index].description_replacement}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceReplacementList[index].quantity}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceServiceList[index].cost}</td>
            <td style="border: 1px solid black; border-collapse: collapse; text-align: center;">${invoiceReplacementList[index].quantity * invoiceServiceList[index].cost}</td>
        </tr>`;
    }



    let fileName = `${billNo}.pdf`;
    let contentHTML = await `
        <!doctype html>
        <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <div id="pageHeader">
                <h3 style="text-align: center;"><b>FACTURA</b></h3>
                <p style="text-align: center;"><b>Compañia de Soporte Tecnico RestTeam </b></p>
                <p style="text-align: center;"><b>Santa Cruz - Bolivia </b></p>
            </div>
            <div style="padding-left: 5%;>
                <p style="text-align: left;">-----------------------------------------------------------------------------------------------</p>
                <small style="text-align: left;"><b>NIT: 1020703023</b></small><br>
                <small style="text-align: left;"><b>NRO. FACTURA: ${billNo}</b></small><br>
                <small style="text-align: left;"><b>NRO. SOPORTE: ${supportNo}</b></small><br>
                <small style="text-align: left;"><b>CANT. DIAS SOPORTE: ${invoiceHeader.quantity_day_support}</b></small>
                <p style="text-align: left;">-----------------------------------------------------------------------------------------------</p>
                <small style="text-align: left;"><b>FECHA DE EMISION: ${invoiceHeader.service_date.toLocaleString()}</b></small><br>
                <small style="text-align: left;"><b>NIT: ${invoiceHeader.nit}</b></small><br>
                <small style="text-align: left;"><b>RAZON SOCIAL: ${invoiceHeader.corporate}</b></small>
                <p style="text-align: left;">-----------------------------------------------------------------------------------------------</p>
            </div>
            <div style="padding: 3%;">
                <table class="egt" style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Descripcion</th>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Cantidad</th>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Precio Uni.</th>
                            <th style="border: 1px solid black; border-collapse: collapse;" bgcolor="C9E6FA">Total</th>
                        </tr>
                    </thead>
                    ${invoiceDataString}
                    <tr>
                     <td colspan="3" style="border: 1px solid black; border-collapse: collapse;">Total A Pagar</td>
                     <td style="border: 1px solid black; border-collapse: collapse;text-align: center">${invoiceHeader.total_cost}</td>
                    </tr>
                    </tbody>
                </table>
                <div id="pageFooter" style="text-align: center;">
                    <b>printed by: </b>${username}
                </div>
            </div>
        </body>
        </html>`;
    await externalLibrary.generatePDFInvoice(contentHTML, fileName);
    return await fileName;
}

const wachtInvoice = async (req, res) => {
    const noBill = req.params.bill;
    let filePath = `${addressInvoice}${noBill}.pdf`;
    console.log(filePath);
    fs.readFile(filePath, function (err, data) {
        console.log("The PDF File does exists");
        res.contentType("application/pdf");
        res.send(data);
    });
}

const getInvoiceOwner = async (req, res) => {
    console.log(req.params.ci);
    const list_invoice = await serviceInvoiceModel.getInvoiceOwnerData(req.params.ci);
    res.json({list_invoice});
}

module.exports = {
    getListServiceInvoice,
    getRegisterInvoice,
    putServiceInvoice,
    putEnableDisable,
    generatePDFFileInvoice,
    wachtInvoice,
    getInvoiceOwner
}
