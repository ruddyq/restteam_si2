const serviceManageModel = require('../../../models/SupportServiceManagement/serviceManage/serviceManage.model');
const externalLibrary = require('../../../config/externalLibrary');

/**
 * get view list service
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const getServiceManage = async (req, res) => {
    const listservice = await serviceManageModel.getListService();
    res.status(200).render('SupportServiceManagement/serviceManage/service_manage', {
        listservice
    })
}
/**
 * Method POST: register new Services
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const postServiceManage = async (req, res) => {
    const { description_service, service_guarantee } = req.body;
    if (await serviceManageModel.getVerifyPermitService(req.user.id_user, 55)) {
        const id_service = await serviceManageModel.insertService(description_service, service_guarantee);
        if (id_service > 0) {
            if (await externalLibrary.writeBackLog(req.user.username, `register service  ${description_service}`, req.device.type)) {
                console.log("Register to bitacora");
            } else {
                console.log("Not register to bitacora");
            }
            req.flash('success_msg', 'Service successfully registered');
            res.redirect('/support_service_manage/service_manage');
        } else {
            req.flash('error_msgs', 'Existing service.');
            res.redirect('/support_service_manage/service_manage');
        }
    } else {
        req.flash('error_msgs', `Error! ${req.user.username} not Authorized.`);
        res.redirect('/support_service_manage/service_manage');
    }
}
/**
 * METHOD GET : get view for service updated
 * @param {*} req
 * @param {*} res
 */
const getUpdateService = async (req, res) => {
    if (await serviceManageModel.getVerifyPermitService(req.user.id_user, 59)) {
        const id_service = req.params.id_service;
        const list_service = await serviceManageModel.getServiceUpdate(id_service);
        res.render('SupportServiceManagement/serviceManage/edit_service_manage', {
            list_service,
            id_service
        });
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method PUT: update service
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const putServiceManage = async (req, res) => {
    if (await serviceManageModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { description_service, service_guarantee } = req.body;
        if (await serviceManageModel.updateService(req.params.id_service, description_service, service_guarantee)) {
            if (await externalLibrary.writeBackLog(req.user.username, `update service  ${description_service} with id ${req.params.id_service}`, req.device.type)) {
                console.log("Registered update to bitacora");
            } else {
                console.log("Not registered update to bitacora");
            }
            req.flash('success_msg', 'Service successfully updated');
            res.redirect(`/support_service_manage/service_manage`);
        } else {
            req.flash('error_msgs', 'Service Not updated, change description');
            res.redirect(`/support_service_manage/service_manage/update/${req.params.id_service}`);

        }
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method PUT: include or exclude service to a guarantee
 * @param {*} req : request (peticion)
 * @param {*} res : response (respuesta)
 */
const includeExcludeServiceGuarantee = async (req, res) => {
    if (await serviceManageModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { id_service } = req.params;
        if (await serviceManageModel.excludeIncludeToGuaranteeService(id_service)) {
            req.flash('success_msg', `Service ID: ${id_service} channged successfully`);
            res.redirect(`/support_service_manage/service_manage`);
        } else {
            req.flash('error_msgs', `Error! I have a problem wirth change service ID: ${id_service}`);
            res.redirect(`/support_service_manage/service_manage`);
        }
    } else {
        req.flash('error_msgs', `Error! Not Authorized. ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

module.exports = {
    getServiceManage,
    postServiceManage,
    putServiceManage,
    getUpdateService,
    includeExcludeServiceGuarantee
}
