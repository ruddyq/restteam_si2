const technicalModel = require('../../../models/SupportServiceManagement/technicalManage/technicalManage.model');
const externalLibrary = require('../../../config/externalLibrary');
const userModel = require('../../../models/UserManagement/userManage/user.model');

const getTechnical = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user,46)){
        const technicalList = await technicalModel.getTechnical();
        var specialtyList = await technicalModel.getQuery(`select * from specialty;`);
        const reportSupportService = await userModel.getReportPrincipal();
        res.status(200).render('SupportServiceManagement/technicalManage/technical_manage', {
          technicalList, specialtyList, reportSupportService
        });
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/');
    }
}

const postTechnical = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 47)){
        const{name, email, phone, specialty} = req.body;
        console.log(specialty);
        var idTechnical = await technicalModel.registerTechnical(name, email, phone, specialty);
        if(idTechnical>0){
            if(await externalLibrary.writeBackLog(req.user.username, `register technical with id: ${idTechnical}, ${name}, ${email}, ${phone}`,req.device.type)){
                console.log('write register technical succefully');
            }else{
                console.log('write register technical failure');
            }
            req.flash('success_msg', `Technical Registered Successfully with id: ${idTechnical}`);
            res.redirect('/support_service_management/technical_manage');
            
        }else{
            const errors = [];
            errors.push({ text: "Error!. Your Data was not saved" });
            res.status(200).render('SupportServiceManagement/technicalManage/technical_manage', {
                name, email, phone, errors               
            });
        }

    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/technical_manage');
    }
}

const enableDisableTechnical = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 49)){
        const{status} = req.body;
        let idTechnical = req.params.id;
        var enableDisable = await technicalModel.getQuery(`select enable_disable_technical(${idTechnical});`);
        if(enableDisable){
            if(status==="disable"){
                if(await externalLibrary.writeBackLog(req.user.username,`Disable technical with id: ${idTechnical}`,req.device.type)){
                    console.log("write Disable technical succesfuly");
                }else{
                    console.log("write Disable technical failure");
                }
                req.flash('success_msg', `Technical with id: ${idTechnical} Disable Successfully`);
                res.redirect('/support_service_management/technical_manage');

            }else{
                if(await externalLibrary.writeBackLog(req.user.username,`Enable technical with id: ${idTechnical}`,req.device.type)){
                    console.log("write Enable technical succesfuly");
                }else{
                    console.log("write Enable technical failure");
                }
                req.flash('success_msg', `Technical with id: ${idTechnical} Enable Successfully`);
                res.redirect('/support_service_management/technical_manage');
            }
            
        }else{
            req.flash('error_msgs', 'Error! Enable/Disable Fail.');
            res.redirect('/support_service_management/technical_manage');
        }

    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/technical_manage');
    }

}

const getUpdateTechnical = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 48)){
        let idTechnical = req.params.id;
        var dataTechnical = await technicalModel.getQuery(`select "name" , email , phone from technical where id_technical = ${idTechnical};`);
        dataTechnical = dataTechnical[0];
        var specialtyList = await technicalModel.getSpecialtyList(idTechnical);
        let b= true;
        if(specialtyList.length===0){
            b= false;
        }
        var technicalSpecialty = await technicalModel.getTechnicalSpecialty(idTechnical);
        res.status(200).render('SupportServiceManagement/technicalManage/technical_update', {
          specialtyList, technicalSpecialty, dataTechnical,  idTechnical, b
        });
    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/technical_manage');
    }
}

const putUpdateTechnical = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 48)){
        const{name, email, phone} = req.body;
        let idTechnical = req.params.id;
        if(await technicalModel.updateTechnical(idTechnical, name, email, phone)){
            if(await externalLibrary.writeBackLog(req.user.username, `update technical with id: ${idTechnical}, ${name}, ${email}, ${phone}`, req.device.type)){
                console.log("write update technical successfully");
            }else{
                console.log("write update technical fail");
            }
            req.flash('success_msg', `Update Technical Successfully! with id: ${idTechnical}`);
            res.redirect('/support_service_management/technical_manage');
        }else{
            req.flash('error_msgs', `Error Update Technical Failed! with id: ${idTechnical}`);
            res.redirect(`/support_service_management/technical_manage/technical_update/${idTechnical}`);
        }

    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/technical_manage');
    }
}

const postTechnicalSpecialty = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 47)){
        const{specialty} = req.body;
        let idTechnical = req.params.id;
        if(await technicalModel.registerTechnicalSpecialty(idTechnical, specialty)){
            if(await externalLibrary.writeBackLog(req.user.username, `register technical specialty with id: ${idTechnical}, id specialty: ${specialty}`,req.device.type)){
                console.log('write register technical specialty succefully');
            }else{
                console.log('write register technical specialty failure');
            }
            req.flash('success_msg', `Technical Specialty Add Successfully with id: ${idTechnical}`);
            res.redirect(`/support_service_management/technical_manage/technical_update/${idTechnical}`);
        }else{
            req.flash('error_msgs', 'Error! Technical Specialty NOT Added');
            res.redirect(`/support_service_management/technical_manage/technical_update/${idTechnical}`);
        }

    }else{
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/technical_manage');
    }
    
}

const getSpareBox = async (req, res) => {
    if(await technicalModel.userHavePrivilege(req.user.id_user, 77)){
        var id_technical = req.params.id;
        console.log(id_technical);
        var listReplacement = await technicalModel.getListReplacement(id_technical);
        var b=true;
        if(listReplacement.length===0){
            b=false;
        }listSpareBox
        var listSpareBox = await technicalModel.getListSpareBox(id_technical);
        res.status(200).render('SupportServiceManagement/technicalManage/spare_box', {
            id_technical, listReplacement, listSpareBox,b
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/support_service_management/technical_manage');
    }       
}

module.exports = {
    getTechnical,
    postTechnical,
    enableDisableTechnical,
    getUpdateTechnical,
    putUpdateTechnical,
    postTechnicalSpecialty,
    getSpareBox
}