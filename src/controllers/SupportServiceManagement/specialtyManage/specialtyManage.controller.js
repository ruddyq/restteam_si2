const specialtyModel = require('../../../models/SupportServiceManagement/specialtyManage/specialtyManage.model');
const externalLibrary = require('../../../config/externalLibrary');

const getSpecialty = async (req, res) => {
    var idUser = req.user.id_user;
    if (await specialtyModel.userHavePrivilege(idUser, 51)){
        const specialtyList = await specialtyModel.getSpecialtys();
        res.render('SupportServiceManagement/specialtyManage/specialty_manage', {
            specialtyList
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/support_service_management/specialty_manage');
    }
}

const postSpecialty = async (req, res) => {
    var idUser = req.user.id_user;
    if (await specialtyModel.userHavePrivilege(idUser, 50)){
        const {description_specialty} = req.body;
        if (await specialtyModel.insertSpecialty(description_specialty)) {
            if( await externalLibrary.writeBackLog(req.user.username, `Specialty register:  ${description_specialty}`, req.device.type)){
                console.log('Registrado en la Bitacora Exitosamente');
            } else {
                console.log('Fallo al registrar en la bitacora');
            }
            req.flash('success_msg', 'Especialidad registrado exitosamente');
            res.redirect('/support_service_management/specialty_manage');
        } else {
            req.flash('error_msgs', 'Error! Especialidad ya existe');
            res.redirect('/support_service_management/specialty_manage');
        }
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/support_service_management/specialty_manage');
    }
}

const getUpdateSpecialty = async (req, res) => {
    var idUser = req.user.id_user;
    if (await specialtyModel.userHavePrivilege(idUser, 50)){
        const specialtyData = await specialtyModel.getSpecialtyUpdateData(req.params.id_specialty);
        const id_specialty = req.params.id_specialty;
        
        res.render('SupportServiceManagement/specialtyManage/specialty_update',{
            specialtyData,
            id_specialty
        });
    }else{
        req.flash('error_msgs', 'Error! No Autorizado.');
        res.redirect('/support_service_management/specialty_manage');
    }
}

const putUpdateSpecialty = async (req, res) => {
    const {description_specialty} = req.body;
    if ( await specialtyModel.updateSpecialty(req.params.id_specialty, description_specialty)){
        if (await externalLibrary.writeBackLog(req.user.username, `Update Specialty id: ${req.params.id_specialty} (${description_specialty})`, req.device.type)) {
            console.log('Registrado en la Bitacora Exitosamente');
        } else {
            console.log('Fallo al registrar en la bitacora');
        }
        req.flash('success_msg', 'Especialidad Actualizado exitosamente');
        res.redirect('../specialty_manage');
    }else{
        req.flash('error_msgs', 'Error! Especialidad no actualizado. La Descripcion ya existe');
        res.redirect('/support_service_management/specialty_manage');
    }
    
}

module.exports = {
    getSpecialty,
    postSpecialty,
    getUpdateSpecialty, 
    putUpdateSpecialty
}