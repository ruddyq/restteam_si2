const supportServiceModel = require('../../../models/SupportServiceManagement/supportServiceManage/supportService.model');
const externalLibrary = require('../../../config/externalLibrary');

const getSupportService = async (req, res) => {
    var listSupport = await supportServiceModel.getListSupportService();
    var listTechnical = await supportServiceModel.getListTechnical();
    var listRequest = await supportServiceModel.getQuery('select request_no from request_support_service where state_request=1 and request_no not in(select request_no from technical_support_service);');
    let b = true;
    if (listRequest.length === 0) {
        b = false;
    }
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 67)) {
        res.status(200).render('SupportServiceManagement/supportServiceManage/support_service_manage', {
            listSupport, listTechnical, listRequest, b
        });
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/');
    }
}

const postSupportService = async (req, res) => {
    const { date, observation, requestNo, idTechnical } = req.body;
    var idUser = req.user.id_user;
    console.log(requestNo);
    if (await supportServiceModel.getVerifyPermitService(idUser, 66)) {
        var supportNo = await supportServiceModel.insertSupportService(date, observation, requestNo, idTechnical);
        if (supportNo > 0) {
            if (await externalLibrary.writeBackLog(req.user.username, `register support service ${supportNo} with ${date}, ${observation}, ${requestNo}, ${idTechnical}`, req.device.type)) {
                console.log('write register support service successfully');
            } else {
                console.log('write register support service failure');
            }
            req.flash('success_msg', `Support Service N° ${supportNo} added Successfully`);
            res.redirect(`/support_service_management/support_service_manage/${supportNo}`);

        } else {
            var listTechnical = await supportServiceModel.getListTechnical();
            var listRequest = await supportServiceModel.getQuery('select request_no from request_support_service;');
            var technicalName = await supportServiceModel.getQuery(`select "name" from technical where id_technical=${idTechnical}`);
            technicalName = technicalName[0].name;
            var errors = [];
            errors.push({ text: "Error: Los datos no fueron guardados" });
            res.status(200).render('SupportServiceManagement/supportServiceManage/support_service_manage', {
                date,
                observation,
                requestNo,
                idTechnical,
                errors,
                listTechnical,
                listRequest
            });
        }
    } else {
        req.flash('error_msgs', 'Error! Not Authorized.');
        res.redirect('/support_service_management/support_service_manage');
    }
}
/**
 * Method Put to Change State of a Support Service
 * @param {*} req : request
 * @param {*} res : response
 */
const putStateSupportService = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 67)) {
        var supportNo = req.params.support_no;
        if (await supportServiceModel.changeStateSupportService(supportNo)) {
            if (await externalLibrary.writeBackLog(req.user.username, `Changed state to finished of support service N° ${supportNo}`, req.device.type)) {
                console.log("write (changed state to finished of support service) successfully");
            } else {
                console.log("write (changed state to finished of support service) fail");
            }
            req.flash('success_msg', `Servicio de Soporte Tecnico N° ${supportNo} Finalizado Exitosamente, REGISTRE FACTURA!`);
            res.redirect('/support_service_management/support_service_manage');
        } else {
            req.flash('error_msgs', 'Error! No se pudo cambiar el estado a Finalizado');
            res.redirect('/support_service_management/support_service_manage');
        }
    } else {
        req.flash('error_msgs', 'Error! Not Autorizado.');
        res.redirect('/support_service_management/support_service_manage');
    }
}
/**
 * Method GET: get view update support service 
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getUpdateSupportService = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const supportServiceData = await supportServiceModel.getSupportServiceData(req.params.support_no);
        const listServiceToAssign = await supportServiceModel.getServicesAvailable(req.params.support_no);
        const serviceListOfSupport = await supportServiceModel.getServiceListOfSupportService(req.params.support_no);
        const replacementListOfSupport = await supportServiceModel.getReplacementListOfSupportService(req.params.support_no);
        const listReplacementToAssign = await supportServiceModel.getReplacementAvailable(req.params.support_no);
        let partialCost = 0;
        for (let indexListService = 0; indexListService < serviceListOfSupport.length; indexListService++) {
            const { support_no, id_service, description_service, cost } = await serviceListOfSupport[indexListService];
            partialCost = (partialCost + parseFloat(cost));
            serviceListOfSupport[indexListService] = await {
                support_no,
                id_service,
                description_service,
                cost,
                partialCost
            };
        }
        partialCost = 0;
        for (let indexListReplacement = 0; indexListReplacement < replacementListOfSupport.length; indexListReplacement++) {
            const { support_no, cod_replacement, description_replacement, quantity, cost } = await replacementListOfSupport[indexListReplacement];
            partialCost = (partialCost + parseFloat(cost) * quantity);
            replacementListOfSupport[indexListReplacement] = await {
                support_no,
                cod_replacement,
                description_replacement,
                quantity,
                cost,
                partialCost
            };
        }
        res.render('SupportServiceManagement/supportServiceManage/support_service_update', {
            support_no: req.params.support_no,
            supportServiceData,
            listServiceToAssign,
            serviceListOfSupport,
            listReplacementToAssign,
            replacementListOfSupport
        });
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method PUT: update date and observation of the support service 
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putUpdateSupportServiceData = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { support_no } = req.params;
        const { date_service, observations } = req.body;
        if (await supportServiceModel.updateSupportServiceData(support_no, date_service, observations)) {
            if (await externalLibrary.writeBackLog(req.user.username, `update support service No. ${support_no} data: (${date_service}, ${observations})`, req.device.type)) {
                console.log('write backlog successfully');
            } else {
                console.log('problems to write backlog');
            }
            req.flash('success_msg', `The support service was updated successfully`);
            res.redirect(`/support_service_manage/support_service_manage/update/${support_no}`);
        } else {
            req.flash('error_msgs', 'Support service not updated');
            res.redirect(`/support_service_manage/support_service_manage/update/${support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method PUT: add service of the support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putServiceAddSupportTechnical = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { support_no } = req.params;
        const { id_service, cost_service } = req.body;
        if (await supportServiceModel.addServiceToSupportService(support_no, id_service, cost_service)) {
            if (await externalLibrary.writeBackLog(req.user.username, `add service ID: ${id_service} cost: ${cost_service} to support service No. ${support_no}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write backlog');
            }
            req.flash('success_msg', `The Service was added to support service successfully`);
            res.redirect(`/support_service_manage/support_service_manage/update/${support_no}`);
        } else {
            req.flash('error_msgs', 'Problems to add new Services');
            res.redirect(`/support_service_manage/support_service_manage/update/${support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method DELETE: delete service of the support service 
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const deleteServiceToSupportTechnical = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        if (await supportServiceModel.deleteServiceToSupportService(req.params.support_no, req.params.id_service)) {
            if (await externalLibrary.writeBackLog(req.user.username, `delete service with ID: ${req.params.id_service} in Support No:${req.params.support_no}`, req.device.type)) {
                console.log('write to the backlog successfully');
            } else {
                console.log('problems to write to the backlog');
            }
            req.flash('success_msg', `The Service was deleted to the support technical service.`);
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        } else {
            req.flash('error_msgs', 'Problems to delete Service');
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method PUT: add replacement of the support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const putReplacementAddSupportTechnical = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { support_no } = req.params;
        const { cod_replacement, quantity, cost } = req.body;
        console.log(support_no, quantity, cost);
        if (await supportServiceModel.addReplacementToSupportService(support_no, cod_replacement, quantity, cost)) {
            if (await externalLibrary.writeBackLog(req.user.username, `added replacement cod: ${cod_replacement}, count: ${quantity}, cost: ${cost}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write to backlog');
            }
            req.flash('success_msg', `The Replacement was added successfully.`);
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        } else {
            req.flash('error_msgs', `The spare box of the technical doesn't sufficient. replacement doesn't register.`);
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method DELETE: delete a replacement of the support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const deleteReplacementToSupportTechnical = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        if (await supportServiceModel.deleteReplacementToSupportService(req.params.support_no, req.params.cod_replacement)) {
            if (await externalLibrary.writeBackLog(req.user.username, `delete replacement cod: ${req.params.cod_replacement}, to Support No.:${req.params.support_no}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write to the backlog');
            }
            req.flash('success_msg', `The Replacement was deleted successfully.`);
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        } else {
            req.flash('error_msgs', 'The replacement was not deleted.');
            res.redirect(`/support_service_manage/support_service_manage/update/${req.params.support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect('/support_service_manage/service_manage');
    }
}

/**
 * Method GET: get view for support service to add services and replacement 
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const getAddServicesReplacementToTheSupportService = async (req, res) => {
    const { support_no } = req.params;
    const SupportServiceData = await supportServiceModel.getAllSupportServiceData(support_no);
    const listServiceToAssign = await supportServiceModel.getServicesAvailable(support_no);
    const serviceListOfSupport = await supportServiceModel.getServiceListOfSupportService(support_no);
    const replacementListOfSupport = await supportServiceModel.getReplacementListOfSupportService(support_no);
    const listReplacementToAssign = await supportServiceModel.getReplacementAvailable(support_no);
    let partialCost = 0;
    for (let indexListService = 0; indexListService < serviceListOfSupport.length; indexListService++) {
        const { support_no, id_service, description_service, cost } = await serviceListOfSupport[indexListService];
        partialCost = (partialCost + parseFloat(cost));
        serviceListOfSupport[indexListService] = await {
            support_no,
            id_service,
            description_service,
            cost,
            partialCost
        };
    }
    partialCost = 0;
    for (let indexListReplacement = 0; indexListReplacement < replacementListOfSupport.length; indexListReplacement++) {
        const { support_no, cod_replacement, description_replacement, quantity, cost } = await replacementListOfSupport[indexListReplacement];
        partialCost = (partialCost + parseFloat(cost) * quantity);
        replacementListOfSupport[indexListReplacement] = await {
            support_no,
            cod_replacement,
            description_replacement,
            quantity,
            cost,
            partialCost
        };
    }
    res, res.render('SupportServiceManagement/supportServiceManage/support_service_add_spare_services', {
        SupportServiceData,
        listServiceToAssign,
        serviceListOfSupport,
        listReplacementToAssign,
        replacementListOfSupport
    });
}

/**
 * Method POST: add services for support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postAddServiceToSupportService = async (req, res) => {
    const { support_no } = req.params;
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { id_service, cost_service } = req.body;
        if (await supportServiceModel.addServiceToSupportService(support_no, id_service, cost_service)) {
            if (await externalLibrary.writeBackLog(req.user.username, `add service ID: ${id_service} cost: ${cost_service} to support service No. ${support_no}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write backlog');
            }
            req.flash('success_msg', `The Service was added to support service successfully`);
            res.redirect(`/support_service_management/support_service_manage/${support_no}`);
        } else {
            req.flash('error_msgs', 'Problems to add new Services');
            res.redirect(`/support_service_management/support_service_manage/${support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect(`/support_service_management/support_service_manage/${support_no}`);
    }
}

/**
 * Method POST: add replacement for support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const postAddReplacementToSupportService = async (req, res) => {
    const { support_no } = req.params;
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        const { cod_replacement, quantity, cost } = req.body;
        console.log(support_no, quantity, cost);
        if (await supportServiceModel.addReplacementToSupportService(support_no, cod_replacement, quantity, cost)) {
            if (await externalLibrary.writeBackLog(req.user.username, `added replacement cod: ${cod_replacement}, count: ${quantity}, cost: ${cost}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write to backlog');
            }
            req.flash('success_msg', `The Replacement was added successfully.`);
            res.redirect(`/support_service_management/support_service_manage/${support_no}`);
        } else {
            req.flash('error_msgs', `The spare box of the technical doesn't sufficient. replacement doesn't register.`);
            res.redirect(`/support_service_management/support_service_manage/${support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect(`/support_service_management/support_service_manage/${support_no}`);
    }
}

/**
 * Method DELETE: delete services for support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const deleteServiceToSupportService = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        if (await supportServiceModel.deleteServiceToSupportService(req.params.support_no, req.params.id_service)) {
            if (await externalLibrary.writeBackLog(req.user.username, `delete service with ID: ${req.params.id_service} in Support No:${req.params.support_no}`, req.device.type)) {
                console.log('write to the backlog successfully');
            } else {
                console.log('problems to write to the backlog');
            }
            req.flash('success_msg', `The Service was deleted to the support technical service.`);
            res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
        } else {
            req.flash('error_msgs', 'Problems to delete Service');
            res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
    }
}

/**
 * Method DELETE: delete replacement for support service
 * @param {*} req : request (peticion) 
 * @param {*} res : response (respuesta)
 */
const deleteReplacementToSupportService = async (req, res) => {
    if (await supportServiceModel.getVerifyPermitService(req.user.id_user, 59)) {
        if (await supportServiceModel.deleteReplacementToSupportService(req.params.support_no, req.params.cod_replacement)) {
            if (await externalLibrary.writeBackLog(req.user.username, `delete replacement cod: ${req.params.cod_replacement}, to Support No.:${req.params.support_no}`, req.device.type)) {
                console.log('write to backlog successfully');
            } else {
                console.log('problems to write to the backlog');
            }
            req.flash('success_msg', `The Replacement was deleted successfully.`);
            res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
        } else {
            req.flash('error_msgs', 'The replacement was not deleted.');
            res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
        }
    } else {
        req.flash('error_msgs', `Not Authorized, ${req.user.username} blocked.`);
        res.redirect(`/support_service_management/support_service_manage/${req.params.support_no}`);
    }
}

module.exports = {
    getSupportService,
    postSupportService,
    putStateSupportService,
    getUpdateSupportService,
    putUpdateSupportServiceData,
    putServiceAddSupportTechnical,
    deleteServiceToSupportTechnical,
    putReplacementAddSupportTechnical,
    deleteReplacementToSupportTechnical,
    getAddServicesReplacementToTheSupportService,
    postAddServiceToSupportService,
    postAddReplacementToSupportService,
    deleteServiceToSupportService,
    deleteReplacementToSupportService
}