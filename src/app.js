/**
 * Imports
 */
const express = require('express');
const path = require('path');
const expressHandlebars = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
const morgan = require('morgan');
const device = require('express-device');
const multer = require('multer');

const { v4: uuidv4 } = require('uuid');

const storage = multer.diskStorage({
    destination: path.join(__dirname,'../../security/profiles'),
    filename: (req,file,cb)=>{
        cb(null,uuidv4()+path.extname(file.originalname).toLowerCase()); 
    }
});

/**
 * 
 * Initializations
 */
const app = express();
require('./config/database');
require('./config/passport');

/**
 * Settings
 */
app.set('port', process.env.PORT || 3000);                  //using a port assigned for OS; else 3000 port
app.set('views', path.join(__dirname, 'views'));
var hbs = expressHandlebars.create({
    defaultLayout: 'main',                                 // main file
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),   // partitions hbs
    extname: '.hbs',                                        // files extensions
    helpers : {
        foo: function (a, b, opts) {
             return (a==b)? opts.fn(this) : opts.inverse(this); ; 
        }
    }
})
app.engine('.hbs', hbs.engine);

app.set('view engine', '.hbs');                             // using handlebars

/**
 * Middleware
 */
app.use(device.capture());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(methodOverride('_method'));                        // you can to send html methods as put, delete
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true
}));                                                        //you can initialize sessions user 
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());                                           // motor of functionality messages
app.use(multer({
    storage:storage,
    //limits: {fileSize: 1000000} 
}).single('image'));

/**
 * Globals  Variables
 */
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error_msgs = req.flash('error_msgs');
    res.locals.error = req.flash('error');
    res.locals.success = req.flash('success');
    res.locals.user = req.user || null;
    next();
});

/**
 * Routes
 */
app.use(require('./routes/UserManagement/userManage.route'));
app.use(require('./routes/UserManagement/roleManage.route'));
app.use(require('./routes/UserManagement/privilegeManage.route'));
app.use(require('./routes/UserManagement/bitacoraManage.route'));
app.use(require('./routes/CompanyManagement/companyManage.route'));
app.use(require('./routes/CompanyManagement/contractManage.route.js'));
app.use(require('./routes/WarehouseManagement/exitNoteManage.route'));
app.use(require('./routes/WarehouseManagement/workshopManage.route'));
app.use(require('./routes/WarehouseManagement/replacementManage.route'));
app.use(require('./routes/WarehouseManagement/entryNoteManage.route'));
app.use(require('./routes/SupportServiceManagement/supportServiceManage.route'));
app.use(require('./routes/SupportServiceManagement/serviceManage.route'));
app.use(require('./routes/SupportServiceManagement/serviceInvoice.route'));
app.use(require('./routes/SupportServiceManagement/specialtyManage.route'));
app.use(require('./routes/SupportServiceManagement/technicalManage.route'));
app.use(require('./routes/WarehouseManagement/devolutionNoteManage.route'));
app.use(require('./routes/OwnerManagement/productManage.route'));
app.use(require('./routes/OwnerManagement/ownerManage.route'));
app.use(require('./routes/OwnerManagement/productCategory.route'));
app.use(require('./routes/OwnerManagement/guaranteePolicyManage.route'));
app.use(require('./routes/OwnerManagement/requestSupportServiceManage.route'));
app.use(require('./routes/OwnerManagement/typeSupportService.route'));
app.use(require('./routes/OwnerManagement/bitacoraOwnerManage.route'));
app.use(require('./routes/WarehouseManagement/providerManage.route'));
app.use(require('./routes/WarehouseManagement/replacementCategoryManage.route'));
app.use(require('./routes/CompanyManagement/representativeManage.route'));
app.use(require('./routes/index.route'));

/**
 * Static Files
 */
app.use(express.static(path.join(__dirname, 'public')));     //styles files (*.css, *.js)

module.exports = app;
