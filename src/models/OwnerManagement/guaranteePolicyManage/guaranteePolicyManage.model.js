const pool =  require('../../../config/database');

module.exports = {

    /**
     * validate permit User
     * @param {integer} idUser : user ID 
     * @param {*} idPrivilege : privilege ID
     */
    async getVerifyPermitService(idUser, idPrivilege){
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get Data List of the 
     */
    async getListGuaranteePolicy(){
        try {
            const responseGuaranteePolicy = await pool.query(`select gp.cod_policy, to_char(gp.date_purchase::DATE,'Day dd Mon yyyy') as date_purchase, to_char(gp.deadline::DATE,'Day dd Mon yyyy') as deadline, gp.proof, gp.state_guarantee, gp.code_owner, o."name", p.description_product, p.model, p.brand, pc.description_category, gp.nro_invoice, cc.name_company from guarantee_policy gp, "owner" o, product p, product_category pc, client_company cc where gp.ci_owner=o.ci and gp.cod_product=p.cod_product and gp.id_category=pc.id_category and gp.nit_client_company=cc.nit order by date_purchase`);
            return responseGuaranteePolicy.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get last status of the validate guarantee policy
     * @param {string} codPolicy : guarantee policy code
     */
    async validateGuaranteePolicy(codPolicy, newState){
        try {
            const response = await pool.query(`select validate_policy_guarantee('${codPolicy}', ${newState})`);
            return response.rows[0].validate_policy_guarantee;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get Data [cod_policy@ci_owner@code_owner] for load to QR code
     * @param {String} codPolicy : Guarantee Policy Code
     */
    async getDataForLoadQR(codPolicy){
        try {
            const response = await pool.query(`select cod_policy, ci_owner, code_owner from guarantee_policy gp where cod_policy='${codPolicy}'`);
            return await `${response.rows[0].cod_policy}@${response.rows[0].ci_owner}@${response.rows[0].code_owner}`;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get email and name of the owner
     * @param {String} codPolicy : Guarantee Policy Code
     */
    async getDataOwner(codPolicy){
        try {
            const responseDataOwner = await pool.query(`select o.email, o."name" from guarantee_policy gp, "owner" o where gp.ci_owner=o.ci and cod_policy='${codPolicy}'`);
            return responseDataOwner.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Company List available if its is enable and its contract is enable
     */
    async getCompanyListAvailable(){
        try {
            const responseCompanyList = await pool.query(`select nit, name_company from client_company cc, contract_company cc2 where cc.nit=cc2.nit_company and cc2.end_date>now() and cc."enable" and cc2.status_contract`);
            return responseCompanyList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get data of the product and its owner 
     * @param {integer} codProductOwner : code Product Owner
     */
    async getRegisterDataProductOwner(codProductOwner){
        try {
            const resposneDataProductOwner = await pool.query(`select o."name", p.description_product, p.model, p.brand, pc.description_category from product_owner po, "owner" o, product p, product_category pc where po.ci=o.ci and po.cod_product=p.cod_product and p.id_category=pc.id_category and po.code=${codProductOwner}`);
            return resposneDataProductOwner.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * 
     * @param {String} policyCode : policy code 
     * @param {String} datePurchase : date of the purchase 
     * @param {String} endDate : end date of conclusion policy
     * @param {integer} proofNo : invoice no of purchase
     * @param {bigint} nitCompany : nit company
     * @param {integer} codProductOwner : cod Product Owner registered
     * @param {String} filename : file name proof 
     */
    async registerGuaranteePolicy(policyCode, datePurchase, endDate, proofNo, nitCompany, codProductOwner, filename){
        try {
            const response = await pool.query(`select register_guarantee_policy('${policyCode}', cast('${datePurchase}' as date), cast('${endDate}' as date), '${filename}', '${proofNo}', cast(${nitCompany} as bigint), ${codProductOwner})`);
            return response.rows[0].register_guarantee_policy;
        } catch (error) {
            console.error("Error Guarantee Policy: ", error);
            return false;
        }
    },

    /**Mobile */

    async getListPolicy(ci){
        try{
            var response = await pool.query(`select gp.cod_policy, gp.date_purchase , gp.deadline , gp.state_guarantee , gp.nro_invoice , gp.code_owner , gp.nit_client_company, cc.name_company , cc.phone , cc.email , p.description_product , p.brand , p.model
            from guarantee_policy gp , client_company cc, product p 
            where gp.nit_client_company = cc.nit and gp.cod_product = p.cod_product and gp.ci_owner = '${ci}'
            order by gp.deadline desc, gp.state_guarantee desc, cc.name_company ;
            `);
            response = response.rows;

            response.forEach(element => {
                element.date_purchase = element.date_purchase.toLocaleDateString();
                element.deadline = element.deadline.toLocaleDateString();
            });
            return response;
        }catch (error) {
            console.error(error);
            return null;
        }

    }
}