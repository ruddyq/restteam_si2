const pool = require('../../../config/database');

module.exports = {
    
    async getVerifyPermit(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getTypeSupportServiceList(){
        try{
            const listProductCategory = await pool.query(`select id_type, description_type, "enable", quantity_type_support(id_type) 
                                                          from type_support_service tss 
                                                          order by quantity_type_support desc ;`);
            return listProductCategory.rows;
        }catch (error){
            console.error(error);
            return null;
        }
    },
    async registerTypeSupportService(description){
        try{
            const response = await pool.query(`select register_type_support('${description}');`);
            return response.rows[0].register_type_support;
        }catch (error){
            console.error(error);
            return false;
        }
    }, 

    async enableDisableTypeSupportService(idType){
        try{
            const response = await pool.query(`select enable_disable_type_support_service(${idType});`);
            return response.rows[0].enable_disable_type_support_service;
        }catch (error){
            console.error(error);
            return false;
        }
    }, 

    async getDataTypeSupportService(id){
        try{
            const dataCategory = await pool.query(`select description_type from type_support_service where id_type= ${id};`);
            return dataCategory.rows[0].description_type;
        }catch (error){
            console.error(error);
            return null;
        } 
    },
    async updateTypeSupportService(id,description){
        try{
            console.log("cerca...");
            const update = await pool.query(`update type_support_service set description_type = '${description}' where id_type = ${id};`);
            console.log("llego!");
            return update;
        }catch (error){
            console.error(error);
            return false;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },
    


}