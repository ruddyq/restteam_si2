const pool = require('../../../config/database');

module.exports = {

    async getVerifyPermit(idUser, idPrivilege) {
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async ListProductCategory() {
        try {
            const listProductCategory = await pool.query('select * from product_category;');
            return listProductCategory.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },
    async registerProductCategory(description) {
        try {
            const dataCategory = await pool.query(`select register_product_category('${description}')`);
            return dataCategory.rows[0].register_product_category;
        } catch (error) {
            console.error(error);
            return null;
        }
    },
    async getDataProductCategory(id_category) {
        try {
            const dataCategory = await pool.query(`select description_category from product_category where id_category= ${id_category}`);
            return dataCategory.rows[0].description_category;
        } catch (error) {
            console.error(error);
            return null;
        }
    },
    async updateProductCategory(id_category, description) {
        try {
            const updateCategory = await pool.query(`select update_product_category(${id_category},'${description}')`);
            return updateCategory.rows[0].update_product_category;
        } catch (error) {
            console.error(error);
            return null;
        }
    }


}