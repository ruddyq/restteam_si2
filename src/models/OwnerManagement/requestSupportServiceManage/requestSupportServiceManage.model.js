const pool = require('../../../config/database');

module.exports = {
    /**
     * validate permit User
     * @param {integer} idUser : user ID 
     * @param {*} idPrivilege : privilege ID
     */
    async getVerifyPermitService(idUser, idPrivilege){
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get Request Support Services List 
     */
    async getRequestSupportServiceList(){
        try {
            const response = await pool.query(`select rss.request_no, rss.description, rss.cod_policy, rss.date_request as date_r, to_char(rss.date_request ::DATE,'Day dd Mon yyyy') as date_request, rss.state_request, tss.description_type, rss.code_owner ,o."name", p.description_product, p.brand, p.model, pc.description_category from request_support_service rss, type_support_service tss, "owner" o, product p, product_category pc where rss.ci_owner=o.ci and rss.cod_product=p.cod_product and rss.id_category=pc.id_category and rss.id_type=tss.id_type order by date_r desc`);
            return response.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Data of Request Support Service
     * @param {integer} requestNo Request Support Service No.
     */
    async getDataRequest(requestNo){
        try {
            const responseData = await pool.query(`select rss.request_no, rss.description, to_char(rss.date_request ::DATE,'Day dd Mon yyyy') as date_request, tss.description_type, o.ci, o."name", o.email, p.description_product, p.brand, 
            p.model, pc.description_category from request_support_service rss, type_support_service tss, "owner" o, product p, product_category pc where rss.ci_owner=o.ci and rss.cod_product=p.cod_product and rss.id_category=pc.id_category and rss.id_type=tss.id_type and rss.request_no=${requestNo}`);
            return responseData.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Accept Request Support Service
     * @param {integer} requestNo Request Support Service No.
     */
    async changeStateRequest(requestNo, state){
        try {
            const response = await pool.query(`select enable_disable_request(${requestNo}, ${state})`);
            return response.rows[0].enable_disable_request;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /*Mobile */

    /**
     * Get Data List of the 
     */
    async getTypeSupportServiceList(){
        try {
            const response = await pool.query('select description_type from type_support_service where "enable" = true ;');
            return response.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },


    async registerRequestSupport(description, codPolicy, descriptionType, codeOwner){
        try{
            var idType = await pool.query(`select id_type from type_support_service where description_type = '${descriptionType}';`);
            idType = idType.rows[0].id_type;
            var response = await pool.query(`select register_request_support(cast('${description}' as text), cast('${codPolicy}' as text), cast(${idType} as smallint), ${codeOwner});`);
            response = response.rows[0].register_request_support;
            return response;
        }catch(error) {
            console.error(error);
            return -1;
        }
    },

    async getRequestSupportServiceListId(ci){
        try{
            var response = await pool.query(`select rss.request_no , tss.description_type, p.description_product , description , rss.cod_policy , rss.date_request, rss.state_request, get_company_name(rss.cod_policy )
            from request_support_service rss, type_support_service tss, product p 
            where rss.id_type = tss.id_type  and rss.cod_product = p.cod_product   
            and rss.ci_owner = ${ci}
            order by rss.date_request desc, get_company_name, rss.cod_policy;`);
            response = response.rows;
            response.forEach(element => {
                element.date_request = element.date_request.toLocaleDateString();    
            });
            return response;
        }catch(error){
            console.error(error);
            return null;
        }
    },

    /**
     * Get Data for request without guarantee
     * @param {integer} codProductOwner : Product Owner Code
     */
    async getDataOfCodProductOwner(codProductOwner){
        try {
            const dataProductOwner = await pool.query(`select po.code, o."name", p.description_product, p.brand, p.model, pc.description_category from product_owner po, "owner" o, product p, product_category pc where o.ci=po.ci and p.cod_product=po.cod_product and p.id_category=pc.id_category and po.code=${codProductOwner}`);
            return dataProductOwner.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Register a request support service without guarantee policy
     * @param {String} description : description of problem 
     * @param {String} typeSupport : type support service
     * @param {integer} codProductOwner : code Product Owner
     */
    async registerRequestSupportWithoutPolicy(description, typeSupport, codProductOwner){
        try {
            const idTypeSupportService = await pool.query(`select id_type from type_support_service where description_type = '${typeSupport}'`);
            const requestNo = await pool.query(`select register_request_support_without_policy('${description}', cast(${idTypeSupportService.rows[0].id_type} as smallint), ${codProductOwner})`);
            return requestNo.rows[0].register_request_support_without_policy;
        } catch (error) {
            console.error(error);
            return -1;
        }
    }
}