const pool = require('../../../config/database');

module.exports = {

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getProduct(){
        try {
            const responseListProduct = await pool.query(`select p.cod_product ,p.description_product,p.product_image, p.model, p.brand, p.enable, c.description_category 
            from product p, product_category c where p.id_category=c.id_category order by cod_product;`);
            return responseListProduct.rows;
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    async getListCategory(){
        try {
            var listCategory = await pool.query(`select id_category, "description_category" from product_category;`);
            return listCategory.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async insertProduct(description_product, model, brand, id_category, product_image) {
        try {
            const registerProduct = await pool.query(`select register_product('${description_product}', '${model}', '${brand}', cast(${id_category} as smallint),'${product_image}')`);
            return registerProduct.rows[0].register_product;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    async enableDisableProduct(cod_Product) {
        try {
            const statusProduct = await pool.query(`select enable_disable_product(${cod_Product}) as "result"`);
            return statusProduct.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getProductDataUpdate(cod_product) {
        try {
            const noteData = await pool.query(`select cod_product, p.description_product, p.model, p.brand, pc.id_category, pc.description_category, p.product_image from product p, product_category pc where p.id_category=pc.id_category and cod_product=${cod_product}`);
            return noteData.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },


    async getCategoryListForProduct(cod_product) {
        try {
            const categoryList = await pool.query(`select id_category, description_category from product_category where id_category not in (select id_category from product where cod_product=${cod_product})`);
            return categoryList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async updateProduct(cod_product, description_product, model, brand, idCategory, filename){
        try {
            const updateProduct = await pool.query(`select update_product(${cod_product}, '${description_product}', '${model}', '${brand}', ${idCategory}, '${filename}')`);
            return updateProduct.rows[0].update_product;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getListProductEnable(){
        try {
            const response = await pool.query(`select p.cod_product, p.description_product, p.model, p.brand, pc.description_category, p.product_image from product p, product_category pc where p.id_category=pc.id_category and p."enable"`);
            return response.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async registerProductOwner(codProduct, ciOwner){
        try {
            const responseRegister = await pool.query(`select register_product_owner(${ciOwner}, ${codProduct})`);
            return responseRegister.rows[0].register_product_owner;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    async getProductList(ci){
        try{
            var response= await pool.query(`select po.code, p.description_product, p.model, p.brand, pc.description_category, p.product_image from product p, product_owner po, product_category pc where po.cod_product = p.cod_product and po.id_category = p.id_category and p.id_category = pc.id_category and po.ci=${ci} order by po.code asc`);
            return response.rows;
        }catch(error){
            console.error(error);
            return null;
        }
    },

    async getDataOwner(ci){
        try {
            const name = await pool.query(`select name from owner where ci=${ci}`);
            return name.rows[0].name;
        } catch (error) {
            console.log(e);
            return null;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

}