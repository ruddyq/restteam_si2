const pool = require('../../../config/database');
const externalLibrary = require('../../../config/externalLibrary');

module.exports = {

    async getListProductOwner(){
        try{
            const listProductOwner = await pool.query('select owner.ci,name,phone,email,enable, count(owner.ci)as quantity from owner,product_owner where owner.ci = product_owner.ci group by owner.ci;');
            return listProductOwner.rows;
        }catch (error){
            console.error(error);
            return null;
        }
    }, 
    async enableDisableProductOwner(ci){
        try{
            const listProductOwner = await pool.query(`select enable_disable_product_owner(${ci})`);
            return listProductOwner.rows[0].enable_disable_product_owner;
        }catch (error){
            console.error(error);
            return false;
        }
    },
    async getVerifyPermit(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * verify Login of th owner
     * @param {String} email : owner email
     * @param {*} password : password's owner (CI)
     */
    async getDataOwner(email){
        try {
            const response = await pool.query(`select ci, name from "owner" where email='${email}'`);
            return (response.rows[0]!==null)? response.rows[0] : -1;
        } catch (error) {
            console.error("Error Login Owner: ", error);
            return -1;
        }
    },

    async registerOwner(ci, issuedPlaced, name, phone, email, password){
        try {
            var response = await pool.query(`select register_owner(${ci}, '${issuedPlaced}', '${name}', ${phone}, '${email}');`);
            response = response.rows[0].register_owner;
            if(response){
                if(await externalLibrary.saveNewPasswordOwner(ci, password)){
                    console.log("owner's password saved successfully");
                    return true;
                }
                console.log("owner's password failure");
            }
            return false;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    async getOwnerNotifications(ci){
        try {
            var response = await pool.query(`select p.cod_product , p.description_product , rss.request_no, tss.service_date 
                                             from technical_support_service tss, request_support_service rss, product p
                                             where tss.request_no = rss.request_no and p.cod_product = rss.cod_product and rss.ci_owner = ${ci} and tss.state_support_service = true
                                             order by tss.service_date desc;`);
            response = response.rows;
            response.forEach(element => {
                element.service_date = element.service_date.toLocaleDateString();
            });
            return response;
        } catch (error) {
            console.error(error);
            return null;
        }
    }

}