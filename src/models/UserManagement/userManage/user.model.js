const fs = require('fs');
const bcryptjs = require('bcryptjs');
const helpers = require('../../../helpers/address');
const pool = require('../../../config/database');
const externalLibrary = require('../../../config/externalLibrary');


module.exports = {
    /**
     * register user to database and password.json and /public/uploads
     * @param {string} username
     * @param {string} password
     * @param {string} email
     * @param {integer} role
     * @param {string} user_profile
     */
    async insertUser(username, password, email, role, user_profile) {
        try {
            let passwordEncrypt = await externalLibrary.encryptPassword(password);
            let idUser = await pool.query(`select register_user($1, $2, $3, $4);`, [username, email, user_profile, role]);
            idUser = idUser.rows[0].register_user;
            if (await externalLibrary.saveNewPasswordUser(idUser, passwordEncrypt)) {
                return true;
            } else {
                console.error('Not saved new password in password.json');
                return false;
            }
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query
     */
    async getUser(query) {
        try {
            let response = await pool.query(query);
            return response;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    /**
     * update user to database, password.json and public/uploads
     * @param {integer} idUser : id user
     * @param {string} username : name of the user
     * @param {string} email : email of user
     * @param {string} profile : file name of image
     * @param {integer} idRole : id role assigned
     */
    async updateUser(idUser, username, email, profile, idRole) {
        try {
            var response = await pool.query(`select update_user(cast(${idUser} as smallint),cast('${username}' as varchar(50)),cast('${email}' as text),cast('${profile}' as text),cast(${idRole} as smallint));`);
            response = response.rows[0].update_user;
            return response;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    /**
     * verify if exists email of the user
     * @param {string} email : user email
     */
    async emailUserExists(email) {
        try {
            const countUsers = await pool.query(`SELECT COUNT(*) FROM "user" WHERE email='${email}'`);
            console.log(countUsers.rows);
            return countUsers.rows[0].count > 0 ? true : false;
        } catch (e) {
            console.log(e, 'Error in user.model.js/emailUserExists(email)');
            return false;
        }
    },

    /**
     * verify existence user in system, without his password and email
     * @param {string} email : user email
     * @param {string} password : password user
     */
    async matchPassword(email, password) {
        let idUser = await pool.query(`select id_user from "user" where email='${email}'; `);
        idUser = idUser.rows[0].id_user;
        const passwordCompare = await this.getPassword(idUser);
        return await bcryptjs.compare(password, passwordCompare);
    },

    /**
     * get user password without password.json
     * @param {integer} idUser : user id
     */
    async getPassword(idUser) {
        const json_password = await fs.readFileSync(helpers.addressThePasswordFile, 'utf-8');
        let listPassword = JSON.parse(json_password);
        var password = await listPassword.find(function (dato) {
            if (dato.id_user === idUser) {
                return true;
            } else {
                console.log("We're still looking for.");
                return false;
            }
        });
        return password.pass;
    },

    /**
     * get SQL query
     * @param {string} query : SQL query
     * @param {function callback} callback : execute ended principal function
     */
    async getUser(query, callback) {
        try {
            let response = await pool.query(query, callback);
            return response;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    /**
     * get id user without his email
     * @param {string} email
     */
    async getIdUser(email) {
        const resultIdUser = await pool.query(`select id_user from "user" where email='${email}'`);
        const idUser = resultIdUser.rows[0].id_user;
        return idUser;
    },

    /**
     * get list Roles for selection to assign user
     */
    async getListRoles() {
        try {
            const listRoles = await pool.query(`SELECT id_role,role_name FROM "role" where "enable" ='true';`);
            return listRoles.rows;
        } catch (e) {
            console.error(e, 'Does not Roles exists');
        }
    },

    /**
     * verify user status if it's enabled or disabled
     * @param {string} email : user email
     */
    async enabledUser(email) {
        try {
            const responseEnabled = await pool.query(`select r."enable" as role_enable, u."enable" as user_enable from "role" r, "user" u where r.id_role=u.id_role and u.email='${email}'`);
            return responseEnabled.rows[0].role_enable && responseEnabled.rows[0].user_enable;
        } catch (error) {
            console.error(error);
        }
    },

    /**
     * modify user status to enable or disable
     * @param {integer} idUser : id user
     */
    async enableDisableUser(idUser) {
        try {
            let b = await pool.query(`select enable_disable_user(${idUser});`);
            return b.rows[0].enable_disable_user;
        } catch (e) {
            console.log(e);
            return false;
        }
    },

    /**
     * Get Report for Technical Service for each technical
     */
    async getReportPrincipal() {
        try {
            const responseReport = await pool.query(`select "name", get_count_services_technical(id_technical) from technical`);
            return responseReport.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Request List was waiting
     */
    async getReportRequest() {
        try {
            const responseListRequest = await pool.query(`select rss.request_no, rss.cod_policy, to_char(rss.date_request::DATE,'Day dd Mon yyyy') as date_request, tss.description_type from request_support_service rss, type_support_service tss where tss.id_type=rss.id_type and state_request=2 order by date_request desc limit 5`);
            return responseListRequest.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Report Technical Service for View Principal
     */
    async getReportTechnicalService() {
        try {
            const responseSupportService = await pool.query(`select to_char(tss.service_date::DATE,'dd Mon yyyy') as service_date, count(distinct support_no) from technical_support_service tss where tss.service_date in (select service_date from technical_support_service order by service_date desc limit 5) group by tss.service_date`);
            return responseSupportService.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getListUser() {
        try {
            var listUser = await pool.query(`select id_user, username from "user";`);
            return listUser.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Verify user have privilege of a user
     * @param {int} idUser : id user
     * @param {int} idPrivilege : id privilege
     */
    async userHavePrivilege(idUser, idPrivilege) {
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    /**
     * Get Reports made by User
     */
    async getReportsInvoiceUser() {
        try {
            const reports = await pool.query(`select username,count(service_invoice.id_user)
            from service_invoice,"user"
            where service_invoice.id_user = "user".id_user
            group by username;`);
            return reports.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },
    /**
     * Get User data who made reports
     */
    async getDataReportsInvoiceUser() {
        try {
            const reportsUser = await pool.query(`select service_invoice.id_user,username,role_name,count(service_invoice.id_user)
            from service_invoice,"user",role
            where service_invoice.id_user = "user".id_user and "user".id_role=role.id_role
            group by service_invoice.id_user,username,role_name;`);
            return reportsUser.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getUserData(idUser) {
        try {
            const userData = await pool.query(`select u.id_user, u.userName, u.user_profile, u.email, u."enable", r.role_name  from "user" u, "role" r where u.id_role= r.id_role and id_user=${idUser}`);
            return userData.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getUserActivity(idUser) {
        try {
            var user = await pool.query(`select username from "user" where id_user=${idUser};`);
            return user.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getReportUser(username) {
        try {
            const reportUser = await pool.query(
                `select to_char(service_date::DATE,'dd Mon yyyy') as service_date, count(service_invoice) 
            from service_invoice, "user" 
            where "user".username='${username}' and "user".id_user =service_invoice.id_user group by service_date`);
            return reportUser.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

}
