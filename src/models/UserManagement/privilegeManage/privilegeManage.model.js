/**
 *       Import Files dependents
 */
const pool = require('../../../config/database');
//Array of the ID privileges permanent of the system 
const arrayPrivilegesConst = [3, 4, 5, 6, 9, 10, 11, 110, 13, 14, 15, 16, 18, 19, 20, 21, 23, 24, 25, 27, 28, 29, 32, 33, 34, 37, 38, 39, 40, 42, 43, 44, 46, 47, 48, 49, 52, 53, 54, 57, 58, 59, 109, 62, 63, 64, 66, 67, 68, 70, 71, 72, 74, 75, 76, 78, 81, 82, 83, 84, 87, 88, 89, 92, 93, 94, 101, 96, 98, 102, 100, 103, 105, 106, 107, 108, 113, 114, 115, 118, 119, 120];

/**
 * Get tree json of the privileges
 * @param {Array} privilegesFathersList : Array of the privileges fathers
 */
async function loadPrivilegesSorted(privilegesFathersList) {
    try {
        let listJson = new Array();
        for (let index = 0; index < privilegesFathersList.length; index++) {
            const subPrivilegeList = await pool.query(`select * from privilege p where id_super_privilege=${privilegesFathersList[index].id_privilege}`);
            if (await subPrivilegeList.rowCount > 0) {
                list_sub_pr = await loadPrivilegesSorted(subPrivilegeList.rows);
                dataJson = await {
                    id_privilege: privilegesFathersList[index].id_privilege,
                    description_privilege: privilegesFathersList[index].description_privilege,
                    list_sub_pr
                };
                listJson.push(dataJson);
            } else {
                dataJson = await {
                    id_privilege: privilegesFathersList[index].id_privilege,
                    description_privilege: privilegesFathersList[index].description_privilege,
                };
                listJson.push(dataJson);
            }
        }
        return await listJson;
    } catch (error) {
        console.error(error, " Error in load Privileges, read view register role manage");
        return null;
    }
}

module.exports = {
    /**
     * get List Privileges with its hierarchy
     */
    async getPrivileges() {
        try {
            const privilegesFathers = await pool.query('select id_privilege, description_privilege from privilege where id_super_privilege is null');
            if (await privilegesFathers.rowCount > 0) {
                const listSorted = await loadPrivilegesSorted(privilegesFathers.rows);
                return listSorted;
            } else {
                return privilegesFathers.rows;
            }
        } catch (error) {
            console.log("Error in get privileges", error);
            return null;
        }
    },

    /**
     * Get List of options to assign privileges
     */
    async getOptionsPrivileges() {
        try {
            const responsePrivilege = await pool.query(`select id_privilege, description_privilege from privilege where id_privilege not in (${arrayPrivilegesConst})`);
            return responsePrivilege.rows;
        } catch (error) {
            console.error("Error en view register privileges", error);
            return null;
        }
    },

    /**
     * Get 
     * @param {string} description : description of the privilege
     * @param {integer} idSuperPrivilege : ID super privilege
     */
    async insertPrivilege(description, idSuperPrivilege) {
        try {
            const response = await pool.query(`select register_privilege('${description}', ${idSuperPrivilege})`);
            return response.rows[0].register_privilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}