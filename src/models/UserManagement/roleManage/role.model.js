/**
 *       Import Files dependents
 */
const pool = require('../../../config/database');
const externalLibrary = require('../../../config/externalLibrary');

/**
 * Get count privileges contains onto to the privilege
 * @param {int} idPrivilege : id Privilege selected
 */
async function getCountSubPrivileges(idPrivilege) {
    try {
        let countPrivileges = 0;
        let privilegesList = await pool.query(`select id_privilege from privilege where id_super_privilege=${idPrivilege}`);
        for (let index = 0; index < privilegesList.rowCount; index++) {
            countPrivileges = countPrivileges + await getCountSubPrivileges(privilegesList.rows[index].id_privilege) + 1;
        }
        return countPrivileges;
    } catch (error) {
        console.error(error, "Error in count sub privileges for read privileges");
        return null;
    }
}

/**
 * Get Tree of the Privileges in a json
 * @param {Array} privilegesFathersList : Array privileges fathers
 */
async function loadPrivilegesSorted(privilegesFathersList) {
    try {
        let listJson = new Array();
        for (let index = 0; index < privilegesFathersList.length; index++) {
            const subPrivilegeList = await pool.query(`select * from privilege p where id_super_privilege=${privilegesFathersList[index].id_privilege}`);
            if (await subPrivilegeList.rowCount > 0) {
                list_sub_pr = await loadPrivilegesSorted(subPrivilegeList.rows);
                dataJson = await {
                    id_privilege: privilegesFathersList[index].id_privilege,
                    description_privilege: privilegesFathersList[index].description_privilege,
                    list_sub_pr
                };
                listJson.push(dataJson);
            } else {
                dataJson = await {
                    id_privilege: privilegesFathersList[index].id_privilege,
                    description_privilege: privilegesFathersList[index].description_privilege,
                };
                listJson.push(dataJson);
            }
        }
        return await listJson;
    } catch (error) {
        console.error(error, " Error in load Privileges, read view register role manage");
        return null;
    }
}

/**
 * 
 * @param {int} idPrivilege 
 * @param {Array} listPrivileges : 
 */
async function loadPrivileges(idPrivilege, listPrivileges) {
    listPrivileges.push(idPrivilege);
    let listSubPrivileges = await pool.query(`select id_privilege from privilege where id_super_privilege=${idPrivilege}`);
    listSubPrivileges = listSubPrivileges.rows;
    for (let index = 0; index < listSubPrivileges.length; index++) {
        await loadPrivileges(listSubPrivileges[index].id_privilege, listPrivileges);
    }
}

/**
 * Get List Role 
 * @param {*} idRole : role ID
 */
async function getListRoleHasPrivileges(idRole) {
    let arrayListContainer = Array();
    let listPrivileges = await pool.query(`select id_privilege from role_privilege where id_role=${idRole}`);
    listPrivileges = await listPrivileges.rows;
    for (let index = 0; index < listPrivileges.length; index++) {
        await loadPrivileges(listPrivileges[index].id_privilege, arrayListContainer);
    }
    return arrayListContainer;
}

function searchPrivilege(idSuperPrivilege, permissionList) {
    for (let index = 0; index < permissionList.length; index++) {
        if (permissionList[index] == idSuperPrivilege) {
            return true;
        }
    }
    return false;
}

async function resortPrivilege(listPrivilege, listSuperPrivileges) {
    if (listPrivilege.length === listSuperPrivileges.length) {
        for (let index = listPrivilege.length - 1; index >= 0; index--) {
            let idSuperPrivilege = listSuperPrivileges[index].id_super_privilege;
            if (await searchPrivilege(idSuperPrivilege, listPrivilege)) {
                await listPrivilege.splice(index, 1);
                await listSuperPrivileges.splice(index, 1);
            }
        }
        return listPrivilege;
    } else {
        console.error("error in resort permissions");
        return null;
    }
}

module.exports = {
    /**
     * get role of the system web for a little report in role manage
     */
    async getListRoles() {
        try {
            let roleList = await pool.query('select id_role, role_name, description, "enable", get_count_user_in_role(id_role) from role ORDER BY id_role');
            let countPrivileges = await pool.query('SELECT count(*) FROM privilege');
            countPrivileges = countPrivileges.rows[0].count;
            let listRole = roleList.rows;
            for (let roleIndex = 0; roleIndex < roleList.rowCount; roleIndex++) {
                const privileges = await pool.query(`select id_privilege from role_privilege where id_role=${roleList.rows[roleIndex].id_role}`);
                let countPrivilegesTotal = 0;
                for (let indexPrivilege = 0; indexPrivilege < privileges.rowCount; indexPrivilege++) {
                    countPrivilegesTotal = countPrivilegesTotal + await getCountSubPrivileges(privileges.rows[indexPrivilege].id_privilege);
                }
                porcentualRole = Math.round(((countPrivilegesTotal + privileges.rowCount) / countPrivileges * 100) * 100) / 100;
                let jsonRole = {
                    id_role: listRole[roleIndex].id_role,
                    role_name: listRole[roleIndex].role_name,
                    description: listRole[roleIndex].description,
                    enable: listRole[roleIndex].enable,
                    get_count_user_in_role: listRole[roleIndex].get_count_user_in_role,
                    porcentualPermit: porcentualRole
                };
                listRole[roleIndex] = await jsonRole;
            }
            return listRole;
        } catch (e) {
            console.error("Error in get list role in privilege Model", e);
            return null;
        }
    },

    /**
     *  get query to table privileges in expresssi2 for assign to a new role
     */
    async getPrivileges() {
        try {
            const privilegesFathers = await pool.query('select id_privilege, description_privilege from privilege where id_super_privilege is null');
            if (await privilegesFathers.rowCount > 0) {
                const listSorted = await loadPrivilegesSorted(privilegesFathers.rows);
                return listSorted;
            } else {
                return privilegesFathers.rows;
            }
        } catch (e) {
            console.error("Get list privilege privilege model", e);
            return null;
        }
    },

    /**
     * Function to verify if exists role through its role_name or description
     * RETURNS boolean
     * @param {string} roleName  
     * @param {string} descriptionRole 
     */
    async existsRole(roleName, descriptionRole) {
        try {
            const response = await pool.query(`select count(*) as exist from role where role_name='${roleName}'`);
            if (response.rows[0].exist > 0) {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            console.error(e);
            return false;
        }
    },

    /**
     * verify role copy duplicate
     * @param {String} roleName : role name
     * @param {String} description : description role 
     */
    async verifyRoleUpdate(roleName, idRole) {
        try {
            const response = await pool.query(`select count(*) as exist from role where role_name='${roleName}' and id_role<>${idRole}`);
            if (response.rows[0].exist > 0) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Register role with us role_name description and its permission assigned
     * and register activity in the Bitacora
     * RETURNS boolean
     * @param {string} roleName 
     * @param {string} roleDescription 
     * @param {array[integer]} permissions 
     * @param {string} user 
     */
    async insertRoleWithItsPrivileges(roleName, roleDescription, permissions, user, device) {
        try {
            const idRole = await pool.query(`select register_role('${roleName}', '${roleDescription}');`);
            if (! await externalLibrary.writeBackLog(`${user}`, `Create new Role: ${roleName}`, device)) {
                console.error('could not register activity in externalLibrary');
            }
            const id_role = idRole.rows[0].register_role;
            const listSuperPrivileges = await pool.query(`select id_super_privilege from privilege where id_privilege in (${permissions})`);
            permissions = await resortPrivilege(permissions, listSuperPrivileges.rows);
            await permissions.forEach(async permission => {
                const responsePrivilege = await pool.query(`select register_rol_privilege(${id_role},${permission})`);
                if (responsePrivilege.rows[0].register_rol_privilege === -1) {
                    return false;
                }
                if (!await externalLibrary.writeBackLog(`${user}`, `Assign new privilege id:${permission} for the role: ${roleName} (cod: ${permission})`, device)) {
                    console.error('could not register activity in externalLibrary at permission');
                }
            });
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    },

    /**
     * Update status Role: Enable to Disable and disable to enable
     * @param {integer} idRole : role ID 
     */
    async enableDisableRole(idRole) {
        try {
            const resultStatusRole = await pool.query(`select enable_disable_role(${idRole}) as "result"`);
            return resultStatusRole.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get view of the Role Details
     * @param {integer} idRole : id role 
     */
    async getPrivilegesTheRole(idRole) {
        const responseListPrivileges = await pool.query(`select p.id_privilege, p.description_privilege from privilege p, role_privilege rp where p.id_privilege=rp.id_privilege and rp.id_role=${idRole}`);
        return responseListPrivileges.rows;
    },

    /**
     * Get role name and description also the id role
     * @param {integer} idRole : id role
     */
    async getRoleDetails(idRole) {
        const responseRole = await pool.query(`select id_role, role_name, description from "role" where id_role=${idRole}`);
        return responseRole.rows;
    },

    /**
     * Get Privileges that doesn't have the role
     * @param {integer} idRole : id role
     */
    async getPrivilegesDoesNotHaveRole(idRole) {
        const arrayIdPrivileges = await getListRoleHasPrivileges(idRole);
        if (arrayIdPrivileges.length > 0) {
            const responseListPrivileges = await pool.query(`select p.id_privilege, p.description_privilege, get_description_super_privilege(p.id_super_privilege) from privilege p where id_privilege not in (${arrayIdPrivileges})`);
            return responseListPrivileges.rows;
        } else {
            return new Array();
        }
    },

    /**
     * Update role data
     * @param {integer} idRole 
     * @param {string} roleName 
     * @param {string} description 
     * @param {string} user 
     */
    async updateRole(idRole, roleName, description, user, device) {
        try {
            await pool.query(`update "role" set role_name='${roleName}', description='${description}' where id_role=${idRole}`);
            await externalLibrary.writeBackLog(user, `updated role with ID: ${idRole} as name: ${roleName} and description: ${description}`, device);
            return true;
        } catch (error) {
            console.error(error);
            await externalLibrary.writeBackLog(user, `updated failed role with ID: ${idRole}`);
            return false;
        }

    },

    /**
     * Assign new list privilege in role
     * @param {array[integer]} permission 
     * @param {integer} idRole 
     * @param {string} user 
     */
    async assignPrivilegesToRole(permission, idRole, user, device) {
        await permission.forEach(async idPrivilege => {
            if (!await pool.query(`select register_rol_privilege(${idRole},${idPrivilege})`)) {
                await externalLibrary.writeBackLog(user, `Assigned failed privilege (ID: ${idPrivilege}) to role with ID: ${idRole}`, device);
                return false;
            }
            await externalLibrary.writeBackLog(user, `Assigned privilege (ID: ${idPrivilege}) to role with ID: ${idRole}`, device);
        });
        return true;
    },

    /**
     * Remove a list of the id privileges in a role
     * Exception (false because this role has 1 privileges only and can't remove) 
     * @param {array[integer]} listIdPrivilege 
     * @param {integer} idRole 
     */
    async removePrivilegeTheRole(listIdPrivilege, idRole, user, device) {
        await listIdPrivilege.forEach(async idPrivilege => {
            const responseQuery = await pool.query(`select delete_role_privilege(${idPrivilege}, ${idRole}) as response`);
            if (!responseQuery.rows[0].response) {
                await externalLibrary.writeBackLog(user, `failed remove assignation privilege (ID: ${idPrivilege}) to role with ID: ${idRole}`, device);
                return false;
            }
            await externalLibrary.writeBackLog(user, `removed assignation privilege (ID: ${idPrivilege}) to role with ID: ${idRole}`, device);
        });
        return true;
    },

    /**
     * verify permissions of the privileges
     * @param {integer} idUser : id user 
     * @param {integer} idPrivilege : id privilege
     */
    async verifyPermitUser(idUser, idPrivilege) {
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}