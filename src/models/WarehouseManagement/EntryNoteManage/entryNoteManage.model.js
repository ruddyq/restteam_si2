const pool = require('../../../config/database');

module.exports = {
    /**
     * get query to table entry_note in expresssi2
     */
    async getListEntryNotes() {
        try {
            var listEntryNotes = await pool.query(`select en.income_no , en.entry_date , en.description , p.provider_name , w."name" 
                                                     from entry_note en , provider p , workshop w 
                                                     where en.nit_ci_provider = p.nit_ci and en.cod_workshop = w.cod_workshop ;`);
            listEntryNotes = listEntryNotes.rows;
            listEntryNotes.forEach(element => {
                element.entry_date = element.entry_date.toLocaleDateString();
            });
            return listEntryNotes;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    /**
     * get query to table company in expresssi2
     */
    async enableDisableCompany(nit) {
        try {
            const enableDisableCompany = await pool.query(`select enable_disable_client_company(${nit});`);
            return enableDisableCompany.rows[0].enable_disable_client_company;
        } catch (e) {
            console.error(e);
            return false;
        }
    },

    async insertEntryNote(entryDate,description,codWorkshop, nitCi){
        try{
            var insertEntryNote = await pool.query(`select register_entry_note(cast('${entryDate}' as date),cast('${description}' as text) ,cast(${codWorkshop} as smallint), ${nitCi});`);
            insertEntryNote = insertEntryNote.rows[0].register_entry_note; 
            return insertEntryNote;
        } catch (e) {
            console.error(e);
            return -1;
        }
    },

    /**
     * Get permit update if the user has enable or role too
     * @param {integer} idUser 
     */
    async getPermitUpdateCompany(idUser){
        try {
            console.log('iniciando', idUser);
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser},6) as verify`);
            console.log(responsePermit);
            return responsePermit.rows[0].verify;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get entry note data for update
     * @param {bigint} nit company nit 
     */
    async getEntryNoteUpdateData(incomeNo){
        try {
            var response = await pool.query(`select en.income_no,entry_date, description, w.cod_workshop ,w."name" 
                                               from workshop w ,entry_note en
                                               where w.cod_workshop = en.cod_workshop and en.income_no = ${incomeNo};`);
            console.log(response.rows[0]);
            response = response.rows[0];
            response.entry_date= response.entry_date.toLocaleDateString();
            return response;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get true if a entry note was update
     * @param {integer} incomeNo 
     * @param {string} description
     * @param {integer} codWorkshop
     */

    async updateEntryNote(incomeNo,description){
        try {
            const response = await pool.query(`update entry_note set description='${description}' 
                                               where income_no=${incomeNo};`);
            return response;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    
    /**
     * Get quantity of days elapsed since the register of entry note
     * @param {integer} incomeNo 
     */
    async timeElapsed(incomeNo){
        try{ 
            var timeNote = await pool.query(`select entry_date from entry_note en where income_no =${incomeNo}; `);
            timeNote = timeNote.rows[0].entry_date;
            var timeNow = await pool.query('select now();');
            timeNow = timeNow.rows[0].now;
            var cantDays = (timeNow-timeNote)/86400000;
            console.log(cantDays);
            return cantDays;
        }catch (e){
            console.log(e);
            return -1;
        }

    },
/* -------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------Methods For Income Detail---------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------*/ 

    async getListIncomeDetail(incomeNo){
        try {
            const listEntryNotes = await pool.query(`select * from income_detail where income_no=${incomeNo};`);
            return listEntryNotes.rows;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async getListReplacement(incomeNo){
        try {
            var codWorkshop = await pool.query(`select cod_workshop from entry_note where income_no = ${incomeNo};`);
            codWorkshop = codWorkshop.rows[0].cod_workshop;
            var listReplacement = await pool.query(`select cod_replacement, description_replacement 
                                                    from replacement r 
                                                    where "enable" = true and description_replacement 
                                                    not in (select id.replacement 
                                                            from income_detail id, entry_note en ,workshop w 
                                                            where id.income_no= en.income_no  and en.cod_workshop = w.cod_workshop 
                                                            and w.cod_workshop = ${codWorkshop} and en.income_no = ${incomeNo}
                                                           );`);
            return listReplacement.rows;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async insertIncomeDetail(incomeNo, replacement, quantity){
        try{
            var incomeDetail = await pool.query(`select register_income_detail(${incomeNo}, cast('${replacement}'as text), cast(${quantity} as smallint));`);
            console.log(incomeDetail.rows[0]);
            incomeDetail = incomeDetail.rows[0].register_income_detail; 
            return incomeDetail;
        } catch (e) {
            console.error(e);
            return -1;
        }

    },

    async deleteIncomeDetail(incomeNo, idIncomeDetail){
        try{
            var deletedDetail = await pool.query(`delete from income_detail where income_no = ${incomeNo} and id_income_detail = ${idIncomeDetail};`);
            return deletedDetail;
        } catch (e) {
            console.error(e);
            return false;
        }

    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

}