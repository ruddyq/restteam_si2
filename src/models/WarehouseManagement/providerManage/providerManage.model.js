const pool = require('../../../config/database');

module.exports = {
    
    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getListProvider(){
        try{
            const listProvider = await pool.query(`select *from provider ORDER BY provider_name;`);
            return listProvider.rows;
        }catch(error){
            console.error(error);
            return null;
        }
    },

    async insertProvider(nit_ci, provider_name, address, email, phone){
        try {
            const response = await pool.query(`select register_provider(${nit_ci}, '${provider_name}', '${address}', '${email}', ${phone});`);
            return response.rows[0].register_provider;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async enableDisableProvider(nit_ci) {
        try {
            const statusProvider = await pool.query(`select enable_disable_provider(${nit_ci}) as "result"`);
            return statusProvider.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    
    async getProviderUpdateData(nit_ci){
        try {
            const response = await pool.query(`select provider_name, address, email, phone from provider where nit_ci=${nit_ci}`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async updateProvider(nit_ci, provider_name, address, email, phone){
        try {
            const updateProvider = await pool.query(`select update_provider(${nit_ci}, '${provider_name}', '${address}', '${email}', ${phone})`);
            return updateProvider.rows[0].update_provider;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}
