const pool = require('../../../config/database');

module.exports = {

    async getReplacementCategory(){
        try {
            const replCategList = await pool.query(`select *from replacement_category ORDER BY id_category`);
            return replCategList.rows;
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async insertReplacementCategory(description_category){
        try {
            const response = await pool.query(`select register_replacement_category(cast('${description_category}'as text))`);
            return response.rows[0].register_replacement_category;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getReplacementCategoryUpdateData(id_category){
        try {
            const response = await pool.query(`select description_category from replacement_category where id_category=${id_category}`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async updateReplacementCategory(id_category, description_category){
        try {
            const updateCategory = await pool.query(`select update_replacement_category(${id_category},'${description_category}' );`);
            return updateCategory.rows[0].update_replacement_category;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
    
}