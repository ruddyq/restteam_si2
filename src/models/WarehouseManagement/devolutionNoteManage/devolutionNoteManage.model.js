const pool = require('../../../config/database');

module.exports = {
    /**
     * get query to table devolution_note in expresssi2
     */

    async getTechnicalList() {
        try {
            const responseTechnicalList = await pool.query(`select id_technical, "name" from technical where "enable"=true`);
            return responseTechnicalList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getWorkshopList() {
        try {
            const responseWorkshopList = await pool.query(`select cod_workshop,"name" from workshop`);
            return responseWorkshopList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getListNotesReport() {
        try {
            const responseListDevolutionNotes = await pool.query(`select n.note_no,to_char(n.note_date::DATE,'dd Mon yyyy') as note_date, n.description, t."name" as techname, w."name", getAvailableModifyNote(n.note_no,2) as availableUpdate, getAvailableModifyNote(n.note_no,2) as availableRegisterDetail from note n, workshop w, technical t where n.cod_workshop=w.cod_workshop and 
            n.id_technical=t.id_technical and n.note_type=false group by n.note_no, t."name", w."name" order by n.note_no`);
            return responseListDevolutionNotes.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async registerDevolutionNote(note_date, description, id_technical, cod_workshop) {
        try {
            const registerNote = await pool.query(`select register_devolution_note(cast('${note_date}' as date), '${description}', ${id_technical}, cast(${cod_workshop} as smallint))`);
            return registerNote.rows[0].register_devolution_note;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getDevolutionNoteData(note_no) {
        try {
            const noteData = await pool.query(`select note_no, description, t.id_technical, t."name" from note n,technical t where n.id_technical=t.id_technical and note_no=${note_no}`);
            return noteData.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getTechnicalListforDevolutionNote(note_no) {
        try {
            const technicalList = await pool.query(`select id_technical, "name" from technical where id_technical not in (select id_technical from note where note_no=${note_no}) and "enable"=true`);
            return technicalList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async updateDataDevolutionNote(note_no, description, id_technical) {
        try {
            const updateDevolution = await pool.query(`select n.id_technical, r.cod_replacement, r.id_category, sum(quantity) as stock 
			                                                from note n, note_detail nd, replacement r
                                                            where n.note_no=nd.note_no and nd.replacement=r.description_replacement and n.note_no=${note_no}
                                                            group by r.cod_replacement, r.id_category, n.id_technical`);
            for (let indexDetailList = 0; indexDetailList < updateDevolution.rowCount; indexDetailList++) {
                await pool.query(`select update_stock_decrement_Dev(${updateDevolution.rows[indexDetailList].id_technical}, ${updateDevolution.rows[indexDetailList].stock}, ${updateDevolution.rows[indexDetailList].cod_replacement}, ${note_no})`);
                await pool.query(`select update_stock_increment_Dev(${id_technical}, ${updateDevolution.rows[indexDetailList].stock}, ${updateDevolution.rows[indexDetailList].cod_replacement}, ${note_no})`);
            }
            await pool.query(`update note set description='${description}', id_technical=${id_technical} where note_no=${note_no}`);
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getListReplacementOfTheTechnical(note_no) {
        try {
            const technicalReplacement = await pool.query(`select sb.cod_replacement, sb.quantity, sb.id_technical, r.cod_replacement, r.description_replacement, r."enable"=true
                                                            from spare_box sb, replacement r, note n  where sb.id_technical=n.id_technical and r.cod_replacement=sb.cod_replacement and n.note_no=${note_no}`);                      
            return technicalReplacement.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getListDetailNoteDetail(note_no) {
        try {
            const responseNoteDetails = await pool.query(`select note_no, id_note_detail, replacement, quantity from note_detail where note_no=${note_no}`);
            return responseNoteDetails.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async registerDevolutionNoteDetail(note_no, cod_replacement, quantity) {
        try {
            const responseIdDetail = await pool.query(`select register_devolution_note_detail(${note_no}, ${cod_replacement}, cast(${quantity}as smallint));`);
            console.log(responseIdDetail.rows[0].register_devolution_note_detail);
            return responseIdDetail.rows[0].register_devolution_note_detail;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    async deleteDetailDevolution(note_no, id_detail) {
        try {
            const deleteNote = await pool.query(`select delete_detail_of_note_devolution(cast(${note_no} as integer), cast(${id_detail}as integer));`);
            return deleteNote.rows[0].delete_detail_of_note_devolution;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    async getNoteData(note_no){
        try {
            const dataNote = await pool.query(`select n.note_no, to_char(n.note_date::DATE,'dd Mon yyyy') as note_date, n.description, t."name" as technical_name, w."name" from note n, workshop w, technical t where n.cod_workshop=w.cod_workshop and n.id_technical=t.id_technical and n.note_type=false and n.note_no=${note_no} group by n.note_no, t."name", w."name" order by n.note_no`);
            return dataNote.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getDetailData(note_no){
        try {
            const dataDetailNote = await pool.query(`select id_note_detail, replacement, quantity from note_detail where note_no=${note_no}`);
            return dataDetailNote.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    }
}
