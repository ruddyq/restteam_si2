const pool = require('../../../config/database');

module.exports = {

    /**
     * verify permissions of the privileges
     * @param {integer} idUser : id user 
     * @param {integer} idPrivilege : id privilege
     */
    async verifyPermitUser(idUser, idPrivilege) {
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get technical list available
     */
    async getTechnicalAvailable() {
        try {
            const responseTechnicalList = await pool.query(`select id_technical, "name" from technical where "enable"=true`);
            return responseTechnicalList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get list workshop
     */
    async getWorkshopList() {
        try {
            const responseWorkshopList = await pool.query(`select cod_workshop,"name" from workshop`);
            return responseWorkshopList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get exit note list report
     */
    async getListNotesReport() {
        try {
            const responseListExitNotes = await pool.query(`select n.note_no, to_char(n.note_date::DATE,'dd Mon yyyy') as note_date, n.description, t."name" as techname, getCountReplacementsInNote(n.note_no) as count_replacement, w."name", getAvailableModifyNote(n.note_no,2) as availableUpdate, getAvailableModifyNote(n.note_no,2) as availableRegisterDetail from note n, workshop w, technical t where n.cod_workshop=w.cod_workshop and 
            n.id_technical=t.id_technical and n.note_type=true group by n.note_no, t."name", w."name" order by n.note_no`);
            return responseListExitNotes.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * register exit note and save to database
     * @param {date} dateNote : date of exit note
     * @param {string} description : description of exit note
     * @param {integer} idTechnical : id technical
     * @param {integer} idWorkshop : id workshop
     */
    async exitNoteRegister(dateNote, description, idTechnical, idWorkshop) {
        try {
            const responseRegisterNote = await pool.query(`select register_exit_note('${dateNote}', '${description}', ${idTechnical}, cast(${idWorkshop} as smallint))`);
            return responseRegisterNote.rows[0].register_exit_note;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    /**
     * get detail exit note list 
     * @param {integer} noteNo : exit note no 
     */
    async getListExitNoteDetail(noteNo) {
        try {
            const responseNoteDetails = await pool.query(`select note_no, id_note_detail, replacement, quantity from note_detail where note_no=${noteNo}`);
            return responseNoteDetails.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get replacement available in workshop available
     * @param {integer} noteNo : exit note no. 
     */
    async getListReplacementAvailableInWorkshop(noteNo) {
        try {
            const responseReplacement = await pool.query(`select r.cod_replacement, r.description_replacement from replacement r, workshop w, warehouse_inventory wi, note n where n.cod_workshop=w.cod_workshop and r.cod_replacement=wi.cod_replacement and wi.cod_workshop=w.cod_workshop and r."enable"=true and n.note_no=${noteNo}`);
            return responseReplacement.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * register detail exit note to exit note list
     * @param {integer} noteNo : note no. 
     * @param {integer} codReplacement : code replacement
     * @param {*} quantity : integer
     */
    async registerExitNoteDetail(noteNo, codReplacement, quantity) {
        try {
            const responseIdDetail = await pool.query(`select register_exit_note_detail(${noteNo}, ${codReplacement}, cast(${quantity} as smallint))`);
            return responseIdDetail.rows[0].register_exit_note_detail;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    /**
     * delete detail exit note
     * @param {integer} noteNo : note no. 
     * @param {integer} idDetail : id detail of exit note
     */
    async deleteDetailOfTheExitNote(noteNo, idDetail) {
        try {
            const response = await pool.query(`select delete_detail_of_note(${noteNo}, ${idDetail})`);
            return response.rows[0].delete_detail_of_note;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },

    /**
     * get exit note data to update
     * @param {integer} noteNo : exit note No. 
     */
    async getExitNoteData(noteNo) {
        try {
            const responseExitNoteData = await pool.query(`select note_no, description, t.id_technical, t."name" from note n,technical t where n.id_technical=t.id_technical and note_no=${noteNo}`);
            return responseExitNoteData.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get Technical list to as
     * @param {integer} noteNo : note No. 
     */
    async getTechnicalListToAssignExitNote(noteNo) {
        try {
            const responseTechnicalList = await pool.query(`select id_technical, "name" from technical where id_technical not in (select id_technical from note where note_no=${noteNo}) and "enable"=true order by "name"`);
            return responseTechnicalList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * update exit note data and updating stock in spare box and warehouse inventory tables
     * @param {integer} exitNoteNo : exit note No.
     * @param {string} description : description of exit note
     * @param {integer} idTechnical : id technical
     */
    async updateDataExitNote(exitNoteNo, description, idTechnical) {
        try {
            const responseDetailList = await pool.query(`select n.id_technical, r.cod_replacement, sum(quantity) as stock from note n, note_detail nd, replacement r
            where n.note_no=nd.note_no and nd.replacement=r.description_replacement and n.note_no=${exitNoteNo} group by r.cod_replacement, n.id_technical`);
            for (let indexDetailList = 0; indexDetailList < responseDetailList.rowCount; indexDetailList++) {
                await pool.query(`select update_stock_in_spare_box_decrement(${responseDetailList.rows[indexDetailList].id_technical}, ${responseDetailList.rows[indexDetailList].stock}, ${responseDetailList.rows[indexDetailList].cod_replacement}, ${exitNoteNo})`);
                await pool.query(`select update_stock_in_spare_box_increment(${idTechnical}, ${responseDetailList.rows[indexDetailList].stock}, ${responseDetailList.rows[indexDetailList].cod_replacement}, ${exitNoteNo})`);
            }
            await pool.query(`update note set description='${description}', id_technical=${idTechnical} where note_no=${exitNoteNo}`);
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get Data of the Exit Note
     * @param {integer} noteNo : note No.
     */
    async getExitNotePrincipalData(noteNo){
        try {
            const responseDataExitNote = await pool.query(`select n.note_no, to_char(n.note_date::DATE,'dd Mon yyyy') as note_date, n.description, t."name" as technical_name, getCountReplacementsInNote(n.note_no) as count_replacement, w."name" from note n, workshop w, technical t where n.cod_workshop=w.cod_workshop and n.id_technical=t.id_technical and n.note_type=true and n.note_no=${noteNo} group by n.note_no, t."name", w."name" order by n.note_no`);
            return responseDataExitNote.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Detail of the Exit Note Data
     * @param {integer} noteNo note No.
     */
    async getExitNoteDetailData(noteNo){
        try {
            const responseDetailExitNote = await pool.query(`select id_note_detail, replacement, quantity from note_detail where note_no=${noteNo}`);
            return responseDetailExitNote.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    }
}