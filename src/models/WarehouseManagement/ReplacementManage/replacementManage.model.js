const pool = require('../../../config/database');

module.exports = {
    /**
     * get list Replacement with stock for each workshop assigned
     * and its data replacement
     */
    async getListReplacement(){
        const countReplacement = await pool.query(`select r.cod_replacement, r.description_replacement, r."enable", rc2.description_category from replacement r, replacement_category rc2 where r.id_category=rc2.id_category order by cod_replacement`);
        let listReplacement = Array();
        for (let index = 1; index <= countReplacement.rowCount; index++) {
            const codSpare = countReplacement.rows[index-1].cod_replacement;
            const response = await pool.query(`select w."name" , wi.quantity from workshop w,warehouse_inventory wi where w.cod_workshop=wi.cod_workshop and wi.cod_replacement=${codSpare}`);
            let indexWarehouseInventory = 0;
            let containerWarehouseInventory = Array();
            while (indexWarehouseInventory<response.rowCount) {
                containerWarehouseInventory.push({
                    name : response.rows[indexWarehouseInventory].name,
                    quantity : response.rows[indexWarehouseInventory].quantity
                });
                indexWarehouseInventory++;
            }
            let spare = {
                cod_replacement : countReplacement.rows[index-1].cod_replacement,
                description : countReplacement.rows[index-1].description_replacement,
                enable : countReplacement.rows[index-1].enable,
                category : countReplacement.rows[index-1].description_category,
                stock : containerWarehouseInventory
            }
            await listReplacement.push(spare);
        }
        return listReplacement;
    },

    /**
     * change status of the replacement to enable or disable
     * @param {integer} codReplacement : code replacement
     */
    async enableDisableReplacement(codReplacement){
        try {
            const responseResultStatus = await pool.query(`select enable_disable_replacement(${codReplacement})`);
            return responseResultStatus.rows[0].enable_disable_replacement;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * verify privileges and permissions of the user with id privilege
     * @param {integer} idUser : id User
     * @param {*} idPrivilege  : id Privilege
     */
    async verifyPermit(idUser, idPrivilege){
        try {
            console.log(idUser, idPrivilege);
            const responsePermit = await pool.query(`select verifypermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async registerReplacement(description_i, id_category){
        try{
            const insertReplacement = await pool.query(`select register_replacement('${description_i}', ${id_category})`);
            return insertReplacement.rows[0].register_replacement;
        }catch(error){
            console.error(error);
            return false;
        }
    },

    async update_replacement(cod_replacement,description_replacement, description_category){
        try{
            const updateReplacement = await pool.query(`select update_replacement(${cod_replacement},'${description_replacement}','${description_category}')`);
            console.log(updateReplacement.rows[0].update_replacement);
            return updateReplacement.rows[0].update_replacement;
        }catch(error){
            console.error(error);
            return false;
        }
    },
    async getDataReplacement(cod_replacement_i){
        try{
            const dataReplacement = await pool.query(`select r.cod_replacement, r.description_replacement, rc.id_category ,rc.description_category 
                                                        from replacement r,replacement_category rc 
                                                        where r.id_category= rc.id_category and r.cod_replacement=${cod_replacement_i};`);
            return dataReplacement.rows[0];
        }catch(error){
            console.error(error);
            return null;
        }
    },

    async getCategoryReplacement(){
        try{
            const categoryReplacement = await pool.query(`select * from replacement_category ;`);
            return categoryReplacement.rows;
        }catch(error){
            console.error(error);
            return null;
        }
    },

    async getReportReplacementTendency(){
        try {
            const response = await pool.query(`select r.cod_replacement, r.description_replacement, to_char(tss.service_date::DATE,'Month'), count(distinct r.description_replacement), date_part('month', tss.service_date) from list_spare_parts lsp, replacement r, technical_support_service tss where lsp.cod_replacement=r.cod_replacement and tss.support_no=lsp.support_no group by r.cod_replacement, r.description_replacement, to_char(tss.service_date::DATE,'Month'), date_part('month', tss.service_date) order by date_part('month', tss.service_date)`);
            return response.rows;
        } catch (error) {
            console.log("Error in replacement Tendency", error);
            return null;
        }
    },

    async getReportReplacement(codReplacement){
        try {
            const response = await pool.query(`select r.cod_replacement, r.description_replacement, to_char(tss.service_date::DATE,'Month'), count(distinct r.description_replacement), date_part('month', tss.service_date) from list_spare_parts lsp, replacement r, technical_support_service tss where lsp.cod_replacement=r.cod_replacement and tss.support_no=lsp.support_no and r.cod_replacement=${codReplacement} group by r.cod_replacement, r.description_replacement, to_char(tss.service_date::DATE,'Month'), date_part('month', tss.service_date) order by date_part('month', tss.service_date)`);
            return response.rows;
        } catch (error) {
            console.log("Error in get Report Replacement Specific", error);
            return null;
        }
    }
}
