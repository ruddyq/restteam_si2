const pool = require('../../../config/database');

module.exports = {
    
    async getListWorkshop(){
        try{
            const listWorkshop = await pool.query(`select * from workshop order by cod_workshop`);
            return listWorkshop.rows;
        }catch(e){
            console.log(e);
            return false;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async insertWorkshop(name, address, latitude, longitude){
        try {
            const insertWorkshop = await pool.query(`select register_workshop('${name}','${address}', '${latitude}', '${longitude}');`);
            return insertWorkshop.rows[0].register_workshop;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getListReplacement(codWorkshop){
        try {
            var listReplacement = await pool.query(`select r.cod_replacement, r.description_replacement
                                                    from replacement r
                                                    where r."enable" = true and
                                                    r.cod_replacement not in(select cod_replacement
                                                                             from warehouse_inventory wi
                                                                             where cod_workshop = ${codWorkshop}
                                                                            );`);
            listReplacement = listReplacement.rows;
            console.log(listReplacement);
            return listReplacement;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async getListWarehouseInventory(codWorkshop){
        try {
            var listReplacement = await pool.query(`select rc.description_category, r.cod_replacement, r.description_replacement , wi.quantity  
                                                    from replacement r ,warehouse_inventory wi , replacement_category rc
                                                    where r.id_category = wi.id_category and r.cod_replacement = wi.cod_replacement and r.id_category = rc.id_category and wi.cod_workshop =${codWorkshop}
                                                    order by wi.quantity;`);
            listReplacement = listReplacement.rows;
            return listReplacement;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async insertWarehouseInventory(codWorkshop, codReplacement, quantity){
        try {
            var insertInventory = await pool.query(`select register_parts_to_warehouse_inventory(cast(${codWorkshop} as smallint), ${codReplacement}, ${quantity});`);
            insertInventory = insertInventory.rows[0].register_parts_to_warehouse_inventory;
            return insertInventory;
        } catch (error) {
            console.error(error);
            return false;
        }

    },

    async getDataWorkshop(cod_workshop){
        try{
            const getDataWorkshop= await pool.query(`SELECT * FROM workshop WHERE cod_workshop=${cod_workshop}`);
            return getDataWorkshop.rows[0];
        }catch(error){
            console.error(error);
            return false;
        }
    },
    
    async updateDataWorkshop(cod_workshop_i,name_i, address_i, latitude_i, longitude_i){
        try{
            const updataWorkshop = await pool.query(`select update_workshop(${cod_workshop_i} , '${name_i}', '${address_i}', '${latitude_i}', '${longitude_i}');`);
            return updataWorkshop.rows[0].update_workshop;
        }catch(error){
            console.error(error);
            return null;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    /**
     * Get Stpck minimal of the workshop replacement
     * @param {int} codWorkshop : code of the wprkshop
     */
    async getStockMinimalOfWorkshop(codWorkshop){
        try {
            const response = await pool.query(`select replacement, cast(avg(quantity) as integer) from income_detail id, entry_note en where en.income_no=id.income_no and en.cod_workshop=${codWorkshop} group by replacement`);
            return response.rows;
        } catch (error) {
            console.log("Error in workshop into stock minimal", error);
            return null;
        }
    }
}
