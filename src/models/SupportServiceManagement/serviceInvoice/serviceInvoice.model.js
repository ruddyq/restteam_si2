const pool = require('../../../config/database');

module.exports = {
    async getListInvoice() {
        try {
            const listInvoice = await pool.query(`select bill_no, nit , corporate , total_cost ,to_char(service_date::DATE,'dd Mon yyyy') as service_date, quantity_day_support, username,service_invoice.enable as enable_i, support_no from service_invoice, "user" where service_invoice.id_user = "user".id_user ;`);
            //listInvoice =listInvoice.rows;
            /*listInvoice.forEach(element =>{
                element.service_date = element.service_date.toLocaleDateString();
            });*/
            return listInvoice.rows;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getVerifyPermitService(idUser, idPrivilege) {
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async insert_invoice(nit, corporate, total_cost, quantity_day_support, support_nro, id_user) {
        try {
            const insertInvoice = await pool.query(`select insert_invoice($1,$2,$3,$4,$5,$6)`, [nit, corporate, total_cost, quantity_day_support, support_nro, id_user]);
            return insertInvoice.rows[0].insert_invoice;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },
    async existsInvoice(support_no) {
        try {
            const existsDataInvoice = await pool.query(`select exists_invoice(${support_no})`);
            return existsDataInvoice.rows[0].exists_invoice;
        } catch (error) {
            console.error(error);
            return -1;
        }
    },
    async enable_disableInvoice(bill_no) {
        try {
            const enableDisable = await pool.query(`select enable_disable_invoice(${bill_no})`);
            return enableDisable.rows[0].enable_disable_invoice;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getListInvoiceReport() {
        try {
            const listInvoice = await pool.query(`select client_company.nit,client_company.name_company,
            count (service_invoice.corporate) as invoice_count, sum(service_invoice.total_cost) as total_sum
            from service_invoice,technical_support_service,request_support_service,guarantee_policy,client_company
      			where service_invoice.support_no = technical_support_service.support_no and
      			technical_support_service.request_no =  request_support_service.request_no and
      			request_support_service.cod_policy = guarantee_policy.cod_policy and
      			guarantee_policy.nit_client_company = client_company.nit
      			group by client_company.nit;`);
            return listInvoice.rows;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getListInvoiceReportOfCompany(nit_client_company) {
        try {
            const listInvoices = await pool.query(`select service_invoice.nit,service_invoice.corporate,to_char(service_invoice.service_date::DATE,'dd Mon yyyy')as service_date,service_invoice.total_cost
            from service_invoice,technical_support_service,request_support_service,guarantee_policy,client_company
      			where service_invoice.support_no = technical_support_service.support_no and
      			technical_support_service.request_no =  request_support_service.request_no and
      			request_support_service.cod_policy = guarantee_policy.cod_policy and
      			guarantee_policy.nit_client_company = client_company.nit and
      			client_company.nit =${nit_client_company}
      			group by service_invoice.nit,service_invoice.corporate,service_invoice.service_date,service_invoice.total_cost;`);
            return listInvoices.rows;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getDataInvoiceReport(nit) {
        try {
            const invoice = await pool.query(`select guarantee_policy.nit_client_company,name_company,sum(service_invoice.total_cost) as total_sum
            from client_company,service_invoice,technical_support_service,request_support_service,guarantee_policy
            where service_invoice.support_no = technical_support_service.support_no and
            technical_support_service.request_no =  request_support_service.request_no and
            request_support_service.cod_policy = guarantee_policy.cod_policy and
            guarantee_policy.nit_client_company = ${nit} and
			client_company.nit =${nit}			
            group by guarantee_policy.nit_client_company,name_company;`);
            return invoice.rows;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    async getListInvoiceReportClientCompany() {
        try {
            const response = await pool.query(`select cc.nit, cc.name_company, cc.corporate, count(distinct gp.cod_policy), sum(distinct si.total_cost) from service_invoice si, technical_support_service tss, request_support_service rss, product_owner po, guarantee_policy gp, client_company cc where si.support_no=tss.support_no and tss.request_no=rss.request_no and po.code=rss.code_owner and gp.code_owner=po.code and cc.nit=gp.nit_client_company group by cc.nit`);
            return response.rows;
        } catch (error) {
            console.log("Error in getListInvoiceReportOfCompany line 102", error);
            return null;
        }
    },
    async getReportInvoiceCompanyNIT(nit) {
        try {
            const response = await pool.query(`select si.bill_no, o."name", gp.cod_policy, rss.request_no, t."name" technical_name, si.total_cost from service_invoice si, technical_support_service tss, request_support_service rss, product_owner po, "owner" o, product p, guarantee_policy gp, client_company cc, technical t where si.support_no=tss.support_no and tss.request_no=rss.request_no and t.id_technical=tss.id_technical and rss.code_owner=po.code and po.ci=o.ci and po.cod_product=p.cod_product and po.code=gp.code_owner and gp.nit_client_company=cc.nit and cc.nit=${nit}`);
            return response.rows;
        } catch (error) {
            console.log("Error in getReportInvoiceCompanyNIT line 111", error);
            return null;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    async getDataInvoice(billNo) {
        try {
            var response = await pool.query(`select nit, corporate , total_cost ,service_date, quantity_day_support ,support_no from service_invoice where bill_no = ${billNo};`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getReplacementList(supportNo) {
        try {
            var response = await pool.query(`select r.description_replacement , lsp."cost" ,lsp.quantity 
                                             from list_spare_parts lsp ,replacement r 
                                             where lsp.cod_replacement = r.cod_replacement and lsp.support_no = ${supportNo};`);
            return response.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async getServiceList(supportNo) {
        try {
            var response = await pool.query(`select s.description_service, ls."cost" 
                                             from list_service ls, service s 
                                             where ls.id_service = s.id_service and ls.support_no = ${supportNo};`);
            return response.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async billIsCanceled(noBill) {
        try {
            const existsDataInvoice = await pool.query(`select bill_is_canceled(${noBill})`);
            return existsDataInvoice.rows[0].bill_is_canceled;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async verifyWarningInvoiceSupportService(supportNo) {
        try {
            const response = await pool.query(`select count(*) from service_invoice si where si.support_no=${supportNo}`);
            return response.rows[0].count <= 2 ? true : false;
        } catch (error) {
            console.log("Error in verifyWarningInvoiceSupportService for count avaiable one onvoce only", error);
            console.log("Error -1");
            return false;
        }
    },

    async getInvoiceOwnerData(ciOwner){
        try {
            const response = await pool.query(`select si.bill_no, si.nit, si.corporate, si.service_date, rss.request_no, tss.state_support_service, si.total_cost, u.username from service_invoice si, technical_support_service tss, request_support_service rss, "user" u where si.support_no=tss.support_no and tss.request_no=rss.request_no and si.id_user=u.id_user and rss.ci_owner=${ciOwner}`);
            return response.rows;
        } catch (error) {
            console.log("Error in invoice owner", error);
            return null;
        }
    }
}
