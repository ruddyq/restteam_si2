const pool = require('../../../config/database');

module.exports = {

    async getSpecialtys(){
        try {
            const specialtyList = await pool.query(`select *from specialty ORDER BY id_specialty`);
            return specialtyList.rows;
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    async insertSpecialty(description_specialty){
        try {
            const response = await pool.query(`select register_specialty('${description_specialty}')`);
            return response.rows[0].register_specialty;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    
    async getSpecialtyUpdateData(id_specialty){
        try {
            const response = await pool.query(`select description_specialty from specialty where id_specialty=${id_specialty}`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async updateSpecialty(id_specialty, description_specialty){
        try {
            const updateSpecialty = await pool.query(`select update_specialty (${id_specialty},'${description_specialty}' );`);
            return updateSpecialty.rows[0].update_specialty;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}