const pool = require('../../../config/database');

module.exports = {

    async getTechnical(){
        try {
            const technicalList = await pool.query(`select id_technical, "name" , email , phone , "enable", getTechnicalSpecialty(id_technical) 
                                                    from technical;`);
            return technicalList.rows;
        } catch (error) {
            console.log(error);
            return null;
        }
    },

    async registerTechnical(name, email, phone, specialtyList){
        try{
            var idTechnical = await pool.query(`select register_technical_personal('${name}', '${email}',${phone});`);
            idTechnical = idTechnical.rows[0].register_technical_personal;
            let b= this.registerTechnicalSpecialty(idTechnical, specialtyList);
            return idTechnical;
        }catch (error){
            console.error(error);
            return -1;
        }
    },

    async getSpecialtyList(idTechnical){
        try{
            var specialtyList = await pool.query(`select * from specialty s 
                                                  where id_specialty not in(select id_specialty 
                                                                            from technical_specialty
                                                                            where id_technical = ${idTechnical}
                                                                            );`);
            specialtyList = specialtyList.rows;
            return specialtyList;
        }catch (error){
            console.error(error);
            return null;
        }
        
    },

    
    async getTechnicalSpecialty(idTechnical){
        try{
            var specialtyList = await pool.query(`select ts.id_specialty , s.description_specialty 
                                                  from technical_specialty ts, specialty s
                                                  where ts.id_specialty = s.id_specialty and ts.id_technical =${idTechnical};`);
            specialtyList = specialtyList.rows;
            return specialtyList
        }catch (error){
            console.error(error);
            return null;
        }
        
    },

    async updateTechnical(idTechnical, name, email, phone){
        try{
            var updateTechnical = await pool.query(`update technical set "name" = '${name}', email ='${email}', phone = ${phone} 
                                                where id_technical = ${idTechnical};`);
            return updateTechnical;
        }catch (error){
            console.error(error);
            return false;
        }

    },

    async registerTechnicalSpecialty(idTechnical, specialtyList){
        try{
            specialtyList.forEach(async element => {
                await pool.query(`select assign_specialty(${idTechnical}, cast(${element} as smallint));`);
            });
            return true;
        }catch (error) {
            console.error(error);
            return false;
        }

    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },

    async getListReplacement(id_technical){
        try {
            var listReplacement = await pool.query(`(select cod_replacement from spare_box where id_technical = ${id_technical});`);
            listReplacement = listReplacement.rows;
            console.log(listReplacement);
            return listReplacement;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async getListSpareBox(id_technical){
        try {
            var listReplacement = await pool.query(`select s.id_technical, r.cod_replacement, r.description_replacement, rc.id_category, rc.description_category, s.quantity 
                                                            from replacement r , replacement_category rc, spare_box s 
                                                                where r.cod_replacement = s.cod_replacement and rc.id_category= s.id_category and s.id_technical =${id_technical}`);
            listReplacement = listReplacement.rows;
            return listReplacement;            
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

}