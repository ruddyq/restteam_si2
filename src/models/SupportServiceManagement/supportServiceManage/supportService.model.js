const pool = require('../../../config/database');

module.exports = {
    /**
     * get query to table company in expresssi2
     */
    async getListSupportService() {
        try {
            var listSupport = await pool.query(`select tss.support_no , tss.service_date , tss.observations , tss.state_support_service ,
                                                  tss.request_no , t."name", tss. total_cost 
                                                  from technical_support_service tss, technical t
                                                  where tss.id_technical = t.id_technical 
                                                  order by service_date desc;`
                                                  );
            listSupport = listSupport.rows;
            listSupport.forEach(element => {
                element.service_date = element.service_date.toLocaleDateString();
                
            });
            return listSupport;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    /**
     * get query to table company in expresssi2
     */
    async getListTechnical() {
        try {
            const listTechnical = await pool.query('select id_technical ,"name" from technical where "enable"=true;');
            return listTechnical.rows;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    /**
     * get query to table company in expresssi2
     */
    async insertSupportService(date, observations, requestNo, idTechnical) {
        try {
            console.log(date," ", observations, " " ,requestNo, " ",idTechnical);
            console.log(idTechnical);
            console.log(requestNo);
            var support = await pool.query(`select register_support_service('${date}' ,'${observations}', ${requestNo}, ${idTechnical});`);
            support = support.rows[0].register_support_service;
            console.log("Nro Soporte: "+support);
            return support;
        } catch (e) {
            console.error(e);
            return -1;
        }
    },

    async getVerifyPermitService(idUser, idPrivilege){
        try {
            var validatePrivilege = await pool.query(`select verifypermituser(${idUser}, ${idPrivilege});`);
            validatePrivilege = validatePrivilege.rows[0].verifypermituser;
            return validatePrivilege;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get SQL query
     * @param {string} query : SQL query 
     */
    async getQuery(query) {
        try {
            let response = await pool.query(query);
            return response.rows;
        } catch (e) {
            console.log(e);
            return null;
        }
    },
    
    /**
     * Get date and observation of the support service
     * @param {integer} supportNo : Support No. 
     */
    async getSupportServiceData(supportNo){
        try {
            const dataSupportService = await pool.query(`select to_char(service_date::DATE,'YYYY-MM-DD') as service_date, observations from technical_support_service where support_no=${supportNo}`);
            return dataSupportService.rows[0];
        } catch (error) {
            console.error(error);
            return null;
        }
    },
    
    /**
     * get List services of a support service technical
     * support No, ID service, description service, cost($us)
     * @param {integer} supportNo : Support No.
     */
    async getServiceListOfSupportService(supportNo){
        try {
            const responseServiceList = await pool.query(`select ls.support_no, s.id_service, s.description_service, ls."cost" from list_service ls, service s where ls.id_service=s.id_service and support_no=${supportNo}`);
            return responseServiceList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get List Service for assign to list services of the Support Technical Service
     * @param {integer} supportNo : support No.
     */
    async getServicesAvailable(supportNo){
        try {
            const responseServiceList = await pool.query(`select id_service, description_service from service where id_service not in (select id_service from list_service where support_no=${supportNo}) order by description_service`);
            return responseServiceList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Replacement List of a support technical service
     * @param {integer} supportNo : support No. 
     */
    async getReplacementListOfSupportService(supportNo){
        try {
            const responseReplacementList = await pool.query(`select support_no, r2.cod_replacement, r2.description_replacement , lsp.quantity, lsp."cost" from list_spare_parts lsp, replacement r2 where lsp.cod_replacement=r2.cod_replacement and support_no=${supportNo}`);
            return responseReplacementList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * Get Replacement Available to register to support technical service 
     * @param {integer} supportNo : Support No.
     */
    async getReplacementAvailable(supportNo){
        try {
            const responseReplacementList = await pool.query(`select cod_replacement, description_replacement from replacement where cod_replacement not in (select cod_replacement from list_spare_parts where support_no=${supportNo}) and enable=true order by description_replacement`);
            return responseReplacementList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * update date and observation of the support technical service
     * @param {integer} supportNo : Support No.
     * @param {*} serviceDate : date of service
     * @param {*} observation : observations
     */
    async updateSupportServiceData(supportNo, serviceDate, observation){
        try {
            await pool.query(`update technical_support_service set service_date='${serviceDate}', observations='${observation}' where support_no=${supportNo}`);
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Add new service to Support Service
     * @param {integer} supportNo : support No
     * @param {*} idService : id service to add
     * @param {*} cost : cost of the service to add
     */
    async addServiceToSupportService(supportNo, idService, cost){
        try {
            const responseAdd = await pool.query(`select register_service_in_support(${supportNo}, ${idService}, ${cost} )`);
            return responseAdd.rows[0].register_service_in_support;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Delete a service of the Support Technical Service
     * @param {integer} supportNo : support No.
     * @param {*} idService : id Service
     */
    async deleteServiceToSupportService(supportNo, idService){
        try {
            const response = await pool.query(`select delete_service_to_support_service(${supportNo}, ${idService})`);
            return response.rows[0].delete_service_to_support_service;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
    
    /**
     * Add replacement to the support technical services
     * @param {integer} supportNo : support No.
     * @param {*} codReplacement : replacement Code
     * @param {*} quantity : quantity of the replacement
     * @param {*} cost : cost of the replacement
     */
    async addReplacementToSupportService(supportNo, codReplacement, quantity, cost){
        try {
            const responseResult = await pool.query(`select register_spare_parts(${supportNo}, ${codReplacement}, ${quantity}, cast(${cost} as decimal(12,2)))`);
            return responseResult.rows[0].register_spare_parts;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * delete a replacement of the support technical service
     * @param {integer} supportNo : support No.
     * @param {integer} codReplacement : code Replacement
     */
    async deleteReplacementToSupportService(supportNo, codReplacement){
        try {
            const responseResult = await pool.query(`select delete_replacement_support_service(${supportNo}, ${codReplacement})`);
            return responseResult.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * Get Data of the Support technical service with request support service and technical data
     * @param {integer} supportNo : support No.
     */
    async getAllSupportServiceData(supportNo){
        try {
            const responserServiceData = await pool.query(`select tss.support_no, rss.request_no, o."name" as owner_name, to_char(tss.service_date::DATE,'Day dd-Mon-yyyy') as service_date, t."name", rss.cod_policy, rss.description, tss.observations, tss.total_cost from technical_support_service tss, technical t, request_support_service rss, "owner" o where tss.id_technical=t.id_technical and tss.request_no=rss.request_no and o.ci=rss.ci_owner and tss.support_no=${supportNo}`);
            return responserServiceData.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async changeStateSupportService(supportNo){
        try {
            await pool.query(`update technical_support_service set state_support_service = true where support_no = ${supportNo};`);
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}