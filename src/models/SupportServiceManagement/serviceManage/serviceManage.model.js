const pool = require('../../../config/database');

module.exports = {
    /**
* verify permission of the privilege (id privilege) to user (id user)
* @param {integer} idUser : id User
* @param {integer} idPrivilege : id Privilege
*/
    async getVerifyPermitService(idUser, idPrivilege) {
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get list services
     */
    async getListService() {
        try {
            const list = await pool.query(`select * from service order by id_service;`);
            return list.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async insertService(description_service, service_guarantee) {
        try {
            const insert_Service = await pool.query(`select register_service ('${description_service}',${service_guarantee});`);
            return insert_Service.rows[0].register_service;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async updateService(id_service, description_service, service_guarantee) {
        try {
            const updateService = await pool.query(`select update_service(${id_service},'${description_service}',${service_guarantee});`);
            return updateService.rows[0].update_service;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getServiceUpdate(id_service) {
        try {
            const updateDateService = await pool.query(`select description_service,service_guarantee from service where id_service =${id_service};`);
            return updateDateService.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async excludeIncludeToGuaranteeService(idService){
        try {
            await pool.query(`update service set service_guarantee=not service_guarantee where id_service=${idService}`);
            return true;
        } catch (error) {
            console.log("Error in excludeIncludeToGuaranteeService line: 62: ", error);
            return false;
        }
    }

}
