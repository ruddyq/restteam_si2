const pool = require('../../../config/database');

module.exports = {
    
    async getListRepresentative() {
        try {
            const listRepres = await pool.query('select * from representative_company;');
            return listRepres.rows;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    async userHavePrivilege(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async insertRepresentative(ci, issued_place, name, address, email, phone){
        try {
            const response = await pool.query(`select register_representative(${ci}, '${issued_place}', '${name}', '${address}', '${email}', ${phone});`);
            return response.rows[0].register_representative;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async getRepresentativeUpdateData(ci){
        try {
            const response = await pool.query(`select name_representative, address, email, phone from representative_company where ci=${ci}`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    async updateRepresentative(ci, name, address, email, phone){
        try {
            const updateRepresentative = await pool.query(`select update_representative(${ci}, '${name}', '${address}', '${email}', ${phone})`);
            return updateRepresentative.rows[0].update_representative;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}