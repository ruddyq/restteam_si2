const pool = require('../../../config/database');

module.exports = {
    /**
     * get query to table company in expresssi2
     */
    async getListCompanies() {
        try {
            const listCompanies = await pool.query('select cc.nit, cc.name_company, cc.corporate, cc.email, cc.phone, cc."enable", rc.name_representative from client_company cc, representative_company rc where cc.ci_representative=rc.ci order by cc.name_company');
            return listCompanies.rows;
        } catch (e) {
            console.error(e);
            return null;
        }
    },

    /**
     * get query to table company in expresssi2
     */
    async enableDisableCompany(nit) {
        try {
            const enableDisableCompany = await pool.query(`select enable_disable_client_company(${nit});`);
            return enableDisableCompany.rows[0].enable_disable_client_company;
        } catch (e) {
            console.error(e);
            return false;
        }
    },

    async insertCompany(nit, name_company, corporate, name_representative, email, phone){
        try{
            const insertCompany = await pool.query(`select register_company($1,$2,$3,$4,$5,$6);`,[nit,name_company,corporate,name_representative,email,phone]);
            return insertCompany.rows[0].register_company;
        } catch (e) {
            console.error(e);
            return false;
        }
    },

    /**
     * Get company data for update
     * @param {bigint} nit company nit 
     */
    async getCompanyUpdateData(nit){
        try {
            const response = await pool.query(`select nit, corporate, email, phone, "enable", ci_representative from client_company where nit=${nit}`);
            return response.rows[0];
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * update data of the client company
     * @param {integer} nit : nit of client company
     * @param {*} nameRepresentative : new name of personal representative of client company
     * @param {*} email : new email of the client company
     * @param {*} phone : new phone no. to update
     * @param {*} enable : new status of the client company
     */
    async updateCompany(nit, ciRepresentative, email, phone, enable){
        try {
            await pool.query(`update client_company set ci_representative=${ciRepresentative}, email='${email}', phone=${phone},"enable"=${enable} where nit=${nit}`);
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * verify if the user has the privilege for enable process
     * @param {integer} idUser : id user
     * @param {integer} idPrivilege : id privilege
     */
    async getVerifyPermitUser(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser},${idPrivilege}) as verify`);
            return responsePermit.rows[0].verify;
        } catch (error) {
            console.error(error);
            return false;
        }
    },
/*********************************************************/
/****************** Invoice report PDF*******************/
/*******************************************************/    
    /**
     * get query to table in expresssi2
     */    
    async getInvoiceForCompany(){
        try{
            const getInvoiceForCompany = await pool.query('select owner.name,cod_policy, technical_support_service.request_no,technical.name,total_cost from owner, guarantee_policy,technical, request_support_service,technical_support_service  where owner.ci = request_support_service.ci_owner and request_support_service.cod_product = guarantee_policy.cod_product and technical_support_service.id_technical = technical.id_technical and request_support_service.request_no = technical_support_service.request_no;');
            return getInvoiceForCompany.rows;
        }catch (e){
            console.error(e);
            return null;
        }
    },
    /**
     * get query to table in expresssi2
     */
    async getInvoiceReportPDF(){
        try {
            const invoiceReport = await pool.query(`select client_company.nit as nit,client_company.name_company as name_corporate, client_company.corporate as comporate,
                                                            count (service_invoice.corporate) as quantily, sum(total_cost) as total_cost
                                                            from client_company, service_invoice
                                                            where client_company.nit = service_invoice.nit
                                                            group by client_company.nit;`);
            
            return invoiceReport.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    }
}