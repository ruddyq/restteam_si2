const pool = require('../../../config/database');

module.exports = {

    /**
     * verify permission of the privilege (id privilege) to user (id user)
     * @param {integer} idUser : id User
     * @param {integer} idPrivilege : id Privilege 
     */
    async getVerifyPermitContract(idUser, idPrivilege){
        try {
            const responsePermit = await pool.query(`select verifyPermitUser(${idUser}, ${idPrivilege}) as result`);
            return responsePermit.rows[0].result;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get list contract with its data for each client company 
     * @param {integer} nit : nit of the client company 
     */
    async getListContract(nit) {
        try {
            const responseContracts = await pool.query(`select cn.nit, cc.cod_contract, substring(cc.description_contract from 1 for 150) as description_contract , 
            to_char(cc.start_date::DATE,'Day dd Mon yyyy') as start_date, to_char(cc.end_date::DATE,'Day dd Mon yyyy') as end_date, rc.name_representative, cn.nit, cc.status_contract from contract_company cc, client_company cn, representative_company rc where cc.nit_company = cn.nit and rc.ci=cn.ci_representative and cn.nit = ${nit} order by cc.cod_contract`);
            return responseContracts.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get corporate of the client company
     * @param {integer} nit : nit of the client company
     */
    async getCorporateClient(nit) {
        try {
            const responseCorporate = await pool.query(`select corporate from client_company where nit=${nit}`);
            return responseCorporate.rows[0].corporate;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * save a contract to database
     * @param {string} nameCorporate : corporate of the client company 
     * @param {integer} codContract : contract code
     * @param {date} beginDate : date of contract initialize
     * @param {date} endDate : date of conclusion contract
     * @param {string} description : description of the contract
     */
    async registerContract(nameCorporate, codContract, beginDate, endDate, description, fileName) {
        try {
            const getNit = await pool.query(`select nit from client_company where corporate='${nameCorporate}'`);
            const response = await pool.query(`select generate_contract_company(cast(${getNit.rows[0].nit} as bigint), ${codContract}, '${beginDate}', '${endDate}', '${description}','${fileName}')`);
            return response.rows[0].generate_contract_company;
        } catch (error) {
            console.error(error);
            return false;
        }
    },

    /**
     * get NIT of client company
     * @param {string} corporate : corporate of client company 
     */
    async getNIT(corporate) {
        try {
            const response = await pool.query(`select nit from client_company where corporate='${corporate}'`);
            return response.rows[0].nit;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get conclusion date of the last contract registered of the client company
     * @param {integer} nit : nit client company 
     */
    async getLastDateContract(nit) {
        try {
            const responseLastDate = await pool.query(`select get_last_date_contract_company(cast(${nit} as bigint))`);
            console.log("date last:", responseLastDate.rows[0].get_last_date_contract_company);
            return (responseLastDate.rows[0].get_last_date_contract_company!==null)? responseLastDate.rows[0].get_last_date_contract_company : '2010-01-01';
        } catch (error) {
            console.error(error);
            return ;
        }
    },

    /**
     * get list contract registered of the system
     */
    async getContractsList() {
        try {
            const contractsList = await pool.query(`select  c.name_company, cc.nit_company, cc.cod_contract ,cc.description_contract ,to_char(cc.start_date::DATE,'Day dd Mon yyyy') as start_date, to_char(cc.end_date::DATE,'Day dd Mon yyyy') as end_date from contract_company cc,client_company c where cc.nit_company = c.nit order by c.name_company, cc.cod_contract;`);
            return contractsList.rows;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    /**
     * get data of the contract's company
     * @param {integer} codContract : contract code 
     */
    async getDataCompleteContract(codContract){
        try {
            const responseDataContract = await pool.query(`select file_contract from contract_company cc where cod_contract=${codContract}`);
            return responseDataContract.rows[0].file_contract;
        } catch (error) {
            console.error(error);
            return null;
        }
    },

    async changeStatusContract(codContract, nit){
        try {
            const result = await pool.query(`select enable_disable_contract(${codContract}, cast(${nit} as bigint))`);
            return result.rows[0].enable_disable_contract;
        } catch (error) {
            console.error(error);
            return false;
        }
    }
}