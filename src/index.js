const app = require('./app');
/**
 * Start Server
 */
app.listen(app.get('port'), () => {
    console.log(`Server started on port ${ app.get('port') }`);
});